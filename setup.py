from setuptools import setup

setup(
    name='ValcDashes',
    version='0.1',
    packages=['project', 'project.apps', 'project.test', 'project.utils'],
    url='',
    license='',
    author='VytenisNaujokas',
    author_email='naujokas@valcon-int.com',
    description=''
)
