from dash_extensions.enrich import Input, Output, ALL
from apps import identificationApp, uploadApp, linearizationApp, compareTuningApp, saveApp,\
    loginApp, trendApp, reviewApp, simpleTunningApp, cascadeTunningApp, analysisApp
from apps.app import app
from dash import dcc, html, callback_context
from apps.utils.navbar import navbar, error
from flask_login import logout_user, current_user
# logger configuration that can be disable in this file
import logging
# Configure the logger
# create logger
logger = logging.getLogger('index')
# set loging level DEBUG, INFO, WARNING, ERROR, CRITICAL
logger.setLevel(logging.INFO)
# define formatting
formatter = logging.Formatter('%(asctime)s - %(levelname)s - [%(filename)s:%(lineno)d] - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)


app.layout = html.Div([
    navbar,
    html.Div(id='page-content'),
    dcc.Store('tuning-model-store', storage_type='session', data={}),
    dcc.Store('saved-model-store', storage_type='session', data={}),
    dcc.Store('loop-selection-store', storage_type='session', data=0),
    dcc.Store(id="data-store", storage_type='session', data={}),
    dcc.Location(id='url', refresh=False),
])


# this will select which page layout to load
@app.callback(
    Output('page-content', 'children'),
    [Input('url', 'pathname'),
    Input('loop-selection-store', 'data')])
def display_page(pathname, loopStore):
    logger.debug(callback_context.triggered[0])
    if current_user.is_authenticated:
        if pathname == '/':
            return loginApp.dashboard
        if pathname == '/save':
            return saveApp.layout
        if pathname == '/compare':
            return compareTuningApp.serve_layout()
        if pathname == '/linearization':
            return linearizationApp.serve_layout()
        if pathname == '/tune/simple':
            return simpleTunningApp.serve_layout()
        if pathname == '/tune/cascade':
            return cascadeTunningApp.serve_layout()
        if pathname == '/identification':
            return identificationApp.serve_layout()
        # if pathname == '/model':
        #     return modelMatrixApp.layout
        if pathname == '/review':
            return reviewApp.layout
        if pathname == '/upload':
            return uploadApp.layout
        if pathname == '/trend':
            return trendApp.serve_layout()
        if pathname == '/loop':
            return loginApp.Loop        
        if pathname == '/analysis':
            return analysisApp.serve_layout()
        # if pathname == '/tunematrix':
        #     return tuneMatrixApp.layout
        if pathname == '/logout':
            logout_user()
            return loginApp.login
    else:
        if pathname == '/':
            return loginApp.login
        if pathname == '/create':
            return loginApp.create
        if pathname == '/save':
            return loginApp.failed
        if pathname == '/compare':
            return loginApp.failed
        if pathname == '/linearization':
            return loginApp.failed
        if pathname == '/tune/simple':
            return loginApp.failed
        if pathname == '/tune/cascade':
            return loginApp.failed
        if pathname == '/identification':
            return loginApp.failed
        # if pathname == '/model':
        #     return loginApp.failed
        if pathname == '/upload':
            return loginApp.failed
        if pathname == '/trend':
            return loginApp.failed        
        if pathname == '/analysis':
            return loginApp.failed  
        # if pathname == '/tunematrix':
        #     return loginApp.failed
        if pathname == '/logout':
            return loginApp.login
    return error


#used in gunicorn command
server = app.server



if __name__ == '__main__':
    #with database_context_manager(database_pool) as db:
        #db.initialize()
    app.run_server(debug=True, host="0.0.0.0")