import dash_bootstrap_components as dbc
from dash import html, callback_context
from dash_extensions.enrich import Input, Output, State, ALL, no_update
from .utils.database.database_manager import UserData
from dash.exceptions import PreventUpdate
from .utils import components as utl
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user

clear_popup = dbc.Modal(
    [
        dbc.ModalHeader('Clear models'),
        dbc.ModalFooter([
            utl.Button('clear-cancel', 'Cancel'),
            utl.Button('clear-delete', 'Clear')
        ])
    ], id='clear-pop-up'
)

layout = dbc.Container([
    # html.H1('Model matrix'),
    html.Hr(style={'margin-top':0, 'width': '100%'}),
    dbc.Row([dbc.Table(bordered=True, className='m-3', id='model-table')]),
    dbc.Row([utl.Button('clear-models-btn', 'Clear')]),
    html.Div(clear_popup),

])
@app.callback(
    Output("clear-pop-up", "is_open"),
    Input('clear-models-btn', 'n_clicks'), 
    Input('clear-cancel', "n_clicks"), 
    Input('clear-delete', 'n_clicks'),
    State('clear-pop-up', "is_open")
)
def delete(n1, n2, n3, is_open):
    '''Deletes all models'''
    ctx = callback_context
    callback_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    if callback_id == 'clear-delete':
        # Delete all saved models 
        with database_context_manager(database_pool) as db:
            # Read all models for current user and current loop
            models = db.getAttr(current_user.username, 'models')

            # Delete all non Temp models
            for name in models.name:
                if name != 'Temp':
                    db.deleteAttr(current_user.username, 'models', name) 
    if n1 or n2 or n3:
        return not is_open
    return is_open


@app.callback(
    Output('model-table', 'children'),
    Input('clear-pop-up', "is_open"),
    Input({'id': 'loop-data-select-button', 'data': ALL}, 'n_clicks'),
)
def update_table(is_open, dummy):
    '''Populate model table'''
    #when no loop selection was done or when clear modal is open
    #if dummy.count(None) == len(dummy) or is_open:
    #    print('prevent')
    #    return no_update
    #read all models of current user, current loop
    with database_context_manager(database_pool) as db:
       models = db.getAttr(current_user.username, 'models')

    # relevant columns
    cols = ['name', 'model_type', 'K', 't1', 't2', 'td']
    # get relevant data from df
    df = models.loc[models.name != "Temp", models.columns.isin(cols)]
    #capitalized column names
    df.columns = df.columns.str.capitalize()
    # create dcc component
    table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)
    return table


