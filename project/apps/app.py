import flask
import dash_bootstrap_components as dbc
from dash_extensions.enrich import DashProxy, MultiplexerTransform, ServersideOutputTransform, RedisStore, BlockingCallbackTransform
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin
from pathlib import Path
import os
# Configure the logger
import logging
# create logger
logger = logging.getLogger('app')
# set loging level NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
logger.setLevel(logging.INFO)
# logger.setLevel(logging.DEBUG)
# define formatting
formatter = logging.Formatter('%(asctime)s - %(levelname)s - [%(filename)s:%(lineno)d] - %(message)s')
# apply the formatting
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)

server = flask.Flask(__name__)
absolute_path = Path(__file__).parent #added to solve issue with relative path
path = Path(str(absolute_path.parent)+'/shared/users.sqlite').resolve()
# path = Path('/shared/users.sqlite').resolve()
server.config.update(
    SECRET_KEY="secret_sauce", #static key allows for proper multi session operation
    # SECRET_KEY=os.urandom(12),
    SQLALCHEMY_DATABASE_URI='sqlite:///'+str(path)+'?check_same_thread=False',
    SQLALCHEMY_TRACK_MODIFICATIONS=False
)



# Initialize SQLAlchemy
db = SQLAlchemy(server)

# Initialize Dash app
app = DashProxy(__name__, server=server,
                external_stylesheets=[dbc.themes.BOOTSTRAP], transforms=[MultiplexerTransform(),
                                                                         ServersideOutputTransform(),
                                                                        #  BlockingCallbackTransform(10)
                                                                         ],
                                                                            #  backend=RedisStore())],
                                                                             suppress_callback_exceptions=True)
# Setup the LoginManager for the server
login_manager = LoginManager()
login_manager.init_app(server)
login_manager.login_view = '/login'


class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))



db.create_all()



# callback to reload the user object
@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))