import plotly.graph_objects as go
from .utils import components as utl
import dash_bootstrap_components as dbc
from dash import dcc, html, callback_context, no_update, dash_table
from dash_extensions.enrich import Input, Output, State, ALL
from dash.exceptions import PreventUpdate
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user
from .utils import model
from datetime import datetime
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup
from apps.trendApp import rate
import numpy as np
from pandas.tseries.offsets import DateOffset
import pandas as pd
import traceback
from .utils.functions import getXRange, save_data, load_data
from inspect import currentframe #this is used to read callback name, for debugging
from .app import logger #logger object for debuging
logger.info(f'{__name__}')
# parameter name mapping, used in callback_context data extraction
k_dict = {'K':'K', 't1':'t1', 't2':'t2', 'td':'td', 'y0':'y0', 'rate0':'Angle', 'Filter':'filter'}
           
log_trigger = False
tuning_layouts = {
    "SODTF": [        
        dbc.Col([utl.Input({'type': 'tuning', 'var': 'K'}, 'Gain', 'Gain',)], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 't1'}, 'T1, s', 'T1, s')], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 't2'}, 'T2, s', 'T2, s')], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 'td'}, 'Td, s', 'Td, s')], width = 2),
        dbc.Col([utl.Input_ext({'type': 'tuning', 'var': 'y0'}, 'y0', 'y0')], width = 2),
    ],
    "SODITF": [
        dbc.Col([utl.Input({'type': 'tuning', 'var': 'K'}, 'Gain', 'Gain')], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 't1'}, 'T1, s', 'T1, s')], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 't2'}, 'T2, s', 'T2, s')], width = 2),
        dbc.Col([utl.Input({'type': 'tuning', 'var': 'td'}, 'Td, s', 'Td, s')], width = 2),        
        dbc.Col([utl.Input_ext({'type': 'tuning', 'var': 'y0'}, 'y0', ' y0')], width = 2),
        dbc.Col([utl.Input_ext({'type': 'tuning', 'var': 'rate0'}, 'Rate0, y/s', 'Rate0, y/s', step=1e-6)], width = 2),
    ]
}


def graph(df, ins, outs, model_type, ident:dict):
    # list of available identification points and their marker labels
    stamps = {'step-start':'S0',
              'step-end':'S1',
              'Td':'Td', 
              'T1':'T1', 
              'T2':'T2'} 
    # response data point and axis selection
    (out_var, axis) = (outs, 'y2') if model_type != "SODITF" else ('Rate', 'y3')
    # step and response data
    u = {}
    y = {}
    marku = []
    marky = []
    # iterate through all timestamps
    for i, stamp in enumerate(stamps):
        # get data if value is defined
        if stamp in ident:
            # check if timestamp is valid, ommit if out of range
            if ident[stamp] not in df.index:
                continue
            # convert to timestamp
            t = datetime.strptime(ident[stamp], '%Y-%m-%d %H:%M:%S.%f' 
                                             if '.' in ident[stamp] else '%Y-%m-%d %H:%M:%S')            
            # store as step or response
            if i < 2:
                u[t] = df[df.index.isin([t])][ins].values[0]
                marku.append(stamps[stamp])
            else:
                y[t] = df[df.index.isin([t])][out_var].values[0]
                marky.append(stamps[stamp])

    trace1 = go.Scatter(x=df.index, y=df [ins], name=ins, yaxis='y', marker_color="#1f77b4")
    trace2 = go.Scatter(x=df.index, y=df [outs], name=outs, yaxis='y2', marker_color="#ff7f0e")
    data = [trace1, trace2]
    # only show response if it is present
    if 'Response' in df.columns:
        trace3 = go.Scatter(x=df.index, y=df ['Response'], name='Response', yaxis='y2', marker_color="#d62728")
        data.append(trace3)
    if model_type=="SODITF":        
        # only show response if it is present
        if 'Response' in df.columns:
            trace4 = go.Scatter(x=df.index, y=df ['Rate response'], name='Rate response', yaxis='y3', marker_color="green")
            data += [trace4]
        trace5 = go.Scatter(x=df.index, y=df ['Rate'], name='Rate', yaxis='y3', marker_color="grey")
        data += [trace5]
        layout = go.Layout(xaxis=dict(domain=[0.04, 1]),
                yaxis=dict(titlefont=dict(color="#1f77b4"), tickfont=dict(color="#1f77b4"), position=0.04),
                yaxis2=dict(titlefont=dict(color="#ff7f0e"), tickfont=dict(color="#ff7f0e"),
                            anchor="free", overlaying="y", side="left", position=0.02),
                # yaxis3=dict(titlefont=dict(color="#d62728"), tickfont=dict(color="#d62728"),
                            # anchor="free", overlaying="y", side="left", position=0.08),
                yaxis3=dict(titlefont=dict(color="#777777"), tickfont=dict(color="#777777"),
                            anchor="free", overlaying="y", side="left", position=0),                
                # height=600,
                legend=dict(orientation='h', traceorder = 'normal'),
                uirevision=True,
                hovermode = 'x',
                dragmode='select',
                margin = dict(l=30, r=30, t=30, b=30) #helps to avoid excessive margins around the graph
                )
    else:
        layout = go.Layout(xaxis=dict(domain=[0.04, 1]),
                        yaxis=dict(titlefont=dict(color="#1f77b4"), tickfont=dict(color="#1f77b4"), position=0.04),
                        yaxis2=dict(titlefont=dict(color="#ff7f0e"), tickfont=dict(color="#ff7f0e"),
                                    anchor="free", overlaying="y", side="left", position=0.02),
                        # yaxis3=dict(titlefont=dict(color="#d62728"), tickfont=dict(color="#d62728"),
                        #             anchor="free", overlaying="y", side="left", position=0),
                        # RGB alpha added to make axis opaque
                        yaxis3=dict(titlefont=dict(color='rgba(119, 119, 119, 0.2)'), 
                                    tickfont=dict(color='rgba(119, 119, 119, 0.2)'),
                            anchor="free", overlaying="y", side="left", position=0), 
                        # height=600,
                        legend=dict(orientation='h', traceorder = 'normal'),
                        uirevision=True,
                        hovermode = 'x',
                        dragmode='select',
                        margin = dict(l=30, r=30, t=30, b=30) #helps to avoid excessive margins around the graph
                        )
        # added for better intuition regarding T2 selection
        trace5 = go.Scatter(x=df.index, y=df ['Rate'], name='Rate', yaxis='y3', marker_color="#777777", opacity=0.2)
        data += [trace5]
    # identification points
    traceu = go.Scatter(x=list(u.keys()), y=list(u.values()), name='Step data', yaxis='y', mode='markers+text', 
                        marker_color="black", marker=dict(size=4, symbol='square-open', 
                                                          opacity=1), text=marku, textposition='bottom center',
                        showlegend=False, hoverinfo='skip')#do not show data or trace lable
    tracey = go.Scatter(x=list(y.keys()), y=list(y.values()), name='Response data', yaxis=axis, mode='markers+text', 
                        marker_color="black", marker=dict(size=4, symbol='square-open', 
                                                          opacity=1), text=marky, textposition='bottom center',
                        showlegend=False, hoverinfo='skip')#do not show data or trace lable
    data += [traceu, tracey]
    fig = go.Figure(data=data, layout=layout)
    return fig

# define modal objects
deletePop = deleteAttrPopup(
                    app=app,
                    name='model')
savePop = saveAttrPopup(
                    app=app,
                    name='model')
loadPop = loadAttrPopup(
                    app=app,
                    name='model')
def serve_layout():
    with database_context_manager(database_pool) as db:
        dataTables = db.getDataTables(current_user.username)
        currModel = db.getAttrValues(current_user.username, 'models', 'Temp')
        models = db.getAttr(current_user.username, 'models')
        
    clear_popup = dbc.Modal(
        [
            dbc.ModalHeader('Are you sure you want to clear all saved models for this loop?'),
            dbc.ModalFooter([
                utl.Button('clear-model-cancel', 'Cancel'),
                utl.Button('clear-model-delete', 'Clear')
            ])
        ], id='clear-model-pop-up',
        is_open=False
    )
   
    show_popup = dbc.Modal(
        [
            dbc.ModalHeader([
                    dbc.Col('Show models   ', width=2),
                    dbc.Col([
                        dcc.Download(id="csv-download"),
                        html.Button("Download CSV", id="csv-button", n_clicks=0)], width=2)]),
            dbc.ModalBody(id='show-model-content')
        ], id='show-mod-pop-up', size="xl",
        scrollable=True,
        is_open=False
    )
    

    # display available data tables
    data_options = [{'label': col_name, 'value': col_name} for col_name in dataTables
                    if col_name != 'trend_data']
       
    model_types = [{'label': col_name, 'value': col_name} for col_name in model.models.keys()]
    # tunning layout used for initialization
    tunning_layout = "SODTF"
    if 'model_type'in currModel and currModel['model_type'] in tuning_layouts:
        tunning_layout = currModel['model_type']
    else:
        currModel['model_type'] = tunning_layout
    # data_selection used for initialization
    data_selection = ''
    if len(data_options)>0:
        data_selection = data_options[0]['label']
    # get stored datatable if it exists
    if currModel != {}:
        try:
            # only use it if it still exists in the database
            if currModel['datatable'] is not None and currModel['datatable'] in dataTables:
                data_selection=currModel['datatable']
            else:
                # select the first table from the list otherwise
                if len(dataTables)>0:
                    data_selection=dataTables[0]
                currModel['datatable'] = data_selection
        except:
            pass
    # get point list
    points = []
    pt_selection=[]
    if data_selection:
        with database_context_manager(database_pool) as db:
            DATA = db.getData(current_user.username, data_selection, numRows=1e9).sort_index(ascending=True)
        # save data to parquet
        save_data(DATA, "main", current_user.username)
        df = DATA
        points = [{'label': col_name, 'value': col_name} for col_name in list(df.columns)]
        # update point selection
        if 'In' in currModel and currModel['In'] in df.columns:
            pt_selection.append(currModel['In'])
        else:
            pt_selection.append(points[0]['label'])
        if 'Out' in currModel and currModel['Out'] in df.columns:
            pt_selection.append(currModel['Out'])
        else:
            pt_selection.append(points[1]['label'])
        
        currModel['In']=pt_selection[0]
        currModel['Out']=pt_selection[1]
    else:
        # this will prevent point dropdown errors
        pt_selection = ['', '']
    
    # update current model
    with database_context_manager(database_pool) as db:
        logger.debug(f'Updating model: {currModel}')
        db.updateAttr(current_user.username, 'models', currModel)

    layout = dbc.Container(
        [
            # html.H1('Netta Identification app'),
            html.Div(id = 'dummy', n_clicks=0),
            html.Hr(style={'margin-top':0, 'width': '100%'}),
            
            # Add loading indicators right after hr, following trendApp.py pattern
            utl.LoadingSpinner(id='model-loading-update_point_options'),
            utl.LoadingSpinner(id='model-loading-post_load_model'),
            utl.LoadingSpinner(id='model-loading-update_tuning_layout'),
            utl.LoadingSpinner(id='model-loading-init_value_control'),
            utl.LoadingSpinner(id='model-loading-update_zoom'),
            utl.LoadingSpinner(id='model-loading-update_graph'),
            utl.LoadingSpinner(id='model-loading-update_tuning_values'),
            utl.LoadingSpinner(id='model-loading-add_fit'),
            utl.LoadingSpinner(id='model-loading-show'),
            utl.LoadingSpinner(id='model-loading-clear'),
            utl.LoadingSpinner(id='model-loading-download_data'),
            
            # Storage components
            dcc.Store('identification-store', storage_type='session', data=0),
            dcc.Store('zoom-store', storage_type='session', data=0),
            
            dcc.Graph(id='graph', figure=go.Figure(layout=go.Layout(#height=600,
                        margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                        )),
                      config={'scrollZoom': True},
                      style={'height':'68vh'}),# animate=True), AK:Commented during VD-91
            dbc.Row([
                dbc.Col(html.Div(utl.Input('graph-show-min', 'Minutes', 'Show, min', step=1, value=0), 
                                         title='Display window duration in minutes'), width=2),
                dbc.Col(html.Div(dbc.ButtonGroup([
                        dbc.Button('| <', 
                                id={"type":'graph-slider', "index": 0}, className="mb-3", outline=True, 
                                color="dark", size="md", title="Oldest data"),
                        dbc.Button('<<', 
                                id={"type":'graph-slider', "index": 1}, className="mb-3", outline=True, 
                                color="dark", size="md"),
                        dbc.Button('_|_', 
                                id={"type":'graph-slider', "index": 2}, className="mb-3", outline=True, 
                                color="dark", size="md", title="Show middle data points"),
                        dbc.Button('>>', 
                                id={"type":'graph-slider', "index": 3}, className="mb-3", outline=True, 
                                color="dark", size="md"),
                        dbc.Button('> |', 
                                id={"type":'graph-slider', "index": 4}, className="mb-3", outline=True, 
                                color="dark", size="md", title="Most recent data")]),
                                style={'display': 'flex', 'justify-content': 'center'}), width=4),
                dbc.Col(dbc.DropdownMenu(
                            label = "Model menu",
                            children = [
                                dbc.DropdownMenuItem('Save', savePop.btn_id),
                                dbc.DropdownMenuItem('Load', loadPop.btn_id),
                                dbc.DropdownMenuItem('Delete', deletePop.btn_id),
                                dbc.DropdownMenuItem(divider=True),
                                dbc.DropdownMenuItem('Zoom Reset', 'zoom-reset-b')], 
                            color='primary', group=True, style={'width': '100%'}), width=1),
                dbc.Col(dbc.DropdownMenu(
                            label = "Model matrix",
                            children=[
                                dbc.DropdownMenuItem('Show', 'show-model-b'),
                                dbc.DropdownMenuItem('Clear', 'clear-model-b')],
                            color='secondary', group=True, style={'width': '100%'}), width=1),], 
                            justify='center', align='center', style={'height':'8vh'}),
            dbc.Row(children=[
                        dbc.Col([
                            dbc.Card(style={'margin-bottom': '10px'},
                                     children=[
                                        dbc.CardHeader("Identification",
                                                        style={'padding':'0px 5px'}),
                                        dbc.CardBody(
                                            children = dcc.RadioItems(
                                                            id='identification-radio',
                                                            options=[
                                                                {'label': 'step-start', 'value': 'step-start'},
                                                                {'label': 'step-end', 'value': 'step-end'},
                                                                {'label': 'Td', 'value': 'Td'},
                                                                {'label': 'T1', 'value': 'T1'},
                                                                {'label': 'T2', 'value': 'T2'}
                                                            ],
                                                            value='step-start',
                                                            labelStyle={#'display': 'inline-block', 
                                                                        'margin-right': '20px',
                                                                        'font-size': '18px'},
                                                            style={'align-items': 'center'}),
                                                            style={'padding':'0px 5px'})])], 
                                                            width = "auto"),
                        dbc.Col([dbc.Row(id='tuning-layouts', children = tuning_layouts[tunning_layout])])],
                        style={'height':'8vh'}),
            dbc.Row([
                dbc.Col([utl.Selector('ins-in', 'CO', options=points, value=pt_selection[0])], width=2),
                dbc.Col([utl.Selector('outs-in', 'CV', options=points, value=pt_selection[1])], width=2),
                dbc.Col([utl.Selector('data-in', 'Data', options=data_options, value=data_selection)], width=2),
                dbc.Col([utl.Selector('model-in', 'Model', options=model_types, value=tunning_layout)], width=2),
                # dbc.Col(dbc.ButtonGroup([
                #     utl.Button('fit-b', 'Data Fit', color='info')], 
                #     vertical=True, style={'width': '100%'}), width=1),
                dbc.Col([utl.Input({'type': 'tuning', 'var': 'Filter'}, 'F', 'Filter, s', step = 1)], width = 2),     
            ], style={'height':'5vh'}),
            html.Div([clear_popup, 
                      show_popup, 
                      savePop.layout(), 
                      loadPop.layout(), 
                      deletePop.layout()])
            # dcc.Store('model_parameters', storage_type="local") #AK: VD-86 - no more used
        ], fluid=True)

    return layout

# trigger modal object callbacks
deletePop.register_callbacks()
savePop.register_callbacks()
loadPop.register_callbacks()

def getInit(df, currModel):
        '''Function to get intial response value.
        If step start and stop times are defined, these values will be used
        if step times are not defined, steps will be identified,
        if there are no steps in the selected data, first data sample will be taken'''
        rate0 = 0
        y0 = 0
        # get identification values
        ident = eval(currModel['ident']) if str(currModel['ident']) != 'None' else {}
        # when identification values are available
        if (not ('step-start' in ident and ident['step-start'] in df.index) or not
             ('Td' in ident and ident['Td'] in df.index)):
            # when identification data is not available
            # initial values should be found by analyzing the data
            ufrange = np.abs(df[currModel['In']].max() - df[currModel['In']].min())
            ufdiff = np.abs(np.diff(df[currModel['In']]))
            # step moment is where the data start changing by 5% of the total change
            t0idx = np.where(ufdiff > ufrange * 0.05)[0] if not isinstance(ufrange, pd.Series) else [0]
            # if steps are not identified take first values in the dataframe and 
            # use first five values for average calculation
            if len(t0idx)<1:
                df = df.loc[:df.index[min(5, df.index.size-1)]]
            else:                
                # take at most 5s of data
                df = df.loc[df.index[t0idx[0]]:df.index[t0idx[0]] + 
                                        DateOffset(seconds=min(max(
                                            currModel['td'] if currModel['td'] is not None else 0, 0), 
                                            5))]
            
            #get initial values for integrating process
            if currModel['model_type'] == "SODITF":
                rate0 = np.mean(df['Rate'].values)
                y0 = df[currModel['Out']][0]
            else:
                y0 = np.mean(df[currModel['Out']].values)
        else:
            # only for self regulating process intial value 
            # should be taken during step moment
            if currModel['model_type'] == "SODITF":
                rate0s = df[(df.index >= ident['step-start']) & (df.index <= ident['Td'])]['Rate'].values
                # only use 5 first values
                rate0 = np.mean(rate0s[:min(5, len(rate0s))])
                y0 = df[currModel['Out']][0]
            else:
                y0s = df[(df.index >= ident['step-start']) & (df.index <= ident['Td'])][currModel['Out']].values
                # only use 5 first values
                y0 = np.mean(y0s[:min(5, len(y0s))])
        return y0, rate0


@app.callback(
    Output('ins-in', 'options'),
    Output('outs-in', 'options'), 
    Output('ins-in', 'value'),
    Output('outs-in', 'value'),
    Output('model-loading-update_point_options', 'children'),
    Input('data-in', 'value'),
    State('ins-in', 'value'),
    State('outs-in', 'value'),
    prevent_initial_call=True
)
def update_point_options(data_name, ins, outs):
    '''Populate In and Out dropdown values'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    if data_name is None:
        raise PreventUpdate

    with database_context_manager(database_pool) as db:
        # get current model configuration
        currModel = db.getAttrValues(current_user.username,'models', 'Temp')
        # get the new data
        df = db.getData(current_user.username, data_name, numRows=1e9).sort_index(ascending=True)
    # save data to parquet
    save_data(df, "main", current_user.username)

    # do not update when no data is available
    if df is None:
        raise PreventUpdate
    # if data changes currModel fields should be updated
    # reset the in/out selection
    if currModel:
        ins = currModel['In'] if currModel['In'] in df.columns else df.columns[0]
        outs = currModel['Out'] if currModel['Out'] in df.columns else df.columns[1]

    # DateTime column is excluded by default because it is index
    options = [{'label': col_name, 'value': col_name} for col_name in df.columns]
    return options, options, ins, outs, no_update


@app.callback(
    Output('model-in', 'value'),
    Output('data-in', 'value'),
    Output('model-loading-post_load_model', 'children'),
    Input(loadPop.store_id, 'data'),
    State({'type': 'conf', 'var': ALL}, 'value'),
)
def post_load_model(store, init):
    ''' Postprocess for model loading.It read loaded model parameters and update model layout'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # do not execute this callback if it was not triggered
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    if id != loadPop.store_id:
        raise PreventUpdate
    # during loading store is a dictionary, otherwise a number
    if type(store)==dict:
        return store['model_type'], store['datatable'], no_update
    else:
        raise PreventUpdate

@app.callback(
    Input('model-in', 'value'),
    Output('tuning-layouts', 'children'),
    Output('model-loading-update_tuning_layout', 'children'),
    # prevent_initial_call=True
)
def update_tuning_layout(model_type):
    '''Update identification app layout, but only if new selection is made'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
        # model type is not selected
    if model_type is None:
        raise PreventUpdate
    
    # write model type change to database
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username,'models', {'model_type': model_type})

    return tuning_layouts [model_type], no_update

@app.callback(
    Output({'type': 'tuning', 'var': ALL}, 'readOnly'), 
    Output('model-loading-init_value_control', 'children'),
    Input({'type': 'conf', 'var': ALL}, 'value')
)
def init_value_control(init):
    '''A function to activate/deactivate initial value input fields based on 
    checkbox selection '''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    '''This callback will control model parameter input modes, making possible to freeze user-defined initial values'''
    # collect input dictionary
    inputs = {eval(k.replace('.value', ''))['var']:v==['1'] 
                  for k, v in callback_context.inputs.items()}
    # collect outputs with all values set to False
    outputs = {el['id']['var']:False for el in callback_context.outputs_list[0]}
    # update outputs
    outputs.update(inputs)
    
    return list(outputs.values()), no_update

@app.callback(
    Output('zoom-store', 'data'),    
    Output('graph-show-min', 'value'),
    Output('model-loading-update_zoom', 'children'),
    Input('graph', 'selectedData'),
    Input('zoom-reset-b', 'n_clicks'),
    Input({"type":'graph-slider', "index": ALL}, 'n_clicks'),
    Input('data-in', 'value'),
    State('graph-show-min', 'value'),
    State('zoom-store', 'data'),
    # prevent_initial_call=True
)
def update_zoom(selection, n1, slider, data_name, window, store):
    '''When all information for graph is available call update graph'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # get data from parquet
    DATA = {}
    DATA["main"] = load_data(current_user.username, "main")
    if DATA["main"] is None:
        raise PreventUpdate
    modelParameters = {}
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    logger.debug(id)
    with database_context_manager(database_pool) as db:
        # get current loop data
        currModel = db.getAttrValues(current_user.username, 'models', 'Temp')
    
    logger.debug(f'Current model: {currModel}')
    # get data and data range
    df = DATA["main"]    
    # get data range of the whole data table
    ranges = {'DateTime':(DATA['main'].index[0], DATA['main'].index[-1])}
    # "measure" duration of 600 samples
    if not window:
        # but previous selection exists
        if 'timeStart' in currModel and currModel['timeStart'] and 'timeStop' in currModel and currModel['timeStop']:
            window = np.ceil((currModel['timeStop'] - 
                                currModel['timeStart']).total_seconds()/60)
        else: # else default window is equivalent to 600 data samples
            currModel['timeStart'] = df.index[0]
            currModel['timeStop'] = df.index[600 if len(df.index) > 601 else len(df.index)-1] 
            # calculate windown from default seletion         
            window = np.ceil((currModel['timeStop'] - 
                                        currModel['timeStart']).total_seconds()/60)
    #daterange reset, no df filtering
    if id =='zoom-reset-b' or \
        ('datatable' in currModel and currModel['datatable']!=data_name):
        # default window is equivalent to 600 data samples
        modelParameters['timeStart'] = df.index[0]
        modelParameters['timeStop'] = df.index[600 if len(df.index) > 601 else len(df.index)-1] 
        # calculate windown from default seletion
        window = np.ceil((modelParameters['timeStop'] - 
                            modelParameters['timeStart']).total_seconds()/60)
    #daterange zoom
    elif id=='graph' and selection:
        xRange = getXRange(selection)
        modelParameters['timeStart'] = xRange[0]
        modelParameters['timeStop'] = xRange[1]
        # calculate windown from seletion
        window = np.ceil((modelParameters['timeStop'] - 
                            modelParameters['timeStart']).total_seconds()/60)
    #daterange no change, filter using existing data
        
    elif "graph-slider" in id: #when slider button was activated
        idx_slider = eval(id)['index']
        # get all timestamps
        if idx_slider==0:#get window of oldest data
            # use first data sample time as a start time                
            modelParameters['timeStart'] = ranges['DateTime'][0]
            # offset it by window duration and limit not to exceed available data
            modelParameters['timeStop'] = min(ranges['DateTime'][1], modelParameters['timeStart'] + 
                                                DateOffset(minutes=window))
        
        elif idx_slider==1:#scroll half a window to an older data
            # get midpoint of current window, it will be a new end point, limit it not to exceed available data
            modelParameters['timeStart'] = max(ranges['DateTime'][0], currModel['timeStart'] - 
                                                DateOffset(minutes=window/2))
            # calculate new start point by substracting window from end point
            modelParameters['timeStop'] = min(ranges['DateTime'][1], modelParameters['timeStart'] + 
                                                DateOffset(minutes=window))

        elif idx_slider==2:#get data window from the middle
            # get middle datetime
            # # use current window to calculate
            if 'timeStart' in currModel and currModel['timeStart'] and 'timeStop' in currModel and currModel['timeStop']:
                middle = currModel['timeStart'] + (currModel['timeStop'] - currModel['timeStart'])/2
            else:#use all data to calculate
                middle = df.index.to_series().median()
            # calculate start and stop, limiting added not to exceed data range
            modelParameters['timeStart'] = max(ranges['DateTime'][0], middle -  
                                            DateOffset(minutes=window/2))                
            modelParameters['timeStop'] = min(ranges['DateTime'][1], middle +  
                                            DateOffset(minutes=window - window/2))
            
        elif idx_slider==3:#scroll half a window to a newer data
            # get midpoint of current window, it will be a new start point, limit it not to exceed available data
            modelParameters['timeStop'] = min(ranges['DateTime'][1], currModel['timeStop'] + 
                                                DateOffset(minutes=window/2))
            # calculate new start point by substracting window from end point
            modelParameters['timeStart'] = max(ranges['DateTime'][0], modelParameters['timeStop'] - 
                                                DateOffset(minutes=window))

        else:#get window of newest data
            # use the last data sample time as a stop time
            modelParameters['timeStop'] = ranges['DateTime'][1]
            # offset it by window duration
            modelParameters['timeStart'] = max(ranges['DateTime'][0], modelParameters['timeStop'] - 
                                                DateOffset(minutes=window))
    
    else:# check if start and stop dates are withing data range
        # get the full data range to check if start date is within it
        if currModel['timeStart'] is None or currModel['timeStart'] < ranges['DateTime'][0] or\
                currModel['timeStart'] > ranges['DateTime'][1]:
            modelParameters['timeStart'] = ranges['DateTime'][0]
            modelParameters['timeStop'] = min(ranges['DateTime'][1], modelParameters['timeStart'] + DateOffset(minutes=window))

    
    # update currModel with new parameter values
    currModel.update(modelParameters) 
    # recalculate window
    window = np.ceil((currModel['timeStop'] - 
                    currModel['timeStart']).total_seconds()/60)
        # update current model
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'models', currModel)
    # update store to trigger other events
    return store+1, window, no_update

#AK: VD-94 - updated to write all parameters, not only tuning
# fields are updated when layout loads (children) and when load modal status change
@app.callback(
    Input('tuning-layouts', 'children'),
    Input("dummy", "n_clicks"), #used for automatic data fit, not used now
    Input('graph', 'clickData'),
    Input({'type': 'conf', 'var': ALL}, 'value'),
    Input('ins-in', 'value'),
    Input('outs-in', 'value'),
    Input('zoom-store', 'data'), # added for initial value update on zoom change
    State('identification-radio', 'value'),
    State('model-in', 'value'),      
    State('data-in', 'value'),      
    Output({'type': 'tuning', 'var': ALL}, 'value'),
    Output('model-loading-update_tuning_values', 'children'),
)
def update_tuning_values(dummy, dummy1, click_data, init, ins, outs, store, radio_value, model_type, data_name):
    '''Layout initialization triggers update of all fields, but only if data is available, 
    otherwise default selections are made'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # get trigger id
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    if not id:
        raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA["main"] = load_data(current_user.username, "main")
    if DATA["main"] is None:
        raise PreventUpdate
    data={}
    with database_context_manager(database_pool) as db:
        currModel = db.getAttrValues(current_user.username, 'models', 'Temp')
    # read loop data tables
    # do not update if there is no Temp model
    if not currModel:
        raise PreventUpdate
    logger.debug(f'Trigger id: {id}')
    # update model current model when input values change
    currModel['In'] = ins
    currModel['Out'] = outs           
    currModel['datatable'] = data_name

    # read datatable for calculating the model parameters
    df = None
    # only read datatable if tagnames and table name is defined
    # set below is used to avoid duplicate column request
    if None not in [ins, outs, data]:
        df = DATA["main"].loc[:,list(set([ins, outs]))].copy()
        if currModel["timeStart"] >= df.index[0] and currModel["timeStop"] <= df.index[-1]:
            df = df.loc[currModel["timeStart"]:currModel["timeStop"]]
    # calculate initial values
    if df is not None:
        # calculate initial values only if any flag is set, else use entered value
        logger.debug(init)
        # calculate rate for integrating models
        if model_type == "SODITF":
            df['Rate'] = rate(df[currModel['Out']], currModel['filter'])
        
        # update model initial values, only if enabled by the user
        logger.debug(f'init value calculation - {id}-{init}')
        if len(init)>1 and all(init):
            currModel['y0'], currModel['Angle'] = getInit(df, currModel)
        elif init[0]:#y0 should be calculated
            currModel['y0'], _ = getInit(df, currModel)    
        elif len(init)>1 and init[1]:#rate0 should be calculated
            _, currModel['Angle'] = getInit(df, currModel)                            


        # update the initial value parameter used below
        # added if statements to prevent non existent key usage
        if model_type == "SODITF":
            y0 = currModel['Angle'] if 'Angle' in currModel else 0
        else:
            y0 = currModel['y0'] if 'y0' in currModel else 0

    # update identification parameters
    
    if click_data and id == 'graph':
        # copy current identification params
        ident = eval(currModel['ident']) if str(currModel['ident']) != 'None' else {}
        # get current point index, indexes will be used
        ident[radio_value] = str(df.index[click_data['points'][0]['pointIndex']])
        # verify selected data, by checking that timestamp magnitude constraint is maintained:
        # S0<S1<Td<T1 and T2
        truths = {  'step-start' : [True, True, True, True, True],
                    'step-end' : [False, True, True, True, True],
                    'Td' : [False, True, True, True, True],
                    'T1' : [False, False, False, True, False],
                    'T2' : [False, False, False, True, True]}
        
        truthTable = pd.DataFrame(truths, index=list(truths.keys()))
    # check for correct order
        # convert to datetime
        identDT = {key:datetime.strptime(ident[key], '%Y-%m-%d %H:%M:%S.%f' 
                                            if '.' in ident[key] else '%Y-%m-%d %H:%M:%S') 
                                            for key in ident}

        # compare with values of other and match with truthTable
        # do not check self
        keys = list(identDT.keys())
        keys.remove(radio_value)
        # loop though all remaining indices
        for idnt2 in keys:
            # if checked element is datetime
            if isinstance(identDT[idnt2], datetime):
                # compare first element with the second and match result with truthTable
                if (identDT[radio_value] >= identDT[idnt2] and not truthTable.loc[radio_value, idnt2])\
                or (identDT[radio_value] <= identDT[idnt2] and truthTable.loc[radio_value, idnt2]):
                    # remove the element from the dictionary
                    # if it is still present
                    if idnt2 in ident:
                        ident.pop(idnt2)

    # check if all timestamps are valid, remove if they are out of range

        for idnt in keys:
            if idnt in ident:
                if ident[idnt] not in df.index:
                    ident.pop(idnt)
                    continue
            
        # store identification
        currModel['ident'] = str(ident)
        # when any timestamps is saved
        if len(ident.keys())>0:
            # get values and calculate parameters
            # use rate for integrating process
            if model_type == "SODITF":
                out_var = 'Rate'
            else:
                out_var = currModel['Out']
            # get initial step value
            if 'step-start' in ident:
                u0 = df[df.index.isin([ident['step-start']])][currModel['In']].values[0]
            else:
                u0 = df.loc[df.index[0]][currModel['In']]
            # get final step value
            if 'step-end' in ident:
                u1 = df[df.index.isin([ident['step-end']])][currModel['In']].values[0]
            else:
                u1 = df.loc[df.index[-1]][currModel['In']]
            

            # final y value
            if 'T1' in ident:
                y1 = (df[df.index.isin([ident['T1']])][out_var].values[0] - y0)/0.632 + y0
                # calculate parameter values
                currModel['K'] = (y1-y0)/(u1-u0)
            # get timestamps in correct format
            
            # get step time when start and end is not defined
            if 'step-start' not in ident and 'step-end' not in ident:
                ufrange = np.abs(np.max(df[currModel['In']]) - np.min(df[currModel['In']]))
                ufdiff = np.abs(np.diff(df[currModel['In']]))
                # step moment is where the data start changing by 5% of the total change
                t0idx = np.where(ufdiff > ufrange * 0.05)[0]
                if t0idx is not None:
                    ident['step-start'] = df.iloc[t0idx].index.format()[0]
            elif 'step-start' not in ident:
                ident['step-start'] = ident['step-end']
            
            # convert strings to datetime objects
            timestamps = {key:datetime.strptime(ident[key], '%Y-%m-%d %H:%M:%S.%f' 
                                        if '.' in ident[key] else '%Y-%m-%d %H:%M:%S') 
                                        for key in ident}
            
            # start is used as a reference because in response simulation it is treated as a start
            if 'step-start' in ident:
                if 'Td' in ident:
                        currModel['td'] = (timestamps['Td'] - timestamps['step-start']).total_seconds()
                if 'T1' in ident:               
                    if 'Td' in ident:
                        currModel['t1'] = (timestamps['T1'] - timestamps['Td']).total_seconds()
                    else:
                        currModel['t1'] = (timestamps['T1'] - timestamps['step-start']).total_seconds()   
                            
            # T2 is optional                    
                if 'T2' in ident:               
                    if 'Td' in ident:
                        currModel['t2'] = max((timestamps['T2'] - timestamps['Td']).total_seconds(), 0)
                    else:
                        currModel['t2'] = max((timestamps['T2'] - timestamps['step-start']).total_seconds(), 0)
    # update model parameters
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'models', currModel)      
    if currModel != {}:
        # get correct parameter order 
        output_k = [el['id']['var'] for el in callback_context.outputs_list[0]]                
        # added if statement for key existance check
        data['params'] = [round(currModel[k_dict[k]], 4 if k_dict[k] not in ['K', 'Angle'] else 6) 
                            if k_dict[k] in currModel and currModel[k_dict[k]] else 0 if k_dict[k] not in ['K', 'filter'] else 1
                            for k in output_k]
        return data['params'], no_update
    # default return values            
    par = [''] * len(keys)
    logger.debug(f'Fake update:\n{par}')
    return par, no_update
@app.callback(    
    Input('fit-b', 'n_clicks'),
    State("dummy", "n_clicks"),
    Output("dummy", "n_clicks"),
    Output('model-loading-add_fit', 'children'),
    prevent_initial_call=True,
)
def addFit(n1, dummy):
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    '''This callback fits the model and triggers update callback'''
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    if id == 'fit-b' and n1:
        with database_context_manager(database_pool) as db:
            # get current model parameters
            currModel = db.getAttrValues(current_user.username, 'models', 'Temp')
            # get current loop data
            # create filter for data range, this will be used for data filtering in SQL
            where_query = {}
            if 'timeStart' in currModel and currModel['timeStart'] and 'timeStop' in currModel and currModel['timeStop']:
                where_query = {'DateTime':{0:(currModel["timeStart"]),
                                           1:(currModel["timeStop"])}}
            # get used current loop data
            df = db.getData(current_user.username, currModel["datatable"],
                            sel_query=[currModel['In'],
                                    currModel['Out'],
                                    'DateTime'],
                            where_query=where_query)        

            
            # calculate rate for integrating process
            if currModel['model_type'] == "SODITF":
                df ['Rate'] = rate(df[currModel['Out']], currModel['filter'])
            
            # for integrating process store initial rate as Angle
            if currModel['model_type'] == "SODITF":
                # currModel['Angle'] = rate0
                y0 = currModel['Angle']
            else:
                y0 = currModel['y0']    

            #fit current model
            parameters = list(model.modelFit(df, currModel, y0))

            #write parameters to database
            keys = ['K', 't1', 't2', 'td', 'Angle']
            paramDict = {key:parameters[i] for i, key in enumerate(keys)}
            db.updateAttr(current_user.username, 'models', paramDict)
            
            #since tuning entry fields do not exist when page loads, 
            #this dummy counter was used to implement update of parameters after fitting is done
            return dummy+1, no_update
           
    # raise PreventUpdate
    raise PreventUpdate

@app.callback(
    Output('csv-download', 'data'),
    Output('model-loading-download_data', 'children'),
    Input('csv-button', "n_clicks"),
    prevent_initial_call=True
)
def download_data(n_clicks):
    if n_clicks is None:
        raise PreventUpdate
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # Read all models for current user and current loop
    with database_context_manager(database_pool) as db:        
        models = db.getAttr(current_user.username, 'models')
        # remove current model
        df = models.loc[models.name != "Temp"]
        # remove irrelevant columns
        df = df.drop(['loop', 'ident', 'Angle'], axis=1)
        #column name capitalization
        df.columns = df.columns.str.capitalize()

    # Trigger the file download
    return dcc.send_data_frame(df.to_csv, "data.csv"), no_update

@app.callback(
    Output('show-mod-pop-up', "is_open"),
    Output('show-model-content', 'children'),
    Input('show-model-b', "n_clicks"),
    prevent_initial_call=True
)
def show(n1):
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    '''This function opens a modal window to show all available models'''
    if n1 is None:
        raise PreventUpdate
    # Read all models for current user and current loop
    with database_context_manager(database_pool) as db:        
        models = db.getAttr(current_user.username, 'models')
        # remove current model
        df = models.loc[models.name != "Temp"]
        # do not show table if there are no models
        if len(df.index) == 0:
            raise PreventUpdate
        # remove irrelevant columns
        df = df.drop(['loop', 'ident', 'Angle'], axis=1)
        #column name capitalization
        df.columns = df.columns.str.capitalize()
        
        # Set id based on controller name
        df['id'] = df['Name']
    
        # Set index based on controller name
        df.set_index('id', inplace=True, drop=False)
        # create dcc component
        #when no controllers are saved do not return table
        # styling: header in bold, table rows stripped 
        if len(df.index) > 0:
            table = dash_table.DataTable(
                id='model-datatable-row-ids',
                columns=[
                    {'name': i, 'id': i, 'deletable': False} for i in df.columns
                    # omit the id column
                    if i != 'id'
                ],
                data=df.to_dict('records'),
                # editable=True,
                filter_action="native",
                sort_action="native",
                sort_mode='multi',
                row_selectable=False,
                row_deletable=False,
                page_action='native',
                page_current= 0,
                page_size= 100,
                style_data_conditional=[
                    {
                        'if': {'row_index': 'odd'},
                        'backgroundColor': 'rgb(220, 220, 220)',
                    }],
                style_header={
                    'backgroundColor': 'rgb(210, 210, 210)',
                    'color': 'black',
                    'fontWeight': 'bold'
                }
            )

        return True, table

@app.callback(
    Output('clear-model-pop-up', "is_open"),
    Output('model-loading-clear', 'children'),
    Input('clear-model-b', 'n_clicks'), 
    Input('clear-model-cancel', "n_clicks"), 
    Input('clear-model-delete', 'n_clicks'),
    State('clear-model-pop-up', "is_open"),
    prevent_initial_call=True
)
def clear(n1, n2, n3, is_open):
    '''Deletes all models'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    ctx = callback_context
    callback_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    if callback_id == 'clear-model-delete':
        # Delete all saved models 
        with database_context_manager(database_pool) as db:
            # Read all models for current user and current loop
            models = db.getAttr(current_user.username, 'models')

            # Delete all non Temp models
            for name in models.name:
                if name != 'Temp':
                    db.deleteAttr(current_user.username, 'models', name) 
    if n1 or n2 or n3:
        return not is_open, no_update
    return is_open, no_update


@app.callback(
    Output('graph', 'figure'),
    Output('model-loading-update_graph', 'children'),
    Input({'type': 'tuning', 'var': ALL}, 'value'),    
    Input('zoom-store', 'data'),
    Input({'type': 'conf', 'var': ALL}, 'value'),
    State('data-in', 'value'),
    State('model-in', 'value'),
    State('ins-in', 'value'),
    State('outs-in', 'value'),
    prevent_initial_call=True
)
def update_graph(params, store, init, data_name, model_type, ins, outs):
    '''When all information for graph is available call update graph'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    if None in [ins, outs, data_name, model_type]:
            raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA["main"] = load_data(current_user.username, "main")
    if DATA["main"] is None:
        raise PreventUpdate
    modelParameters = {}
    # reading model parameters, for correct mapping
    modelParameters = {k_dict[el['id']['var']]:el['value'] for el in callback_context.inputs_list[0]}            
    modelParameters.update({
        'model_type': model_type,
        'datatable': data_name,
        'In':ins,
        'Out':outs
        })
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    with database_context_manager(database_pool) as db:
        # get current loop data
        currModel = db.getAttrValues(current_user.username, 'models', 'Temp')
        logger.debug(f'Current model: {currModel}')
        #update data range in current model
        #daterange reset, no df filtering
        
    # update currModel with new parameter values
    currModel.update(modelParameters) 

        # copy data to avoid changing original data
    df = DATA["main"].loc[:,list(set([ins, outs]))].copy()
    if 'timeStart' in currModel and currModel['timeStart'] >= df.index[0] and \
        'timeStop' in currModel and currModel["timeStop"] <= df.index[-1]:
        df = df.loc[currModel["timeStart"]:currModel["timeStop"]]
    # calculation enabled for all processes for better 
    # intuition during identification
    df ['Rate'] = rate(df[currModel['Out']], currModel['filter'])

    # update current model
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'models', currModel)
    try:            
        # create model response
        plant = model.modelFactory(**currModel)
        # only simulate if data exists
        if ins in df.columns:
            df ['Response'], df ['Rate response'], _ = plant.getSimulatedResponse(df.index,
                                                                                df [ins].values,
                                                                                y0 = currModel['y0'],
                                                                                ratey0=currModel['Angle'] if 'Angle' in currModel else 0)
    except Exception:
        logger.debug(f'Failed with model: {currModel}')
        traceback.print_exc()
        pass
    # get identification parameters
    ident = eval(currModel['ident']) if 'ident' in currModel and str(currModel['ident']) != 'None' else {}       
    return graph(df, ins, outs, model_type, ident), no_update