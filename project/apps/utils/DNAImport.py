import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import re


def resample(data, name):
    '''This routine takes a single tagname data and resamples it to 1s resolution'''
    # last sample is nonsense
    data['Time'].pop()
    data['Value'].pop()
    # convert to datetime
    time = pd.to_datetime(data['Time'])

    # get time in seconds
    dtimes = (time-time[0]).total_seconds()
    dtimes = np.asarray(dtimes, dtype=np.int32)

    # get even time samples for every second
    xn = np.linspace(int(dtimes[0]), int(dtimes[-1]), int(dtimes[-1]), 
                    dtype = np.int32)
    
    # get values as array
    samples = np.asarray(data['Value'], np.float64)

    # create interpolation function
    f = interp1d(dtimes, samples, fill_value="extrapolate")

    # resample data, eps is added to avoid division by zero
    yn = f(xn+1e-9)

    # store data to dataframe
    df = pd.DataFrame(
        data = {name: yn}, 
        index=pd.Timestamp(time[0]) + pd.to_timedelta(xn, unit='s')
    )
    return df


def convert(text):
    '''Valmet DNA trend export contains data samples with different timestamps.
    It is compressed to avoid duplicate data.
    For this reason special care is required when handling this data.
    This routine will read the text file contents and create a dictionary.
    Later timestamps and values of each dictionary key are converted to pandas and resampled.
    Finally, all dataframes are merged and forward and backfilled to generate the complete dataframe.'''
    data = {}
    # first two rows contain tags and data descriptions
    # Read headers
    row0 = text.readline().strip().split('\t\t')
    row1 = text.readline().strip().split('\t')
    
    # Initialize dictionary
    for tag_full in row0:
        # extract and format tag name
        name = re.sub(r"^(.+) \/ (\S+) \((\w+)\)\s+(\S+.*)$", r"\2:\3 - \1(\4)", tag_full)                      

        data[name] = {field: [] for field in row1}
        # remove last new line symbol
        try:        
            data[name].pop('\n')
        except:
            continue
    
    # remove last new line symbol
    try:
        data.pop('\n')
    except:
        pass

    # loop through the remaining text
    for line in text:  
        # collect data
        values = line.split('\t')
        # iterate through points
        i = 0
        for tag in data:
            for field in data[tag]:
                if values[i]:
                    data[tag][field].append(values[i])
                i+=1

    # collect all the data
    dfs = []
    # collect all dataframes with resampled data
    for tag in data:
        if len(data[tag]['Time']) > 3:
            dfs.append(resample(data[tag], tag))
    # join data into a single dataframe
    df = pd.concat(dfs, axis=1).ffill().bfill()
    # rename Time to DateTime before return
    df = df.rename_axis('DateTime')
    return df