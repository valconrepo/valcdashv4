from dash import Dash, Output, Input, State, html, dcc, callback, MATCH, callback_context
import uuid
import dash_bootstrap_components as dbc
import Utils as utl


# All-in-One Components should be suffixed with 'AIO'
class SaveModalAIO(html.Div):  # html.Div will be the "parent" component

    # A set of functions that create pattern-matching callbacks of the subcomponents
    class ids:
        open_button = lambda aio_id: {
            'component': 'ClickWithModalAIO',
            'subcomponent': 'open_button',
            'aio_id': aio_id
        }
        cancel_button = lambda aio_id: {
            'component': 'ClickWithModalAIO',
            'subcomponent': 'cancel_button',
            'aio_id': aio_id
        }
        ok_button = lambda aio_id: {
            'component': 'ClickWithModalAIO',
            'subcomponent': 'ok_button',
            'aio_id': aio_id
        }
        input = lambda aio_id: {
            'component': 'ClickWithModalAIO',
            'subcomponent': 'input',
            'aio_id': aio_id
        }
        modal = lambda aio_id: {
            'component': 'ClickWithModalAIO',
            'subcomponent': 'modal',
            'aio_id': aio_id
        }

    # Make the ids class a public class
    ids = ids

    # Define the arguments of the All-in-One component
    def __init__(self, name, aio_id=None):
        if aio_id is None:
            aio_id = str(uuid.uuid4())

        modal = dbc.Modal([
            dbc.ModalHeader(f'{name} save'),
            dbc.ModalBody([
                utl.Input(self.ids.input(aio_id), f'{name} name', f'{name} Name', type='text'),
            ]),
            dbc.ModalFooter([
                utl.Button(self.ids.cancel_button(aio_id), 'Cancel'),
                utl.Button(self.ids.ok_button(aio_id), 'OK')
            ])
        ])

        button = utl.Button(self.ids.open_button(aio_id), 'Cancel'),

        # Define the component's layout
        super().__init__([modal, button])

    @callback(
        [Output(ids.modal(MATCH), "is_open")],
        [Input(ids.open_button(MATCH), "n_clicks"), Input(ids.cancel_button(MATCH), "n_clicks"),
         Input(ids.ok_button(MATCH), 'n_clicks')],
        [State(ids.modal(MATCH), "is_open"), State(ids.input(MATCH), 'value')],
    )
    def modal_update(n1, n2, n3, is_open, name):
        callback_id = callback_context.triggered[0]['prop_id'].split('.')[0]
        if callback_id == 'mod-save':
            if name is None:
                raise PreventUpdate
            ok_function()
        if n1 or n2 or n3:
            return not is_open
        return is_open

    save_popup = dbc.Modal(
        [
            dbc.ModalHeader('Linearization save'),
            dbc.ModalBody([
                utl.Input('lin-name-in', 'Linearization name', 'Linearization name', type='text'),
            ]),
            dbc.ModalFooter([
                utl.Button('lin-cancel', 'Cancel'),
                utl.Button('lin-save', 'Save')
            ])
        ], id='save-lin-pop-up'
    )
