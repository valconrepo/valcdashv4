import numpy as np
from control.matlab import *
import scipy as sc


def lqrpid(sys, Q, R, **kwargs):
    # LQRPID LQR - based PID output - feedback controller design for LTI ss systems.
    #           [F, P, E] = LQRPID(sys, Q, R, N) calculates the optimal( if l = n / 2) or the
    #           sub - optimal( if l ~ = n / 2) output - feedback PID(Proportional - Integral
    #           -Derivative) gain matrix F = [Kp, Ki, Kd], such that:
    #
    #           *For a continuous - time state - space model SYS, the output - feedback law:
    #
    #           u = - Kp y - Ki Integral y dt - Kd dy / dt,
    #
    #           minimizes the cost function:
    #
    #           J = Integral {x'Qx + u'Ru + 2x'Nu} dt
    #
    #        Subject to the system dynamics dx / dt = Ax + Bu; y = Cx
    #
    #        INPUTS:
    #           REQUIRED
    #               SYS - state - space LTI system(ss: sys.a, sys.b, sys.c...)
    #                Q - weighting matrix related to states(Q >= 0 if N == 0)
    #                R - positive definite weighting matrix(R > 0)
    #
    #           OPTIONAL
    #               N - The matrix N is set to zero when omitted.
    #                   If N ~ = 0 then Q - N * inv(R) * N '>=0 (use eig to check)
    #
    #            OUTPUTS:
    #                F - static output - feedback gain matrix containing the  PID gains F = [Kp, Ki, Kd]
    #                P - Lyapunov matrix
    #                E - Closed - loop system eigen values
    #
    #            ASSUMPTIONS:
    #                    - The pair([A, 0; C, 0], [B; 0]) is stabilizable,
    #                    - R > 0 and Q - N * inv(R) * N' >= 0,
    #                    - Q - N * inv(R) * N' and A-B*inv(R)*N' have no unobservable mode on the
    #                        imaginary axis
    #
    #             OTHER INFO:
    #                    - The size of the weighting matrices is AUGMENTED:
    #                        Q(n + l, n + l), R(m, m), N(n + l, m)
    #                    where
    #                        n - number of states,
    #                        m - number of inputs,
    #                        l - number of outputs,
    #                    because the system is augmented with additional state variable(s)
    #                    for the PID controller design.
    #                    - Optimal solution is for l=n / 2. If l~=n / 2, an approximation is
    #                        calculated and tested for stability.
    #
    #              REQUIREMENTS:
    #
    #               Matlab:
    #                - control system toolbox installed
    #                Octave:
    #                - control package installed and loaded
    #
    #                This function is based on:
    #                    S.Mukhopadhyay: P.I.D.equivalent of optimal regulator,
    #                Electronics Letters, Vol. 14, No. 25, pp. 821 - 822, 1978.
    #
    #                 Tested with Matlab 2014b / Octave 4.0
    #
    #                See also LQR DLQR, LQRY, LQI, LQGREG, LQGTRACK, LQG, CARE, DARE.
    #
    #                Version: 1.2; Author(s): Adrian Ilka 24 - 03 - 2017

    As = sys.A
    Bs = sys.B
    Cs = sys.C
    Ds = sys.D
    ns = len(As[0, :])
    ms = len(Bs[0, :])
    ls = len(Cs[:, 0])

    default_N = np.zeros((ns + ls, ms))
    if 'N' in kwargs.keys():
        N = kwargs['N']
    else:
        N = default_N

    Qnm = np.shape(Q)
    Rnm = np.shape(R)
    Nnm = np.shape(N)

    # Testing the weighting matrices
    if (Qnm[0] != ls) or (Qnm[1] != ns):
        raise ValueError('Error n.%d:\nThe Q matrix has wrong size.\nFor more info see help LQRPID / Assumptions')
    if (Rnm[0] != ms) or (Rnm[1] != ms):
        raise ValueError('Error n.%d:\nThe R matrix has wrong size.\nFor more info see help LQRPID / Assumptions')
    if (Nnm[0] != ns + ls) or (Nnm[1] != ms):
        raise ValueError('Error n.%d:\nThe N matrix has wrong size.\nFor more info see help LQRPID / Assumptions')

    # Testing other dependencies
    if Ds != 0:
        rv = 4
        raise ValueError('Error n.%d:\nThis approach allows only sys.d=0.\nFor more info see help LQRPID', rv)
    if np.linalg.eig(Q - N * np.linalg.inv(R) * N.conj().T)[0].min() < 0:
        rv = 5
        raise ValueError(
            'Error n.%d:\nThe Q-NR^-1N^T needs to be positive semidefinite.\nFor more info see help LQRPID', rv)
    if np.linalg.eig(R)[0].min() <= 0:
        rv = 5
        raise ValueError('Error n.%d:\nThe R needs to be positive definite (R>0).\nFor more info see help GOFCD', rv)
    # % Augmenting the system
    A = np.array([[As, np.zeros((ns, ls))], [Cs, np.zeros((ls, ls))]])
    B = np.array([[Bs], [np.zeros((ls, ms))]])
    C = np.array([[Cs, np.zeros((ls, ls))], [np.zeros((ls, ns)), np.identity(ls)]])
    Cd = np.array([[Cs, np.zeros((ls, ls))], [np.zeros((ls, ns)), np.zeros((ls, ls))]])

    n = len(A[0, :])
    # m = len(B[1,:])
    # l = len(C[:, 1])
    # Controller design
    [_, Pr, Er] = lqr(A, B, Q, R, N)
    P11 = Pr[0:ns - 1, 0: ns - 1]
    P12 = Pr[0:ns - 1, ns: ns + ls - 1]
    sKp = np.linalg.inv(R) * (Bs.conj().T * P11 + N[0:ns - 1, 0:ms - 1].conj().T)
    sKi = np.linalg.inv(R) * (Bs.conj().T * P12 + N[ns - 1 + 0:ns - 1 + ls - 1, 0:ms - 1].conj().T)
    sC = np.array([[Cs], [Cs * As - Cs * Bs * sKp]])
    Kpd = sKp * np.linalg.pinv(sC)

    Kp = Kpd[0:ms - 1, 0: ls - 1]
    Kd = Kpd[0:ms - 1, ls: ls + ls - 1]
    Ki = (np.identity(ms) + Kd * Cs * Bs) * sKi

    Fr = [Kp, Ki, Kd]

    if ls == ns / 2:
        return Fr
        # print(' ')
        # print('Optimal PID controller structure (l=n/2):')
        # print('      F(%dx%d) = [ Kp(%dx%d)  Ki(%dx%d)  Kd(%dx%d)]\n', ms, 3 * ls, ms, ls, ms, ls, ms, ls)
    else:
        F = np.array([Kp, Ki])
        Fd = np.array([Kd, np.zeros((Kp.size()))])
        Ac = A - B * F * C
        Ad = np.identity(n) + B * Fd * Cd
        Ah = np.linalg.inv(Ad) * Ac
        uRu = C.conj().T * F.conj().T * R * F * C + Ah.conj().T * Cd.conj().T * Fd.conj().T * R * Fd * Cd * Ah + Ah.conj().T \
              * Cd.conj().T * Fd.conj().T * R * F * C + C.conj().T * F.conj().T * R * Fd * Cd * Ah
        xNu = -N * (F * C + Fd * Cd * Ah) - (
                Ah.conj().T * Cd.conj().T * Fd.conj().T + C.conj().T * F.conj().T) * N.conj().T
        Qh = Q + uRu + xNu
        Pr = sc.linalg.solve_continuous_lyapunov(Ah.conj().T, Qh)
        if np.linalg.eig(Pr)[0].min() <= 0:
            raise ValueError(
                'Error n.%d:\nSub-optimal PID controller design failed!\nTry to change the weighting matrices. For '
                'more info see help LQRPID.')
        Er = np.linalg.eig(Ah)

        return Fr

        # print(' ')
        # print('Sub-optimal PID controller structure (l~=n/2):')
        # print('      F(%dx%d) = [ Kp(%dx%d)  Ki(%dx%d)  Kd(%dx%d)]\n', ms, 3 * ls, ms, ls, ms, ls, ms, ls);
