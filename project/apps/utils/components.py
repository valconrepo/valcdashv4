import dash_bootstrap_components as dbc
from dash import dcc, html

height = {'height': '3.5vh'}
def Input(identity, placeholder, name, type='number', persistence=True, step = 0.0001, className = "mb-3", value=1):
    return dbc.InputGroup(
        [
            dbc.InputGroupText(name,style=height),
            dbc.Input(placeholder=placeholder, id=identity, type=type, step=step, 
                      persistence=persistence, debounce=True, value=value,style=height), # VD-214 debounce here prevents 
                    #   refresh until Enter is pressed of field is deactivated
        ],
        className=className, style=height
    )
def Input_ext(identity:dict, placeholder, name, type='number', persistence=True, step = 0.0001, className = "mb-3", value=1):
    if isinstance(identity, dict):
        id_chk = identity.copy()
        id_chk['type'] = 'conf'
    else:
        id_chk = identity+'-conf'
    stl = {'position': 'relative'}
    stl.update(height)
    return dbc.InputGroup(
        [            
            dbc.InputGroupText(name, style=stl),
            dbc.Input(placeholder=placeholder, id=identity, type=type, step=step, 
                      persistence=persistence, debounce=True, value=value, style=height), # VD-214 debounce here prevents 
                    #   refresh until Enter is pressed of field is deactivated
            dcc.Checklist(id=id_chk, options=[{'label': '', 'value': '1'}], persistence=persistence,
                inline=True,
                style={'position': 'relative', 'left': '-0.8em', 'top': '-0.25em'}),
        ],
        className=className, style=height
    )


def Selector(identity, name, options=None, persistence=True, value="", className = "mb-3 pt-0 pb-0", style=height):
    return dbc.InputGroup(
        [
            dbc.InputGroupText(name, style=style, class_name=className),
            dbc.Select(id=identity, options=options, persistence=persistence, value=value, style=style, class_name=className),
        ],
        className=className, style=height
    )


def Slider(identity, name, min=0, max=10, step=0.1, value=0, className = "mb-3"):
    return html.Div(
        [
            dbc.Label(name, html_for=identity),
            dcc.Slider(id=identity, min=min, max=max, step=step, value=value, persistence=True),
        ],
        className=className,
    )


def Button(identity, name, color="primary", href = None, className="mr-1 pt-0 pb-0"):
    return dbc.Button(name, color=color, className=className, id=identity, href=href, style=height)


def Upload(identity):
    return dcc.Upload(
        id=identity,
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True, #VD-101 multiple file upload is not implemented
    ),



error = dbc.Container(
    [
        html.H1("Error", className="display-3"),
        html.P(
            "Something wrong. This page cannot be displayed",
            className="lead",
        ),
    ]
)

# Add a new function for the global spinner
def LoadingSpinner(id="global-spinner"):
    return dbc.Spinner(
        html.Div(id=id),
        size='sm',
        delay_show=500,
        color='primary',
        spinner_style={
            "width": "3rem",
            "height": "3rem",
            "position": "fixed",
            "bottom": "0rem",
            "left": "1rem",
            "zIndex": 9999
        },
        type='grow'
    )
