from . import database_pool

class database_context_manager:
    def __init__(self, pool: database_pool):
        self.pool = pool

    def __enter__(self):
        self.obj = self.pool.acquire()
        return self.obj

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.pool.release(self.obj)
