import redis
from dataclasses import dataclass, asdict, field, is_dataclass
import pandas as pd
import json
from typing import List, Any, Callable
from ..model import plant, PID, modelFactory, SODTF, OvPID
from dateutil import parser
from abc import ABC, abstractmethod
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, Float, DateTime, String, Text, Boolean, UniqueConstraint, select, insert, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import and_
from pathlib import Path
from inspect import currentframe
from ..functions import save_data, load_data

from dash.exceptions import PreventUpdate
# logger configuration that can be disable in this file
import logging
# Configure the logger
# create logger
logger = logging.getLogger('DB')
# set loging level DEBUG, INFO, WARNING, ERROR, CRITICAL
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.NOTSET)
# logger.setLevel(logging.INFO)
# define formatting
formatter = logging.Formatter('%(asctime)s - %(levelname)s - [%(filename)s:%(lineno)d] - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)

@dataclass
class user_configuration:
    pass

@dataclass
class element_list(ABC):
    values: List[Any] = field(default_factory=list)

    def delete(self, name: str):
        to_delete = self.get(name)
        self.values.remove(to_delete)

    def get(self, name: str) -> Any:
        for element in self.values:
            if element.name == name:
                return element

    def append(self, new_element: Any) -> None:
        element_names = self.get_name_list()
        if new_element.name not in element_names:
            self.values.append(new_element)

    def get_name_list(self) -> List[str]:
        return [element.name for element in self.values]

    @abstractmethod
    def serialize(self) -> List[dict]:
        pass

    @staticmethod
    @abstractmethod
    def deserialize(serialized_data: dict):
        pass


class time_series_list(element_list):
    values: List[pd.DataFrame] = field(default_factory=list)

    def serialize(self) -> List[dict]:
        serialized_time_series = []
        for tS in self.values:
            tS.index = tS.index.astype(str)
            serialized_time_series.append({'name': tS.name, 'data': tS.to_dict()})
        return serialized_time_series

    @staticmethod
    def deserialize(serialized_data: dict):
        time_series = []
        for serialized_time_series in serialized_data:
            if 'data' in serialized_time_series:
                time_series.append(pd.DataFrame.from_dict(serialized_time_series['data']))
                time_series[-1].name = serialized_time_series['name']
                try:
                    time_series[-1].index = [parser.parse(date) for date in time_series[-1].index]
                except:
                    continue #added to avoid errors when no loops are available except the dummy Loop0
            else:
                time_series.append({'name': 'Dummy', 'data': pd.DataFrame()})
        return time_series_list(time_series)


class model_list(element_list):
    values: List[plant] = field(default_factory=list)

    def serialize(self):
        return [model.serialize() for model in self.values]

    @staticmethod
    def deserialize(serialized_data):
        models = []
        for serialized_model_data in serialized_data:
            if serialized_model_data is not None:
                models.append(modelFactory(**serialized_model_data))
        return model_list(models)


class controllers_list(element_list):
    values: List[PID] = field(default_factory=list)

    def serialize(self):
        return [controller.serialize() for controller in self.values]

    @staticmethod
    def deserialize(serialized_data):
        controllers = []
        for serialized_controller_data in serialized_data:
            if serialized_controller_data is not None:
                controller = modelFactory(**serialized_controller_data)
                controller.name = serialized_controller_data['name']
                controllers.append(controller)

        return controllers_list(controllers)


@dataclass
class linearization_points:
    X: List[float] = field(default_factory=list)
    Y: List[float] = field(default_factory=list)
    name: str = None
    x_in_percent: bool = False
    y_in_percent: bool = False
    data_name: str = None
    x_variable_name: str = None
    y_variable_name: str = None
    number_of_values: int = 5

    def serialize(self):
        return asdict(self)

    @staticmethod
    def deserialize(serialized_data: dict):
        return linearization_points(**serialized_data)


class linearization_list(element_list):
    values: List[linearization_points] = linearization_points()

    def serialize(self) -> List[dict]:
        return [element.serialize() for element in self.values]

    @staticmethod
    def deserialize(serialized_data: List[dict]):
        return linearization_list([linearization_points.deserialize(data) for data in serialized_data])


@dataclass
class LoopData:
    loop_name: str
    current_controller: PID = OvPID()
    current_model: plant = plant()
    current_linearization: linearization_points = linearization_points()
    time_series: time_series_list = time_series_list()
    models: model_list = model_list()
    controllers: controllers_list = controllers_list()
    linearizations: linearization_list = linearization_list()

    def save_current_model(self, name: str):
        if name not in self.models.get_name_list():
            self.current_model.name = name
            self.models.append(self.current_model)

    def save_current_controller(self, name: str):
        if name not in self.controllers.get_name_list():
            self.current_controller.name = name
            self.controllers.append(self.current_controller)

    def save_current_linearization(self, name: str):
        if name not in self.linearizations.get_name_list():
            self.current_linearization.name = name
            self.linearizations.append(self.current_linearization)

    def serialize(self):
        serialized_time_series = self.time_series.serialize()
        serialized_models = self.models.serialize()
        serialized_controllers = self.controllers.serialize()
        serialized_linearization = self.linearizations.serialize()
        serialized_current_model = self.current_model.serialize()
        serialized_current_controller = self.current_controller.serialize()
        serialized_current_linearization = self.current_linearization.serialize()
        return {'loop_name': self.loop_name, 'time_series': serialized_time_series,
                'models': serialized_models,
                'controllers': serialized_controllers,
                'linearizations': serialized_linearization,
                'current_model': serialized_current_model,
                'current_controller': serialized_current_controller,
                'current_linearization': serialized_current_linearization}

    @staticmethod
    def deserialize(serialized_data: dict):
        time_series = time_series_list.deserialize(serialized_data['time_series'])
        models = model_list.deserialize(serialized_data['models'])
        controllers = controllers_list.deserialize(serialized_data['controllers'])
        linearizations = linearization_list.deserialize(serialized_data['linearizations'])
        current_model = modelFactory(**serialized_data['current_model'])
        current_controller = modelFactory(**serialized_data['current_controller'])
        current_linearization = linearization_points.deserialize(serialized_data['current_linearization'])
        return LoopData(loop_name=serialized_data['loop_name'], controllers=controllers, models=models,
                        time_series=time_series, current_model=current_model, current_controller=current_controller,
                        linearizations=linearizations, current_linearization=current_linearization)


@dataclass
class UserData:
    loops: List[LoopData] = field(default_factory=list)
    configuration_data: user_configuration = user_configuration()
    currentLoop: int=0
    def to_json(self) -> str:
        serialized_loops = [loop.serialize() for loop in self.loops]
        serialized_user_data = {'configuration_data': asdict(self.configuration_data), 'loops': serialized_loops,'currentLoop': self.currentLoop}
        return json.dumps(serialized_user_data)

    @classmethod
    def from_json(cls, json_data: str):
        serialized_data = json.loads(json_data)
        loops = [LoopData.deserialize(loop) for loop in serialized_data['loops']]
        return UserData(configuration_data=user_configuration(**serialized_data['configuration_data']), loops=loops, currentLoop=serialized_data['currentLoop'])


class Redis_manager:
    redis_host = "localhost"
    redis_port = 6379
    redis_password = ""

    def __init__(self):
        self.redis_handler = redis.StrictRedis(host=self.redis_host,
                                               port=self.redis_port,
                                               password=self.redis_password,
                                               decode_responses=True)
        #self.write(UserData())

    def write(self, username, data: UserData) -> None:
        self.redis_handler.set(username, data.to_json())

    def read(self,username) -> UserData:
        serialized_data = self.redis_handler.get(username)
        if serialized_data==None:
            self.initialize(username, 'Loop 0')
            serialized_data = self.redis_handler.get(username)
        readData=UserData.from_json(serialized_data)
        if readData.loops.__len__()<1:
            self.initialize(username,'Loop 0')
            readData=UserData.from_json(self.redis_handler.get(username), username)
        return readData

    def initialize(self,username, loopName):
        user_data = UserData()
        loop = LoopData(loopName)
        user_data.loops.append(loop)
        self.write(username,user_data)

absolute_path = Path(__file__).parent.parent.parent.parent #added to solve issue with relative path
        
class SQLite_manager:
    '''Class for data storage using SQLite'''

    def __init__(self):
        self.path = Path(str(absolute_path)+'/shared/users.sqlite').resolve()
        self.root = self.path.parent
        self.engine = create_engine('sqlite:///'+str(self.path), 
                                    connect_args={'check_same_thread':False, 'timeout': 15})#, echo = True)
        self.meta = MetaData(bind=self.engine)
        self.attributes = ['controllers', 'models', 'linearizations', 'trends', 'analysiss']
        self.columns = {
            'loops': {
                'name': Text,
                'model': Text,
                'controller': Text,
                'linearization': Text,
                'in': Text,
                'out': Text,
                'datatable': Text,
                'is_current': Boolean
                },
            'models': {
                'name': Text,
                'model_type': Text,
                'datatable': Text,
                'K': Float,
                't1': Float,
                't2': Float,
                'td': Float,
                'y0': Float,
                'Angle': Float,
                'In': Text,
                'Out': Text,
                'timeStart': DateTime,
                'timeStop': DateTime,
                'loop': Text,
                'ident': Text,
                'filter': Float
            },
            'controllers': {
                'name': Text,
                'model': Text,
                'loop': Text,
                'controller_type': Text,
                'P': Float,
                'I': Float,
                'D': Float,
                'F': Float,
                'T1': Float,
                'scaling': Float,
                'disturbance': Text,
                'layout': Text,
                'PID1': Text,
                'PID2': Text,
                'smith':Boolean                
            },
            'linearizations': {
                'name': Text,
                'datatable': Text,
                'X': Text,
                'Y': Text,
                'timeStart': DateTime,
                'timeStop': DateTime,
                'x_variable_name': Text,
                'y_variable_name': Text,
                'x_in_percent': Boolean,
                'y_in_percent': Boolean,
                'number_of_values': Integer,
                'loop': Text,
                'x_params': Text,
                'y_params': Text
            },
            'trends': {
                'name': Text,
                'datatable': Text,
                'checkbox': Text,
                'yaxis': Text,
                'color': Text,
                'loop': Text,
                'timeStart': DateTime,
                'timeStop': DateTime,
                'calculations': Text,
                'calc_type': Text,
                'compound': Text
            },
            'analysiss': {
                'name': Text,
                'datatable': Text,
                'chart_type': Text,
                'loop': Text,
                'chart_config': Text,
                'filters_config': Text
            }
        }
        MetaData.reflect(self.meta)
        #self.write(UserData())

    def initLoops(self, username:str):
        '''Initialize user tables'''
        # Create tables dynamically based on the column definitions
        tables = []
        for table_name, columns in self.columns.items():
            constraint = UniqueConstraint('name', name=f'{username}idx_{table_name}') if table_name == 'loops' else UniqueConstraint('name', 'loop', name=f'{username}idx_{table_name}')
            table = Table(
                username + '_' + table_name, self.meta,
                *[Column(column_name, column_type) for column_name, column_type in columns.items()],
                constraint,
                extend_existing=True
            )
            # Add the table to the metadata
            tables.append(table)
        self.meta.create_all(self.engine)
    
    def getAttrTables(self, username, attribute:str=""):
        '''Get all attribute tables'''
        logger.info(f'{currentframe().f_code.co_name}')
        if attribute =="":
            return {attribute: self.meta.tables[username+"_"+attribute] for attribute in self.attributes}
        else:
            return self.meta.tables[username+"_"+attribute]

    
    def getLoops(self, username:str):
        '''
        Get user loops and return ids and names of all and current loops
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)
        try:
            # try to access all configuration tables and create new if any is non existent
            loops = meta_data.tables[username+"_loops"]
            attributes = self.getAttrTables(username)
            # controllers = self.meta.tables[username+"_controllers"]
            # models = self.meta.tables[username+"_models"]
            # linearizations = self.meta.tables[username+"_linearizations"]
        except KeyError: #if table is not found it is initialized
            self.initLoops(username)
            MetaData.reflect(self.meta)
            loops = meta_data.tables[username+"_loops"]
        
        query = select(loops.c.name)
        loopData = list(self.engine.execute(query))

        if len(loopData)<1: #no loops are present, create Dummy loop 'Loop 1"
            query_insert = loops.insert().values(name="Loop 1", is_current = True)
            self.engine.execute(query_insert)
            loopData = list(self.engine.execute(query))

        query_current = select(loops.c.name)\
                .where(loops.c.is_current == True)              
        loopCurrent = list(self.engine.execute(query_current))[0]
        loopData = [loop[0] for loop in loopData]
        return loopData, loopCurrent[0]
    
    def selectLoop(self, username:str, loopname:str):
        '''Change is_current loop field'''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)
        loops = meta_data.tables[username+"_loops"]

        # deactivate actual is_current loop
        query = loops.update()\
            .where(loops.c.is_current == True)\
            .values(is_current = False)
        self.engine.execute(query)

        # activate selected loop as is_current
        query = loops.update()\
            .where(loops.c.name == loopname)\
            .values(is_current = True)
        self.engine.execute(query)

    def createLoop(self, username:str, loopname:str):
        '''Create new loop'''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)
        loops = meta_data.tables[username+"_loops"]

        # insert new row in loops table
        query_insert = loops.insert().values(name=loopname)
        self.engine.execute(query_insert)

    def deleteLoop(self, username:str, loopname:str):
        '''Delete loop and all its' dependencies'''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        base = declarative_base()
        MetaData.reflect(meta_data)
        loops = meta_data.tables[username+"_loops"]    
        attributes = self.getAttrTables(username)
        # models = self.meta.tables[username+"_models"]
        # controllers = self.meta.tables[username+"_controllers"]
        # linearizations = self.meta.tables[username+"_linearizations"]

        # delete loop entry in loops table
        query_delete = loops.delete().where(loops.c.name==loopname)
        self.engine.execute(query_delete)

        #delete all related attributes
        for attr in attributes:
            query_delete = attributes[attr].delete().where(attributes[attr].c.loop == loopname)
            self.engine.execute(query_delete)

        # # delete loop entries in models table
        # query_delete = models.delete().where(models.c.loop==loopname)
        # self.engine.execute(query_delete)

        # # delete loop entries in controllers table
        # query_delete = controllers.delete().where(controllers.c.loop==loopname)
        # self.engine.execute(query_delete)

        # # delete loop entries in linearizations table
        # query_delete = linearizations.delete().where(linearizations.c.loop==loopname)
        # self.engine.execute(query_delete)


        #delete loop data
        filter = f'{username}_{loopname}_'
        allTables = meta_data.tables.keys()
        # delete data tables from current user and current loop     
        for dataTable in allTables:
            try:
                if filter in dataTable:
                    table = meta_data.tables[dataTable]
                    if table is not None:
                        base.metadata.drop_all(self.engine, [table], checkfirst=True)
            except:
                continue

    def getData(self, username:str, data_name:str, sel_query:list=[], where_query:dict={}, numRows:int=600) -> pd.DataFrame:
        '''
        Read in loop data of current loop.
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)

        #get current loop
        _, currentLoop = self.getLoops(username)
        dataTableName = f'{username}_{currentLoop}_{data_name}'

        # do not update when datatable is missing
        if dataTableName in meta_data.tables:
            dataT = meta_data.tables[dataTableName]
        elif (self.root / f'{dataTableName}.parquet').exists():
            df = pd.read_parquet(self.root / f'{dataTableName}.parquet')
            if sel_query:
                df = df[sel_query]
            if where_query:
                for key, value in where_query.items():
                    df = df[(df[key] >= value[0]) & (df[key] <= value[1])]
            return df[:min(len(df),numRows)]
        else:
            raise PreventUpdate
        
        # if table exists get current data table
        try:
            # only load requested elements, or all if not specified
            if sel_query:
                # select traces that exist in the table
                sel = eval(", ".join([f'dataT.c["{item}"]' for item in sel_query
                                      if item in dataT.columns.keys()]))
                query = select(sel)   
            else:
                query = dataT.select()            

            # load data based on query
            if where_query:
                where_binaries = []
                for key in where_query:
                    for element in where_query[key]:
                        if element == 0:
                            where_binaries.append(dataT.c[key] >= where_query[key][element])
                        else:
                            where_binaries.append(dataT.c[key] <= where_query[key][element])
                query = query.where(and_(*where_binaries))

            # if query is not defined load data based on number of rows
            else:
                #  get max number of rows
                maxNumRows = self.engine.execute(func.count((dataT.columns[0]))).scalar()
                # limit number of row to max posible
                Rows = min(numRows, maxNumRows)
                query = query.limit(Rows)


            df = pd.DataFrame(list(self.engine.execute(query)), columns=query.columns.keys())
            # find datetime column and set it as index
            dateTimeCol = [col for col in df.columns if df[col].dtype == 'datetime64[ns]']
            df = df.set_index([dateTimeCol[0]])
            return df
        # Return none
        except:
            return None

    def deleteData(self, username:str, data_name:str):
        '''
        Deletes data table and all dependencies:
            -models
            -controllers
            -linearizations
            -trends
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta = MetaData(bind=self.engine)
        MetaData.reflect(meta)        
        base = declarative_base()

        #get datatable name and delete table entry
        # loops = self.meta.tables[username+"_loops"]
        # query = select(loops.c.datatable)\
        #                 .where(loops.c.is_current == True)
        _, currentLoop = self.getLoops(username)
        dataTableName = f'{username}_{currentLoop}_{data_name}'
        # query = loops.update()\
        #     .where(loops.c.is_current == True)\
        #     .values(datatable = "")

        # delete datatable        
        try:
            # Remove parquet files related to the data table
            parquet_files = list(self.root.glob(f'{username}_{currentLoop}_{data_name}*.parquet'))
            for parquet_file in parquet_files:
                parquet_file.unlink()
            table = meta.tables[dataTableName]
            if table is not None:
                base.metadata.drop_all(self.engine, [table], checkfirst=True)

            attributes = self.getAttrTables(username)

            #get all models to delete tables
            query_select = select(attributes['models'].c.name)\
                                .where(attributes['models'].c.datatable==dataTableName)
            modelsDel = [item[0] for item in list(self.engine.execute(query_select))]

            #delete all directly and indirectly affected elements
            for attr in attributes:
                #controllers are deleted based on models
                if attr == 'controllers':
                    query_delete = attributes[attr].delete().where(attributes[attr].c.model.in_(modelsDel))
                else:
                    query_delete = attributes[attr].delete().where(attributes[attr].c.datatable==dataTableName)
                self.engine.execute(query_delete)

            # #delete all dependent models, get list of deleted models
            # models = self.meta.tables[username+"_models"]
            # query_select = models.select(models.c.name).where(models.c.datatable==dataTableName)
            # modelsDel = [item[0] for item in list(self.engine.execute(query_select))]
            # query_delete = models.delete().where(models.c.datatable==dataTableName)
            # self.engine.execute(query_delete)
                    
            # #delete all dependent controllers
            # controllers = self.meta.tables[username+"_controllers"]
            # query_delete = controllers.delete().where(controllers.c.model.in_(modelsDel))
            # self.engine.execute(query_delete)

            # #delete all dependent linearization     
            # linearizations = self.meta.tables[username+"_linearizations"]
            # query_delete = linearizations.delete().where(linearizations.c.datatable==dataTableName)
            # self.engine.execute(query_delete)
        except: pass
      
    def putData(self, username:str, data:pd.DataFrame, data_name:str, if_exists:str='replace', use_parquet:bool=True):
        '''
        Store DataFrame as a separate table, or update if datatable already exists
        Table name {username}-{currentLoop}-{data_name}.
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)
        
        #data table name will code user, loop and data names
        _, currentLoop = self.getLoops(username)
        dataTableName = f'{username}_{currentLoop}_{data_name}'

        #update loop datatable field
        loops = meta_data.tables[username+"_loops"]
        
        query = loops.update()\
            .where(loops.c.name == currentLoop)\
            .values(datatable = dataTableName)
        self.engine.execute(query)
        #create new table from pandas dataframe
        # delete datatable before rewrite
        try:
            table = meta_data.tables[dataTableName]
            base = declarative_base()
            base.metadata.drop_all(self.engine, [table], checkfirst=True)
        except:
            pass
        if use_parquet:
            save_data(data, f'{currentLoop}_{data_name}', username, self.root)
            data.to_parquet(dataTableName+'.parquet')
        else:
            data.to_sql(dataTableName, self.engine, if_exists=if_exists)
    
    def getDataTables(self, username:str) -> list:
        '''This function collects all available data tables for the current user'''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)
        # get current loop
        _, currentLoop = self.getLoops(username)
        filter = f'{username}_{currentLoop}_'
        allTables = meta_data.tables.keys()
        # collect data tables from current user and current loop
        tables = [table.replace(filter,"") for table in allTables if filter in table]
        # Check for parquet files in the root directory
        parquet_files = list(self.root.glob(f'{username}_{currentLoop}_*.parquet'))
        for parquet_file in parquet_files:
            table_name = parquet_file.stem.replace(f'{username}_{currentLoop}_', '')
            tables.append(table_name)
        return tables
    
    def getAttr(self, username:str, attrName:str, all=False) -> pd.DataFrame:
        '''
        Function to read different loop attributes:
        -models
        -controllers
        -linearizations
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        #get attributes based on current loop
        meta_data = MetaData(bind=self.engine)
        #find current loop
        _, currentLoop = self.getLoops(username)
        
        #whole table will be read to pandas
        #and filtered to have a better formatting
        attrTableName = username+"_"+attrName
        attrTable = pd.read_sql_table(attrTableName, self.engine.connect())
        # added option to get all attributes, if all=True
        if all:            
            return attrTable
        else:
            return attrTable[attrTable['loop'] == currentLoop]

    def updateAttr(self, username:str, attrName:str, update:dict):
        '''
        Function to update current loop current attribute table entry values
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        logger.debug(f'updateAttr: {username},{attrName},{update}')
        #get attributes based on current loop
        #find current loop
        _, currentLoop = self.getLoops(username)
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]

        # check if all columns are present, update based on columns definition if not
        required_columns = self.columns[attrName].keys()
        logger.debug(f'Required columns: {required_columns}')
        missing_columns = [col for col in required_columns if col not in attribute.columns]        
        logger.debug(f'Attribute columns: {attribute.columns}')
        logger.debug(f'Missing columns: {missing_columns}')
        if missing_columns:
            logger.info(f"Table '{username}_{attrName}' is missing columns: {missing_columns}.")
            # Run the routine to create the table with all necessary columns
            for column in missing_columns:
                # alter query
                query = f'alter table {username}_{attrName} add column {column} {self.columns[attrName][column]()}'
                logger.info(query)
                self.engine.execute(query)

        # update current attribute field, create new row if it does not exists
        # get current rows
        query_TR = attribute.select()\
                .where((attribute.c.loop == currentLoop) & (attribute.c.name == 'Temp'))
        TempR = len(self.engine.execute(query_TR).fetchall())
        query = attribute.update()\
                .where((attribute.c.loop == currentLoop) & (attribute.c.name == 'Temp'))\
                .values(**update)
        # if one Temp row exists
        if TempR == 1:
            # try two times to update the database
            for i in range(2):
                try:
                    if self.engine.execute(query):
                        break
                except Exception as e:
                    logger.debug(f'Problem updating DB:\n {e}')
                    continue
        # else create Temp row
        elif TempR < 1:
            self.createAttr(username, attrName)
            self.engine.execute(query)
        

    def saveAttr(self, username:str, attrName:str, name:str):
        '''Writes attribute name and creates new attribute row'''
        logger.info(f'{currentframe().f_code.co_name}')
        # Write attribute name to a current attribute
        _, currentLoop = self.getLoops(username)
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]
        # If attribute with such name exists update it's values from Temp
        if len(list(self.engine.execute(attribute.select()
                                        .where((attribute.c.loop == currentLoop) & 
                                               (attribute.c.name == name))))) > 0:
            #update existing attribute with values from Temp
            self.copyAttr(username, attrName, 'Temp', name)

        # otherwise rename Temp and create new attribute row
        else:
            # rename current attribute
            self.updateAttr(username, attrName, {'name': name})
            # Create new current attribute
            self.createAttr(username, attrName)
            # Update the new attribute to have the same settings as the previous one
            self.copyAttr(username, attrName, name, 'Temp')
            
    def copyAttr(self, username:str, attrName:str, copyFrom:str, copyTo:str):
        '''Copies all attribute fields except is_current and name'''
        logger.info(f'{currentframe().f_code.co_name}')
        _, currentLoop = self.getLoops(username)
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]
        # Update the new attribute to have the same settings as the previous one
        modelCopy = self.getAttrValues(username, attrName, copyFrom)
        # do not copy is_current and name
        # modelCopy.pop('is_current')
        modelCopy.pop('name')
        query_update = attribute.update()\
            .where((attribute.c.name == copyTo) & (attribute.c.loop == currentLoop))\
            .values(**modelCopy)
        self.engine.execute(query_update)  

    def createAttr(self, username:str, attrName:str):
        '''Creates new attribute table row and sets it as current'''
        logger.info(f'{currentframe().f_code.co_name}')
        #get attributes based on current loop
        meta_data = MetaData(bind=self.engine)        
        MetaData.reflect(meta_data)
        #find current loop
        _, currentLoop = self.getLoops(username)

        #whole table will be read to pandas
        #and filtered to have a better formatting
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]

        # make current attribute row not current
        # try:
        #     query = attribute.update()\
        #         .where((attribute.c.is_current == True) & (attribute.c.loop == currentLoop))\
        #         .values(is_current = False)
        #     self.engine.execute(query)
        # except:
        #     pass


        # create new attribute row and make it current
        query = attribute.insert()\
            .values(loop = currentLoop, name = "Temp")
        logger.debug(f'Creating new attribute table row: {query}')
        self.engine.execute(query)

    def getAttrValues(self, username:str, attrName:str, name:str) -> dict:
        '''
        Read the whole attribute table row based on name
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        #find current loop
        _, currentLoop = self.getLoops(username)
        # get relevant table
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]
        query_select = attribute.select().\
            where((attribute.c.name == name) & (attribute.c.loop == currentLoop))
        attrValues = self.engine.execute(query_select)
        
        
        # get column names
        keys = list(attrValues.keys())
        # initialize values
        values = []

        # get number of rows, if I use attrValues it will get empty
        TempR = len(self.engine.execute(query_select).fetchall())
        # if Temp row is found try to get attribute value
        if TempR == 1:
            # try two times to get value from the database
            for i in range(2):
                try:                   
                    values = list(attrValues.first())
                    break
                except Exception as e:
                    logger.debug(f'Problem reading DB:\n {e}')
                    continue
        # if Temp row is not found, create new row
        elif TempR < 1:
            self.createAttr(username, attrName)
            attrValues = self.engine.execute(query_select)
            values = list(attrValues.first())
        # try:
        #     values = list(attrValues.first())
        # # if entry does not exists, create Temp entry
        # except Exception:
        #     traceback.print_exc()
        #     self.createAttr(username, attrName, conn)
        #     attrValues = conn.execute(query_select)
        #     values = list(attrValues.first())
        # return dictionary of all attribute fields
        return dict(zip(keys, values))

    def deleteAttr(self, username:str, attrName:str, name:str, currentLoop:str=""):
        '''
        Deleted attribute table row based on name
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        # if currentloop is provided as in reviewApp, use it, 
        # else get current loop from DB
        if currentLoop=="":
            #find current loop
            _, currentLoop = self.getLoops(username)
        # get relevant table
        attribute = self.getAttrTables(username, attrName)
        # attribute = self.meta.tables[username+"_"+attrName]
        query_delete = attribute.delete().\
            where((attribute.c.name == name) & (attribute.c.loop == currentLoop))
        logger.debug(f'Deleting attribute table rows: {query_delete}') 
        rslt = self.engine.execute(query_delete)      
        logger.debug(f'Deleting attribute table rows: {rslt.rowcount}') 
    
    def getRanges(self, username:str, data_name:str, sel_query:list=[], where_query:dict={}) -> dict:
        '''
        Get min and max values for specified columns in the current loop's data.
        Args:
            username (str): Current user
            data_name (str): Name of the data table
            sel_query (list): List of column names to get ranges for. If empty, gets ranges for all columns
            where_query (dict): Filtering conditions to apply
        Returns:
            dict: Dictionary with column names as keys and (min, max) tuples as values
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)

        # get current loop
        _, currentLoop = self.getLoops(username)
        dataTableName = f'{username}_{currentLoop}_{data_name}'

        # do not update when datatable is missing
        if dataTableName in meta_data.tables:
            dataT = meta_data.tables[dataTableName]
        else:
            raise PreventUpdate
        
        # if table exists get current data table
        try:
            # Determine which columns to get ranges for
            columns_to_check = sel_query if sel_query else [col.name for col in dataT.columns]
            # Filter out any columns that don't exist in the table
            columns_to_check = [col for col in columns_to_check if col in dataT.columns.keys()]
            
            # Create min/max selections for each column
            range_selections = []
            for col in columns_to_check:
                range_selections.extend([
                    func.min(dataT.c[col]).label(f'{col}_min'),
                    func.max(dataT.c[col]).label(f'{col}_max')
                ])
            
            # Build the query
            query = select(range_selections)

            # Apply where conditions if specified
            if where_query:
                where_binaries = []
                for key in where_query:
                    for element in where_query[key]:
                        if element == 0:
                            where_binaries.append(dataT.c[key] >= where_query[key][element])
                        else:
                            where_binaries.append(dataT.c[key] <= where_query[key][element])
                query = query.where(and_(*where_binaries))

            # Execute query and get results
            result = self.engine.execute(query).first()
            
            # Convert result into dictionary
            ranges = {}
            for col in columns_to_check:
                min_val = result[f'{col}_min']
                max_val = result[f'{col}_max']
                ranges[col] = (min_val, max_val)
                
            return ranges
            
        except Exception as e:
            logger.error(f"Error getting column ranges: {str(e)}")
            return None
        
    def alterData(self, username:str, data_name:str, column:str="", expand:bool=True):
        '''
        !!!!IT IS NOT POSSIBLE TO ALTER SQLITE TABLE!!!!
        This method changes data table by adding or removing columns. It takes the following arguments:
        -username - currently connected user
        -data_name - data table to alter
        -sel_query - columns to alter
        -expand - method of alteration, True - add columns, False - delete columns
        '''
        logger.info(f'{currentframe().f_code.co_name}')
        meta_data = MetaData(bind=self.engine)
        MetaData.reflect(meta_data)

        #get current loop
        _, currentLoop = self.getLoops(username)
        dataTableName = f'{username}_{currentLoop}_{data_name}'

        dataT = meta_data.tables[dataTableName]
        
        if expand:
            # if column does not exist
            if column not in dataT.columns.keys():
                query = f'ALTER TABLE {dataTableName} ADD COLUMN {column} FLOAT'
        else:
            # check that column exists
            if column in dataT.columns.keys():
                query = f'ALTER TABLE {dataTableName} DROP COLUMN {column}'
        # execute query
        self.engine.execute(query)
    
    def getUserList(self)->list:
        '''Returns list of all users'''
        logger.info(f'{currentframe().f_code.co_name}')
        # get users table
        table = self.meta.tables['users']
        # read user list
        query_select = table.select().\
            where(table.c.username != 'admin').\
            with_only_columns([table.c.username])
        users = self.engine.execute(query_select)
        # return the result as list
        return [row[table.c.username] for row in users]


def deleteDBTable(path:Path, tableName:str):
   engine = create_engine('sqlite:///'+str(path))
   base = declarative_base()
   metadata = MetaData(engine)
   metadata.reflect(bind=engine)
   table = metadata.tables[tableName]
   if table is not None:
      base.metadata.drop_all(engine, [table], checkfirst=True)

def saveDBTables(username:str, dst_path:str):
    '''This function copies user tables and removes user prefix
    this allows for data sharing between users.
    Before storing data is converted to strings, column datatypes are also
    recorded for reverse operation.'''
    # connect to master database
    DBpath = str(absolute_path)+'/shared/users.sqlite'
    engine_src = create_engine('sqlite:///' + DBpath)

    # loop through all table and find tables with user prefix
    metadata_src = MetaData(engine_src)
    metadata_src.reflect(bind=engine_src)
    user = username+'_'
    tables = {}
    tables['colTypes'] = {}
    
    for table in metadata_src.tables.keys():
        if table.startswith(user):
            # read table to pandas dataframe
            df = pd.read_sql_table(table, engine_src.connect())

            # remove index column
            # df = df.set_index(df.columns[0])
            
            #strip the begining, so other users can reuse this data
            table_userless = table[len(user):]
            # collect datatypes for reimport
            tables['colTypes'][table_userless] = {col: str(df[col].dtype) for col in df.columns}
            # convert dataframe to dictionary, string datatype is used due to JSON limitations
            tables[table_userless] = df.astype(str).to_dict(orient='index') 

    # write the dictionary to a JSON file
    with open(dst_path, 'w') as f:
        json.dump(tables, f, indent=2)    

def loadDBTables(username:str, src_path:str):
    '''This function loads tables and adds user prefix.
    Note: this will overwrite existing tables.
    Before writing data datatypes are restored.'''
    # connect to master database
    DBpath = str(absolute_path)+'/shared/users.sqlite'
    engine_dst = create_engine('sqlite:///' + DBpath)
    
    # remove all user tables in destination
    metadata_dst = MetaData(engine_dst)
    metadata_dst.reflect(bind=engine_dst)
    base = declarative_base()
    user = username+'_'
    # for table in metadata_dst.tables.keys():              
    #     if table.startswith(user):
    #         base.metadata.drop_all(engine_dst, [metadata_dst.tables[table]], checkfirst=True)
    # read JSON data 
    with open(src_path, 'r') as f:
        tables = json.load(f)
    # print(tables)
    # get datatypes
    colTypes = tables.pop('colTypes')
    # store JSON tables to SQLite adding user prefix
    for table_name in tables.keys():
        #get the data
        df = pd.DataFrame.from_dict(tables[table_name], orient='index')
        # if table contains rows import them
        if df.shape[0]>0:
            #set data type
            df = df.astype(colTypes[table_name]) 
            #set index              
            df = df.set_index(df.columns[0])            
            df.to_sql(user + table_name, 
                        con = engine_dst.connect(), 
                        if_exists='replace')




if __name__ == '__main__':
    '''redis_manager = Redis_manager()
    user_data = UserData()
    data_frame = pd.DataFrame()
    loop = LoopData('test')
    loop.time_series.append(data_frame)
    pid = SODTF(**{'t1': 1, 't2': 1, 't3': 1, 'td': 1, 'K': 1})
    loop.models.append(pid)
    user_data.loops.append(loop)
    redis_manager.write(user_data)'''

