from .database_manager import SQLite_manager

size = 20

free = []
in_use = []
for _ in range(size):
    free.append(SQLite_manager())


def acquire() -> SQLite_manager:
    if len(free) <= 0:
        raise Exception('No more objects available')
    r = free [0]
    free.remove(r)
    in_use.append(r)
    return r


def release(r: SQLite_manager):
    in_use.remove(r)
    free.append(r)
