import plotly.graph_objects as go
from apps.utils import components as utl
from apps.utils import tune as utlTune #added because originally utl was used to reference tune lib
import dash_bootstrap_components as dbc
from dash import dcc, html, callback_context, no_update
from dash_extensions.enrich import Input, Output, State, ALL
from dash.exceptions import PreventUpdate
import numpy as np
from apps.app import app
from apps.utils.database.database_context_manager import database_context_manager
from apps.utils.database import database_pool
from apps.utils.model import OvPID, modelFactory
from flask_login import current_user
from inspect import currentframe #this is used to read callback name, for debugging
from apps.app import logger #logger object for debuging
logger.info(f'{__name__}')

# columns that will be used in show window
tune_show_columns = {
    'Simple': ['name', 'loop', 'model', 'controller_type', 'P', 'I', 'D', 'F', 'T1', 'scaling', 'disturbance', 'layout'],
    'Cascade':['name', 'loop', 'controller_type', 'PID1', 'PID2', 'layout'],
}   
# methods for layout container elements
def figure(id, width, graph_rows=1, group=0):
    height=f'{int(69/graph_rows)}vh' #this calculated graph height to always take the same space in the layout
    if group > 0:
        return dbc.Col([
            dcc.Graph(id={'group':group, 'var':id}, figure=go.Figure(layout=go.Layout(#height=600,
                margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                )), style={'height':height})], width=width)
    else:
        return dbc.Col([
            dcc.Graph(id=id, figure=go.Figure(layout=go.Layout(#height=600,
                margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                )), style={'height':height})], width=width)

def PID_parameters(group = 0):
    '''This method populates PID parameter inputs'''
    # use complex id when grouping is used
    if group > 0:
        return {
            'K': utl.Input({'group':group, 'var':'K-C-in'}, 1, '', className="mb-0", value=1),
            'Ti, s': utl.Input({'group':group, 'var':'Ti-C-in'}, 10, '', className="mb-0", value=10),
            'Kd': utl.Input({'group':group, 'var':'Td-C-in'}, 0, '', className="mb-0", value=0),
            'Tf, s': utl.Input({'group':group, 'var':'Tf-C-in'}, 5, '', className="mb-0", value=5),
            'Filter T1,s': utl.Input({'group':group, 'var':'filter-t1'}, 0, '', className="mb-0", value=0),
            'Scaling gain': utl.Input({'group':group, 'var':'scaling-gain'}, 1, '', className="mb-0", value=1),
            'Output gain': utl.Input({'group':group, 'var':'outputGain'}, 1, '', className="mb-0", value=1)}
    else:
        return [
            utl.Input('K-C-in', 1, 'K', value=1),
            utl.Input('Ti-C-in', 10, 'Ti, s', value=10),
            utl.Input('Td-C-in', 0, 'Kd', value=0),
            utl.Input('Tf-C-in', 5, 'Tf, s', value=5),
            utl.Input('filter-t1', 0, 'Filter T1,s', value=1),
            utl.Input('scaling-gain', 1, 'Scaling gain', value=1)]
def models(group = 0):
    '''This method return model selector object'''
    if group > 0:
        return utl.Selector({'group':group, 'var':'current-model'}, 'Model', persistence=False)
    else:
        return utl.Selector('current-model', 'Model', persistence=False)
    
def tuning(group = 0):
    '''This method returns all layout elements that are used for PID parameter calculation'''
    if group > 0:
        return [
            utl.Selector({'group':group, 'var':'tuning-method'}, 'Tuning method', options=
                        [{'label': col_name, 'value': col_name}
                        for col_name in utlTune.tuningMethods.keys()], value='Lambda'),      
            dbc.Col(id={'group':group, 'var':'tuning-parameters'})]
    else:
        return dbc.Row([
            utl.Selector('tuning-method', 'Tuning method', options=
                        [{'label': col_name, 'value': col_name}
                        for col_name in utlTune.tuningMethods.keys()], value='Lambda'),      
            dbc.Col(id='tuning-parameters'),
            #div added to remove margin on the left
            html.Div(dbc.Checkbox(id='tuning-smith', label='Smith predictor'), className='ml-0')]) 

def noise(group = 0):
    if group > 0:
        return utl.Input({'group':group, 'var':'sim-noise'}, '', 'Noise', step=0.0001, className="mb-0", value=0)  
    else:
        return utl.Input('sim-noise', '', 'Noise', step=0.0001, value=0)

def disturbance(group = 0):
    '''Get disturbance configuration interface'''
    if group > 0:
        return {
            'Size':utl.Input({'group':group, 'type': 'disturbance', 'var': 'size'}, 0, '', 
                             step=0.01, className="mb-0", value=0),
            'Filter, s':utl.Input({'group':group, 'type': 'disturbance', 'var': 'tau'}, 0, '', 
                               step=1, className="mb-0", value=0),
            'Start, s':utl.Input({'group':group, 'type': 'disturbance', 'var': 'start'}, 0, '', 
                              step=1, className="mb-0", value=0),
            'End, s':utl.Input({'group':group, 'type': 'disturbance', 'var': 'end'}, 0, '', 
                            step=1, className="mb-0", value=0)}
    else:
        return \
    dbc.Card(style={'margin-bottom': '0px'},
        children=[
            dbc.CardHeader('Disturbance: ', style={'padding':'0px 5px'}),
            dbc.CardBody(
            children = [
                dbc.Row(
                [
                    dbc.Col(utl.Input({'type': 'disturbance', 'var': 'size'}, 0, 'Size', step=0.01, value=0)),
                    dbc.Col(utl.Input({'type': 'disturbance', 'var': 'tau'}, 0, 'Filter, s', step=1, value=0))
                    ]),
                dbc.Row(
                [
                    dbc.Col(utl.Input({'type': 'disturbance', 'var': 'start'}, 0, 'Start, s', step=1, value=0)),
                    dbc.Col(utl.Input({'type': 'disturbance', 'var': 'end'}, 0, 'End, s', step=1, value=0))
                    ])],                            
                style={'padding':'0px 5px'})])

def simulation(group=0):
    '''Get interface for simulation duration and sample count'''
    if group > 0:
        return dbc.Card(style={'margin-bottom': '0px'},
                        children=[
                            dbc.CardHeader("Simulation:", style={'padding':'0px 5px'}),
                            dbc.CardBody(
                            children = [dbc.Col(
                                [dbc.Row(utl.Input({'group':group, 'var':'sim-stop'}, '', 'Duration, s', step=1, value=100)),
                                dbc.Row(utl.Input({'group':group, 'var':'sim-number'}, '', 'Samples', step=1, value = 1000))])],
                                style={'padding':'0px 5px'})])
    else:
        return dbc.Card(style={'margin-bottom': '0px'},
                        children=[
                            dbc.CardHeader("Simulation:", style={'padding':'0px 5px'}),
                            dbc.CardBody(
                            children = [dbc.Col(
                                [dbc.Row(utl.Input('sim-stop', '', 'Duration, s', step=1, value=100)),
                                dbc.Row(utl.Input('sim-number', '', 'Samples', step=1, value=1000))])],
                                style={'padding':'0px 5px'})])

def get_DDMenu(id, items):
    '''This methods populates DropDownMenu with elements in items.'''
    return html.Div(dbc.DropdownMenu(
                            id = id,
                            label = 'Parameters: ',
                            color = 'white',
                            children = [dbc.Row([
                                            dbc.Col(dbc.Label(name), width=4),
                                            dbc.Col(input, width=8)
                                            ]) for name, input in items.items()],
                            toggle_style={'width': '100%', 
                                          'text-align': 'left',
                                          'font-size': '15px',
                                          'font-weight': 'bold',
                                          "white-space": "normal", 
                                          "word-break": "keep-all"},#allows label to break accross lines
                            toggle_class_name="mt-0 pt-0")) 

def PID_card(title, group):
    return \
    dbc.Card(style={'margin-bottom': '0px'},
        children=[
            dbc.CardHeader(f'{title}: ', style={'padding':'0px 5px'}),
            dbc.CardBody(
                children = [
                    dbc.Row(get_DDMenu({'group':group, 'var':'PID'}, PID_parameters(group))),
                    dbc.Row([
                        dbc.Col(models(group), width=8),
                        dbc.Col(noise(group), width=4)]),
                    dbc.Row(get_DDMenu({'group':group, 'var':'disturbance'}, disturbance(group))),
                    dbc.Row([
                        dbc.Col(tuning(group)[0], width=7),
                        dbc.Col(tuning(group)[1], width=5)])
                    ], style={'padding':0})
        ])

def graph(df, title, labels=['CV']):
    data = []
    colors = {'PID':"#1f77b4", 'P':"#AA0000", 'I':"#00AA00", 'D':"#0000AA", 'CV':"#1f77b4", 'SP':"#AA0000", 
              'Prediction':"#AA0000", 'Prediction + td':"#00AA00", 'Prediction error':"#0000AA"}
    for i, trace in enumerate(df[1:]):
        if isinstance(trace, np.ndarray):
            data.append(go.Scatter(x=df [0], y=trace, name=labels[i], yaxis='y', marker_color=colors[labels[i]]))
    layout = go.Layout(xaxis=dict(domain=[0,1]),#0.1, 0.95]),
                       yaxis=dict(titlefont=dict(color=colors['PID']), tickfont=dict(color=colors['PID'])),
                    #    height=600,
                       legend=dict(
                           x=0.1, y=1, 
                           traceorder='normal', orientation='h'),
                       uirevision=True,
                       title=title,
                       title_font=dict(size=14),
                       margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                       )

    fig = go.Figure(data=data, layout=layout)
    return fig

# general buttons
def gen_buttons(savePop, loadPop, deletePop, showPop, clearPop, width=12, group=0): 
    # recalculate witdth, when it is for the whole window, then each element takes 1
    w = 12/width
    # differentiate Tune button for different layouts
    if group > 0:
        tuneBtn = utl.Button({'group':group,'var':'tune-b'}, 'Tune', color='info')
    else:
        tuneBtn = utl.Button('tune-b', 'Tune', color='info')
    return[
        dbc.Col(dbc.ButtonGroup([tuneBtn],
            vertical=True, style={'width': '100%'}), width=w),
        dbc.Col([
            dbc.DropdownMenu(
                label = "Tuning menu",
                children = [
                    dbc.DropdownMenuItem('Save', savePop.btn_id),
                    dbc.DropdownMenuItem('Load', loadPop.btn_id),
                    dbc.DropdownMenuItem('Delete', deletePop.btn_id)],
                    color='primary', group=True, 
                    style={'width': '100%'},
                    toggle_style=utl.height,
                    toggle_class_name="pt-0 pb-0"),
            dbc.DropdownMenu(
                label = "Tuning matrix",
                children=[
                    dbc.DropdownMenuItem('Show', showPop.btn_id),
                    dbc.DropdownMenuItem('Clear', clearPop.btn_id)],
                    color='secondary', group=True, 
                    style={'width': '100%'},
                    toggle_style=utl.height,
                    toggle_class_name="pt-0 pb-0")
            ], width=w)
            ]

class Simple():
    '''
    This creates simple layout elements in rows:
        1 - graphs pid 1
        2 - pid parameters
        3 - model selection, tuning config, noise and disturbance
    '''
    def __init__(self, savePop, loadPop, deletePop, showPop, clearPop):
        self.loadPop = loadPop #this is used in post_load callback
        self.layout = [
            html.Hr(style={'margin-top':0, 'width': '100%'}),
            utl.LoadingSpinner(id='simple-loading-apply_scaling'),
            utl.LoadingSpinner(id='simple-loading-update_model'),
            utl.LoadingSpinner(id='simple-loading-tune'),
            utl.LoadingSpinner(id='simple-loading-show_tuning_parameters'),
            utl.LoadingSpinner(id='simple-loading-post_load_model'),
            # firs graph row
            dbc.Row([figure(id='graph-tuning-co', width=6), figure(id='graph-tuning-cv', width=6)]),
            # Second graph row            
            dbc.Row([]),
            # pid parameter row
            dbc.Row([dbc.Col(par) for par in PID_parameters()]),
            # tuning and artifacts row
            dbc.Row([         
                # tuning elements
                dbc.Col(tuning()), 
                dbc.Col(  
                    # model selector
                    [models(),
                     # noise
                     noise()]),
                
                # disturbance
                dbc.Col(disturbance(), width = 3),
                # tune, menu and matrix buttons
                *gen_buttons(savePop, loadPop, deletePop, showPop, clearPop),
                # simulation time and sample
                dbc.Col(simulation())
                ])]   
                        
    def register_callbacks(self):
        '''This method is called after applicaiton layout is created and class instance
        should start "listening" to callbacks'''
        
        @app.callback(
            Output('K-C-in', 'value'),
            Output('Ti-C-in', 'value'),
            Output('Td-C-in', 'value'),
            Output('simple-loading-apply_scaling', 'children'),
            Input('scaling-gain', 'value'),
            prevent_initial_call=True
        )
        def apply_scaling(scaling):
            '''Updates controller parameters based on scaling'''
            logger.info(currentframe().f_code.co_name)
            # check callback to prevent initial update
            if 'scaling-gain' not in str(callback_context.triggered[0]) or scaling is None:
                raise PreventUpdate
            # read controller parameters
            with database_context_manager(database_pool) as db:
                controllerData = db.getAttrValues(current_user.username, 'controllers', 'Temp')
            # apply scalling if it was modified        
                if 'scaling'not in controllerData or controllerData['scaling'] == scaling or\
                  controllerData['scaling'] is None:
                    raise PreventUpdate
                else:
                    controller = OvPID(**controllerData)
                    
                    # here the change in scalling is important because if new scalling is 1, 
                    # then no scaling is applied   
                    controllerData['scaling'] = scaling / controllerData['scaling']
                    controller.apply_scaling(OvPID(**controllerData))
                    parameters = controller.getParameters()
                    return round(parameters['P'], 4), round(parameters['I'],  4), round(parameters['D'], 4), no_update
        @app.callback(
            Output('graph-tuning-cv', 'figure'),
            Output('graph-tuning-co', 'figure'),
            Output('simple-loading-update_model', 'children'),
            Input('K-C-in', 'value'),
            Input('Ti-C-in', 'value'),
            Input('Td-C-in', 'value'),
            Input('Tf-C-in', 'value'),
            Input('filter-t1', 'value'),
            Input('sim-stop', 'value'),
            Input('sim-number', 'value'),
            Input('sim-noise', 'value'),
            Input({'type': 'disturbance', 'var': ALL}, 'value'),
            State('current-model', 'value'),    
            State('scaling-gain', 'value'),    
            State('tuning-smith', 'value')
        )
        def update_model(K, Ti, Td, Tf,T1,symstop,symnumber,noise,disturb,
                         currentModelName, scaling_gain, smith):
            '''Updates controller parameters and plots the controller response'''
            logger.info(currentframe().f_code.co_name)
            if None in [currentModelName, K, Ti, Td, Tf,T1]:
                raise PreventUpdate
            # enabling of PID decomposition
            # disturbance configuration
            disturbance = {
                'size': disturb[0],
                'tau': disturb[1],
                'start': disturb[2],
                'end': disturb[3]
                }
            # read selected model and define plant object
            with database_context_manager(database_pool) as db:
                modelData = db.getAttrValues(current_user.username, 'models', currentModelName)
                logger.debug(f'Model: {modelData}')
                # prevent udpate if model does not exist
                if modelData == {}:
                    raise PreventUpdate 
                
                # define controller
                controllerData = {'P': K, 'I': Ti, 'D': Td, 'F': Tf, 'T1':T1, 'model': currentModelName,
                                'scaling': scaling_gain, 'disturbance':disturbance}
            
                # import was updated, OvPID import was missing
                controller = OvPID(**controllerData, id=1)  

                if smith:
                    #Collect three models:
                    current_models = []
                    # full model
                    current_models.append(modelFactory(**modelData, id=1))
                    # model without deadtime
                    mdlData = modelData.copy()
                    mdlData.update({'td':0})
                    current_models.append(modelFactory(**mdlData, id=2))
                    # model with only a deadtime
                    mdlData = modelData.copy()
                    mdlData.update({'K':1, 't1':0, 't2':0})
                    current_models.append(modelFactory(**mdlData, id=3))      
                    # here Res is a dictionary that holds measurements at different points
                    Res, coTime = utlTune.Loop.closedSmithCO(current_models, [controller])\
                        .getStepResponse(symstop,symnumber, [noise], [disturbance])
                    
                    # graph data, responses + time
                    COdata = [[]]*5
                    COdata[1], COdata[0] = np.asarray(Res['p1']), coTime
                    COlabels = ['PID']
                    # iterate through all COs models to get the responses
                    coModels = ['P', 'I', 'D']
                    COlabels += coModels
                    for i, coModel in enumerate(coModels):
                        COdata[i+2] = np.asarray(Res[f'p{coModel}1'])

                    CVdata = [[]]*5
                    CVlabels = ['CV', 'Prediction', 'Prediction + td', 'Prediction error']
                    CVdata[1], CVdata[0] = np.asarray(Res['y1']), coTime
                    CVdata[2] = np.asarray(Res['y2'])
                    CVdata[3] = np.asarray(Res['y3'])
                    CVdata[4] = CVdata[1] - np.asarray(Res['y3'])
                    
                    # create figures                               
                    figCV = graph(CVdata, 'Controlled variable', CVlabels)
                    figCO = graph(COdata, 'Controller output', COlabels)
                else:
                    current_model = modelFactory(**modelData, id=1)
                    # import was updated, OvPID import was missing
                    controller = OvPID(**controllerData, id=1)        
                    # here Res is a dictionary that holds measurements at different points
                    Res, coTime = utlTune.Loop.closedCascadedCO([current_model], [controller])\
                        .getStepResponse(symstop,symnumber, [noise], [disturbance])
                    
                    # graph data, responses + time
                    COdata = [[]]*5
                    COdata[1], COdata[0] = np.asarray(Res['p1']), coTime
                    COlabels = ['PID']
                    # iterate through all COs models to get the responses
                    coModels = ['P', 'I', 'D']
                    COlabels += coModels
                    for i, coModel in enumerate(coModels):
                        COdata[i+2] = np.asarray(Res[f'p{coModel}1'])
                    
                    # create figures           
                    figCV = graph([coTime, np.asarray(Res['y1'])], 'Controlled variable')
                    figCO = graph(COdata, 'Controller output', COlabels)

                # data to save
                data = dict(controller.actualParameters)
                # remove field that is not used in Simple controller
                data.pop('outputGain')
                data.pop('id')
                data['controller_type'] = controller.__class__.__name__
                data['disturbance'] = str(disturbance)                 
                data['layout'] = self.__class__.__name__
                data['smith'] = smith if smith else False

                # get currect attribute
                controllerData = db.getAttrValues(current_user.username, 'controllers', 'Temp')
                # udpate it with the latest settings
                controllerData.update(data)
                db.updateAttr(current_user.username, 'controllers', controllerData)
            return figCV, figCO, no_update
        @app.callback(
            Output('K-C-in', 'value'),
            Output('Ti-C-in', 'value'),
            Output('Td-C-in', 'value'),
            Output('Tf-C-in', 'value'),
            Output('simple-loading-tune', 'children'),
            Input('tune-b', 'n_clicks'),
            State('sim-stop', 'value'),
            State('sim-number', 'value'),
            State({'type': 'tuning-variable', 'index': ALL}, 'value'),
            State('current-model', 'value'),
            State('tuning-method', 'value'),
            State('Tf-C-in', 'value'), #added to retain derivative filter value
            State('tuning-smith', 'value'),
            prevent_initial_call=True
        )
        def tune(clicks,symstop,symnumber, tuningValues, currentModelName, 
                 tuningMethodName, Tf, smith):
            '''Tunes and updates controller parameters'''
            logger.info(currentframe().f_code.co_name)
            if None in [tuningValues, clicks]:
                raise PreventUpdate
            with database_context_manager(database_pool) as db:
                # get selected model
                currModel = db.getAttrValues(current_user.username, 'models', currentModelName)
                # zero out td for smith tuning
                if smith:
                    currModel['td'] = 0
                current_model = modelFactory(**currModel)
                
                #get current controller, Temp is always the current
                current_controller = OvPID(**db.getAttrValues(current_user.username, 'controllers', 'Temp'))

                # define tuner class and tune the controller based on model
                tuner: utlTune.TuningMethod = utlTune.tuningMethods [tuningMethodName]
                tuningParameters = dict(zip(tuner.getTuningVariables(), tuningValues))

                tunedController = tuner.tune(currentModel=current_model, tuningParameters=tuningParameters,
                                            initialController=current_controller)
                tunedControllerParameters = tunedController.getParameters()

                # Update the current controller for this loop
                # db.updateAttr(current_user.username, 'controllers', tunedControllerParameters)
            if tunedControllerParameters is None:
                raise PreventUpdate
            return round(tunedControllerParameters ['P'], 4), \
                round(tunedControllerParameters ['I'], 4), round(tunedControllerParameters ['D'], 4), Tf if Tf else 1, no_update
        @app.callback(
            Output('tuning-parameters', 'children'),
            Output('simple-loading-show_tuning_parameters', 'children'),
            Input('tuning-method', 'value'),
            # prevent_initial_call=True AK:VD-86
        )
        def show_tuning_parameters(tuningMethodName):
            '''Populates tuning variable column'''
            logger.info(currentframe().f_code.co_name)
            tuner = utlTune.tuningMethods [tuningMethodName]
            tuningVariables = tuner.getTuningVariables()
            return [dbc.Col(utl.Input({'type': 'tuning-variable',
                                    'index': tuningVariable}, tuningVariable, tuningVariable)) for tuningVariable in
                                    tuningVariables], no_update

        # This callback is used during app initialization
        @app.callback(
            Output('K-C-in', 'value'), Output('Ti-C-in', 'value'),
            Output('Td-C-in', 'value'), Output('Tf-C-in', 'value'),
            Output('current-model', 'value'), Output('current-model', 'options'),
            Output('filter-t1', 'value'), Output('scaling-gain', 'value'), 
            Output('tuning-smith', 'value'),
            Output('simple-loading-post_load_model', 'children'),
            Input(self.loadPop.store_id, 'data'),
            State('current-model', 'options'),
            prevent_initial_call=True
        )
        def post_load_controller(store, options):
            ''' Postprocess for controller loading.
            It reads loaded controller parameters and updates layout'''
            logger.info(currentframe().f_code.co_name)
            # do not execute this callback if it was not triggered
            id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if id != self.loadPop.store_id:
                raise PreventUpdate
            with database_context_manager(database_pool) as db:
                # during initialization store is number,
                # during loading it is a dictionary
                if type(store) == dict:
                    controller = store
                # this is the initialization
                else:
                    # read current controller
                    controller = db.getAttrValues(current_user.username, 'controllers', 'Temp')
                # Reads the models for current user and current loop
                models = db.getAttr(current_user.username, 'models')        

                # List all models except the current - Temp    
                options = [{'label': model_name, 'value': model_name} 
                                            for model_name in models['name']
                                            if model_name != 'Temp']        
                current_model = ""
                if len(options) > 0:
                        current_model = options[0]['value']                    
                if controller is not None:
                    try:
                        # only use it if it still exists in the database
                        if controller['model'] is not None and controller['model'] in list(models['name']):
                            current_model=controller['model']
                        else:
                            # update current controller
                            db.updateAttr(current_user.username, 'controllers', {'model': current_model})
                    except:
                        pass
            
            return custom_get(controller, 'P', 1), custom_get(controller, 'I', 10), \
            custom_get(controller, 'D', 0), custom_get(controller, 'F', 5), \
            custom_get(controller, 'model', current_model), options,custom_get(controller, 'T1', 0), \
            custom_get(controller, 'scaling', 1), custom_get(controller, 'smith', False), no_update

def custom_get(controller:dict, key:str, default_value):
    '''This method extends get method by also using value when field value is present but empty'''
    value = controller.get(key, default_value)
    return value if value else default_value    

class Cascade(Simple):
    '''
    This creates cascade layout elements in rows:
        1 - graphs pid 1
        2 - graphs pid 2
        3 - pid parameters
        4 - model selection, tuning config, noise and disturbance
    '''
    def __init__(self, savePop, loadPop, deletePop, showPop, clearPop):
        # run initialization rutine of inherited class
        super().__init__(savePop, loadPop, deletePop, showPop, clearPop)
        self.layout = [
            html.Hr(style={'margin-top':0, 'width': '100%'}),
            *[utl.LoadingSpinner(id={'group':group, 'var':'cascade-loading-apply_scaling'}) for group in range(1, 3)],
            *[utl.LoadingSpinner(id={'group':group, 'var':'cascade-loading-update_model'}) for group in range(1, 3)],
            *[utl.LoadingSpinner(id={'group':group, 'var':'cascade-loading-tune'}) for group in range(1, 3)],
            *[utl.LoadingSpinner(id={'group':group, 'var':'cascade-loading-show_tuning_parameters'}) for group in range(1, 3)],
            *[utl.LoadingSpinner(id={'group':group, 'var':'cascade-loading-post_load_model'}) for group in range(1, 3)],
            # firs graph row - PID1
            dbc.Row([figure(id='graph-tuning-co', width=6, graph_rows=2, group=1), 
                     figure(id='graph-tuning-cv', width=6, graph_rows=2, group=1)]),
            # Second graph row - PID2           
            dbc.Row([figure(id='graph-tuning-co', width=6, graph_rows=2, group=2), 
                     figure(id='graph-tuning-cv', width=6, graph_rows=2, group=2)]),
            # pid parameter row, two columns of PID parameters
            dbc.Row([
                dbc.Col( PID_card('Master (1)', 1), width=4),
                dbc.Col( PID_card('Slave (2)', 2), width=4),
                # tuning and artifacts row
                dbc.Col(dbc.Row([            
                        # tune, menu and matrix buttons
                        *gen_buttons(savePop, loadPop, deletePop, showPop, clearPop, width=4, group=1),
                        # simulation time and sample
                        dbc.Col(simulation(group=1), width=5)
                        ]), width=4)
                ]),
            ]

    def apply_scaling_callback(self, group):        
        @app.callback(
            Output({'group': group, 'var':'K-C-in'}, 'value'),
            Output({'group': group, 'var':'Ti-C-in'}, 'value'),
            Output({'group': group, 'var':'Td-C-in'}, 'value'),
            Output({'group': group, 'var':'cascade-loading-apply_scaling'}, 'children'),
            Input({'group': group, 'var':'scaling-gain'}, 'value'),
            prevent_initial_call=True
        )
        def apply_scaling_c(scaling):
            '''Updates controller parameters based on scaling'''
            logger.info(currentframe().f_code.co_name)
            # check callback to prevent initial update
            if 'scaling-gain' not in str(callback_context.triggered[0]) or scaling is None:
                raise PreventUpdate
            pid = f'PID{group}'
            # read controller parameters
            with database_context_manager(database_pool) as db:
                # read current controller
                controller = db.getAttrValues(current_user.username, 'controllers', 'Temp')
                # get specific controller
                controller = controller[pid] if pid in controller else None
                # convert to dictionary
                controllerData = eval(controller) if controller is not None else {}
                # apply scalling if PID is stored and contains data
                if controllerData is None or 'scaling' not in controllerData or controllerData['scaling'] is None:
                    raise PreventUpdate
                # apply scalling if it was modified        
                if controllerData['scaling'] == scaling:
                    raise PreventUpdate
                else:
                    controller = OvPID(**controllerData)
                    
                    # here the change in scalling is important because if new scalling is 1, 
                    # then no scaling is applied
                    controllerData['scaling'] = scaling / controllerData['scaling']
                    controller.apply_scaling(OvPID(**controllerData))
                    parameters = controller.getParameters()
                    return round(parameters['P'], 4), round(parameters['I'],  4), round(parameters['D'], 4), no_update
        
    def post_load_controller_callback(self, group):
        # This callback is used during app initialization
        @app.callback(
            Output({'group': group, 'var':'K-C-in'}, 'value'),
            Output({'group': group, 'var':'Ti-C-in'}, 'value'),
            Output({'group': group, 'var':'Td-C-in'}, 'value'),
            Output({'group': group, 'var':'Tf-C-in'}, 'value'),
            Output({'group': group, 'var':'current-model'}, 'value'),
            Output({'group': group, 'var':'current-model'}, 'options'),
            Output({'group': group, 'var':'filter-t1'}, 'value'),
            Output({'group': group, 'var':'scaling-gain'}, 'value'),
            Output({'group': group, 'var':'outputGain'}, 'value'),
            Output({'group': group, 'var':'cascade-loading-post_load_model'}, 'children'),
            Input(self.loadPop.store_id, 'data'),
            State({'group': group, 'var':'current-model'}, 'options'),
            prevent_initial_call=True
        )
        def post_load_controller_c(store, options):
            ''' Postprocess for controller loading.
            It reads loaded controller parameters and updates layout'''
            logger.info(currentframe().f_code.co_name)
            # do not execute this callback if it was not triggered
            id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if id != self.loadPop.store_id:
                raise PreventUpdate
            # controller id
            pid = f'PID{group}'
            
            with database_context_manager(database_pool) as db:
                # during initialization store is number,
                # during loading it is a dictionary
                if type(store) == dict:         #load case
                    controller = eval(store[pid])        
                else:                           #initialization case                
                    # read current controller
                    controller = db.getAttrValues(current_user.username, 'controllers', 'Temp')
                    # get specific controller
                    controller = controller[pid] if pid in controller else None
                    # convert to dictionary
                    controller = eval(controller) if controller is not None else {}
                # Reads the models for current user and current loop
                models = db.getAttr(current_user.username, 'models')        

                # List all models except the current - Temp    
                options = [{'label': model_name, 'value': model_name} 
                                            for model_name in models['name']
                                            if model_name != 'Temp']        
                current_model = ""
                if len(options) > 0:
                        current_model = options[0]['value']                    
                if controller is not None:
                    try:
                        # only use it if it still exists in the database
                        if controller['model'] is not None and controller['model'] in list(models['name']):
                            current_model=controller['model']
                        else:
                            controller['model'] = current_model
                            # update current controller
                            db.updateAttr(current_user.username, 'controllers', 
                                            {pid: str(controller)})
                    except:
                        pass
            # returns read or initial values
            return custom_get(controller, 'P', 1),  custom_get(controller, 'I', 10), \
            custom_get(controller, 'D', 0), custom_get(controller, 'F', 5),  \
            custom_get(controller, 'model', current_model), options, custom_get(controller, 'T1', 0),\
            custom_get(controller, 'scaling', 1), custom_get(controller, 'outputGain', 1), no_update
    
    def show_tuning_parameters_callback(self, group):        
        @app.callback(
            Output({'group': group, 'var':'tuning-parameters'}, 'children'),
            Output({'group': group, 'var':'cascade-loading-show_tuning_parameters'}, 'children'),
            Input({'group': group, 'var':'tuning-method'}, 'value'),
        )
        def show_tuning_parameters_c(tuningMethodName):
            '''Populates tuning variable column'''
            logger.info(currentframe().f_code.co_name)
            tuner = utlTune.tuningMethods [tuningMethodName]
            tuningVariables = tuner.getTuningVariables()
            return [dbc.Col(utl.Input({
                'group':group,
                'index': tuningVariable, 
                                       'type': 'tuning-variable'}, tuningVariable, tuningVariable)) 
                                    for tuningVariable in tuningVariables], no_update

    def tune_callback(self, group):
        @app.callback(
            Output({'group': group, 'var':'K-C-in'}, 'value'),
            Output({'group': group, 'var':'Ti-C-in'}, 'value'),
            Output({'group': group, 'var':'Td-C-in'}, 'value'),
            # Output({'group': group, 'var':'Tf-C-in'}, 'value'),
            Output({'group': group, 'var':'cascade-loading-tune'}, 'children'),
            Input({'group':1,'var':'tune-b'}, 'n_clicks'),
            State({'type': 'tuning-variable', 'index': ALL, 'group': group}, 'value'),
            State({'group': group, 'var':'current-model'}, 'value'),
            State({'group': group, 'var':'tuning-method'}, 'value'),
            # State({'group': group, 'var':'Tf-C-in'}, 'value'), #added to retain derivative filter value
            prevent_initial_call=True
        )
        def tune_c(clicks, tuningValues, currentModelName, tuningMethodName):#, Tf):
            '''Tunes and updates controller parameters'''
            logger.info(currentframe().f_code.co_name)
            if None in [tuningValues, clicks]:
                raise PreventUpdate
            with database_context_manager(database_pool) as db:
                # get selected model
                current_model = modelFactory(**db.getAttrValues(current_user.username, 'models', currentModelName))
                
                #get current controller, Temp is always the current
                current_controller = OvPID(**db.getAttrValues(current_user.username, 'controllers', 'Temp'))

                # define tuner class and tune the controller based on model
                tuner: utlTune.TuningMethod = utlTune.tuningMethods [tuningMethodName]
                tuningParameters = dict(zip(tuner.getTuningVariables(), tuningValues))

                tunedController = tuner.tune(currentModel=current_model, tuningParameters=tuningParameters,
                                            initialController=current_controller)
                tunedControllerParameters = tunedController.getParameters()

                # Update the current controller for this loop
                # db.updateAttr(current_user.username, 'controllers', tunedControllerParameters)
            if tunedControllerParameters is None:
                raise PreventUpdate
            return round(tunedControllerParameters ['P'], 4), \
                round(tunedControllerParameters ['I'], 4), round(tunedControllerParameters ['D'], 4), no_update

    def update_PID_label_callback(self, group):
        @app.callback(
            Output({'group':group, 'var':'PID'}, 'label'),
            Input({'group': group, 'var':'K-C-in'}, 'value'),
            Input({'group': group, 'var':'Ti-C-in'}, 'value'),
            Input({'group': group, 'var':'Td-C-in'}, 'value'),
            Input({'group': group, 'var':'Tf-C-in'}, 'value'),
            Input({'group': group, 'var':'filter-t1'}, 'value'),
            Input({'group': group, 'var':'scaling-gain'}, 'value'),
            Input({'group': group, 'var':'outputGain'}, 'value'),
        )
        def update_PID_label_c(K, Ti, Kd, Tf, Filter, Scaling, OutGain):
            '''Detect PID parameter update and update dropdown menu label'''
            logger.info(currentframe().f_code.co_name)
            # do not execute this callback if it was not triggered
            return f'PID: K: {K}, Ti: {Ti}s, Kd: {Kd}, Tf: {Tf}s, \
            Filter: {Filter}s, Scaling: {Scaling}, Output gain: {OutGain}'
                       
    def update_disturbance_label_callback(self, group):
        @app.callback(
            Output({'group':group, 'var':'disturbance'}, 'label'),
            Input({'group':group, 'type': 'disturbance', 'var': ALL}, 'value'),
        )
        def update_disturbance_label_c(vars):
            '''Detect disturbance parameter update and update dropdown menu label'''
            logger.info(currentframe().f_code.co_name)
            return f'\
            Disturbance: Size: {vars[0]}, \
            Filter: {vars[1]}, \
            Start: {vars[2]}{"s" if vars[2] is not None  else ""}, \
            Stop: {vars[3]}{"s" if vars[3] is not None else ""}'
            
    def register_callbacks(self):
        '''This method is called after applicaiton layout is created and class instance
        should start "listening" to callbacks'''
        for group in range(1, 3):
            self.apply_scaling_callback(group)
            self.show_tuning_parameters_callback(group)
            self.post_load_controller_callback(group)
            self.tune_callback(group)
            self.update_PID_label_callback(group)
            self.update_disturbance_label_callback(group)

        @app.callback(
            Output({'group':ALL, 'var':'graph-tuning-cv'}, 'figure'),
            Output({'group':ALL, 'var':'graph-tuning-co'}, 'figure'),
            Output({'group':ALL, 'var':'cascade-loading-update_model'}, 'children'),
            Input({'group':ALL, 'var':'K-C-in'}, 'value'),
            Input({'group':ALL, 'var':'Ti-C-in'}, 'value'),
            Input({'group':ALL, 'var':'Td-C-in'}, 'value'),
            Input({'group':ALL, 'var':'Tf-C-in'}, 'value'),
            Input({'group':ALL, 'var':'filter-t1'}, 'value'),    
            Input({'group':ALL, 'var':'outputGain'}, 'value'),
            Input({'group':1, 'var':'sim-stop'}, 'value'),
            Input({'group':1, 'var':'sim-number'}, 'value'),
            Input({'group':ALL, 'var':'sim-noise'}, 'value'),
            Input({'group':ALL, 'type': 'disturbance', 'var': ALL}, 'value'),
            State({'group':ALL, 'var':'current-model'}, 'value'),    
            State({'group':ALL, 'var':'scaling-gain'}, 'value')
        )
        def update_model_c(K, Ti, Td, Tf,T1, OutGain,symstop,symnumber,noise,disturb,currentModelName, 
                           scaling_gain):
            '''Updates controller parameters and plots the controller response'''
            logger.info(currentframe().f_code.co_name)
            if None in [*currentModelName, *K, *Ti, *Td, *Tf,*T1]:
                raise PreventUpdate
            # collect individual controller data
            PIDs = {}
            current_models = [[]]*2
            controllers = [[]]*2
            disturbances = [[]]*2
            for i in range(2):
                disturbances[i] = {
                    'size': disturb[0+i*4],
                    'tau': disturb[1+i*4],
                    'start': disturb[2+i*4],
                    'end': disturb[3+i*4]
                    }
                PIDs[f'PID{i+1}']={
                    'P': K[i], 
                    'I': Ti[i], 
                    'D': Td[i], 
                    'F': Tf[i], 
                    'T1':T1[i], 
                    'model': currentModelName[i],
                    'scaling': scaling_gain[i], 
                    'outputGain': OutGain[i], 
                    'disturbance': disturbances[i]}
                
                # read selected models and define plant objects
                with database_context_manager(database_pool) as db:
                    modelData = db.getAttrValues(current_user.username, 'models', currentModelName[i])

                    # prevent udpate if model does not exist
                    if modelData == {}:
                        raise PreventUpdate 
                    current_models[i] = modelFactory(**modelData, id=i+1)
                    
                    # import was updated, OvPID import was missing
                    controllers[i] = OvPID(**PIDs[f'PID{i+1}'], id = i+1)        
                
            # here Res is a dictionary that holds measurements at different points        
            Res, coTime = utlTune.Loop.closedCascadedCO(current_models, controllers)\
                .getStepResponse(symstop,symnumber, noise, disturbances)
            
            # iterate through PIDs to collect relevant measurements and create graphs
            figCVs = [[]]*2       
            figCOs = [[]]*2      
            for j in range(2):
                # graph data, responses + time
                COdata = [[]]*5
                CVdata = [[]]*3
                COdata[1], COdata[0] = np.asarray(Res[f'p{j+1}']), coTime
                # for the slave CV this will add SP trace for visual control response evaluation
                CVlabels = ['CV']
                if j == 0:                    
                    CVdata[1], CVdata[0] = np.asarray(Res[f'y{j+1}']), coTime
                else:
                    CVdata[2], CVdata[1], CVdata[0] = np.asarray(Res[f'p{j}']), np.asarray(Res[f'y{j+1}']), coTime
                    CVlabels += ['SP']
                    
                COlabels = ['PID']
                # # iterate through all COs models to get the responses
                coModels = ['P', 'I', 'D']
                COlabels += coModels
                for i, coModel in enumerate(coModels):
                    COdata[i+2] = np.asarray(Res[f'p{coModel}{j+1}'])
                figCOs[j] = graph(COdata, f'Controller {j+1} output', COlabels)
                figCVs[j] = graph(CVdata, f'Controlled variable {j+1}', CVlabels)

            # data to save
            # store PID parameters
            data = {
                'PID1':str(PIDs[f'PID1']),
                'PID2':str(PIDs[f'PID2']),
                'layout': self.__class__.__name__,
                'controller_type': controllers[0].__class__.__name__
            }            
            with database_context_manager(database_pool) as db:
                # get currect attribute
                controllerData = db.getAttrValues(current_user.username, 'controllers', 'Temp')
                # udpate it with the latest settings
                controllerData.update(data)
                db.updateAttr(current_user.username, 'controllers', controllerData)
            return figCVs, figCOs, [no_update,]*2
            

  
# should have option for 3-element control
lay_cascade = None

lay_smith = None # smith predictor control FPPI from: https://www.sciencedirect.com/science/article/pii/S1110016822002216
lay_MIMO = None # MIMO control

tuneLayouts = {
    'Simple' : Simple,
    'Cascade' : Cascade,
    # 'Smith' : lay_smith,
    # 'MIMO' : lay_MIMO,
}