import numpy as np
import pandas as pd
from dateutil import parser
from os import listdir, path
import csv
import openpyxl


def importOvcsv(directory, **kwargs):
    # If filenames are defined include only defined else all files will be included
    filenames = kwargs['filenames'] if 'filenames' in kwargs else listdir(directory)

    # Decode all csv files
    dft = [decodeOvcsv(path.join(directory, file))
           for file in filenames if file.endswith('.csv')]

    # Concatanate all dataframes to one dataframe
    df = pd.concat(dft)

    return df


def decodeOvcsv(df):
    header_row = 0
    # Iterate through rows until the header row is found
    for index, row in df.iterrows():
        if (row == "Date Time").any():
            header_row = index
            break
    
    # get column names, remove Nones
    if header_row > 0:
        columns = {curcol:newcol 
                for curcol, newcol in zip(list(df.columns), list(df.iloc[header_row,:])) 
                if str(newcol)!='nan'}
    else:
        columns = {curcol:curcol for curcol in list(df.columns) if str(curcol)!='nan'}
    # copy only table below header
    df1 = df.iloc[header_row+1:].copy()
    
    # Remove unit and network suffix
    rd = {cn: columns[cn].split('.')[0] for cn in columns if '@' in columns[cn]}    
    # rename columns
    df1.rename(columns=rd, inplace=True)

    # rename first column to 'Date Time'
    df1.rename(columns={list(df1.columns)[0]:'DateTime'}, inplace=True)

    # parse it as datetime
    df1['DateTime'] = pd.to_datetime(df1['DateTime'], errors='coerce')

    # drop rows without DateTime
    df1.dropna(subset=['DateTime'], inplace=True)

    # remove columns with nan
    df1.dropna(axis=1, inplace=True) 

    # Remove poor and bad quality indications and convert to float
    df1.replace({' B$': ''}, regex=True, inplace=True)
    df1.replace({' P$': ''}, regex=True, inplace=True)
    
    # Find all columns with non numeric data
    non_numeric_cols = df1.iloc[:, 1:].apply(pd.to_numeric, errors='coerce').isna().any()
    # Iterate through them two process them and make numeric
    for column in df1.loc[:, non_numeric_cols[non_numeric_cols].index].columns:
        # find non-numeric values
        numeric = pd.to_numeric(df1[column], errors='coerce')
        # get samples for text extraction
        samples = df1[column][numeric.isna()].unique()
        texts = []
        for sample in samples:
            # extract text
            text_el = sample.strip().split(' ')
            text = ' '.join(text_el[:-1]) #last element is value
            value = int(text_el[-1])

            # remove text from column
            df1[column] = df1[column].str.replace(f'{text} ', '')            
            # store texts for later column renam
            texts.append(f'{value}-{text}')            
        # rename the column    
        df1.rename(columns={column: f'{column}:{"/".join(texts)}'}, inplace=True)

    # parse numbers as floats
    df1.iloc[:,1:] = df1.iloc[:,1:].astype(float)

    # set index
    df1.set_index('DateTime', inplace=True)

    return df1
