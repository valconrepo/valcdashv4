from dash import no_update, callback_context
import dash_bootstrap_components as dbc
from dash import dcc, html, Input, Output, State
from apps.app import app
from flask_login import current_user
from apps.utils.database.database_context_manager import database_context_manager
from apps.utils.database import database_pool
from dash_extensions.enrich import ALL
from apps.utils.tuning_layouts import tuneLayouts
from inspect import currentframe #this is used to read callback name, for debugging
from apps.app import logger #logger object for debuging
logger.info(f'{__name__}')

navbar = dbc.NavbarSimple(id='page-navbar', brand="NettaDash", brand_href="/", 
                          color="primary", dark=True,
                          style={"height": "5vh", "padding": "10px 20px"})

error = dbc.Container(
    [
        html.H1("Error", className="display-3"),
        html.P(
            "Something wrong. This page cannot be displayed",
            className="lead",
        ),
    ]
)

# dictionary with navigation bar items
layouts = {
    "/save": "Save",
    "/trend": "Trend",
    "/identification": "Identification",
    "/upload": "Upload",
    "/tune": 
    # "Tune",
    {f'/{layout.lower()}':layout for layout in tuneLayouts},
    "/compare": "Compare",
    "/linearization": "Linearization",
    "/analysis": "Analysis"
}

# dictionary with help article URL
# unavailable articles are not commented
help_url = {
    "/save": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3525443595/Save+app",
    "/trend": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3946053633/Trend+app",
    "/identification": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/2938732589/Identification+tool",
    "/upload": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3513155624/Upload+tool",
    "/tune/simple": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3513548811/SISO+tuning+tool+Simple",
    "/tune/cascade": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/4539973633/MISO+tuning+tool+Cascade",
    "/tune/smith": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3513548811/SISO+tuning+tool",
    "/tune/mimo": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3513548811/SISO+tuning+tool",
    "/compare": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3951919127/Compare+tool",
    "/linearization": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3526131719/Linearization+tool",
    "/analysis": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/4540628993/Analysis+tool",
    "/": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3752001541/Loop+menu",
    "/logout": "https://netta-solutions.atlassian.net/wiki/spaces/OKB/pages/3762880517/Login+system"
}

# this populates navbar items based on login
@app.callback(
    Output('page-navbar', 'children'),
    Input('url', 'pathname'),
)
def navbar_page(pathname):
    logger.info(currentframe().f_code.co_name)

    # add review navigation button if admin is connected
    if current_user.is_authenticated and current_user.username == 'ak':
        layouts['/review'] = "Review"
    # add help
    children = []
    # populate all navigation buttons
    for path, title in layouts.items():
        # if subpages exists create dropdown menu
        if isinstance(title, dict):
            button = dbc.NavItem(dbc.DropdownMenu(
                children=[                    
                        dbc.DropdownMenuItem(dbc.NavLink(title2, href=f"{path}{path2}"), 
                                             id={'type': 'tune-layout', 'index': title2})
                        for path2, title2 in title.items()
                ],                
                menu_variant="dark",
                label="Tune",
                nav=True,
                in_navbar=True
            ))
        else:            
            button = dbc.NavItem(dbc.NavLink(
                title, href=path))
        
        # if navigation selection match button URL, change color
        if path in pathname:    
            button.style =  {"background": "grey"}           
        children.append(button)

    children += [
                # dbc.NavItem(dbc.NavLink("Save", href="/save")),
                # dbc.NavItem(dbc.NavLink("Trend", href="/trend")),
                # dbc.NavItem(dbc.NavLink("Identification", href="/identification")),
                # # dbc.NavItem(dbc.NavLink("Models", href="/model")), 
                # dbc.NavItem(dbc.NavLink("Upload", href="/upload")),
                # dbc.NavItem(dbc.NavLink("Tune", href="/tune")),
                # dbc.NavItem(dbc.NavLink("Compare", href="/compare")),
                # # dbc.NavItem(dbc.NavLink("TuneMatrix", href="/tunematrix")),
                # dbc.NavItem(dbc.NavLink("Linearization", href="/linearization")),
                dbc.Modal(
                    [
                        dbc.ModalHeader(dbc.ModalTitle("Error")),
                        dbc.ModalBody(id='navbar-error-modal-content'),
                    ],
                    id="navbar-error-modal",
                    is_open=False,
                    size="xl",
                ),
                ]
    if current_user.is_authenticated:
        children.insert(0, dbc.Container(id='loop-select-list')) #this prevents navbar flickering
        children.append(dbc.NavItem(dbc.NavLink("Logout", href="/logout")))
        children.append(dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Loop delete")),
                dbc.ModalBody("Choose from list which loop you want to delete"),
                # dbc.Row(id='loop-delete-list'),
                dcc.Dropdown(id='loop-delete-list-values'),
                html.Br(),
                dbc.Button('Delete', id='delete-loops-btn'),
                dbc.ModalFooter(
                    dbc.Button(
                        "Close", id="close-delete", className="ms-auto", n_clicks=0
                    )
                ),
            ],
            id="loop-delete-modal",
            is_open=False,
        ))
        children.append(dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Loop create")),
                dbc.ModalBody("Enter loop name and press create"),
                dbc.Input(id="loop-create-name", type="text", placeholder="Enter loop name here"),
                dbc.Button('Create', id='create-loops-btn'),
                dbc.ModalFooter(
                    dbc.Button(
                        "Close", id="close-create", className="ms-auto", n_clicks=0
                    )
                ),
            ],
            id="loop-create-modal",
            is_open=False,
        ))
    else:
        children.append(dbc.NavItem(dbc.NavLink("Login", href="/")))
    # a button to open help article
    children.append(dbc.NavItem(dbc.Button("Help", 
                                           href=help_url[pathname] 
                                           if pathname in help_url else "", 
                                           title="Navigate to help article" 
                                           if pathname in help_url else "Help article is not available")))
    # add version number to navbar
    children.append(dbc.NavItem(dbc.NavLink("v1.3", disabled=True)))
    return children


def create_loop_list():
    with database_context_manager(database_pool) as db:
        loopData, loopCurrent = db.getLoops(current_user.username)
    loopCurrentId = loopData.index(loopCurrent)
    deleteListOptions = [{'label': name, 'value': 'data-delete-button-' + str(id)} for id, name in enumerate(loopData)]
    selectLoopList = [dbc.DropdownMenuItem(name, {'id': 'loop-data-select-button', 'data': str(id)})
                      for id, name in enumerate(loopData)]
    selectLoopList.append(dbc.DropdownMenuItem('Create new Loop', id='loop-create-button', n_clicks=0))
    selectLoopList.append(dbc.DropdownMenuItem('Delete Loop', id='loop-delete-button', n_clicks=0))
    selectLoopList[loopCurrentId] = dbc.DropdownMenuItem(loopCurrent,
                                                                {'id': 'loop-data-select-button',
                                                                 'data': str(loopCurrentId)},
                                                                style={"background": "#0d6efd"})
    selectList = dbc.DropdownMenu(
        children=selectLoopList,  # remove brackets here        
        label= f'Loop: {loopCurrent}', 
        color="primary",
    ),
    return selectList, deleteListOptions#dcc.Dropdown(options = deleteListOptions, id='loop-delete-list-values')


# this populates available loops based on login
@app.callback(
    Output('loop-select-list', 'children'),
    Output('loop-delete-list-values', 'options'),
    Input('page-navbar', 'children'),
)
def create_loop_lists(dummy):
    logger.info(currentframe().f_code.co_name)

    if current_user.is_authenticated:
        return create_loop_list()
    else:
        return no_update

    # @app.callback(
    #     Output('loop-select-list', 'children'),
    #     Output('loop-delete-list', 'children'),
    #     Input('load_popup-load', 'n_clicks'),
    # )
    # def create_loop_lists(dummy):
    #     if current_user.is_authenticated:
    #         return create_loop_list()
    #     else:
    #         return no_update

# this populates modal loop deletion window
# ---PROBLEM--- 'loop-delete-list-values' is only present when modal is active
# when it is closed this id is missing and callback error is reported
@app.callback(
    Output('loop-select-list', 'children'),
    Output('loop-delete-list-values', 'options'),
    Output('navbar-error-modal', "is_open"),
    Output('navbar-error-modal-content', 'children'),
    [Input('loop-delete-list-values', 'value'),
     Input('delete-loops-btn', 'n_clicks')],
    prevent_initial_call=True,
)
def delete_loop(loopName, deleteButton):
    logger.info(currentframe().f_code.co_name)

    if current_user.is_authenticated and 'delete-loops-btn.n_clicks' == callback_context.triggered[0]['prop_id']:
        if loopName is None:
            return no_update, no_update, True, 'Select loopname'
        with database_context_manager(database_pool) as db:
            loopData, currentLoop = db.getLoops(current_user.username)
            loopDict = {id:name for id, name in enumerate(loopData)}
            loopIds = list(loopDict.keys())

            if len(loopIds) > 1: #more than one loop present
                deleteID = int(loopName.split('-')[3])
                if loopDict[deleteID] == currentLoop: #if current loop is selected for deletion
                    # select last loop if current selection is not last loop
                    if loopDict[loopIds[-1]] != currentLoop:
                        db.selectLoop(current_user.username, loopDict[loopIds[-1]])
                    # select loop before last otherwise
                    else:
                        db.selectLoop(current_user.username, loopDict[loopIds[-2]])              
                db.deleteLoop(current_user.username, loopDict[deleteID]) 
                return create_loop_list()[0], create_loop_list()[1], False, no_update
            else:
                return no_update, no_update, True, 'You cant delete last loop'
    return no_update, no_update, no_update, no_update

# this populates modal loop creation window
@app.callback(
    Output('loop-select-list', 'children'),
    Output('loop-delete-list-values', 'options'),
    Output('navbar-error-modal', "is_open"),
    Output('navbar-error-modal-content', 'children'),
    [Input('loop-create-name', 'value'),
     Input('create-loops-btn', 'n_clicks')],
    prevent_initial_call=True,
)
def create_loop(loopName, createButton):
    logger.info(currentframe().f_code.co_name)

    if current_user.is_authenticated and 'create-loops-btn.n_clicks' == callback_context.triggered[0]['prop_id']:
        with database_context_manager(database_pool) as db:
            #get loop list
            loopsData, _ = db.getLoops(current_user.username)
            if len(loopsData) < 30: #if loop count is less than 30
                if loopName in loopsData: #if loop with that name does not exist
                    return no_update, no_update, True, 'Loop with that name already exists, choose another one'
                else:
                    db.createLoop(current_user.username, loopName)
                    return create_loop_list()[0], create_loop_list()[1], False, no_update
            else:
                return no_update, no_update, True, 'Maximum amount of loops(30) reached, delete unnecessary loops'
    return no_update, no_update, no_update, no_update

# Loop was selected on navbar dropdown
@app.callback(
    Output('loop-select-list', 'children'),
    Output('loop-selection-store', 'data'),
    Input({'id': 'loop-data-select-button', 'data': ALL}, 'n_clicks'),
    State('loop-selection-store', 'data'),
    prevent_initial_call=True,
)
def select_loop(loopName, loopStore):
    if current_user.is_authenticated:
        if callback_context.triggered[0]['prop_id'] != '.':
            if callback_context.triggered[0]['value'] is not None:
                selectID = int(callback_context.triggered[0]['prop_id'].split('"')[3])
                with database_context_manager(database_pool) as db:
                    loopData, _ = db.getLoops(current_user.username) #get all loops
                    loopDict = {id:name for id, name in enumerate(loopData)}
                    db.selectLoop(current_user.username, loopDict[selectID])# update loop selection
                return create_loop_list()[0], loopStore+1    #highlighting selected loop in the dropdown list
    return no_update, no_update


# opens/closes loop delete modal window
@app.callback(
    Output("loop-delete-modal", "is_open"),
    [Input("loop-delete-button", "n_clicks"), Input("close-delete", "n_clicks")],
    [State("loop-delete-modal", "is_open")],
    prevent_initial_call=True,
)
def open_loop_delete_modal(n1, n2, is_open):
    logger.info(currentframe().f_code.co_name)

    if callback_context.triggered[0]['value'] > 0:
        if n1 or n2:
            return not is_open
        return is_open

# opens/closes loop create modal window
@app.callback(
    Output("loop-create-modal", "is_open"),
    [Input("loop-create-button", "n_clicks"), Input("close-create", "n_clicks")],
    [State("loop-create-modal", "is_open")],
    prevent_initial_call=True,
)
def open_loop_create_modal(n1, n2, is_open):
    logger.info(currentframe().f_code.co_name)

    if callback_context.triggered[0]['value'] > 0:
        if n1 or n2:
            return not is_open
        return is_open


def findLoopName(loops, loopName):
    for x in range(loops.__len__()):
        if loops[x].loop_name == loopName:
            return True
    return False

