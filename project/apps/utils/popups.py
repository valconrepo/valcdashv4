import dash_bootstrap_components as dbc
# import components as utl
import uuid
from dash_extensions.enrich import Input, Output, State, no_update
from dash.exceptions import PreventUpdate
from dash import callback_context
from dash import dcc, Dash, html
from apps.app import app
from apps.utils import components as utl
from flask_login import current_user
from apps.utils.database.database_context_manager import database_context_manager
from apps.utils.database import database_pool
from apps.utils.tuning_layouts import tune_show_columns
import logging
from inspect import currentframe #this is used to read callback name, for debugging
from apps.app import logger #logger object for debuging
logger.info(f'{__name__}')
# Set the root logger level to a high value
# this inhibits XSteam warning output, when incorrect configuration is made
logging.getLogger().setLevel(logging.CRITICAL)


def generateUUID():
    return uuid.uuid4()


class deleteAttrPopup:
    '''
    This creates attribute delete popup object
    '''
    def __init__(self, app, name, alt=''):
        self.app = app
        self.name = name
        self.action = 'delete'
        self.alt = alt
        self.btn_id = f'{self.action}-{alt}{name}-b'
        self.optn_id = f'{self.action}-{alt}{name}-in'
        self.action_b_id = f'{self.action}-{alt}{name}-action'
        self.cancel_b_id = f'{self.action}-{alt}{name}-cancel'
        self.modal_id = f'{self.action}-{alt}{name}-pop-up'
        self.store_id = f'{self.action}-{alt}{name}-store'
        self.modal = self.create_modal(
            id=self.modal_id,
            header=f'{self.action.capitalize()} {name}',
            body=self.create_selector([self.optn_id, name.capitalize()]),
            footer=self.create_buttons([self.cancel_b_id, "Cancel"], 
                                       [self.action_b_id, self.action.capitalize()]),
            store=True
        )

    def create_modal(self, id, header, body, footer, store=False):
        '''
        This class creates a modal with a header, body, and footer\n
        Store element is optional and is used when temporary state storage is required,\n
        e.g. when a user wants to delete an attribute, attribute list after deletion is only updated
        only after deletion is completed.
        '''
        return dbc.Modal([
            dbc.ModalHeader(header),
            dbc.ModalBody(body),
            dbc.ModalFooter(footer),
            dcc.Store(self.store_id, data=0) if store else None,
        ], id=id)

    def create_selector(self, id):
        '''This method creates a dropdown list element'''
        return utl.Selector(*id)

    def create_buttons(self, cancel_id, action_id):
        '''This method creates action buttons'''
        return [
            utl.Button(*cancel_id),
            utl.Button(*action_id),
        ]

    def layout(self):
        '''This method is called during application layout creation'''
        return self.modal

    def update_options(self):
        '''Returns list and selection for dropdown menus'''
        with database_context_manager(database_pool) as db:
            #get attribute table
            attributes = db.getAttr(current_user.username, f'{self.name}s')
            
        # if multiple layouts are available, filter out not the current one
        if self.alt:
            attributes = attributes[attributes['layout'] == self.alt]
        attributeList = [{'label': col_name, 'value': col_name} 
                            for col_name in attributes.name 
                            if col_name != 'Temp']
        
        selection = ""
        if len(attributeList)>0:
            selection = attributeList[0]['value']
        return attributeList, selection

    def register_callbacks(self):
        '''This method is called after applicaiton layout is created and class instance
        should start "listening" to callbacks'''
        @self.app.callback(
            Output(self.optn_id, 'options'),
            Output(self.optn_id, 'value'),
            Input(self.btn_id, 'n_clicks'),
            Input(self.store_id, 'data'),
            prevent_initial_call=True
        )
        def delete_update(clicks, store):
            '''Callback that executes dropdown list update'''
            id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if id not in [self.btn_id, self.store_id]:
                raise PreventUpdate
            return self.update_options()
    
        @self.app.callback(
            Output(self.modal_id, "is_open"),
            Output(self.store_id, 'data'),
            [Input(self.btn_id, "n_clicks"), 
             Input(self.cancel_b_id, "n_clicks"), 
             Input(self.action_b_id, 'n_clicks')],
            [State(self.modal_id, "is_open"), 
             State(self.optn_id, 'value'), 
             State(self.store_id, 'data')],
            prevent_initial_call=True
        )
        def delete_action(n1, n2, n3, is_open, name, store):
            '''Callback that executes deletion'''
            logger.info(currentframe().f_code.co_name)
            id = callback_context.triggered[0]['prop_id'].split('.')[0]
            if id == self.action_b_id:
                if name == "":
                    raise PreventUpdate
                with database_context_manager(database_pool) as db:
                    # delete attribute
                    db.deleteAttr(current_user.username, f'{self.name}s', name)
                    # read attributes after deletion
                    trends = db.getAttr(current_user.username, f'{self.name}s')
                    # update trend list
                if trends.__len__() < 2:
                    return not is_open, no_update
                return is_open, store + 1
            if n1 or n2:
                return not is_open, no_update
            return is_open, no_update

class saveAttrPopup(deleteAttrPopup):
    '''
    This creates attribute save popup object
    '''
    def __init__(self, app, name, alt=''):
        self.app = app
        self.name = name
        self.action = 'save'
        self.btn_id = f'{self.action}-{alt}{name}-b'
        self.name_id = f'{self.action}-{alt}{name}-name-in'
        self.action_b_id = f'{self.action}-{alt}{name}-action'
        self.cancel_b_id = f'{self.action}-{alt}{name}-cancel'
        self.modal_id = f'{self.action}-{alt}{name}-pop-up'
        self.modal = self.create_modal(
            id=self.modal_id,
            header=f'{self.action.capitalize()} {name}',
            body=self.create_input(self.name_id),
            footer=self.create_buttons([self.cancel_b_id, "Cancel"], 
                                       [self.action_b_id, self.action.capitalize()]),
        )

    def create_input(self, id):
        '''Entry field element for text input'''
        return utl.Input(id, 'Enter here', f'{self.name.capitalize()} name', type='text')
    
    def register_callbacks(self):
        @app.callback(
            [Output(self.modal_id, "is_open")],
            [Input(self.btn_id, "n_clicks"), 
             Input(self.cancel_b_id, "n_clicks"), 
             Input(self.action_b_id, 'n_clicks')],
            [State(self.modal_id, "is_open"), State(self.name_id, 'value'),
            ],
            prevent_initial_call=True
        )
        def save_action(n1, n2, n3, is_open, name):
            '''Saves current trend'''
            logger.info(currentframe().f_code.co_name)
            callback_id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if callback_id == self.action_b_id:
                logger.debug(f"Saving: {name}-{current_user.username}")
                if name == None:
                    raise PreventUpdate
                with database_context_manager(database_pool) as db:
                    db.saveAttr(current_user.username, f'{self.name}s', name)
                logger.debug(f"Saved: {name}")

            if n1 or n2 or n3:
                return not is_open
            return is_open
        
class loadAttrPopup(deleteAttrPopup):
    '''
    This creates attribute load popup object\n
    Store in this object is used to trigger outside callback for actual update related to loading
    '''
    def __init__(self, app, name, alt=''):
        self.app = app
        self.name = name
        self.action = 'load'
        self.alt = alt
        self.btn_id = f'{self.action}-{alt}{name}-b'
        self.optn_id = f'{self.action}-{alt}{name}-in'
        self.action_b_id = f'{self.action}-{alt}{name}-action'
        self.cancel_b_id = f'{self.action}-{alt}{name}-cancel'
        self.modal_id = f'{self.action}-{alt}{name}-pop-up'        
        self.store_id = f'{self.action}-{alt}{name}-store'
        self.modal = self.create_modal(
            id=self.modal_id,
            header=f'{self.action.capitalize()} {name}',
            body=self.create_selector([self.optn_id, name.capitalize()]),
            footer=self.create_buttons([self.cancel_b_id, "Cancel"], 
                                       [self.action_b_id, self.action.capitalize()]),
            store=True
        )

    def register_callbacks(self):
        @self.app.callback(
            Output(self.optn_id, 'options'),
            Output(self.optn_id, 'value'),
            Input(self.btn_id, 'n_clicks'),
            prevent_initial_call=True
        )
        def load_update(clicks):
            '''Callback that executes dropdown list update'''
            id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if id != self.btn_id:
                raise PreventUpdate
            return self.update_options()
    
        @self.app.callback(
            Output(self.modal_id, "is_open"),
            Output(self.store_id, 'data'),
            [Input(self.btn_id, "n_clicks"), 
             Input(self.cancel_b_id, "n_clicks"), 
             Input(self.action_b_id, 'n_clicks')],
            [State(self.modal_id, "is_open"), 
             State(self.optn_id, 'value'), 
             State(self.store_id, 'data')],
            prevent_initial_call=True
            )
        def load_action(n1, n2, n3, is_open, name, store):
            ''' Load previously saved attribute by overwriting the current.
                Layout is updated using different callbacks that are triggered by store data change.'''
            logger.info(currentframe().f_code.co_name)
            id = callback_context.triggered [0] ['prop_id'].split('.') [0]
            if id == self.action_b_id:
                if name == "":
                    raise PreventUpdate
                with database_context_manager(database_pool) as db:
                    # read loaded attribute
                    parameters = db.getAttrValues(current_user.username, f'{self.name}s', name)            
                    # update current trend
                    parameters.pop('name')
                    db.updateAttr(current_user.username, f'{self.name}s', parameters)
                logger.debug("Loaded")
                return not is_open, parameters
            if n1 or n2:
                return not is_open, no_update
            return is_open, no_update
        
class clearAttrPopup:
    '''
    This creates attribute show popup object
    '''
    def __init__(self, app, name, alt=''):
        self.app = app
        self.name = name
        self.action = 'clear'
        self.alt = alt
        self.btn_id = f'{self.action}-{alt}{name}-b'
        self.action_b_id = f'{self.action}-{alt}{name}-action'
        self.cancel_b_id = f'{self.action}-{alt}{name}-cancel'
        self.modal_id = f'{self.action}-{alt}{name}-pop-up'
        self.store_id = f'{self.action}-{alt}{name}-store'
        self.modal = self.create_modal(
            id=self.modal_id,
            header=f'Are you sure you want to {self.action} all saved {alt} {name}s for this loop?',
            body='',
            footer=self.create_buttons([self.cancel_b_id, "Cancel"], 
                                       [self.action_b_id, self.action.capitalize()]),
            store=False
        )

    def create_modal(self, id, header, body, footer, store=False):
        '''
        This class creates a modal with a header, body, and footer\n
        Store element is optional and is used when temporary state storage is required,\n
        e.g. when a user wants to delete an attribute, attribute list after deletion is only updated
        only after deletion is completed.
        '''
        return dbc.Modal([
            dbc.ModalHeader(header),
            # dbc.ModalBody(body),
            dbc.ModalFooter(footer),
            dcc.Store(self.store_id, data=0) if store else None,
        ], id=id)

    def create_buttons(self, cancel_id, action_id):
        '''This method creates action buttons'''
        return [
            utl.Button(*cancel_id),
            utl.Button(*action_id),
        ]

    def layout(self):
        '''This method is called during application layout creation'''
        return self.modal


    def register_callbacks(self):
        '''This method is called after application layout is created and class instance
        should start "listening" to callbacks'''
        @app.callback(
            Output(self.modal_id, "is_open"),
            Input(self.btn_id, 'n_clicks'), 
            Input(self.cancel_b_id, "n_clicks"), 
            Input(self.action_b_id, 'n_clicks'),
            State(self.modal_id, "is_open"),
            prevent_initial_call=True
        )
        def clear(n1, n2, n3, is_open):
            '''Deletes all attributes'''
            logger.info(currentframe().f_code.co_name)
            ctx = callback_context
            callback_id = ctx.triggered [0] ['prop_id'].split('.') [0]
            if callback_id == self.action_b_id:
                # Delete all saved controllers 
                with database_context_manager(database_pool) as db:
                    # Read all controllers for current user and current loop
                    attributes = db.getAttr(current_user.username, f'{self.name}s')

                    # Delete all non Temp attributes
                    for i in range(attributes.shape[0]):
                        attribute = attributes.iloc[i]
                        
                        if attribute['name'] != 'Temp' and\
                              ('layout' not in attribute or not self.alt or attribute['layout'] == self.alt):
                            db.deleteAttr(current_user.username, f'{self.name}s', attribute['name']) 
            if n1 or n2 or n3:
                return not is_open
            return is_open
        
class showAttrPopup:
    '''
    This creates attribute show popup object
    '''
    def __init__(self, app, name, alt=''):
        self.app = app
        self.name = name
        self.action = 'show'
        self.alt = alt
        self.btn_id = f'{self.action}-{alt}{name}-b'
        self.content_id = f'{self.action}-{alt}{name}-content'
        self.modal_id = f'{self.action}-{alt}{name}-pop-up'
        self.store_id = f'{self.action}-{alt}{name}-store'
        self.modal = self.create_modal(
            id=self.modal_id,
            header=f'Show {name}s',
            store=False
        )

    def create_modal(self, id, header, store=False):
        '''
        This class creates a modal with a header, body, and footer\n
        Store element is optional and is used when temporary state storage is required,\n
        e.g. when a user wants to delete an attribute, attribute list after deletion is only updated
        only after deletion is completed.
        '''
        return dbc.Modal([
            dbc.ModalHeader(header),
            dbc.ModalBody(id= self.content_id),
            dcc.Store(self.store_id, data=0) if store else None,
        ], 
        id=id,
        size="xl",
        scrollable=True
        )

    def create_buttons(self, cancel_id, action_id):
        '''This method creates action buttons'''
        return [
            utl.Button(*cancel_id),
            utl.Button(*action_id),
        ]

    def layout(self):
        '''This method is called during application layout creation'''
        return self.modal


    def register_callbacks(self):
        '''This method is called after application layout is created and class instance
        should start "listening" to callbacks'''
        @app.callback(
            Output(self.modal_id, "is_open"),
            Output(self.content_id, 'children'),
            Input(self.btn_id, "n_clicks"),
            prevent_initial_call=True
        )
        def show(n1):
            '''This function opens a modal window to show all available attributes'''
            logger.info(currentframe().f_code.co_name)
            if n1 is None:
                raise PreventUpdate
            # Read all models for current user and current loop
            with database_context_manager(database_pool) as db:        
                attributes = db.getAttr(current_user.username, f'{self.name}s')
                # remove current attribute
                if 'layout' in attributes and self.alt:
                    df = attributes.loc[(attributes.name != "Temp") & (attributes.layout == self.alt)]
                    # filter columns for complex controllers
                    if self.alt in ['Simple', 'Cascade']:
                        df = df[tune_show_columns[self.alt]]
                else:
                    df = attributes.loc[attributes.name != "Temp"]
                
                if len(df)<1:
                    return True, f'No {self.name}s were found'
                # remove irrelevant columns
                df = df.drop(['loop'], axis=1)
                #column name capitalization
                df.columns = df.columns.str.capitalize()
                table = dbc.Table.from_dataframe(df, striped=True, bordered=False, hover=True)
                return True, table

