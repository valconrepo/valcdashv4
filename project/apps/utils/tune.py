from benderopt import minimize
import numpy as np
from .model import Loop, plant, PID
from abc import ABC, abstractmethod
import apps.utils.model as utl #changed import
from typing import Callable, List
import math

models = ['FODTF', 'SODTF', 'SODITF']


class TuningMethod(ABC):

    @staticmethod
    def tuneIntegral(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
        pass

    @staticmethod
    def tuneSelfRegulating(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
        pass

    @staticmethod
    @abstractmethod
    def getTuningVariables():
        pass

    def _selectTuningFormula(self, currentModel: plant) -> Callable[[plant, dict, PID], PID]:
        if currentModel.__class__.__name__ == 'SODTF':
            formula = self.tuneSelfRegulating
        else:
            formula = self.tuneIntegral
        return formula

    def tune(self, currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
        tuningFormula = self._selectTuningFormula(currentModel)
        tunedPID = tuningFormula(currentModel, tuningParameters, initialController)
        tunedPID.apply_scaling(initialController)
        return utl.convertPID(tunedPID, initialController)


class LQRTuning(TuningMethod):

    @staticmethod
    def generate_cost_function(model: plant, controller: PID, Q, R):
        def cost_function(**kwargs):
            # Calculates PV and CO responses
            cvRespI = Loop.closedCV(model, controller)
            coRespI = Loop.closedCO(model, controller)

            PV_response, pvTime = cvRespI.getStepResponse(100,1000)
            CO_response, coTime = coRespI.getStepResponse(100,1000)

            # Integrates square of responses, multiplied by weights
            return  np.trapz((np.array(PV_response)-1)**2, pvTime) * Q + \
                    np.trapz(np.array(CO_response)**2, coTime) * R
            # return np.trapz([(x - 1) ** 2 for x in PV_response], pvTime) * Q + np.trapz(
            #     [x ** 2 for x in CO_response],
            #     coTime) * R

        return cost_function

    def tune(self, currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
        f = self.generate_cost_function(currentModel, initialController, tuningParameters['Q'],
                                        tuningParameters['R'])

        # We define the parameters we want to optimize:
        optimization_problem_parameters = [
            {
                "name": "P",
                "category": "normal",
                "search_space": {
                    "mu": initialController.actualParameters['P'],
                    "sigma": 10,
                },

            },
            {
                "name": "I",
                "category": "normal",
                "search_space": {
                    "mu": initialController.actualParameters['I'],
                    "sigma": 10,
                },

            },
            {
                "name": "D",
                "category": "normal",
                "search_space": {
                    "mu": initialController.actualParameters['D'],
                    "sigma": 10,
                },

            }
        ]

        # We launch the optimization
        best_sample = minimize(f, optimization_problem_parameters, number_of_evaluation=100)

        tunedController = utl.OvPID(P=best_sample['P'], I=best_sample['I'], D=best_sample['D'], F=1)

        return tunedController

    @staticmethod
    def getTuningVariables() -> List[str]:
        return ['R', 'Q']


class LambdaTuning(TuningMethod):

    @staticmethod
    def tuneIntegral(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        lmbd = tuningParameters['lambda']/no_div_zero(currentModel.K, 1e-5) #2*APD/(currentModel.K * MLD)
        Ti = max(4 * currentModel.td, 2 * lmbd + currentModel.td)
        Kc = Ti / no_div_zero(currentModel.K * (lmbd + currentModel.td) ** 2, 1e-5)
        if tau1 > 0.5 * currentModel.td:
            Td = min(0.25 * Ti, max(0.5 * currentModel.td, tau2))
        else:
            Td = 0
        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def tuneSelfRegulating(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
        # both T1 and T2 are used because there is no constraint as to which of the is the primary (bigger)        
        tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        lmdb = tuningParameters['lambda'] * max(tau1, currentModel.td)
        Ti = max(0.25 * currentModel.td, tau1)
        Kc = Ti / no_div_zero(currentModel.K * (lmdb + currentModel.td), 1e-5) #(currentModel.K * (tuningParameters['lambda'] * currentModel.td + currentModel.td))

        if tau1 > 0.5 * currentModel.td:
            Td = min(0.25 * Ti, max(0.5 * currentModel.td, tau2))
        else:
            Td = 0
        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def getTuningVariables():
        return ['lambda']


class IMCTuning(TuningMethod):

    @staticmethod
    def tuneSelfRegulating(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        tau1 = max(currentModel.t1, currentModel.t2)     
        # tau2 = min(currentModel.t1, currentModel.t2)
        gamma = tuningParameters['gama'] * tau1
        Ti = tau1 + 0.5 * currentModel.td
        Kc = Ti / no_div_zero(currentModel.K * (gamma + 0.5 * currentModel.td), 1e-5)
        Td = min(0.25 * Ti, (tau1 * currentModel.td) / no_div_zero(2 * tau1 + currentModel.td, 1e-5))

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def tuneIntegral(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        # tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        gamma = tuningParameters['gama'] / no_div_zero(currentModel.K, 1e-5)
        Ti = 2 * gamma + currentModel.td
        Kc = Ti / no_div_zero(currentModel.K * (gamma + 0.5 * currentModel.td) ** 2, 1e-5)
        Td = min(0.25 * Ti, tau2)

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def getTuningVariables():
        return ['gama']


class SIMCTuning(TuningMethod):
    # def tune(self, currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:
    #     tuningFormula = self._selectTuningFormula(currentModel)
    #     tunedPID = tuningFormula(currentModel, tuningParameters, initialController)

    #     return utl.convertPID(tunedPID, initialController)

    @staticmethod
    def tuneSelfRegulating(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        gamma = tuningParameters['gama'] * tau1
        Kc = (tau1 + currentModel.td / 3) / no_div_zero(currentModel.K * (gamma + currentModel.td), 1e-5)
        Ti = min(tau1 + currentModel.td / 3, 4 * (gamma + currentModel.td))
        Td = min(0.25 * Ti, tau2)

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def tuneIntegral(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        # tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        gamma = tuningParameters['gama'] / no_div_zero(currentModel.K, 1e-5)
        Kc = 1 / no_div_zero(currentModel.K * (gamma + currentModel.td), 1e-5)
        Ti = 4 * (gamma + currentModel.td)
        Td = min(0.25 * Ti, tau2)

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def getTuningVariables():
        return ['gama']


class ShortCutTuning(TuningMethod):

    @staticmethod
    def tuneSelfRegulating(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        Kc = max(0.2 / no_div_zero(currentModel.K, 1e-5),
                 0.8 * tau1 / no_div_zero(currentModel.K * currentModel.td, 1e-5)) * tuningParameters['aggression']
        Tu = 2 * (1 + (tau1 / no_div_zero(tau1 + currentModel.td, 1e-5)) ** 0.65) * currentModel.td
        Ti = 0.6 * Tu /no_div_zero(min(4, 10 * (4 * currentModel.td / no_div_zero(Tu, 1e-5) - 1) ** 2 + 1), 1e-5)
        Td = min(0.25 * Ti, 0.8 * max(0.0, 0.25 * (Ti - 0.5 * currentModel.td) + tau2))

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def tuneIntegral(currentModel: plant, tuningParameters: dict, initialController: PID) -> PID:        
        tau1 = max(currentModel.t1, currentModel.t2)     
        tau2 = min(currentModel.t1, currentModel.t2)
        Tu = 4 * (1 + (tau1 / no_div_zero(currentModel.td, 1e-5)) ** 0.65) * currentModel.td
        Kc = 0.8 * 1 / no_div_zero(currentModel.K * currentModel.td, 1e-5)
        Ti = 0.6 * Tu
        Td = min(0.25 * Ti, 0.8 * max(0.0, 0.25 * Ti - 0.5 * currentModel.td) + tau2)

        return utl.ISAPID(P=Kc, I=Ti, D=Td, F=1)

    @staticmethod
    def getTuningVariables():
        return ['aggression']


tuningMethods = {
    # 'LQR': LQRTuning(), removed during VD-161
    'Lambda': LambdaTuning(),
    'IMC': IMCTuning(),
    'SIMC': SIMCTuning(),
    'Shortcut': ShortCutTuning()
}

def no_div_zero(denum, eps = 1e-5):
    # get the sign
    sign = math.copysign(1, denum)
    return sign * max(abs(denum), eps)