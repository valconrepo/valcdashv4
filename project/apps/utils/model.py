from robustcontrol.utils import tf, margins
from robustcontrol.InternalDelay import InternalDelay
from scipy.interpolate import interp1d
from abc import ABC, abstractmethod
import numpy as np
import pandas as pd
import sys
from scipy.optimize import curve_fit
import scipy.signal as control # for simulation using vectors
from tbcontrol import blocksim # for simulation using block representation

class PID(ABC):
    s = tf([1, 0], 1)
    parameters = ['P', 'I', 'D', 'F', 'T1', 'scaling', 'outputGain']

    def __init__(self, **kwargs):
        for parameter in self.parameters:
            if parameter not in kwargs.keys() or kwargs[parameter] is None:
                kwargs[parameter] = 1
        self.actualParameters = kwargs
        self.id = kwargs.get('id', '')
        self.name = None

    @abstractmethod
    def toISA(self):
        pass

    @abstractmethod
    def fromISA(self, dummy):
        pass

    @abstractmethod
    def getTF(self) -> InternalDelay:
        pass

    
    @abstractmethod
    def getFilterTF(self) -> InternalDelay:
        pass

    @abstractmethod
    def getPTF(self) -> InternalDelay:
        pass

    @abstractmethod
    def getITF(self) -> InternalDelay:
        pass

    @abstractmethod
    def getDTF(self) -> InternalDelay:
        pass

    @abstractmethod
    def apply_scaling(self, initial_controller) -> None:
        pass

    def getParameters(self):
        return self.actualParameters

    def serialize(self):
        serialized_data = self.getParameters()
        serialized_data.update({'model_type': self.__class__.__name__, 'name': self.name})
        return serialized_data

    @staticmethod
    def deserialize(parameters: dict):
        model_type = parameters['model_type']
        parameters.pop('model_type', None)
        controller = model_type(**parameters)
        controller.name = parameters['name']
        return controller


class ISAPID(PID):

    def getTF(self) -> blocksim.LTI:
        # define controller tf
        controller = tf(1, 1)

        # use integral only when integral term in non-zero
        if abs(self.actualParameters['I']) > 1e-4:
            controller += tf(1, [self.actualParameters['I'], 0])
            
        # use derivative only when derivative term in non-zero
        if abs(self.actualParameters['D']) > 1e-4:
            controller += tf([self.actualParameters['D'], 0], [self.actualParameters['F'], 1])

        controller *= tf(self.actualParameters['P'], 1)        
        # apply scalling
        controller *= self.actualParameters['scaling']*self.actualParameters['outputGain']
        # apply filter
        controller *= tf([1], [self.actualParameters['T1'], 1]) 
        # return controller based on parameters calculated by control library
        return blocksim.LTI(f'PID{self.id}', f'e{self.id}', f'p{self.id}', 
                            controller.numerator.coeffs, controller.denominator.coeffs)
    
    def getFilterTF(self) -> blocksim.LTI:
        return blocksim.LTI(f'Gf{self.id}', f'e{self.id}', f'ef{self.id}', [1], [self.actualParameters['T1'], 1])
    
    def getPTF(self) -> blocksim.LTI:
        scale = self.actualParameters['scaling']*self.actualParameters['P']*self.actualParameters['outputGain']
        return blocksim.LTI(f'P{self.id}', f'e{self.id}', f'pP{self.id}', 
                            [scale], [self.actualParameters['T1'], 1])
    
    def getITF(self) -> blocksim.LTI:
        controller = blocksim.LTI(f'I{self.id}', f'e{self.id}', f'pI{self.id}', [0], [1])     
        # use integral only when integral term in non-zero   
        if abs(self.actualParameters['I']) > 1e-4:
            scale = self.actualParameters['scaling']*self.actualParameters['P']*self.actualParameters['outputGain']
            controller = blocksim.LTI(f'I{self.id}', f'e{self.id}', f'pI{self.id}', 
                                      [scale], 
                                      np.convolve([self.actualParameters['I'], 0],[self.actualParameters['T1'], 1])) 
        return controller        
    
    def getDTF(self) -> blocksim.LTI:
        controller = blocksim.LTI(f'D{self.id}', f'e{self.id}', f'pD{self.id}', [0], [1])     
        # use derivative only when derivative term in non-zero  
        if abs(self.actualParameters['D']) > 1e-4:
            scale = self.actualParameters['scaling']*self.actualParameters['P']*self.actualParameters['outputGain']
            controller = blocksim.LTI(f'D{self.id}', f'e{self.id}', f'pD{self.id}', 
                                      [self.actualParameters['D']*scale, 0], 
                                      np.convolve([self.actualParameters['F'], 1], [self.actualParameters['T1'], 1])) 
        return controller

    def toISA(self):
        return self

    def fromISA(self, dummy):
        return self

    def apply_scaling(self, initial_controller) -> None:
        self.actualParameters['scaling'] = initial_controller.actualParameters['scaling']
        self.actualParameters['P'] = self.actualParameters['P'] / self.actualParameters['scaling']


class OvPID(PID):

    def getTF(self) -> blocksim.LTI:
        # define controller tf
        controller = tf(self.actualParameters['P'], 1)
        # use integral only when integral term in non-zero
        if abs(self.actualParameters['I']) > 1e-4:
            controller += tf(1, [self.actualParameters['I'], 0])

        # use derivative only when derivative term in non-zero
        if abs(self.actualParameters['D']) > 1e-4:
            controller += tf([self.actualParameters['D'], 0], [self.actualParameters['F'], 1])                
        # apply scalling
        controller *= self.actualParameters['scaling']*self.actualParameters['outputGain']
        # apply filter
        controller *= tf([1], [self.actualParameters['T1'], 1]) 
        # return controller based on parameters calculated by control library/
        return blocksim.LTI(f'PID{self.id}', f'e{self.id}', f'p{self.id}', 
                            controller.numerator.coeffs, controller.denominator.coeffs)
    
    
    
    def getFilterTF(self) -> blocksim.LTI:
        return blocksim.LTI(f'Gf{self.id}', f'e{self.id}', f'ef{self.id}', [1], [self.actualParameters['T1'], 1])
    
    def getPTF(self) -> blocksim.LTI:
        scale = self.actualParameters['scaling']*self.actualParameters['outputGain']  
        return blocksim.LTI(f'P{self.id}', f'e{self.id}', f'pP{self.id}', 
                            [scale*self.actualParameters['P']], [self.actualParameters['T1'], 1])
    
    def getITF(self) -> blocksim.LTI:
        controller = blocksim.LTI(f'I{self.id}', f'e{self.id}', f'pI{self.id}', [0], [1])     
        # use integral only when integral term in non-zero   
        if abs(self.actualParameters['I']) > 1e-4:
            scale = self.actualParameters['scaling']*self.actualParameters['outputGain']
            controller = blocksim.LTI(f'I{self.id}', f'e{self.id}', f'pI{self.id}', 
                                      [scale], 
                                      np.convolve([self.actualParameters['I'], 0],[self.actualParameters['T1'], 1])) 
        return controller 
    
    def getDTF(self) -> blocksim.LTI:
        controller = blocksim.LTI(f'D{self.id}', f'e{self.id}', f'pD{self.id}', [0], [1])     
        # use derivative only when derivative term in non-zero  
        if abs(self.actualParameters['D']) > 1e-4:
            scale = self.actualParameters['scaling']*self.actualParameters['outputGain']
            controller = blocksim.LTI(f'D{self.id}', f'e{self.id}', f'pD{self.id}', 
                                      [scale*self.actualParameters['D'], 0], 
                                      np.convolve([self.actualParameters['F'], 1],[self.actualParameters['T1'], 1])) 
        return controller

    def toISA(self) -> ISAPID:
        params = {'P': self.actualParameters['P'],
                  'I': self.actualParameters['P'] * self.actualParameters['I'],
                  'D': self.actualParameters['D'] / self.actualParameters['P'],
                  'F': self.actualParameters['F'],
                  'scaling': self.actualParameters['scaling']}
        return ISAPID(**params)

    def fromISA(self, isa: ISAPID):
        self.actualParameters = {'P': isa.actualParameters['P'],
                                 'I': isa.actualParameters['I'] / isa.actualParameters['P'],
                                 'D': isa.actualParameters['D'] * isa.actualParameters['P'],
                                 'F': isa.actualParameters['F'],
                                 'scaling': isa.actualParameters['scaling']}

        return self

    def apply_scaling(self, initial_controller: PID) -> None:
        self.actualParameters['scaling'] = initial_controller.actualParameters['scaling']
        self.actualParameters['P'] = self.actualParameters['P'] / self.actualParameters['scaling']
        self.actualParameters['I'] = self.actualParameters['I'] * self.actualParameters['scaling']
        self.actualParameters['D'] = self.actualParameters['D'] / self.actualParameters['scaling']


class model(ABC):
    s = tf([1, 0], 1)

    def __init__(self, transferFunction):
        self.transferFunction = transferFunction

    def getTF(self):
        return self.transferFunction

    def getInternalDelay(self):
        return InternalDelay(self.transferFunction)

    @staticmethod
    def getDT(timeSeries):
        return (timeSeries[1] - timeSeries[0]).total_seconds()

    def getSimulatedResponse(self, timeSeries, inputData, y0:float=0, ratey0:float=0):
        '''This function will take initial y value for self regulating process
        and additionally initial rate for integrating process.
        For simulated response scipy will be used for simulation and deadtime will
        be implemented by shifting time.'''
        #get time step
        dt = self.getDT(timeSeries)
        u0 = inputData[0]
        T = np.arange(len(inputData))*dt
        #replaces list comprehension with a faster numpy array operation
        U = inputData - u0
        
        # dummy point initialization
        rate = np.zeros(inputData.shape)

        # here we create LTI object for self regulating object
        tf1 = control.lti(self.K, np.convolve([self.t1, 1], [self.t2, 1]))  

        # calculate values to shift for deadtime
        ndel = 0
        if self.td > dt/2:
            # number of samples to shift
            ndel = int(self.td/dt)

        if self.__class__.__name__ == 'FODITF':
            # for integrating process response is calculated in two steps:
            # 1- get response for rate of change
            _, rate_response, _ = control.lsim(tf1, U, T)

            # perform time shift
            if ndel:
                rate_response = pd.Series(rate_response, index = timeSeries).\
                    shift(ndel).fillna(rate_response[0]).to_numpy()
            
            rate = rate_response + ratey0

            
            # 2- integrate rate of change to get the final response
            tf2 = control.lti(1, [1, 0])
            _, response, _ = control.lsim(tf2, rate, T)
        else:
            _, response, _ = control.lsim(tf1, U, T)            
            
            # perform time shift
            if ndel:
                response = pd.Series(response, index = timeSeries).\
                    shift(ndel).fillna(response[0]).to_numpy()

        #replaces below list comprehension
        final_response = response + y0
        # final_response = [x[0] + starting_value for x in resp]

        #rate is only used for data fitting
        return final_response.ravel(), rate.ravel(), T

    def getStepResponse(self, symstop=100, symnumber=5000, noises=[0], disturbances=[{}]):
        # inputs for simulation
        # AK:23.04.26 - added to argument list
        # if symstop == None:
        #     symstop = 100
        # if symnumber == None:
        #     symnumber = 5000

        # step function
        t = np.linspace(0, symstop, symnumber)
        uf = lambda t: 1.0
        n=['']*len(noises)
        # noise functions
        for i, noise in enumerate(noises):
            if noise > 0:
                noise_s = np.random.normal(0, noise, len(t))
                n[i] = interp1d(t, noise_s, fill_value='extrapolate', axis=0)

        # only add disturbance when:
            # it is non zero
            # it starts within timeframe
            # its' end time is later than start
        d=[[]]*len(disturbances)
        def pulse(size, start, end):
            '''helper function for disturbance pulse simulation'''
            return lambda t: size if start <= t <= end else 0
        for i, disturbance in enumerate(disturbances):
            data = []
            if disturbance and\
                abs(disturbance['size']) > 0 and \
                disturbance['start'] < symstop and \
                disturbance['start'] < disturbance['end'] and\
                type(disturbance['start']) in [int, float]:
                # disturbance trigger
                data.append(pulse(disturbance['size'], disturbance['start'], disturbance['end']))
                # disturbance transfer function
                data.append(blocksim.LTI(name=f'Gd{i+1}', inputname=f'd{i+1}', outputname=f'df{i+1}',
                                    numerator=[1], denominator=[disturbance['tau'], 1]))
                d[i] = data
        resp = self.getResponse(uf, t, n, d)
        return resp, np.asarray(t)

    def getResponse(self, uf, time, ns='', ds=[]):
        # get TF
        TF = self.getTF()
        # alter inputs, simple loop typically have ysp as input
        # flag for simple loop
        simple = False   
        if 'ysp' in TF.inputs: 
            TF.inputs.update({'ysp': uf})
            simple = True
        else:
            TF.inputs.update({'ysp1': uf})
        for i, (n, d) in enumerate(zip(ns, ds)):
            ysum = []
            if n:
                TF.inputs.update({f'n{i+1}': n})
                ysum.append(f'+n{i+1}')
            if d:
                # add new input
                TF.inputs.update({f'd{i+1}': d[0]})
                # add new block
                TF.blocks.append(d[1])
                # add new sum element
                ysum.append(f'+df{i+1}')
            # alter sums
            if ysum:
                y = 'y' if simple else f'y{i+1}'
                yp = 'yp' if simple else f'yp{i+1}'
                # add sum for y
                TF.sums.update({y: (f'+{yp}', *ysum)})
                # change process output name to accomodate above sum change
                TF.blocks[i].outputname = yp 
        return TF.simulate(time)

    def getMargins(self):
        return margins(self.transferFunction)

    def getMarginsIncreasedGain(self, gain):
        return margins(gain * self.transferFunction)


class plant(model):

    def __init__(self, **kwargs):
        def assign_var(var, default):
            return kwargs[var] if var in kwargs else default

        self.td = assign_var('td', 0)
        self.t1 = assign_var('t1', 0)
        self.t2 = assign_var('t2', 0)
        self.K = assign_var('K', 0)
        self.angle = assign_var('Angle', 0)
        self.name = assign_var('name', None)
        self.id = assign_var('id', '')
        self.transferFunction = self._constructPlant()

    def getParameters(self):
        return {'K': self.K, 't1': self.t1, 't2': self.t2,
                'td': self.td, 'Angle': self.angle}

    def serialize(self):
        parameters = {'model_type': self.__class__.__name__, 'name': self.name}
        parameters.update(self.getParameters())
        return parameters

    def get_model_type(self):
        return self.__class__.__name__

    def _constructPlant(self):
        return InternalDelay(tf(1))


class SODTF(plant):
    def _constructPlant(self):
        return blocksim.LTI(f'G{self.id}', f'p{self.id}', f'y{self.id}', self.K, 
                            np.convolve([self.t1, 1], [self.t2, 1]), self.td)


class FODITF(plant):
    def _constructPlant(self):
        return blocksim.LTI(f'G{self.id}', f'p{self.id}', f'y{self.id}', self.K, 
                        np.convolve(np.convolve([self.t1, 1], [self.t2, 1]), [1, 0]), self.td)


class Loop:

    # eventually this method will no more be required
    @staticmethod
    def closedCV(plant: plant, controller: PID) -> model:
        H = InternalDelay(tf(1, 1, deadtime=0))  # H- measurement delay
        one = InternalDelay(tf(1, 1))
        M = controller.getTF() * plant.getTF() / (
                    one + controller.getFilterTF() * controller.getTF() * plant.getTF() * H)  # Whole system tf with feedback loop
        return model(M)

    @staticmethod
    def closedCO(plant: plant, controller: PID) -> model:
        '''This method calculates each PID component separatelly and is used in tunninApp'''
    # diagram sums, only error calculation
        sums = {'e': ('+ysp', '-y'),
                'p': ('+pP', '+pI', '+pD')}
        # diagram inputs, only setpoint - dummy
        inputs = {'ysp': 1}
        
        return model(blocksim.Diagram([
            plant.getTF(), controller.getPTF(), controller.getITF(), controller.getDTF()
            ], sums, inputs))
    
    

    @staticmethod
    def closedCascadedCO(plants: list, controllers: list, compare = False) -> model:
        '''This method calculates each PID component separatelly and is used in tunninApp.
        It is used for cascaded or simple loop design.'''
        # diagram inputs, only external pid setpoint
        inputs = {'ysp1': 1}
        # second PID will receive
        # collect all blocks
        blocks = []
        # diagram error calculation
        sums = {'e1' : ('+ysp1', '-y1'),
                }
        for plnt in plants:
            blocks.append(plnt.getTF())
        for ctrl in controllers:
            if compare:
                blocks.append(ctrl.getTF())
            else:    
                blocks.append(ctrl.getPTF())      
                blocks.append(ctrl.getITF())      
                blocks.append(ctrl.getDTF())      
                # adding pid term summation
                sums[f'p{ctrl.id}'] = (f'+pP{ctrl.id}', f'+pI{ctrl.id}', f'+pD{ctrl.id}')
        
        # G1 received G2 output as and input
        if len(controllers) == 2:
            sums.update({'e2' : ('+p1', '-y2')})
            blocks[0].inputname = 'y2'
        return model(blocksim.Diagram(blocks, sums, inputs))
    @staticmethod
    def closedSmithCO(plants: list, controllers: list, compare = False) -> model:
        '''This method calculates each PID component separatelly and is used in tunninApp.
        It is used for smith predictor loop design.
        It receives Single PID model (controllers) and three plant models (plants):
        - full plant with deadtime
        - plant without deadtime
        - deadtime only'''
        # diagram inputs, only external pid setpoint
        inputs = {'ysp1': 1}
        # second PID will receive
        # collect all blocks
        blocks = []
        # diagram error calculation
        #y2 - plant response without deadtime, #y3 plant y2 response after deadtime
        sums = {'e1' : ('+ysp1', '-y1', '-y2', '+y3'), 
                }
        for plnt in plants:
            blocks.append(plnt.getTF())
        for ctrl in controllers:
            # in compare app we don't need individual components and 
            # simulation is optimized
            if compare:
                blocks.append(ctrl.getTF())
            else:
                blocks.append(ctrl.getPTF())      
                blocks.append(ctrl.getITF())      
                blocks.append(ctrl.getDTF())      
                # adding pid term summation
                sums[f'p{ctrl.id}'] = (f'+pP{ctrl.id}', f'+pI{ctrl.id}', f'+pD{ctrl.id}')
        
        # G2 receives PID output
        blocks[1].inputname = 'p1' #plant without deadtime
        # G3 receives G2 ouput
        blocks[2].inputname = 'y2' #plant as a deadtime
        return model(blocksim.Diagram(blocks, sums, inputs))
    # @staticmethod
    # def closedSmithCO_(plants: list, controllers: list) -> model:
    #     '''This method calculates each PID component separatelly and is used in tunninApp.
    #     It is used for smith predictor loop design.
    #     It receives Single PID model (controllers) and three plant models (plants):
    #     - full plant with deadtime
    #     - plant without deadtime
    #     - deadtime only'''
    #     # diagram inputs, only external pid setpoint
    #     inputs = {'ysp1': 1}
    #     # second PID will receive
    #     # collect all blocks
    #     blocks = []
    #     # diagram error calculation
    #     #y2 - plant response without deadtime, #y3 plant y2 response after deadtime
    #     sums = {'e1' : ('+ysp1', '-y1', '-y2', '+y3'), 
    #             }
    #     for plnt in plants:
    #         blocks.append(plnt.getTF())
    #     for ctrl in controllers:   
    #         blocks.append(ctrl.getTF())     
        
    #     # G2 receives PID output
    #     blocks[1].inputname = 'p1' #plant without deadtime
    #     # G3 receives G2 ouput
    #     blocks[2].inputname = 'y2' #plant as a deadtime
    #     return model(blocksim.Diagram(blocks, sums, inputs))
    
    # @staticmethod
    # def closedCO_(plants: list, controllers: list) -> model:
    #     '''This method calculates PIDs as a whole and is used in compareTunninApp.
    #     It is used for cascaded and simple loop comparison.'''
    #     # diagram inputs, only external pid setpoint
    #     inputs = {'ysp1': 1}
    #     # second PID will receive
    #     # collect all blocks
    #     blocks = []
    #     # diagram error calculation
    #     sums = {'e1' : ('+ysp1', '-y1')
    #             }
    #     for plnt in plants:
    #         blocks.append(plnt.getTF())
    #     for ctrl in controllers:    
    #         blocks.append(ctrl.getTF())
    #     # G1 received G2 output as and input
    #     if len(controllers) == 2:
    #         sums.update({'e2' : ('+p1', '-y2')})
    #         blocks[0].inputname = 'y2' 
    #     return model(blocksim.Diagram(blocks, sums, inputs))
    
    # # eventually this method will no more be required
    # @staticmethod
    # def closedCOs(plant: plant, controller: PID) -> dict:      
    #     # diagram sums, only error calculation
    #     sums = {'e': ('+ysp', '-y')}
    #     # diagram inputs, only setpoint - dummy
    #     inputs = {'ysp': 1}
    #     return blocksim.Diagram([plant.getTF(), controller.getTF(), 
    #                              controller.getPTF(), controller.getITF(), controller.getDTF()], sums, inputs)

    @staticmethod
    def open(plant: plant) -> model:
        return plant


models = {
    'SODTF': SODTF,
    'SODITF': FODITF
}


def convertPID(objectFrom: PID, objectTo: PID) -> PID:
    isa = objectFrom.toISA()
    return objectTo.fromISA(isa)


def modelFactory(**kwargs):
    thismodule = sys.modules[__name__]
    modelF = models[kwargs['model_type']]
    # model_type = getattr(thismodule, kwargs['model_type'])
    return modelF(**kwargs)#model_type(**kwargs)

def modelFit(df: pd.DataFrame, mod:dict, y0=0):
    '''This function fits the model to provided data.
    It takes two parameters:
    -df - dataframe with all data
    -mod - dictionary desribing the model'''    
    #initial params
    initParams=[1,2,2]

    #input function and response data
    uf = np.array(df[mod['In']].values)
    y0 = y0
    if mod['model_type'] == 'SODTF':        
        yreal = np.array(df[mod['Out']].values) 
        # y0 = y0
        # change model type to get correct response function
    else:
        yreal = np.array(df['Rate'].values)
        # y0 = mod['Angle']       
        mod['model_type'] = 'SODTF'

    #data normalization
    min_val = np.min(yreal)
    max_val = np.max(yreal)
    rng_val = max_val-min_val
    #normalized data
    yrealNorm = (yreal-min_val)/rng_val*100
    y0Norm = (y0-min_val)/rng_val*100


    #export all values for testing
    t = pd.to_datetime(df.index, unit='s')
    
    #sampling time in seconds
    
    #response function and optimization routine
    fx = response_wrapper(t, parameters = mod, y0=y0Norm)
    popt, pcov = curve_fit(fx, uf, yrealNorm, bounds=([-np.inf, 0, 0], np.inf), p0=initParams, maxfev=1000, 
                           check_finite=False)
    
    #append known parameters
    popt = np.append(popt, [mod['td'], mod['Angle']])
    
    #eliminate normalization gain
    popt[0]=popt[0]*rng_val/100

    return popt

def response_wrapper(timeSeries:pd.DataFrame, parameters:dict, y0:float=0):
    '''This is a wrapper function that creates a function
    to generate response by feeding parameters externally
    not from model class'''
    def responseFx(inputData, K, t1, t2):
        #override parameters
        parameters['K'] = K
        parameters['t1'] = t1
        parameters['t2'] = t2
        
        # Define the plant model
        plant_model = modelFactory(**parameters)

        # Generate the response based on the input data
        response, _, _ = plant_model.getSimulatedResponse(timeSeries, inputData, y0)
        return response
    return responseFx

