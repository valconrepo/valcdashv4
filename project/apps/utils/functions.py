from datetime import datetime

# Add imports for parquet handling
import pyarrow as pa
import pyarrow.parquet as pq
from pathlib import Path
import os
import tempfile
import pandas as pd

def getXRange(data:dict)->list:
    '''This function will read graph zoom callback to extract x axis range:
        - None when expected data is not read
        - [Xstart, Xend] is returned when x axis zoom is activated'''
    if data == None:
        return None
    else:
        coords = data['range']
    
    if 'x' in list(coords.keys()):        
        formats = ['%Y-%m-%d %H:%M:%S.%f', '%Y-%m-%d %H:%M:%S']
        result = ['']*2
        # it is possible that two timestamps might have a different format
        # therefore they should be individually converted
        for i in range(2):
            # check for correct format
            for fmt in formats:
                try:
                    result[i] = datetime.strptime(coords['x'][i], fmt)
                    # break if the format is correct
                    break
                except:
                    continue
        return result
    #data does not have the expected data
    else:
        return None

# Add function to manage parquet file storage
def get_data_path(username, table_name, temp_dir=tempfile.gettempdir()):
    """Get path to user's temporary parquet file"""
    # temp_dir = tempfile.gettempdir()
    return Path(os.path.join(temp_dir, f"{username}_{table_name}.parquet"))

def save_data(data, table_name, username, temp_dir=tempfile.gettempdir()):
    """Save data to parquet file"""
    path = get_data_path(username, table_name, temp_dir=temp_dir)
    if isinstance(data, pd.DataFrame):
        data.to_parquet(path)
    else:
        raise ValueError("Data must be a pandas DataFrame")

def load_data(username, table_name="main"):
    """Load data from parquet file"""
    path = get_data_path(username, table_name)
    if not path.exists():
        return None
    
    # Read parquet file
    df = pd.read_parquet(path)

    return df