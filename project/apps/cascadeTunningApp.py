import dash_bootstrap_components as dbc
from dash import html
from .app import app
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup, clearAttrPopup, showAttrPopup
from apps.utils.tuning_layouts import tuneLayouts
from .app import logger #logger object for debuging
logger.info(f'{__name__}')

layout = 'Cascade'

# define modal objects
showPop = showAttrPopup(
                    app=app,
                    name=f'controller',
                    alt=layout
                    )
clearPop = clearAttrPopup(
                    app=app,
                    name=f'controller',
                    alt=layout)

deletePop = deleteAttrPopup(
                    app=app,
                    name=f'controller',
                    alt=layout)
savePop = saveAttrPopup(
                    app=app,
                    name='controller',
                    alt=layout)
loadPop = loadAttrPopup(
                    app=app,
                    name='controller',
                    alt=layout)
elements = [savePop, loadPop, deletePop, showPop, clearPop]
lay = tuneLayouts[layout](*elements)

modals = html.Div([pop.layout() for pop in elements])
def serve_layout():
    layout = dbc.Container(
        children=[
            html.Div(children=[*lay.layout], id = 'tune-dynamic-layout'),
            modals
    ], fluid=True)
    return layout
# trigger modal object callbacks
for element in elements:
    element.register_callbacks()
# # register callbacks for main layout
lay.register_callbacks()