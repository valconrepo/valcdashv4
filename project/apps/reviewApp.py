from dash import html, no_update, dash_table
from dash_extensions.enrich import Input, Output, State
import dash_bootstrap_components as dbc
from .utils import components as utl
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
import pandas as pd
import logging
from inspect import currentframe #this is used to read callback name, for debugging
from .app import logger #logger object for debuging
logger.info(f'{__name__}')
# Set the root logger level to a high value
# this inhibits XSteam warning output, when incorrect configuration is made
logging.getLogger().setLevel(logging.CRITICAL)

# get all attributes
with database_context_manager(database_pool) as db:
    # get attribute table list
    attributes  = db.attributes
    # get user list
    users = db.getUserList()
if users:
    layout = dbc.Container([    
        html.Hr(style={'margin-top':0, 'width': '100%'}),
        utl.LoadingSpinner(id='review-loading-show_data'),
        utl.LoadingSpinner(id='review-loading-delete_data'),
        dbc.Row([
        dbc.Col(utl.Selector('user-name', 'User',
                             options=[{'label': user, 'value': user} for user in users],
                             value = users[0])),
        dbc.Col(utl.Selector('table-name', 'Attribute table',
                             options=[{'label': attribute.capitalize(), 'value': attribute} for attribute in attributes],
                             value = attributes[0]))
    ]),
    html.Div(children=[], id='data-contents'),
    ], fluid=True)
else:
    layout = html.Div(children=[], id='data-contents')

@app.callback(
    [Output('data-contents', 'children'),
     Output('review-loading-show_data', 'children')],
    [Input('user-name', 'value'), 
     Input('table-name', 'value')]
)
def show_data(user, table):
    '''Fetch user datatable and return it's contents.'''
    logger.info(currentframe().f_code.co_name)
    # initialize result table
    result=''
    # read all attributes of current user
    with database_context_manager(database_pool) as db:
        df = db.getAttr(user, table, all=True)

    # column name capitalization
    df.columns = df.columns.str.capitalize()
    
    # Set id based on attribute name
    df['id'] = df['Name']
    
    # Set index based on attribute name
    df.set_index('id', inplace=True, drop=False)

    # create dcc component
    # when no attributes are saved do not return table
    if len(df.index) > 0:
        result = dash_table.DataTable(
            id='datatable-row-ids',
            columns=[
                {'name': i, 'id': i, 'deletable': False} for i in df.columns
                # omit the id column
                if i != 'id'
            ],
            data=df.to_dict('records'),
            # editable=True,
            filter_action="native",
            sort_action="native",
            sort_mode='multi',
            row_deletable=True,
            page_action='native',
            page_current= 0,
            page_size= 100,
            style_data_conditional=[
                    {
                        'if': {'row_index': 'odd'},
                        'backgroundColor': 'rgb(220, 220, 220)',
                    }],
            style_header={
                'backgroundColor': 'rgb(210, 210, 210)',
                'color': 'black',
                'fontWeight': 'bold'
            }
        )
    # result is encapsulated to prevent overflow
    return html.Div(result, style={'overflow-x':'scroll', 
                                   'maxHeight': '80vh', 
                                   'overflow-y':'scroll'}), no_update

@app.callback(
    [Output('data-contents', 'children'),
     Output('review-loading-delete_data', 'children')],
    Input('datatable-row-ids', "data_previous"), 
    [State('user-name', 'value'), 
    State('table-name', 'value'),
    State('datatable-row-ids', 'data')],
    prevent_initial_call=True
)
def delete_data(rows_ex, user, table, rows):
    '''Delete data based on user selection.
    This is performed based on changes to attribute table'''
    logger.info(currentframe().f_code.co_name)
    # initially previous rows are None, so no update is needed
    if rows_ex:
        # load previous rows to pandas and find duplicates
        df_rows = pd.DataFrame(rows_ex)
        # commented code is useful when it is not possible to delete
        dupl_rows = df_rows[df_rows.duplicated()]#subset=['id', 'Loop'])]
        logger.debug(f"Deleting: {dupl_rows}")
        # iterate through current and previous rows
        for row_ex in rows_ex:
            # to find deleted rows
            # deleted duplicate rows            
            del_dupl_rows = dupl_rows[(dupl_rows["id"]==row_ex["id"]) & (dupl_rows["Loop"]==row_ex["Loop"])]
            # find deleted or deleted duplicate rows
            if all(row_ex['id'] != row['id'] for row in rows) \
            or len(del_dupl_rows) > 0:
                # remove deleted row from DB
                with database_context_manager(database_pool) as db:
                    db.deleteAttr(user, table, row_ex['id'], row_ex['Loop'])
    # this is dummy return, it is not allowed to have callback without update
    return no_update, no_update