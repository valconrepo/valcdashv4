from dash import html, dcc, no_update, callback_context, dash_table
from dash_extensions.enrich import Input, Output, State, ALL
from dash_extensions.snippets import send_data_frame
import dash_bootstrap_components as dbc
import base64
import io
import pandas as pd
import json
import plotly.express as px
from .utils.database.database_manager import UserData
from flask_login import current_user
from dash.exceptions import PreventUpdate
from .utils import components as utl
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from .utils import OvImport, DNAImport
import logging
import traceback
from inspect import currentframe #this is used to read callback name, for debugging
from .app import logger #logger object for debuging
import chardet
import csv
import os
logger.info(f'{__name__}')
# Set the root logger level to a high value
# this inhibits XSteam warning output, when incorrect configuration is made
logging.getLogger().setLevel(logging.CRITICAL)

layout = dbc.Container([
    dcc.Store('data-table', storage_type='session'),
    # html.H1('Upload app'),
    html.Hr(style={'margin-top':0, 'width': '100%'}),
    dbc.Row([
        dbc.Col(utl.Input('data-name', 'Data name', 'Data name', type='text')), #not clear why we need to name data if we always load it to the same table
        dbc.Col(utl.Selector('import-type', 'Import type',
                             options=[{'label': 'Ovation Trend Export', 'value': 'Ovation'},
                                      {'label': 'Valmet DNA Trend Export', 'value': 'Valmet'},
                                      {'label': 'Other', 'value': 'Other'}])),
        dbc.Col([dbc.Card(style={'margin-bottom': '10px'},
                        children=[dbc.CardHeader("Data file stacking:",
                                                 style={'padding':'0px 5px'}),
                                                 dbc.CardBody(
                                                     children = dcc.RadioItems(
                                                         id='datafile-radio',
                                                         options=[
                                                                {'label': 'vertical', 'value': 'vertical'},
                                                                {'label': 'horizontal', 'value': 'horizontal'}
                                                            ],
                                                    value='vertical',
                                                    labelStyle={#'display': 'inline-block', 
                                                                'margin-right': '20px',
                                                                'font-size': '18px'},
                                                    style={'align-items': 'center'}),
                                                    style={'padding':'0px 5px'})]),
                html.Div(id='datafile-radio-tooltip')], width=2)
    ]),
    dcc.Loading(dbc.Row(dbc.Col(utl.Upload('upload')))),
    html.Div(children=[], id='data-contents'),
    dbc.Button(id='dummy-b', style={'display': 'none'}),
    dbc.Modal(
        [
            dbc.ModalHeader(dbc.ModalTitle("Error")),
            dbc.ModalBody(id='modal-content'),
        ],
        id="error-modal",
        is_open=False,
    ),
    dbc.Modal(
        [
            dcc.Store('data-table-store', data = ''),
            dbc.ModalHeader('Delete data'),
            dbc.ModalBody(id='delete-data-content'),
            dbc.ModalFooter([
                utl.Button('delete-data-cancel', 'Cancel'),
                utl.Button('delete-data-delete', 'Delete')
            ])
        ], id='delete-data-pop-up'
    ),
    dcc.Loading(dbc.Modal(
        [
            dbc.ModalHeader(dbc.ModalTitle("Data")),
            dbc.ModalBody(id='show-modal-content'),
        ],
        id="show-modal",
        is_open=False,
        size="xl",
    )),
], fluid=True)


@app.callback(
    Output('data-contents', 'children'), Output('error-modal', "is_open"),
    Output('modal-content', 'children'), Output('show-modal', "is_open"),
    Output('show-modal-content', "children"),
    Input('upload', 'contents'),
    Input({'id': 'data-show-button', 'data': ALL}, 'n_clicks'),
    State('data-name', 'value'),
    State('import-type', 'value'),
    State('upload', 'filename'),
    State('datafile-radio', 'value'),
    prevent_initial_call=True,
)
def upload(uploaded_contents, n1, data_name, import_type, filename, mergeOpt):
    logger.info(currentframe().f_code.co_name)
    with database_context_manager(database_pool) as db:
        # loopData = db.getData(current_user.username, data_name) #get data of current loop

        callback_id = '.'.join(callback_context.triggered[0]['prop_id'].split('.')[:-1])
        if callback_id == 'upload' and uploaded_contents is not None: #uploading new data            
            # data for this loop is already loaded
            # if loopData is not None and not None in [uploaded_contents]:#not clear why None in uploaded_contents????
            #     return update_tables(loopData), False, no_update, no_update, no_update
            #not all information is available
            if None in [data_name, import_type] or "" in [data_name, import_type]:
                return no_update, True, 'Data name or import type is empty', no_update, no_update
            
            # loading the data
            try:
                dfs = []
                df = None
                files = []
                for file, contents in zip(filename, uploaded_contents):
                    # Get file title without extension
                    file_title = os.path.splitext(file)[0]
                    files.append(file_title)
                    _, content_string = contents.split(',')
                    decoded = base64.b64decode(content_string)
                    raw_data = decoded[:10]  # Sample first 1000 bytes
                    result = chardet.detect(raw_data)
                    detected_encoding = result['encoding']
                    dialect = csv.Sniffer().sniff(contents)
                    # dynamic skiping of empty rows on the top of the sheet
                    # 100 here is an arbitrary number
                    for skip_rows in range(100):
                        try:
                            # file extention can be changed for both historian report and trend export
                            if 'csv' in file:
                                # Assume that the user uploaded a CSV file
                                df = pd.read_csv(io.StringIO(decoded.decode(detected_encoding)), skiprows=skip_rows, 
                                                            skip_blank_lines=False, delimiter=dialect.delimiter)
                            elif 'xls' in file:
                                # Assume that the user uploaded an excel file
                                df = pd.read_excel(io.BytesIO(decoded), skiprows=skip_rows)
                            # when Valmet trend copy is used, it is necessary to resample the data
                            elif 'txt' in file and  import_type == 'Valmet':
                                logger.info(file)
                                df = DNAImport.convert(io.StringIO(decoded.decode(detected_encoding)))                        
                            # process the file to remove Ovation trend artifacts
                            if import_type == 'Ovation':
                                df = OvImport.decodeOvcsv(df)
                            # this will terminate loop once loading is successful
                            break
                        except Exception as e:
                            # traceback.print_exc()
                            continue
               
                    try:
                    # object is assumed to contain non numeric data, therefore date time information
                        dateTimeCol = [col for col in df.columns if df[col].dtype in ['object', 'datetime64[ns]']]
                        
                        #parse it as datetime and drop rows that have errors after parsing
                        df[dateTimeCol[0]] = pd.to_datetime(df[dateTimeCol[0]], errors='coerce')
                        df = df.dropna()

                        # all data tables should have DateTime column
                        df = df.rename(columns={dateTimeCol[0]:"DateTime"})
                        df = df.set_index(["DateTime"])
                        
                    except:
                        pass
                    # Remove duplicate columns, keeping first occurrence
                    df = df.loc[:, ~df.columns.duplicated(keep='first')]
                    dfs.append(df)
                # merging will be performed when more then 
                # one dataframe is loaded
                if len(dfs)>1:
                    if mergeOpt=='vertical':
                        df = pd.concat(dfs, axis=0)
                        # remove duplicate indexes
                        df = df.loc[~df.index.duplicated(keep='first')]
                    else:
                         # Add filename prefix to duplicate columns across dataframes
                        column_counts = {}
                        for df in dfs:
                            for col in df.columns:
                                column_counts[col] = column_counts.get(col, 0) + 1
                        
                        # If duplicates found, reindex all dataframes based on first one
                        has_duplicates = any(count > 1 for count in column_counts.values())
                        if has_duplicates:
                            # add DateTime as a duplicate column
                            # column_counts['DateTime'] = len(dfs)
                            for i in range(0, len(dfs)):
                                if len(dfs[i].index) > 1:
                                    df_delta = int((dfs[i].index[1] - dfs[i].index[0]).total_seconds())                                    
                                    dfs[i].reset_index(inplace=True)
                                    dfs[i].drop('DateTime', axis=1, inplace=True)
                                    dfs[i].index = range(0, len(dfs[i].index))# * df_delta, df_delta)
                                    
                            
                            # Rename duplicate columns in each dataframe with filename prefix
                            for i, df in enumerate(dfs):
                                duplicate_cols = [col for col in df.columns if column_counts[col] > 1]
                                if duplicate_cols:
                                    dfs[i] = df.rename(columns={col: f'{files[i]}_{col}' for col in duplicate_cols})
                        for i, _ in enumerate(dfs):
                        # remove duplicate indexe
                            dfs[i] = dfs[i].loc[~dfs[i].index.duplicated(keep='first')]
                        df = pd.concat(dfs, axis=1)             
                    # df = df.set_index(["DateTime"])
                # when no data is loaded, do not update the database
                if df is None:
                    return no_update, no_update, no_update, no_update, no_update
                df.Name = data_name 

                # replace Null if not all data is available in all files
                df = df.fillna(0)
                db.putData(current_user.username, df, data_name)
                # loopData = df
            except Exception as e:
                traceback.print_exc()
                return no_update, True, f'Error occurred: {e}', no_update, no_update
        elif any(n1) and 'data-show-button' in callback_id: #preview data when show button is pressed
            data_name = eval(callback_id)['data']
            df = db.getData(current_user.username, data_name)
            return no_update, False, no_update, True, dcc.Graph(figure=px.line(df))
        return update_tables(current_user.username, db), no_update, no_update, no_update, no_update

    
def update_tables(user:str, db:database_context_manager(database_pool)):#data: pd.DataFrame, data_name:str):
    contents = []
    dataTables = db.getDataTables(user)
    for dataTable in dataTables:
        # do not show temporary trend data
        if dataTable == 'trend_data':
            continue
        loopData = db.getData(user, dataTable, numRows=1)
        
        if loopData is None:
            table = "Failed to load"
        else:
            # style prevents tagname wrapping and removed space between 
            # table and scrollbar
            table = dbc.Table.from_dataframe(loopData.iloc[:,:].head(0), 
                                             bordered = True, 
                                             hover = True,
                                             style = {'whiteSpace': 'nowrap',
                                                      'margin-bottom': 0})
        contents.append(dbc.Card([
        dbc.Row(html.H4(dataTable)),
        # overflow is prevented by X scrollbar
        dbc.Row(html.Div(table, style={'overflow-x':'scroll'})),
        dbc.Row([
            dbc.Col(utl.Button({'id': 'data-delete-button', 'data': dataTable}, 'Delete')),
            dbc.Col(utl.Button({'id': 'data-show-button', 'data': dataTable}, 'Show')),
            dbc.Col(utl.Button({'id': 'data-download-button', 'data': dataTable}, 'Download'))
            ])
            ], body=True))
    contents.append(dcc.Download(id='data-download'))
    return contents

@app.callback(
    [Output("delete-data-pop-up", "is_open"),
     Output('delete-data-content', 'children'),
     Output('data-contents', 'children'),
     Output('data-table-store', 'data')
     ],
    [Input({'id': 'data-delete-button', 'data': ALL}, 'n_clicks'), 
     Input('delete-data-cancel', "n_clicks"), 
     Input('delete-data-delete', 'n_clicks')],
    [State("delete-data-pop-up", "is_open"),
     State('data-table-store', 'data')],
    prevent_initial_call=True
)
def delete_data(n1, n2, n3, is_open, tablename):
    ''' Delete data table.'''
    logger.info(currentframe().f_code.co_name)
    # do not update is no button was pressed
    if not any([n2, n3] + n1):
        raise PreventUpdate
    
    # get callback id
    id = '.'.join(callback_context.triggered [0] ['prop_id'].split('.') [:-1])
    # when delete button is pressed, show message about attributes to be deleted
    if any(n1) and 'data-delete-button' in id:
        # get table name
        tablename = eval(id)['data']
        # get all attribute tables that use datatable to be deleted
        with database_context_manager(database_pool) as db:
            attributes = {
                'models' : db.getAttr(current_user.username,'models'),
                'controllers' : db.getAttr(current_user.username,'controllers'),
                'linearizations' : db.getAttr(current_user.username,'linearizations'),
                'trends' : db.getAttr(current_user.username,'trends')
            }
            # filter out all attributes that use the data table to be deleted
            for attr in attributes:
                if attr == 'controllers':
                    attributes[attr] = attributes[attr][attributes[attr]['model'].
                                                        isin(list(attributes['models']['name']))]
                else:
                    attributes[attr] = attributes[attr][attributes[attr]['datatable'] == tablename]
            # clean up the results
            results = {}
            for attr in attributes:
                results[attr] = [name for name in list(attributes[attr]['name']) if name != 'Temp']
            # create warning message
            content = [html.P(f'\t{i}: {str(results[i]).replace("[", "").replace("]", "")}') 
                       for i in results if results[i] != []]
            if content!= []:
                message = html.Div(
                            [html.Div(f'Below will be deleted:'),
                             html.Div(content)])
            else:
                message = html.Div(f'Table was not used, delete?')

            return not is_open, message, no_update, tablename
    # when modal delete is pressed, delete data and related attributes
    # update data list and close modal
    # also delete if no attributes were using the data table
    if n3 and 'delete-data-delete' in id:
        # deletes table related to delete button
            with database_context_manager(database_pool) as db:
            #clear table
                db.deleteData(current_user.username, tablename)
                # update data table list
                return not is_open, no_update, update_tables(current_user.username, db), ''
    # when modal cancel is pressed, close modal
    if n2 and 'delete-data-cancel' in id:
        return not is_open, no_update, no_update, ''
    return is_open, no_update, no_update, ''

@app.callback(
    [Output("data-download", "data")],
    [Input({'id': 'data-download-button', 'data': ALL}, 'n_clicks')],
    prevent_initial_call=True
)
def download(n1):
    logger.info(currentframe().f_code.co_name)
    if not n1:
        raise PreventUpdate
    
    callback_id = '.'.join(callback_context.triggered[0]['prop_id'].split('.')[:-1])
    with database_context_manager(database_pool) as db:
        data_name = eval(callback_id)['data']
        df = db.getData(current_user.username, data_name, numRows=1e9)
        df_csv = df.to_csv(index=False)
        return dcc.send_data_frame(df.to_csv, filename=f'{data_name}.csv')