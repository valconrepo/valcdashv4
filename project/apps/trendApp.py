import plotly.graph_objects as go
import plotly.express as px
from .utils import components as utl
import dash_bootstrap_components as dbc
from dash import dcc, html, no_update, callback_context
from dash_extensions.enrich import Input, Output, State, ALL
from dash.exceptions import PreventUpdate
from apps.app import app
import numpy as np
from apps.utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user
import inspect
import pandas as pd
from .utils.functions import getXRange
from pyXSteam.XSteam import XSteam
import logging
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup
import time
import re
from pandas.tseries.offsets import DateOffset
from inspect import currentframe #this is used to read callback name, for debugging
import scipy.signal as controls
from .app import logger #logger object for debuging
from datetime import datetime
from apps.analysisApp import getField, safe_eval_config
from apps.utils.functions import save_data, load_data
logger.info(f'{__name__}')
# Set the root logger level to a high value
# this inhibits XSteam warning output, when incorrect configuration is made
logging.getLogger().setLevel(logging.CRITICAL)
log_trigger = False
#this specifies number of available calculations
calcNr = 6
# tranfer function response
def tf_response(input, gain, zeros:list, poles:list, deadtime = 0, uf1=0):
    '''Helper function for cases where transfer
    functions are involved'''
    # get time samples in seconds
    dt = (input.index[1] - input.index[0]).total_seconds()
    T = np.arange(0, len(input.index)) * dt
    # define transfer function
    tf1 = controls.lti(zeros, poles)
    # get response
    uf0 = input[0]
    U = input.to_numpy() - uf0
    _, response, _ = controls.lsim(tf1, U, T)
    # now I need to introduce the deadtime
    if deadtime > dt/2: 
        response = pd.Series(response, index = input.index)\
            .shift(int(deadtime/dt)).fillna(response[0]).to_numpy()
    # This line of code calls the `simulate` method of the `tf1` object.
    # The `simulate` method takes two arguments: `uf` and `t`.
    # `uf` is a lambda function that takes a time `t` as input and returns a list of values.
    # `t` is an array of time samples.
    # The `simulate` method simulates the response of the transfer function `tf1` to the input `uf` over the time samples `t`.
    # The result is assigned to the variable `response`.
    # return response with intial value bias and gain
    # print(f'scipy: {time.time() - st}')
    return (response + uf1) * gain

# calculation functions
def gain(input, gain=1, bias=0):
    return input * gain + bias

def leadlag(input, gain=1, lead=0, lag=0):
    # positive constraint
    lead = abs(lead)
        # this will ensure that lead is constrained
    lag = abs(lag) if lead == 0 else max(abs(lag), min(lead/10, 1))
    # get transfer function response
    response = tf_response(input, gain, [lead,  1], [lag, 1], 0, input[0])
    return response

def derivative(input, gain=1, filter=0):
    # positive constraint
    filter = abs(filter)
    # get transfer function response
    response = tf_response(input, gain, [1, 0], [max(filter, 1), 1])
    return response

def integrator(input, gain=1, int_time=0):
    # positive constraint
    int_time = abs(int_time)
    # get transfer function response
    response = tf_response(input, gain, [0, 1], [max(int_time, 1), 0])
    return response

def rate(input, smooth=0):
    # positive constraint
    smooth = abs(smooth)
    # get rate of change
    dt = (input.index[1] - input.index[0]).total_seconds()
    t = np.arange(0, len(input.index)) * dt    
    # get smoothed input
    if smooth > 0:
        input = tf_response(input, 1, [0, 1], [smooth, 1])
    response = np.gradient(np.ravel(input), t)
    return response

def transport(input, delay=0):
    # positive constraint
    delay = abs(delay)    
    # get transfer function response
    response = tf_response(input, 1, [0, 1], [0, 1], delay)
    return response

def sodtf(input, gain=1, tau1=0, tau2=0, delay=0):
    # positive constraint
    [tau1, tau2, delay] = list(map(abs, [tau1, tau2, delay]))

    # get transfer function response
    response = tf_response(input, gain, [1], [tau1*tau2, (tau1 + tau2), 1], delay, input[0])
    return response

def soidtf(input, gain=1, tau1=0, tau2=0, delay=0):
    # positive constraint
    [tau1, tau2, delay] = list(map(abs, [tau1, tau2, delay]))

    # get transfer function response
    response = tf_response(input, gain, [1], [tau1*tau2, (tau1 + tau2), 1, 0], delay, input[0])
    return response

def custom(input, gain=1, zeros="[1]", poles="[1]", delay=0, keep_init=0, Xs="[]", Ys="[]"):
    '''This function allows creating any transfer function'''
    # convert strs to lists
    zeros = eval(zeros)
    poles = eval(poles)
    # linear function
    x = eval(Xs) if '[' in Xs else []
    y = eval(Ys) if '[' in Ys else []
    # if coordinates were entered, then input can be processed
    if x and y:
        calc = np.interp(input, x, y)
        # cast back to series
        input = pd.Series(calc, index=input.index)
        
    # get transfer function response
    response = tf_response(input, gain, zeros, poles, delay, input[0] if keep_init > 0 else 0)
    return response


functions={
    'CUSTOM':custom,
    'GainBias':gain,
    'LeadLag':leadlag,
    'Derivative':derivative,
    'Integrator':integrator,
    'Rate':rate,
    'Transport':transport,
    'SODTF':sodtf,
    'SOIDTF':soidtf
}
# this helps collecting steam table methods
STmethods = [
    name for name, obj in inspect.getmembers(XSteam)
    if inspect.isfunction(obj) and obj.__qualname__.startswith(XSteam.__name__) and '__' not in name and '_' in name
]
# parameters that should not be used can be commented here
ST_options = {  "Density":"r",
                "Enthalpy":"h",
                "Entropy":"s",
                "Heat capacity":"C",
                "Internal energy":"u",
                "Pressure":"p",
                # "Speed of sound":"w",
                "Temperature":"t",
                "Vapour fraction":"x",
                # "Viscosity":"m",
                "Volume":"v"}
# this dictionary is used for parameter evaluation
functionsST = {method:getattr(XSteam(XSteam.UNIT_SYSTEM_MKS), method) 
               for method in STmethods 
               if method[0] in list(ST_options.values())}
# this dictionary provides constants and descritions for different unit system options
EU_ST = {
    "Steam table SI-1":[XSteam.UNIT_SYSTEM_BARE, 'm/kg/sec/K/MPa/W'],
    "Steam table SI-2":[XSteam.UNIT_SYSTEM_MKS, 'm/kg/sec/°C/bar/W'],
    "Steam table US":[XSteam.UNIT_SYSTEM_FLS, 'ft/lb/sec/°F/psi/btu']
}

calc_delete_popup = dbc.Modal(
    [
        dbc.ModalHeader('Delete stored traces'),
        dbc.ModalBody([
            utl.Selector('delete-calc-in', 'Trace'),
            ]),
        dbc.ModalFooter([
            utl.Button('delete-calc-cancel', 'Cancel'),
            utl.Button('delete-calc-delete', 'Delete')
        ])
    ], id='delete-calc-pop-up'
)
calc_save_popup = dbc.Modal(
    [
        dbc.ModalHeader('Calculations save'),
        dbc.ModalBody([
            utl.Selector('save-calc-in', 'Calculations'),
            utl.Input('save-calc-name-in', 'Calculation name', 'Calculation name', type='text'),
        ]),
        dbc.ModalFooter([
            utl.Button('save-calc-cancel', 'Cancel'),
            utl.Button('save-calc-save', 'Save')
        ])
    ], id='save-calc-pop-up'
)

# define modal objects
deletePop = deleteAttrPopup(
                    app=app,
                    name='trend')
savePop = saveAttrPopup(
                    app=app,
                    name='trend')
loadPop = loadAttrPopup(
                    app=app,
                    name='trend')
def serve_layout():     
    
    with database_context_manager(database_pool) as db:
        # read loop data tables
        dataTables = db.getDataTables(current_user.username)
        # read current trend
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
        # define dummy if not existing
        if not currentTrend:
            db.createAttr(current_user.username, 'trends')
            currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')


    # display available data tables
    data_options = [{'label': col_name, 'value': col_name} for col_name in dataTables
                    if col_name != 'trend_data']
    selection = ""
    if len(data_options) > 0:
        selection=data_options[0]['value']
    # get stored datatable if it exists
    if currentTrend is not None:
        try:
            # only use it if it still exists in the database
            if 'datatable' in currentTrend and currentTrend['datatable'] in dataTables:
                selection=currentTrend['datatable']
            else:
                # select the first table from the list otherwise
                if len(dataTables)>0:
                    selection = dataTables[0]
            # get the whole data table into global variable
            with database_context_manager(database_pool) as db:
                DATA = db.getData(current_user.username, selection, numRows=1e9).sort_index(ascending=True)
                save_data(DATA, "main", current_user.username)
        except:
            pass
    layout = dbc.Container([
        # html.H1('Netta Trend app'),
        html.Hr(style={'margin-top':0, 'width': '100%'}),
        # loading indication for all time-consuming callbacks
        utl.LoadingSpinner(id = 'trend-loading-update_calculations'),
        utl.LoadingSpinner(id = 'trend-loading-update_fx_params'),
        utl.LoadingSpinner(id = 'trend-loading-update_fx_param_label'),
        utl.LoadingSpinner(id = 'trend-loading-evaluate_compound_calculation'),
        utl.LoadingSpinner(id = 'trend-loading-delete_calc'),
        utl.LoadingSpinner(id = 'trend-loading-save_calc'),
        utl.LoadingSpinner(id = 'trend-loading-update_pts_lists'),
        utl.LoadingSpinner(id = 'trend-loading-post_load_trend'),
        utl.LoadingSpinner(id = 'trend-loading-update_traces'),
        utl.LoadingSpinner(id = 'trend-loading-update_graph'),
        utl.LoadingSpinner(id = 'trend-loading-update_zoom'),
        dcc.Store('trend-store', storage_type='session', data=0),
        dcc.Store('zoom-store', storage_type='session', data = 0),
        dcc.Download(id="trend-download"), # Add download component
        dbc.Row([
            dbc.Col([dbc.Button('Trace configuration', 
                           id="trend-trace-collapse-b", 
                           className="mb-3", outline=True, color="dark", size="md")], width=2),
            dbc.Col([utl.Selector('trend-data-selector', 'Data', #data selector will be inline with table to save space
                                options=data_options, value = selection,
                                persistence=False)], width=3),
            dbc.Col([dbc.ButtonGroup([
                        dbc.DropdownMenu(
                            label="Trend menu",
                            children=[
                                dbc.DropdownMenuItem('Save', id = savePop.btn_id),
                                dbc.DropdownMenuItem('Load', id = loadPop.btn_id),
                                dbc.DropdownMenuItem('Delete', id = deletePop.btn_id),
                                dbc.DropdownMenuItem(divider=True),
                                dbc.DropdownMenuItem('Download Data', id='trend-download-btn'), # Add download button
                                dbc.DropdownMenuItem('Zoom Reset', id = 'zoom-reset-trend-b')],
                                color='primary', group=True),
                        dbc.DropdownMenu(
                            label = "Trace menu",
                            children = [
                                dbc.DropdownMenuItem('Save', id = 'save-calc-b'),                  
                                dbc.DropdownMenuItem('Delete', 'delete-calc-b')],
                                color='secondary', group=True)]),
                    ], width=2),
            dbc.Col(dbc.Row([
                        dbc.Col(html.Div(utl.Input('trend-show-min', 'Minutes', 'Show, min', step=1, value=0), 
                                         title='Display window duration in minutes'), width=10)]), width=3),
            dbc.Col([html.Div(dbc.Button('Calculation configuration', 
                            id="trend-calculation-collapse-b", 
                            className="mb-3", outline=True, color="dark", size="md"),
                            style={'display': 'flex', 'justify-content': 'flex-end'})], width=2)
                            ], style={"height": "6vh"}),
        dbc.Row([
            dbc.Col([
                dbc.Collapse(dbc.Card([
                    dbc.CardBody(
                        children=dbc.Table([
                                    html.Thead(
                                        html.Tr([
                                            html.Th('Nr.', style={"width": "10%"}),
                                            html.Th('Trace', style={"width": "35%"}),
                                            html.Th('Range', style={"width": "30%"}),
                                            html.Th('Y axis', style={"width": "15%"}),
                                            html.Th('Color', style={"width": "10%"})
                                            ]),
                                    style={"position": "sticky", "top": 0, "backgroundColor": "#f8f9fa", "zIndex": 1}
                                            ),
                                    html.Tbody(id='trend-trace-table')
                                    ],
                                    bordered=True,
                                    className="table-sm"
                                    ),
                                    style={"table-layout": "fixed",
                                           'padding':'0px',
                                           'maxHeight': '32vh', 
                                           'overflowY':'scroll'})]), id="trend-trace-collapse")], width=5),
            dbc.Col([
                dbc.Collapse(
                            [dbc.Card([
                                dbc.CardBody(
                                    children=dbc.Table(id='trend-calculations', children=[
                                                html.Thead(
                                                    html.Tr([
                                                        html.Th('Nr.', style={"width": "3%"}),
                                                        html.Th('ST', style={"width": "2%"}),
                                                        html.Th('Trace/Unit system(ST)', style={"width": "30%"}),
                                                        html.Th('Function', style={"width": "20%"}),
                                                        html.Th('Params', style={"width": "45%"}),
                                                        # html.Th('Save', style={"width": "10%"})
                                                        ]),
                                                        style={"position": "sticky", 
                                                            "top": 0, 
                                                            "backgroundColor": "#f8f9fa", 
                                                            "zIndex": 1}
                                                            ),
                                                html.Tbody(id='trend-calculation-table')
                                            ], 
                                                bordered=True,
                                                className="table-sm",
                                                ), style={
                                                    "table-layout": "fixed",
                                                    'padding':'0px',
                                                    'maxHeight': '24vh',
                                                    'overflowY':'scroll'})]),
                            dbc.Card([
                                dbc.CardBody(
                                    children=[
                                        dbc.Row([
                                            dbc.Col([dbc.Input(id='trend-compound-formula', 
                                                            placeholder='Enter formula here', value='', 
                                                            type='text')], width=11),
                                            dbc.Col([html.Div(id='trend-compound-state', children='')], width=1),
                                            ], style={"height": "4vh"})])])], 
                            id="trend-calculation-collapse")], width=7)]),
        dcc.Graph(id='trend-graph', figure=go.Figure(layout=go.Layout(#height=550,
                        margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                        )),
                  config={'scrollZoom': True},
                  responsive=True,
                  style={'height':'74vh'}),                  
        dbc.Row(html.Div(dbc.ButtonGroup([
                    dbc.Button('| <', 
                            id={"type":'trend-slider', "index": 0}, className="mb-3", outline=True, 
                            color="dark", size="md", title="Oldest data"),
                    dbc.Button('<<', 
                            id={"type":'trend-slider', "index": 1}, className="mb-3", outline=True, 
                            color="dark", size="md"),
                    dbc.Button('_|_', 
                            id={"type":'trend-slider', "index": 2}, className="mb-3", outline=True, 
                            color="dark", size="md", title="Show middle data points"),
                    dbc.Button('>>', 
                            id={"type":'trend-slider', "index": 3}, className="mb-3", outline=True, 
                            color="dark", size="md"),
                    dbc.Button('> |', 
                            id={"type":'trend-slider', "index": 4}, className="mb-3", outline=True, 
                            color="dark", size="md", title="Most recent data")]),
                            style={'display': 'flex', 'justify-content': 'center'}), justify='center', align='center',
                  style={'height':'9vh'}),
        html.Div([savePop.layout(), 
                  loadPop.layout(),  
                  deletePop.layout(), 
                  calc_save_popup, 
                  calc_delete_popup])        
    ], fluid=True)

    return layout
# trigger modal object callbacks
deletePop.register_callbacks()
savePop.register_callbacks()
loadPop.register_callbacks()
def paramElement(nr, name, init=0):
    '''Populate dropdown elements for parameter list'''    
    # parameters of string type
    str_params = ['poles', 'zeros', 'Xs', 'Ys']
    element = html.Div(dbc.Row
                    ([
                        dbc.Col(dbc.Label(name), width=6),
                        dbc.Col(dbc.Input(id={"type":"trend-calculation-param-values", 
                                              "index": f'{nr}', 
                                              "param":f'{name}',
                                              "ST":0}, 
                                          type="number" if name not in str_params else "text", 
                                          value=init), width=6)]))
    return element

def paramElementPts(nr, name, pts, value):
    '''Populate nested dropdown for elements in parameter list'''
    element = html.Div(dbc.Row
                       ([
                            dbc.Col(dbc.Label(name), width=2),
                            dbc.Col(dcc.Dropdown(
                                        placeholder = "Select a point for calculation",
                                        options = [{'label': pt, 'value': pt} for pt in pts],
                                        value = value,
                                        id={"type":"trend-calculation-param-values",
                                            "index": f'{nr}',
                                            "param":f'{name}',
                                            "ST":1}), width=10)]),
                        style={'width': '300px'})
    return element

@app.callback(
    Output("trend-trace-collapse", "is_open"),
    [Input("trend-trace-collapse-b", "n_clicks")],
    [State("trend-trace-collapse", "is_open")],
    prevent_initial_call=True
)
def toggle_trace_collapse(n, is_open):
    # Function to handle trace table expand and collapse
    logger.info(currentframe().f_code.co_name)
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("trend-calculation-collapse", "is_open"),
    [Input("trend-calculation-collapse-b", "n_clicks")],
    [State("trend-calculation-collapse", "is_open")],
    prevent_initial_call=True
)
def toggle_calculation_collapse(n, is_open):
    # Function to handle calculation table expand and collapse
    logger.info(currentframe().f_code.co_name)
    if n:
        return not is_open
    return is_open

@app.callback(
    output=[Output('trend-calculation-table', 'children'),
            Output('trend-compound-formula', 'value'),
            Output('trend-loading-update_calculations', 'children')
            ],
    inputs=[
        Input('trend-data-selector', 'value'),
        Input({"type": "trend-calculation-checkbox", "index": ALL}, 'value'),
        # Input('trend-calculation-checkbox-store', 'data'),
        State('trend-compound-formula', 'value'),        
        State({"type":"trend-calculation-params", "index": ALL}, 'label'),
        State('trend-calculation-table', 'children')
    ]
)
def update_calculations(data_name, chckbx_list, formula, calc_label, calc_table):
    '''Populate calculation table:
     - When data selection changes
     - When ST checkbox state changes'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # do not populate when no data table is selected
    if None in [data_name]:
        raise PreventUpdate
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    with database_context_manager(database_pool) as db:    
        # Get the current configuration
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
    # Get the data to extract available tagnames
    df = DATA["main"]
    if df is None:
        raise PreventUpdate
    inputs = list(df.columns)

    # if new data table was selected all invalid selection should be removed
    new_data = data_name != currentTrend['datatable']

    trendCalc = safe_eval_config(currentTrend['calculations'], {}) 
    
    trendCalcTypes = safe_eval_config(currentTrend['calc_type'], []) 
    if new_data:
        # check for points that are available in new data
        for i, calcType in enumerate(trendCalcTypes):
            # if calculation is not used, continue
            if i not in trendCalc:
                continue
            # keeps the calculation if at least one used point is availabel
            # for steam tables check parameters
            if calcType:
                exist = False                    
                for _, pt in trendCalc[i][2].items():
                    # stop searching when first point is found
                    if pt in inputs:
                        exist = True
                # when points are not found, calculation is deleted
                if not exist:
                    del trendCalc[i]
                    # and steam table check reset
                    trendCalcTypes[i] = []
            # for other calculations check input points
            else:
                if trendCalc[i][0] not in inputs:
                    # when points are not found, calculation is deleted
                    del trendCalc[i]                  


        # read data from database, if it exists
        # elements will be cleared when new data table is selected
    loaded_data = {
        'calculations': trendCalc 
                        if currentTrend and currentTrend['calculations'] else {},
        'calc_type': trendCalcTypes
                        if currentTrend and currentTrend['calc_type'] else [],
        'compound':currentTrend['compound'] 
                        if currentTrend and currentTrend['compound'] else ""
                        }
    # option lists for all dropdowns
    inp_optns =  [{"label":input, "value":input} for input in inputs]
    fx_optns =   [{"label":func, "value":func} for func in functions]
    # steam table functions will be grouped based on resulting parameter
    fxST_optns = []
    for ST_option in ST_options:
        # this element is disabled to identify new parameter group
        fxST_optns.append({"label":ST_option, "value":ST_option, 'disabled': True})
        for fxST in STmethods:
            # functions are grouped based on function begining
            if fxST.startswith(ST_options[ST_option]):
                fxST_optns.append({"label":fxST, "value":fxST, 
                                    #adds a tootlip for every function
                                    "title":functionsST[fxST].__doc__.split('\n', 1)[0]})
    # steam table options for EU selection
    ST_optns = [{"label":ST, "value":ST, "title":EU_ST[ST][1]} for ST in EU_ST]

    calculation_table = []
    # get number of updated element if possible
    id = callback_context.triggered[0]['prop_id'].split('.')[0]
    idx = eval(id)['index'] if '{' in  id else None
    # update specific row if it's checkbox was triggered, all selections should be None
    if idx is not None:
        i = idx
        # copy the whole table
        calculation_table = calc_table

        #define nr identifier
        nr = i+1
        # checkbox status
        chkbx = False                
        if chckbx_list[i]:
            chkbx = True

        # checkbox
        checkbox = dcc.Checklist(
            id={"type": "trend-calculation-checkbox", "index": i},
            options=[{"label": "", "value": "selected"}],
            value=["selected"] if chkbx else [],
            inline=True)
        #define input selector
        inp = dcc.Dropdown(
            id={"type": "trend-calculation-input", "index": i},
            options=ST_optns if chkbx else inp_optns,
            value = None,
            style={'white-space':'nowrap'}) # to avoid text wrapping into multiple lines
        #define function selector
        fx = dcc.Dropdown(
            id={"type": "trend-calculation-function", "index": i},
            options=fxST_optns if chkbx else fx_optns,
            value = None)
        #define function parameter selector, 
        # it will populate a dropdown parameter menu if function is selected 
        fx_params = dbc.DropdownMenu(
            id={"type":"trend-calculation-params", "index": i},                    
            label = "Params",
            children = None,
            color='white')
        #define table row
        table_row = html.Tr([
            html.Td(nr),
            html.Td(checkbox, title="Activate for Steam Table calculation"),
            html.Td(inp, title='Select eng. unit system' if chkbx else 'Select function input point'),
            html.Td(fx),
            html.Td(fx_params)
            ],
            style={'vertical-align' : 'middle'})
        # substitute table element
        calculation_table[idx] = table_row
    else:
        # if new data table was selected
        if new_data:
            # clear all calculations and compounds
            calc_label = ["Params"] * calcNr
            
        # populate the whole table
        for i in range(calcNr):
            #define nr identifier
            nr = i+1
            # checkbox status
            chkbx = False                
            if loaded_data['calc_type']:
                if i < len(loaded_data['calc_type']):
                    if loaded_data['calc_type'][i]:
                        chkbx = True
            # checkbox
            checkbox = dcc.Checklist(
                id={"type": "trend-calculation-checkbox", "index": i},
                options=[{"label": "", "value": "selected"}],
                value=["selected"] if chkbx else [],
                inline=True)
            #define input selector
            selection = loaded_data['calculations'][i][0]\
                        if i in loaded_data['calculations'] else None
            inp = dcc.Dropdown(
                id={"type": "trend-calculation-input", "index": i},
                options=ST_optns if chkbx else inp_optns,
                value = selection,
                style={'white-space':'nowrap'}) # to avoid text wrapping into multiple lines
            #define function selector
            selection = loaded_data['calculations'][i][1]\
                        if i in loaded_data['calculations'] else None
            fx = dcc.Dropdown(
                        id={"type": "trend-calculation-function", "index": i},
                        options=fxST_optns if chkbx else fx_optns,
                        value = selection
                        )
            #define function parameter selector, 
            # it will populate a dropdown parameter menu if function is selected 
            if chkbx:
                parameters = inspect.signature(functionsST[selection]).parameters if selection!=None else None
                
                elements = [paramElementPts(i, name, inputs, 
                                        loaded_data['calculations'][i][2][name]) 
                                        for name, _ in parameters.items()] if selection!=None else None
            else:
                parameters = inspect.signature(functions[selection]).parameters if selection!=None else None
                elements = [paramElement(i, name, loaded_data['calculations'][i][2][name] 
                                         if name in loaded_data['calculations'][i][2] else '[]')
                            for name in parameters if name != 'input'] if selection!=None else None
            fx_params = dbc.DropdownMenu(
                id={"type":"trend-calculation-params", "index": i},                    
                label = calc_label[i] if i < len(calc_label) else "Params",
                children=elements,
                color='white'
                #update to load value upon trend app loading, otherwise do not populate
                )
                                
            #define table row
            table_row = html.Tr([
                html.Td(nr),
                html.Td(checkbox, title="Activate for Steam Table calculation"),
                html.Td(inp, title='Select eng. unit system' if chkbx else 'Select function input point'),
                html.Td(fx),
                html.Td(fx_params)
                ],
                style={'vertical-align' : 'middle'})

            #stack them in table
            calculation_table.append(table_row)
    # load compound formula
    compound_formula = loaded_data['compound']
    return calculation_table, compound_formula, no_update


@app.callback(
    output=[Output({"type":"trend-calculation-params", "index": ALL}, 'children'),
            Output('trend-loading-update_fx_params', 'children')
            ],
    inputs=[
        Input({"type": "trend-calculation-function", "index": ALL}, 'value')
    ],
    state=[State({"type":"trend-calculation-params", "index": ALL}, 'children'),
           State('trend-data-selector', 'value'),
           State({"type": "trend-calculation-checkbox", "index": ALL}, 'value')],
    prevent_initial_call=True
)
def update_fx_params(fxs, params, data_name, calcChk):
    '''Update calculation parameter list for the affected calculation row'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # get calculation row number    
    id = callback_context.triggered [0]['prop_id'].split('.') [0]
    if not id:
        raise PreventUpdate
    # get function parameters    
    nr = eval(id)['index']
    #get the function
    fx = fxs[nr]
    # check if change is actual and not a simple load of application
    with database_context_manager(database_pool) as db:
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
        calculations = safe_eval_config(currentTrend['calculations'], {})
    if nr in calculations and calculations[nr][1] == fx:
        raise PreventUpdate
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    # check if calculation is steam table, use appropriate dictionary
    if not calcChk[nr]:
        if fx in list(functions.keys()):
            #get parameter list
            parameters = inspect.signature(functions[fx]).parameters
            #get parameter dictionary with default values
            paramDict = {}
            for name, parameter in parameters.items():
                if parameter.default != inspect.Parameter.empty:
                    paramDict[name] = parameter.default
            #populate parameter dropdown elements, clear dropdown when function is deselected
            elements = [paramElement(nr, name, paramDict[name]) for name in paramDict]

        else: #function was deselected, clear arams elements
            elements = None
    else:
        if fx in STmethods:
            #get parameter list
            parameters = inspect.signature(functionsST[fx]).parameters

            # get data table
            df = DATA["main"]
            # get point list
            ptlist = list(df.columns)
            elements = [paramElementPts(nr, name, ptlist, "") 
                                        for name, _ in parameters.items()]

        else: #function was deselected, clear arams elements
            elements = None
    #replace existing parameter list with updated elements
    params[:] = [no_update]*len(params)
    params[nr] = elements
    return params, no_update

def getSQLData(calcParams, currentTrend, db, user):
    '''Function read only relevant SQL data'''
    # get relevant point list for all calculations at once
    pts = []
    for calc in calcParams:
        params = calcParams[calc]
        if params[0] in EU_ST:
            pts += list(params[2].values())
        else:
            pts.append(params[0])
    # also include DateTime column
    pts.append('DateTime')
    # only read the relevant data    
    # time range filter for SQL
    where_query = {}
    if currentTrend['timeStart'] and currentTrend['timeStop']:
        where_query = {'DateTime':{0:(currentTrend["timeStart"]),
                                    1:(currentTrend["timeStop"])}}
    # logger.info(f'{currentTrend["datatable"]}')
    data = db.getData(user, currentTrend["datatable"], sel_query = pts,
                      where_query = where_query)
    return data

def performCalc(params, df):
    '''Function takes parameters and source dataframe and 
    performs either Steam Table or timeseries calculation'''
    result = None
    fault = False
    param_values = list(params[2].values())
    if params[0] in EU_ST:
        if '' not in param_values:# only when all steam table parameters are defined
            # get method using selected unit system, params contains the following information:
            # 0 - unit system
            # 1 - function
            # 2 - points to be used in the function            
            func = getattr(XSteam(EU_ST[params[0]][0]), params[1])
            # calculate and evaluate if no invalid values are present
            result = df[param_values].apply(lambda row: func(*row), axis=1)
            # when any invalid values are present, nan values are replaced with fault number, 
            # fault of calculation is chagned to True
            if result.isna().any():
                fault = True
                result.fillna(0.999999, inplace=True)   
    else:
        result = functions[params[1]](df[params[0]], *param_values)
            # when any invalid values are present, nan values are replaced with fault number, 
            # fault of calculation is chagned to True
        if np.isnan(result).any() or np.max(result) > 1e9:
                fault = True
                result = np.where(np.isnan(result), 0, result)
                # result.fillna(0.999999, inplace=True) 
    return result, fault

@app.callback(
    Output('zoom-store', 'data'),    
    Output('trend-show-min', 'value'),
    Output('trend-loading-update_zoom', 'children'),
    Input('trend-graph', 'selectedData'),#trigger for zoom change
    Input('zoom-reset-trend-b', 'n_clicks'),
    Input({"type":'trend-slider', "index": ALL}, 'n_clicks'),
    Input('trend-data-selector', 'value'),
    State('trend-show-min', 'value'),
    State('zoom-store', 'data'),
    # prevent_initial_call=True
)
def update_zoom(selection, n1, slider, data_name, window, store):
    '''When all information for graph is available call update graph'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trendParameters = {}
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    logger.debug(id)
    with database_context_manager(database_pool) as db:
        # get current loop data
        currTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
    currZoom = {'timeStart':currTrend['timeStart'], 'timeStop':currTrend['timeStop']}
    logger.debug(f'Current trend: {currTrend}')
    # get data and data range
    df = DATA["main"]    
    # get data range of the whole data table
    ranges = {'DateTime':(DATA['main'].index[0], DATA['main'].index[-1])}
    # update data range in current trend
    # check if any previous selection does not exists,    
    # "measure" duration of 600 samples
    if not window:
        # but previous selection exists
        if currTrend["timeStart"] and currTrend['timeStop']:
            window = np.ceil((currTrend['timeStop'] - 
                                currTrend['timeStart']).total_seconds()/60)
        else: # else default window is equivalent to 600 data samples
            currTrend['timeStart'] = df.index[0]
            currTrend['timeStop'] = df.index[600 if len(df.index) > 601 else len(df.index)-1] 
            # calculate windown from default seletion         
            window = np.ceil((currTrend['timeStop'] - 
                                        currTrend['timeStart']).total_seconds()/60)
    #daterange reset, no df filtering
    if id =='zoom-reset-trend-b' or \
        ('datatable' in currTrend and currTrend['datatable']!=data_name):
        # load new data
        with database_context_manager(database_pool) as db:
            df = db.getData(current_user.username, data_name, numRows=1e9).sort_index(ascending=True)
        # save data to parquet
        save_data(df, "main", current_user.username)
        # default window is equivalent to 600 data samples
        trendParameters['timeStart'] = df.index[0]
        trendParameters['timeStop'] = df.index[600 if len(df.index) > 601 else len(df.index)-1] 
        # calculate windown from default seletion
        window = np.ceil((trendParameters['timeStop'] - 
                            trendParameters['timeStart']).total_seconds()/60)
    #daterange zoom
    elif id=='trend-graph' and selection:
        xRange = getXRange(selection)
        trendParameters['timeStart'] = xRange[0]
        trendParameters['timeStop'] = xRange[1]
        # calculate windown from seletion
        window = np.ceil((trendParameters['timeStop'] - 
                            trendParameters['timeStart']).total_seconds()/60)
    #daterange no change, filter using existing data
        
    elif "trend-slider" in id: #when slider button was activated
        idx_slider = eval(id)['index']
        # get all timestamps
        if idx_slider==0:#get window of oldest data
            # use first data sample time as a start time                
            trendParameters['timeStart'] = ranges['DateTime'][0]
            # offset it by window duration and limit not to exceed available data
            trendParameters['timeStop'] = min(ranges['DateTime'][1], trendParameters['timeStart'] + 
                                                DateOffset(minutes=window))
        
        elif idx_slider==1:#scroll half a window to an older data
            # get midpoint of current window, it will be a new end point, limit it not to exceed available data
            trendParameters['timeStart'] = max(ranges['DateTime'][0], currTrend['timeStart'] - 
                                                DateOffset(minutes=window/2))
            # calculate new start point by substracting window from end point
            trendParameters['timeStop'] = min(ranges['DateTime'][1], trendParameters['timeStart'] + 
                                                DateOffset(minutes=window))

        elif idx_slider==2:#get data window from the middle
            # get middle datetime
            # # use current window to calculate
            if currTrend['timeStart'] and currTrend['timeStop']:
                middle = currTrend['timeStart'] + (currTrend['timeStop'] - currTrend['timeStart'])/2
            else:#use all data to calculate
                middle = df.index.to_series().median()
            # calculate start and stop, limiting added not to exceed data range
            trendParameters['timeStart'] = max(ranges['DateTime'][0], middle -  
                                            DateOffset(minutes=window/2))                
            trendParameters['timeStop'] = min(ranges['DateTime'][1], middle +  
                                            DateOffset(minutes=window - window/2))
            
        elif idx_slider==3:#scroll half a window to a newer data
            # get midpoint of current window, it will be a new start point, limit it not to exceed available data
            trendParameters['timeStop'] = min(ranges['DateTime'][1], currTrend['timeStop'] + 
                                                DateOffset(minutes=window/2))
            # calculate new start point by substracting window from end point
            trendParameters['timeStart'] = max(ranges['DateTime'][0], trendParameters['timeStop'] - 
                                                DateOffset(minutes=window))

        else:#get window of newest data
            # use the last data sample time as a stop time
            trendParameters['timeStop'] = ranges['DateTime'][1]
            # offset it by window duration
            trendParameters['timeStart'] = max(ranges['DateTime'][0], trendParameters['timeStop'] - 
                                                DateOffset(minutes=window))
    
    else:# check if start and stop dates are withing data range
        # get the full data range to check if start date is within it
        if currTrend['timeStart'] is None or currTrend['timeStart'] < ranges['DateTime'][0] or\
                currTrend['timeStart'] > ranges['DateTime'][1]:                  
            trendParameters['timeStart'] = ranges['DateTime'][0]
            trendParameters['timeStop'] = min(ranges['DateTime'][1], trendParameters['timeStart'] + DateOffset(minutes=window))
            
    
    # update currTrend with new parameter values
    currTrend.update(trendParameters) 
    # recalculate window to avoid too big value
    window = np.ceil((currTrend['timeStop'] - 
                    currTrend['timeStart']).total_seconds()/60)
        # update current trend
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'trends', currTrend)
    # update store to trigger other events
    return currZoom, window, no_update

@app.callback(
    output = [Output({"type":"trend-calculation-params", "index": ALL}, 'label'),
              Output({"type":"trend-calculation-params", "index": ALL}, 'color'),
              Output('trend-loading-update_fx_param_label', 'children')
            #   Output('trend-show-min', 'value')
            ],
            # ST attribute below is a flag indicating what triggers a callback
    inputs=[Input({"type": "trend-calculation-param-values", "index": ALL, "param": ALL, "ST": 0}, 'n_submit'),
            Input({"type": "trend-calculation-param-values", "index": ALL, "param": ALL, "ST": 1}, 'value'),
            Input({"type": "trend-calculation-input", "index": ALL}, 'value'),            

            ],
    state=[State({"type":"trend-calculation-params", "index": ALL}, 'label'),
           State('trend-data-selector', 'value'),
           State({"type": "trend-calculation-function", "index": ALL}, 'value'),
           State({"type": "trend-calculation-checkbox", "index": ALL}, 'value'),
           [State({"type":"trend-calculation-param-values", "index": str(idx), "param": ALL, "ST": ALL}, 'value') 
            for idx in range(calcNr)],
           ],
           prevent_initial_call=True
)
def update_fx_param_label(*args):
    '''Update calculation parameter dropdown label and store calculation definition and it's result into database,
    callback is triggered by:
    - Function parameter change
    - Input or Eng. unit system selection change
    - Data zoom selection change
    - Trend slide control
    If it is clear which calculation row was modified, only that calculation should be updated
    '''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data
    #this unpacks arguments
    calcPts = args[2] #calculation points

    act_labels = args[3]# actual labels 
    data_name = args[4] #data table selection
    calcFxs = args[5] #calculation functions
    calcChk = args[6] #calculation checkbox
    calcFxPar = args[7:] #calculation function parameters 
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]

    # get all calculations into dictionary
    calcs_sel = {}    
    # initialize labels list
    labels = ['Params'] * calcNr
    # update label color if calculation is invalid
    # initialize color list
    colors = ['white'] * calcNr

    # get all calculations into dictionary
    for i, fx in enumerate(calcFxs):
        # store only if all attributes are provided
        if not None in [fx, calcPts[i], calcFxPar[i]]:
            # check only if function exists
            funcs = functionsST if calcChk[i] else functions
            # funcs.update(functionsST)
            if fx in funcs:
                parameters = inspect.signature(funcs[fx]).parameters
                #get parameter name list, only take parameters with default values
                paramList = []
                for name, parameter in parameters.items():
                    # store all parameters for steam tables
                    if parameter.default != inspect.Parameter.empty or calcChk[i]:
                        paramList.append(name)
                calcs_sel[i] = (calcPts[i], fx, dict(zip(paramList, calcFxPar[i])))
                # update corresponding label
                labels[i] = ", ".join([f'{name}: {value}' for name, value in zip(paramList, calcFxPar[i])])
        else:
            # remove calculation from global data
            calc = f'C{i+1}'
            if "trend_data" in DATA and calc in DATA["trend_data"].columns:
                DATA["trend_data"].pop(calc)

    # store changes to DB, complex data types are converted to string,
    # reverse convertion is performed using eval() function
    data = {
        'calculations':str(calcs_sel),
        'calc_type':str(calcChk)
    }
    # get all indexes of trigger items
    idxs = []
    # iterate through all items that triggered the callback
    for trig in callback_context.triggered:        
        try:
            # find the index of the item
            item = eval(trig['prop_id'].split('.') [0])['index']
            # add to the list if does not exist
            if item not in idxs:
                idxs.append(item)
        except:
            continue
    with database_context_manager(database_pool) as db:
        # store configuration into database
        db.updateAttr(current_user.username, 'trends', data)
        # get currnt configuration 
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
        #update data range in current trend
    # logger.info(currentTrend)

    # perform calculations
    temp_data = 'trend_data'
    # get the data
    df = DATA["main"]
    df1 = None

    # data table name update to ensure calculation when new data is selected
    currentTrend['datatable'] = data_name
    # update only affected calculation
    if len(idxs) == 1:
        # find affected calculation
        idx = int(idxs[0])

        # # create empty table if temp_data does not exist
        if temp_data not in DATA:
            # get DateTime to get the index
            df1 = pd.DataFrame(index = df.index)
        else: 
            df1 = DATA[temp_data]
            # remove modified column
            cols = df1.columns.to_list()
            if f'C{idx+1}' in cols:
                df1.pop(f'C{idx+1}')
        # color update
        colors = [no_update] * calcNr

        # newly create if it is fully qualified
        if idx in calcs_sel:
            params = calcs_sel[idx]

            # only read the relevant data
            result, fault = performCalc(params, df)
            # change label color when calculation returns
            # faulty results
            if fault:
                colors[idx] = 'pink'
            else:                
                colors[idx] = 'white'
            try:
                df1[f'C{idx+1}'] = result
            except:
                # if dataframe does not exist, create it
                df1 = pd.DataFrame(index = df.index)
                df1[f'C{idx+1}'] = result
        
        # update only relevant label
        labels_temp = labels.copy()
        labels = [no_update] * calcNr
        # if label has not changed from Params, do not update it
        if not "Params" == act_labels[idx] == labels_temp[idx]:
            labels[idx] = labels_temp[idx]

    # update all calculation
    else:

        if len(calcs_sel)>0:
            # get DateTime to get the index and create a table
            df1 = pd.DataFrame(index = df.index)      
        # iterate through fully specified calculations            
            for calc in calcs_sel:
                params = calcs_sel[calc]
                # only read the relevant data
                result, fault = performCalc(params, df)
                # change label color when calculation returns
                # faulty results
                if fault:
                    colors[calc] = 'pink'
                else:                
                    colors[calc] = 'white'
                df1[f'C{calc+1}'] = result

    # store results in a temporary table
    if df1 is None:
        df1 = pd.DataFrame(index = df.index)
    save_data(df1, temp_data, current_user.username)
    return labels, colors, no_update

@app.callback(
    output=[Output('trend-compound-state', 'children'),
            Output('trend-loading-evaluate_compound_calculation', 'children')],
    inputs=[Input({"type":"trend-calculation-params", "index": ALL}, 'label'),
            Input('trend-compound-formula', 'n_submit'),
            Input('trend-data-selector', 'value')],
    state=[State('trend-compound-formula', 'value')],
    prevent_initial_call=True
)
def evaluate_compound_calculation(labels, submit, data_name, formula):
    '''Perform compound calculation when:
    - Calculation has been updated and it is used in the compound formula (includes zoom window update)
    - Compound formula was updated
    - Data selection was modified'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')    
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data
    # find affected calculations
        # get all indexes of trigger items
    idxs = []
    # do not proceed if compound formula is empty
    if not formula and ("trend_data" not in DATA or "Compound" not in DATA['trend_data'].columns):
        return no_update, no_update
    
    # iterate through all items that triggered the callback to collect indexes

    # when calculations are updated, update compound
    # only if affected calculations are used in compound calculation

    # get current trend configuration
    with database_context_manager(database_pool) as db:
        # load trend configuration
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
    # check if data selection is new        
    new_data = currentTrend['datatable'] != data_name
    # create filter for data range, this will be used for data filtering in SQL
    # copy data to avoid modifying original data
    df = DATA["main"].copy()
    # get all trend temporary data
    if 'trend_data' in DATA:
        df1 = DATA["trend_data"]
    else:
        df1 = pd.DataFrame(index = df.index)

    # by default compound formula is empty
    calc_data={}
    calc_data['compound'] = ""
    # only perform compound calculation when formula is not empty and data selection is not new
    try:
        # when new data table is selected, only delete temporary calcualtion table
        # if no calculations are present (indicated by label)
        if new_data and all(labels) == 'Params':
            # pass
            # delete calculations table if it exists

            # reinitialize temporary data table
            df1 = None
            DATA.pop("trend_data")
        else:
            if formula != "":
                
         
                
                # give data table column names symbolic tags, Ts and Cs
                traces = {name:f'T{i+1}' for i, name in enumerate(list(df.columns))}
                traces2 = {f'T{i+1}':name for i, name in enumerate(list(df.columns))}

                # get list of formula indices
                indices = re.findall(r'[A-Z]+\d*', formula)
                
                # get list of points to read from SQL
                pts = [v for k, v in traces2.items() if k in indices]
                # rename df column to get formula indices
                df.rename(columns=traces, inplace=True)

                # Get and join the temporary trend data
                # concat datasets
                df = pd.concat([df, df1], axis=1)

                # mapping for variables
                variables = {column: df[column] for column in df.columns.to_list() if column in indices}
                # perform compound calculation          
                df1['Compound'] = eval(formula, {}, variables)
                # store compound formula if evaluation was a success
                calc_data['compound'] = formula
            # remove compound trace when formula is cleared
            else:
                # remove compound trace from temporary trend data table
                if 'Compound' in df1.columns:
                    df1.pop('Compound')
        # store results in a temporary table
        if df1 is None:
            df1 = pd.DataFrame(index = df.index)
        save_data(df1, "trend_data", current_user.username)
        
        # store compound formula
        with database_context_manager(database_pool) as db:
            db.updateAttr(current_user.username, 'trends', calc_data)
        # use compound calculation status to trigger graph update   
        if calc_data['compound'] == "":                
            return html.Div(""), no_update
        else:            
            return html.Div("OK", style={"color": "green"}), no_update
    except Exception:#failed to evaluate or empty formula
        # traceback.print_exc()           
        # store compound formula
        with database_context_manager(database_pool) as db:
            db.updateAttr(current_user.username, 'trends', calc_data)
        
        # when there is a failure then compound trace should be removed
        if df1 is not None:
            if 'Compound' in df1.columns:
                df1.pop('Compound')
                # delete is not needed because if_exist flag is used inside putData
        # store results in a temporary table
        if df1 is None:
            df1 = pd.DataFrame(index = df.index)
        save_data(df1, "trend_data", current_user.username)
        # use compound calculation status to trigger graph update
        if formula != "":
            return html.Div("FAIL", style={"color": "red"}), no_update
        else:
            return html.Div(""), no_update


@app.callback(
    [Output("delete-calc-pop-up", "is_open"),
    Output('delete-calc-in', 'options'),
    Output('delete-calc-in', 'value'),
    Output('trend-store', 'data'),
    Output('trend-loading-delete_calc', 'children')
    ],
    [Input("delete-calc-b", "n_clicks"), Input('delete-calc-cancel', "n_clicks"), 
     Input('delete-calc-delete', 'n_clicks')],
    [State("delete-calc-pop-up", "is_open"), 
     State('delete-calc-in', 'value'), 
     State('trend-data-selector', 'value'),
     State('trend-store', 'data') ],
    prevent_initial_call=True
)
def delete_calc(n1, n2, n3, is_open, name, data_name, store):
    ''' Delete previously saved calculation.'''
    logger.info(currentframe().f_code.co_name)
    # do not excecute without user action
    if not(n1 or n2 or n3):
        raise PreventUpdate
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    
    # update for calculation point lists
    callback_id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    # when delete button is clicked
    if n3 and callback_id == 'delete-calc-delete':
        if name == "":
            raise PreventUpdate
        with database_context_manager(database_pool) as db:
            # get saved data, all rows
            df = DATA["main"]
            # remove data column
            df.pop(f'{name}')
            # rewrite data table
            try:
                # delete is not needed because if_exist flag is used inside putData
                # db.deleteData(current_user.username, data_name)                
                # store results in a data table
                db.putData(current_user.username, df, data_name)
                # update parquet temporary data table
                save_data(df, "main", current_user.username)
            except:
                print("Failed to save calculation")
            
            # check if modal should be closed
            # modal is closed when no more saved calcs exist
            if len(list(df.columns))<1:
                return not is_open, no_update, no_update, store, no_update
    
    with database_context_manager(database_pool) as db:
        # get trend table row, to get column names
        df = DATA["main"]

        # populate list items and default selection
        attributeList = [{'label': col_name, 
                            'value': col_name} 
                            for col_name in df.columns.to_list()]
        selection = ""
        if len(attributeList)>0:
            selection = attributeList[0]['value']

    if (n1 or n2) and callback_id in ['delete-calc-b', 'delete-calc-cancel']:      
        return not is_open, attributeList, selection, store, no_update
    # store increment will trigger the point list and trace table update
    return is_open, attributeList, selection, store+1, no_update

@app.callback(
    [Output("save-calc-pop-up", "is_open"),
     Output('save-calc-in', 'options'),
     Output('save-calc-in', 'value'),
     Output('trend-store', 'data'),
     Output('trend-loading-save_calc', 'children')
    ],
    [Input('save-calc-b', "n_clicks"), Input('save-calc-cancel', "n_clicks"), 
     Input('save-calc-save', 'n_clicks')],
    [State("save-calc-pop-up", "is_open"), 
     State('save-calc-name-in', 'value'),
     State('save-calc-in', 'value'), 
     State('trend-data-selector', 'value'),
     State('trend-store', 'data')],
    prevent_initial_call=True
)
def save_calc(n1, n2, n3, is_open, name, calc, data_name, store):
    '''Saves selected calculation'''
    logger.info(currentframe().f_code.co_name)
    # do not excecute without user action
    if not(n1 or n2 or n3):
        raise PreventUpdate   
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data
     
    #check which button was clicked
    callback_id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    # when save button is clicked
    if n3 and callback_id == 'save-calc-save':
        # do not update if name is not specified or 
        # calculation to be saved is not selected
        if None in [name, calc]:
            raise PreventUpdate
        # calculations are already saved in pandas dataframe therefore no need to recalculate
        df = DATA["main"]
        if calc in DATA["trend_data"].columns:
            df[name] = DATA["trend_data"][calc]
        
            with database_context_manager(database_pool) as db:
                # update SQL table
                db.putData(current_user.username, df, data_name)
                # update parquet temporary data table
                save_data(df, "main", current_user.username)
  

    attributeList = []
    selection = ""
    if 'trend_data' in DATA:
        df1 = DATA["trend_data"]
        if len(list(df1.columns))>0:
            # populate list items and default selection
            attributeList = [{'label': col_name, 'value': col_name} 
                            for col_name in df1.columns.to_list()]
            selection = ""
            if len(attributeList)>0:
                selection = attributeList[0]['value']
    # toggle popup status and update dropdown
    if (n1 or n2) and callback_id in ['save-calc-b', 'save-calc-cancel']:    
        return not is_open, attributeList, selection, store, no_update
    # store increment will trigger the point list and trace table update
    return is_open, attributeList, selection, store+1, no_update


@app.callback(
    [Output({"type": 'trend-calculation-input', "index": ALL}, 'options'),
     Output('trend-loading-update_pts_lists', 'children')
    ],
    [Input('trend-store', 'data')],
    [ 
     State('trend-data-selector', 'value'),
     State({"type": 'trend-calculation-input', "index": ALL}, 'options'),
     State({"type": 'trend-calculation-checkbox', "index": ALL}, 'value')],
    prevent_initial_call=True
)
def update_pts_lists(store, data_name, calc_pts, calc_chkbx):
    ''' This callback updates calculation point lists after saving or 
    deleting a calculation or trace.'''
    logger.info(currentframe().f_code.co_name)
    # do not execute this update during initial callback
    if store == 0:
        raise PreventUpdate
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    # with database_context_manager(database_pool) as db:
    # get saved data, columns
    df = DATA["main"]

    # populate list items and default selection
    attributeList = [{'label': col_name, 
                        'value': col_name} 
                        for col_name in df.columns.to_list()]

    for i, _ in enumerate(calc_chkbx):
        calc_pts[i] = attributeList      
    return calc_pts, no_update

@app.callback(
    Output('trend-data-selector', 'value'),
    Output('trend-loading-post_load_trend', 'children'),
    Input(loadPop.store_id, 'data'),
    prevent_initial_call=True
)
def post_load_trend(store):
    logger.info(currentframe().f_code.co_name)    
    # do not execute this callback if it was not triggered
    if callback_context.triggered[0]['prop_id'].split('.')[0] != loadPop.store_id:
        raise PreventUpdate
        
    with database_context_manager(database_pool) as db:
        data = db.getData(current_user.username, store['datatable'], numRows=1e9).sort_index(ascending=True)
        save_data(data, "main", current_user.username)        
    return store['datatable'], no_update

@app.callback(
    output=[Output('trend-trace-table', 'children'),
            Output('trend-loading-update_traces', 'children')
    ],
    inputs=[
        Input('trend-data-selector', 'value'),
        Input('trend-store', 'data'),
        Input({"type":"trend-calculation-params", "index": ALL}, 'label'),
        Input('trend-compound-state', 'children'),
        Input('zoom-store', 'data'),
    ]
)
def update_traces(data_name, store, calc_table, compound_state, zoom_store):
    '''Populate trace table when any of the following events happen:
     - Trend data selection changes
     - Traces are deleted
     - Traces are saved
     - Calculations are modified
     - Compound calculation state changes'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # do not populate trace table when no data table is selected
    if None in [data_name]:
        raise PreventUpdate

    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data
    with database_context_manager(database_pool) as db:    
        # Get the current configuration
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
        # Get the data row, to get point list
    # check if zoom window was changed, do not update if not
    if not isinstance(zoom_store, dict) or zoom_store['timeStart'] == None:
        start_time = currentTrend['timeStart']
    else:
        start_time = datetime.strptime(zoom_store['timeStart'], '%Y-%m-%dT%H:%M:%S.%f' if '.' in zoom_store['timeStart'] else '%Y-%m-%dT%H:%M:%S')
    if not isinstance(zoom_store, dict) or zoom_store['timeStop'] == None:
        stop_time = currentTrend['timeStop']
    else:
        stop_time = datetime.strptime(zoom_store['timeStop'], '%Y-%m-%dT%H:%M:%S.%f' if '.' in zoom_store['timeStop'] else '%Y-%m-%dT%H:%M:%S')
    # prevent update if zoom was not changed
    if id == 'zoom-store' and start_time == currentTrend['timeStart'] and \
        stop_time == currentTrend['timeStop']:
        raise PreventUpdate

        
    # get ranges for traces
    df = DATA['main'].loc[currentTrend['timeStart']:currentTrend['timeStop']]
    ranges = {col: (df[col].min(), df[col].max()) for col in df.columns}
    ranges0 = ranges.copy()

    loaded_data = {
        'traces': list(eval(currentTrend['checkbox']).keys()) if currentTrend['checkbox'] else [],
        'checkbox':eval(currentTrend['checkbox']) if currentTrend['checkbox'] else {},
        'yaxis':eval(currentTrend['yaxis']) if currentTrend['yaxis'] else {},
        'color':eval(currentTrend['color']) if currentTrend['color'] else {}
    }            

    # if new data table was selected all selection should be removed
    new_data = data_name != currentTrend['datatable']
    # when new data file is selected, check if it is possible to reuse existing configuration
    if new_data:
        # check if all used points are in the current data file
        reuse = True
        for tag, check in loaded_data['checkbox'].items():
            if check:#check only if checkbox is selected
                # check if tag exists in datatable
                if tag not in ['C1','C2','C3','C4','C5','C6','Compound'] and tag not in ranges0:
                    reuse = False
                    break
        # update new_data flag
        new_data = new_data and not reuse

    # this is the last "new_data" check - store data selection in the database
    data = {}
    data['datatable'] = data_name
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'trends', data)
    
        # Get the temporary data
        try:
            # load temporary data and merge it with uploaded data
            if not new_data:
                
                # get ranges for calculations
                df1 = DATA["trend_data"].loc[currentTrend['timeStart']:currentTrend['timeStop']]
                ranges1 = {col: (df1[col].min(), df1[col].max()) for col in df1.columns}
                # ranges1.pop('DateTime')
                # merge all ranges
                ranges.update(ranges1)
            else:
                # remove temporary data when new input data was selected
                DATA.pop("trend_data")
        except:
            pass
    loaded_data = {
        'traces': list(eval(currentTrend['checkbox']).keys()) if currentTrend['checkbox'] else [],
        'checkbox':eval(currentTrend['checkbox']) if currentTrend['checkbox'] else {},
        'yaxis':eval(currentTrend['yaxis']) if currentTrend['yaxis'] else {},
        'color':eval(currentTrend['color']) if currentTrend['color'] else {}
    }
    # trace_colors
    colors = px.colors.qualitative.Dark24 * 20 #repeat the same 24 colors 20 times, to accomodate 480 traces

    #populate trace table rows with axis and color pickers
    trace_table = []
    # it is neccessary to ensure that yaxis index does not exceed trace 
    # count to avoid graph update errors

    # iterate through all traces and reassign y axis index 
    # if trace is no more available
    yaxis_sel = loaded_data['yaxis']
    # get highest value    
    n = len(ranges)
    # map for multiple axis
    map = {}
    # if point was removed store spare values
    used_values = [yaxis_sel[key] for key in yaxis_sel if key in ranges]

    # get list of unused values for new traces    
    posible_values = list(set(range(1, n+1)) - set(used_values))

    # if point y axis index is too high
    if posible_values:
        for key in yaxis_sel:
            if yaxis_sel[key] > n and yaxis_sel[key] in used_values:
                if yaxis_sel[key] not in map:
                    if len(posible_values)>0:
                        map[yaxis_sel[key]] = posible_values.pop(0)
                yaxis_sel[key] = map[yaxis_sel[key]]
        
    # flag for horizontal ruler
    ruler = False

    # y axis options
    yaxis_optns = [{"label":num+1, "value":num+1} for num in range(n)]
    for i, column in enumerate(ranges):
        
        #value range string
        value_range = f' {round(ranges[column][0], 3)} - {round(ranges[column][1], 3)}'

        # when loading the first time, only the first 10 checkboxes should be set
        checkbox0 = []
        if not any(list(loaded_data['checkbox'].values())) or new_data:
            if i < 1:
                checkbox0 = ["selected"]
            # automatically select new calculations for graph
        elif ranges0 != ranges:
            if column in list(ranges1):
                checkbox0 = ["selected"]


        #define checkbox element
        checkbox = dcc.Checklist(
            id={"type": "trend-trace-checkbox", "index": column},
            options=[{"label": "  " + str(i+1), "value": "selected"}],
            value=loaded_data['checkbox'][column] if column in loaded_data['checkbox'] and not new_data else checkbox0,
            inputStyle={"width": "15px", "height": "15px"}
            )
        #define yaxis selector
        # actual value selection
        if new_data:
            # numbers in sequence when new data is selected
            value = i+1 
        elif column in loaded_data['traces']:
            # previous selection is exists
            value = loaded_data['yaxis'][column]
        else:
            # first unused value
            value = posible_values.pop(0)
        
        yaxis = dcc.Dropdown(
                    id={"type": "trend-trace-yaxis", "index": column},
                    options=yaxis_optns,
                    value = value,
                    style={'width': '75%'},
                    clearable=False
                    )
        #define color picker element
        color_picker = dbc.Input(
            type='color',
            id={"type":"trend-trace-color", "index": column},
            value=loaded_data['color'][column] if column in loaded_data['traces'] and not new_data else colors[i],
            style={"width": 40, "height": 30}
        )

        # add horizontal ruler for elements that are calculated
        if ranges0 != ranges:
            if column in ranges1 and not ruler:
                # set ruler to only add once
                ruler = True
                #define table rulerrow
                table_row = html.Tr([
                    html.Td(html.Hr()),
                    html.Td(html.Hr()),
                    html.Td(html.Hr()),
                    html.Td(html.Hr()),
                    html.Td(html.Hr())
                    ],
                    style={'vertical-align' : 'middle'})

                #stack them in table
                trace_table.append(table_row)

        
        #define table row
        table_row = html.Tr([
            html.Td(checkbox),
            html.Td(column),
            html.Td(value_range),
            html.Td(yaxis),
            html.Td(color_picker)
            ],
            style={'vertical-align' : 'middle'})

        #stack them in table
        trace_table.append(table_row)
    return trace_table, no_update

@app.callback(
    output=[Output('trend-graph', 'figure'),
            Output('trend-loading-update_graph', 'children')
            ],
    inputs=[
        # here params instead of label are checked, 
        # this ensures that point change also updates the graph
        Input({'type': 'trend-trace-checkbox', 'index': ALL}, 'value'),        
        Input({'type': 'trend-trace-color', 'index': ALL}, 'value'),        
        Input({'type': 'trend-trace-yaxis', 'index': ALL}, 'value'),
        State('trend-data-selector', 'value'),        
        State({"type": 'trend-calculation-params', "index": ALL}, 'label'),]
)
def update_graph(*args):
    '''Populate the scatter plots'''
    logger.info(f'{currentframe().f_code.co_name}{"<<" + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # store callback arguments
    checkboxSel = args[0]
    colorSel = args[1]
    yaxisSel = args[2]
    data_name = args[3]

    # calculations are optional therefore only trace inputs are checked
    if None in [data_name] or not checkboxSel or not colorSel or not yaxisSel:
        raise PreventUpdate
    # load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data
    # get all checkboxes
    # only take value elements as inps
    inps = [eval('.'.join(dic.split('.') [:-1])) for dic in list(callback_context.inputs.keys())
            if dic.endswith('.value')]
    # update only relevant data
    data = {}
    # store changes to DB, complex data types are converted to string,
    # reverse convertion is performed using eval() function
    # checkbox selection will only be updated when checkbox triggers the callback
    if 'trend-trace-checkbox' in str(callback_context.triggered):
        checkboxes_ids = [id['index'] for id in inps
                          if id['type'] == 'trend-trace-checkbox']
        checkbox_sel = {idx:check for idx, check in zip(checkboxes_ids, checkboxSel)}

        data['checkbox'] = str(checkbox_sel)
    
    # color selection will only be updated when color selection triggers the callback
    if 'trend-trace-color' in str(callback_context.triggered):
        color_sel_ids = [id['index'] for id in inps
                         if id['type'] == 'trend-trace-color']
        color_sel = {idx:color for idx, color in zip(color_sel_ids, colorSel)}

        data['color'] = str(color_sel)
    
    # axis selection will only be updated when axis selection triggers the callback    
    if 'trend-trace-yaxis' in str(callback_context.triggered):
        yaxis_sel_ids = [id['index'] for id in inps
                         if id['type'] == 'trend-trace-yaxis']
        yaxis_sel = {idx:yaxis for idx, yaxis in zip(yaxis_sel_ids, yaxisSel)}

        data['yaxis'] = str(yaxis_sel)
           
    with database_context_manager(database_pool) as db:
        # store configuration into database
        try:
            # if any changes were made, update the database
            if data:
                db.updateAttr(current_user.username, 'trends', data)
        except:
            pass
        
        # Get the current configuration
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
    # store into local variables
    checkbox_sel = eval(currentTrend['checkbox'])
    yaxis_sel = eval(currentTrend['yaxis'])
    color_sel = eval(currentTrend['color'])

    # get the data
    # collect points to read, DateTime is mandatory
    pts = [k for k, v in checkbox_sel.items() if v == ['selected'] and k in DATA["main"].columns]


    # when start and stop times are undefined, 
    df = DATA["main"]
    # get them from the read data
    if not(currentTrend['timeStart'] and currentTrend['timeStop']):
        currentTrend['timeStart'] = list(df.index)[0]
        currentTrend['timeStop'] = list(df.index)[-1]
        # update current trend
        with database_context_manager(database_pool) as db:
            db.updateAttr(current_user.username, 'trends', currentTrend) 
    df = DATA["main"].loc[currentTrend['timeStart']:currentTrend['timeStop'], pts]
    
    


    
    # Get and join the temporary trend data
    try:
        # if new data table was not selected, 
        # load temporary data and merge it with uploaded data            
        pts = [k for k, v in checkbox_sel.items() if v == ['selected'] and k in DATA["trend_data"].columns]
        if data_name == currentTrend['datatable']:
            df1 = DATA["trend_data"].loc[currentTrend['timeStart']:currentTrend['timeStop'], pts]
            # concat datasets
            df = pd.concat([df, df1], axis=1)
    except:
        pass

    #update graph    

    #populate traces and axes
    traces = []
    y_axes = {}
    #axes customization dictionary
    used_y_axes = {} 
    #get point name list
    columns_raw = df.columns
    #refine based on checkbox selection and configuration data availability
    columns = [column for column in columns_raw 
                            if column in checkbox_sel and
                            column in color_sel and
                            column in yaxis_sel and
                            checkbox_sel[column]]

    #iterate through all points and add them to graph
    for column in columns:
        #axis numbers will be based on used axis dictionary key count and not trace indexes.
        i = len(used_y_axes)
        axisNr = str(i+1) if i>=1 else "" #first y axis is not labeled with number

        #if axis does not exist
        if yaxis_sel[column] not in used_y_axes:
            #add number to dictionary
            used_y_axes[yaxis_sel[column]] = axisNr
            
            #create axes
            #axis offset
            ax_position = max(len(used_y_axes)-1, 0)*0.02#modified to accomodate dynamic trend width
            #axis number should be used to select axis color
            #color should be selected based on matching trace number in color dictionary
            
            #first color definition will be used for axis color
            ax_color = dict(color = color_sel[column])

            # the first Y axis should be anchored
            if i<1:
                y_axes[f'yaxis{axisNr}'] = go.layout.YAxis(
                    titlefont = ax_color, 
                    tickfont = ax_color, 
                    position = ax_position,
                    nticks=4)
            else:
                y_axes[f'yaxis{axisNr}'] = go.layout.YAxis(
                    titlefont = ax_color, 
                    tickfont = ax_color, 
                    anchor="free", 
                    overlaying="y", 
                    position = ax_position,
                    nticks=4,
                    showgrid = False) #secondary axis grid is not shown to reduce clutter   
            
        #else use existing axis number
        else:
            axisNr = used_y_axes[yaxis_sel[column]]
        
        traces.append(
            go.Scatter(x=df.index, y=df [column], 
                       name=column, 
                       yaxis=f'y{axisNr}', 
                       marker_color=color_sel[column])
        )
    
    #store created axis in the layout
    layout = go.Layout(
        xaxis=dict(domain=[0.02*max(len(used_y_axes)-1, 0), 1]),#modified to have dynamic trend width
        **y_axes,
        # height=550,
        legend=dict(orientation='h', traceorder = 'normal'),
                    # yanchor="bottom", y=0, xanchor="right", x=1),#this moves legend onto graph to save space
        uirevision=True,
        hovermode = 'x',
        dragmode='select',
        # autosize = True,
        margin = dict(l=30, r=30, t=30, b=30), #helps to avoid excessive margins around the graph
        ) 
    fig = go.Figure(data=traces, layout=layout)    
    return fig, no_update

# Add new callback for download functionality
@app.callback(
    Output("trend-download", "data"),
    Input("trend-download-btn", "n_clicks"),
    State('trend-data-selector', 'value'),
    State({'type': 'trend-trace-checkbox', 'index': ALL}, 'value'),
    prevent_initial_call=True
)
def download_trend_data(n_clicks, data_name, checkboxSel):
    """Download currently displayed trend data as CSV"""
    if not n_clicks:
        raise PreventUpdate
        
    # Load data
    DATA = {}
    DATA["main"] = load_data(current_user.username)
    if DATA["main"] is None:
        raise PreventUpdate
    trend_data = load_data(current_user.username, "trend_data")
    if trend_data is not None:
        DATA["trend_data"] = trend_data

    with database_context_manager(database_pool) as db:
        currentTrend = db.getAttrValues(current_user.username, 'trends', 'Temp')
        checkbox_sel = eval(currentTrend['checkbox'])

    # Get selected points from main data
    pts = [k for k, v in checkbox_sel.items() if v == ['selected'] and k in DATA["main"].columns]
    df = DATA["main"].loc[currentTrend['timeStart']:currentTrend['timeStop'], pts]

    # Get selected points from trend data if available
    try:
        pts = [k for k, v in checkbox_sel.items() if v == ['selected'] and k in DATA["trend_data"].columns]
        if data_name == currentTrend['datatable']:
            df1 = DATA["trend_data"].loc[currentTrend['timeStart']:currentTrend['timeStop'], pts]
            df = pd.concat([df, df1], axis=1)
    except:
        pass

    # Generate filename with timestamp
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    filename = f"trend_data_{timestamp}.csv"
    
    return dcc.send_data_frame(df.to_csv, filename)