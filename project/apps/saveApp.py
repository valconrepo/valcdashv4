import os
import dash_bootstrap_components as dbc
from dash import dcc
from dash import html, callback_context
from dash_extensions.enrich import Input, Output, State, no_update
from flask_login import current_user
from .utils import components as utl
from .app import app
from .utils.database.database_manager import UserData, saveDBTables, loadDBTables
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
import base64
from pathlib import Path

absolute_path = Path(__file__).parent.parent #added to solve issue with relative path
directory = str(absolute_path) +'/shared/'

models_message_popup = dbc.Modal(id="models-message-popup", is_open=False),
load_popup = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Load project")),
        dbc.ModalBody("Select from list what models you want to load"),
        dcc.Dropdown(id='load-models-dropdrown'),
        dbc.ModalFooter(

            [dbc.Button(
                "Load", id="load_popup-load", className="ms-auto", n_clicks=0
            ),
                dbc.Button(
                    "Close", id="load_popup-close", className="ms-auto", n_clicks=0
                )]
        ),
    ],
    id="models-load-pop-up",
    is_open=False,
)
save_popup = dbc.Modal(
    [
        dbc.ModalHeader('Save project'),
        dbc.Row(
            utl.Input('models-name-in', 'Save name', 'Save name', type='text'),
        ),
        dbc.ModalFooter([
            utl.Button('models-cancel', 'Cancel'),
            utl.Button('models-save', 'Save')
        ])
    ], id='models-save-pop-up',
    is_open=False,
)

download_popup = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Download project")),       
        # AK-23.05.02: changed to dropdown
        dbc.ModalBody("Select from list what datafile you want to download"),
        dcc.Dropdown(id='download-name-dropdown'),
        # dbc.Row(
            # utl.Input('download-name-in', 'Downloaded file name', 'Downloaded file name', type='text'),
        # ),
        dbc.ModalFooter([
            utl.Button('models-download-cancel', 'Cancel'),
            dcc.Download(id="download-models"),
            utl.Button('models-download', 'Download')
        ]),
    ], id='models-download-pop-up',
    is_open=False,
)

upload_popup = dbc.Modal(
    [
        dbc.ModalHeader('Upload project'),
        dbc.Row(
            dcc.Upload(
                id="upload-models-data",
                children=html.Div(
                    ["Drag and drop or click to select a file to upload."]
                ),
                style={
                    "width": "100%",
                    "height": "60px",
                    "lineHeight": "60px",
                    "borderWidth": "1px",
                    "borderStyle": "dashed",
                    "borderRadius": "5px",
                    "textAlign": "center",
                },
                multiple=False,
            ), align='center', justify='center'
        ),
        dbc.ModalFooter([
            dbc.Button('Cancel', id='upload-cancel'),
        ])
    ], id='upload-pop-up',
    is_open=False,
)
delete_popup = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Delete project")),
        dbc.ModalBody("Select from list what project you want to delete"),
        dcc.Dropdown(id='delete-models-dropdrown'),
        dbc.ModalFooter(

            [dbc.Button(
                "Delete", id="delete_popup-delete", className="ms-auto", n_clicks=0
            ),
                dbc.Button(
                    "Close", id="delete_popup-close", className="ms-auto", n_clicks=0
                )]
        ),
    ],
    id="models-delete-pop-up",
    is_open=False,
)
layout = dbc.Container([
    # html.H1('Saved projects'),
    html.Hr(style={'margin-top':0, 'width': '100%'}),
    dbc.Row([
        dbc.Col(utl.Button('save-model-btn', 'Save')),
        dbc.Col(utl.Button('download-models-btn', 'Download')),
        dbc.Col(utl.Button('upload-models-btn', 'Upload')),
        dbc.Col(utl.Button('load-models-btn', 'Load')),
        dbc.Col(utl.Button('delete-models-btn', 'Delete')),
        ]),
    html.Div(save_popup),
    html.Div(upload_popup),
    html.Div(load_popup),
    html.Div(download_popup),
    html.Div(models_message_popup),
    html.Div(delete_popup)
])


@app.callback(
    Output("models-delete-pop-up", "is_open"),
    [Input("delete-models-btn", "n_clicks"), Input("delete_popup-close", "n_clicks")],
    [State("models-delete-pop-up", "is_open")],
    prevent_initial_call=True,
)
def open_delete_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
    Output("models-save-pop-up", "is_open"),
    [Input("save-model-btn", "n_clicks"), Input("models-cancel", "n_clicks")],
    [State("models-save-pop-up", "is_open")],
    prevent_initial_call=True,
)
def open_save_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
    Output("upload-pop-up", "is_open"),
    [Input("upload-models-btn", "n_clicks"), Input("upload-cancel", "n_clicks")],
    [State("upload-pop-up", "is_open")],
    prevent_initial_call=True,
)
def open_upload_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
    Output("models-download-pop-up", "is_open"),
    [Input("download-models-btn", "n_clicks"), Input("models-download-cancel", "n_clicks")],
    [State("models-download-pop-up", "is_open")],
    prevent_initial_call=True,
)
def open_download_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
    Output("models-load-pop-up", "is_open"),
    [Input("load-models-btn", "n_clicks"), Input("load_popup-close", "n_clicks")],
    [State("models-load-pop-up", "is_open")],
    prevent_initial_call=True,
)
def open_load_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

# AK-23.05.02 - Added dropdown for Download modal window
@app.callback(
    Output('load-models-dropdrown', "options"),
    Output('delete-models-dropdrown', "options"),
    Output('download-name-dropdown', "options"),
    Input("load-models-btn", "n_clicks"),
    Input("delete-models-btn", "n_clicks"),
    Input("download-models-btn", "n_clicks"),
    prevent_initial_call=True,
)
def returnList(n1, n2, n3):
    '''Get list of user databases'''
    path = directory + current_user.username + '/'
    if callback_context.triggered[0]['prop_id'] == 'load-models-btn.n_clicks' or \
        callback_context.triggered[0]['prop_id'] == 'delete-models-btn.n_clicks'or \
        callback_context.triggered[0]['prop_id'] == 'download-models-btn.n_clicks':
        listOptions=[{'label': name.replace('.json',''), 'value': name.replace('.json','')} for name in
                os.listdir(path) if (name.endswith('.json'))]
        return listOptions, listOptions, listOptions
    return no_update

# AK-23.05.02 - modified to use dropdown instead of text input
@app.callback(
    Output("download-models", "data"),
    Output('models-message-popup', 'children'),
    Output("models-message-popup", "is_open"),
    [Input("models-download", "n_clicks"), Input('download-name-dropdown', 'value')],
    [State("models-message-popup", "is_open")],
    prevent_initial_call=True,
)
def download_models(n_clicks, inputName, is_open):
    '''Dowload database file to local user directory'''
    path = directory + current_user.username + '/'
    if inputName and callback_context.triggered[0]['prop_id'] == 'models-download.n_clicks':

        message = [
            dbc.ModalHeader('Download started'),
            dbc.ModalBody("File " + inputName + ' download has started'),
            dbc.ModalFooter([
                utl.Button('models-message-popup-close', 'Close')
            ])]

        return dcc.send_file(path + inputName+'.json'), message, not is_open
    return no_update, no_update, no_update


@app.callback(
    Output("upload-pop-up", "is_open"),
    Output('models-message-popup', 'children'),
    Output("models-message-popup", "is_open"),
    Output("upload-models-data", 'contents'),
    [Input("upload-models-data", "filename"), Input("upload-models-data", "contents")],
    [State("upload-pop-up", "is_open")],
    [State("models-message-popup", "is_open")],
    prevent_initial_call=True,
)
def upload_models(uploaded_filenames, uploaded_file_contents, is_open, is_open1):
    '''Upload models from json'''
    path = directory + current_user.username + '/'
    if callback_context.triggered[0]['prop_id'] == 'upload-models-data.contents':
        if uploaded_filenames and uploaded_file_contents:
            if not os.path.exists(path):
                os.mkdir(path)
            fileName = path + uploaded_filenames
            if os.path.exists(fileName):
                message = [
                    dbc.ModalHeader('Upload error'),
                    dbc.ModalBody("File named " + uploaded_filenames + " already exists"),
                    dbc.ModalFooter([
                        utl.Button('models-message-popup-close', 'Close')
                    ])]
                return not is_open, message, not is_open1, None
            
            # decode and save file contents
            _, content_string = uploaded_file_contents.split(',')
            decoded = base64.b64decode(content_string)
            f = open(fileName, "wb")
            f.write(decoded)
            f.close()
            message = [
                dbc.ModalHeader('Upload success'),
                dbc.ModalBody("Project uploaded to server sucessfully as " + uploaded_filenames),
                dbc.ModalFooter([
                    utl.Button('models-message-popup-close', 'Close')
                ])]
            return not is_open, message, not is_open1, None
        else:
            message = [
                dbc.ModalHeader('Upload error'),
                dbc.ModalBody("Upload file is empty or upload file doesn't have a name"),
                dbc.ModalFooter([
                    utl.Button('models-message-popup-close', 'Close')
                ])]
            return not is_open, message, not is_open1, None
    return no_update, no_update, no_update, None


@app.callback(
    Output('models-message-popup', "is_open"),
    [Input("models-message-popup-close", "n_clicks")],
    [State('models-message-popup', "is_open")],
    prevent_initial_call=True,
)
def close_message_popup(n1, is_open):
    if n1 and not callback_context.triggered[0]['value'] is None:
        return not is_open
    return is_open


@app.callback(
    Output("models-load-pop-up", "is_open"),
    Output('models-message-popup', 'children'),
    Output("models-message-popup", "is_open"),
    Input('load-models-dropdrown', 'value'),
    Input('load_popup-load', 'n_clicks'),
    [State("models-load-pop-up", "is_open")],
    [State("models-message-popup", "is_open")],
    prevent_initial_call=True,
)
def load_models(n1, n2, is_open, is_open1):
    '''Load saved user file and rewrites working tables'''
    if callback_context.triggered[0]['prop_id'] == 'load_popup-load.n_clicks' and callback_context.triggered[0][
        'value'] > 0:
        if n1:
            # load tables
            path = directory + current_user.username + '/'
            path_src = path+n1+'.json'
            loadDBTables(current_user.username, path_src)

            message = [
                dbc.ModalHeader('Loading message'),
                dbc.ModalBody("Loading data file " + n1 + ' is successfull'),
                dbc.ModalFooter([
                    utl.Button('models-message-popup-close', 'Close')
                ])]
            return not is_open, message, not is_open1,
        message = [
            dbc.ModalHeader('Load error'),
            dbc.ModalBody("Select which models you want to load from list"),
            dbc.ModalFooter([
                utl.Button('models-message-popup-close', 'Close')
            ])]
        return not no_update, message, not is_open1,
    return no_update, no_update, no_update,


@app.callback(
    Output("models-save-pop-up", "is_open"),
    Output('models-message-popup', 'children'),
    Output("models-message-popup", "is_open"),
    Input("models-save", "n_clicks"),
    Input("models-name-in", "value"),
    [State("models-save-pop-up", "is_open")],
    [State("models-message-popup", "is_open")],
    prevent_initial_call=True,
)
def save_models(n1, inputName, is_open, is_open1):
    '''Saves current SQLite DB file with a new title "inputName"'''
    if callback_context.triggered[0]['prop_id'] == 'models-save.n_clicks' and callback_context.triggered[0][
        'value'] > 0:
        if inputName:

            path = directory + current_user.username + '/'
            if not os.path.exists(path):
                os.mkdir(path)
            path_dst = path+inputName+'.json'

            if os.path.exists(path_dst):
                message = [
                        dbc.ModalHeader('Upload error'),
                        dbc.ModalBody("File named " + inputName + " already exists"),
                        dbc.ModalFooter([
                            utl.Button('models-message-popup-close', 'Close')
                        ])]
                return no_update, message, not is_open1
            else:
 
                saveDBTables(current_user.username, path_dst)

            message = [
                dbc.ModalHeader('Save model success'),
                dbc.ModalBody("File named " + inputName + " successfully created"),
                dbc.ModalFooter([
                    utl.Button('models-message-popup-close', 'Close')
                ])]
            return not is_open, message, not is_open1
        message = [
            dbc.ModalHeader('Save error'),
            dbc.ModalBody("Enter save file name"),
            dbc.ModalFooter([
                utl.Button('models-message-popup-close', 'Close')
            ])]
        return no_update, message, not is_open1
    return no_update, no_update, no_update


@app.callback(
    Output("models-delete-pop-up", "is_open"),
    Output('models-message-popup', 'children'),
    Output("models-message-popup", "is_open"),
    Input('delete-models-dropdrown', 'value'),
    Input('delete_popup-delete', 'n_clicks'),
    [State("models-delete-pop-up", "is_open")],
    [State("models-message-popup", "is_open")],
    prevent_initial_call=True,
)
def delete_models(n1, n2, is_open, is_open1):
    if callback_context.triggered[0]['prop_id'] == 'delete_popup-delete.n_clicks' and callback_context.triggered[0][
        'value'] > 0:
        if n1:
            path = directory + current_user.username + '/'
            os.remove(path + n1 + '.json')
            message = [
                dbc.ModalHeader('Delete message'),
                dbc.ModalBody("Deleting data file " + n1 + ' is successfull'),
                dbc.ModalFooter([
                    utl.Button('models-message-popup-close', 'Close')
                ])]
            return not is_open, message, not is_open1,
        message = [
            dbc.ModalHeader('Delete error'),
            dbc.ModalBody("Select which models you want to load from list"),
            dbc.ModalFooter([
                utl.Button('models-message-popup-close', 'Close')
            ])]
        return not no_update, message, not is_open1,
    return no_update, no_update, no_update
