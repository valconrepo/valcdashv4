from dash import dcc, html, no_update
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, current_user
from .utils import components as utl
from .app import app, Users
from .app import db

create = dbc.Container([
    dbc.Row([
        dbc.Col([
            html.H2('Registration page:', className='text-center')
        ], align='center'
        ),
        dbc.Form([
            dbc.Row(
                [
                    dbc.Col(
                        utl.Input(
                            type="text", identity='username', placeholder="Enter user name", name="User",
                        ),
                        width=4, align='center'
                    ),
                ], align='center', justify='center'
            ),
            dbc.Row(
                [
                    dbc.Col(
                        utl.Input(
                            type="email", identity="email", placeholder="Enter email", name="Email",
                        ),
                        width=4, align='center'
                    ),
                ], align='center', justify='center'
            ),
            dbc.Row(
                [
                    dbc.Col([
                        utl.Input(
                            type="password", identity="password", placeholder="Enter password", name="Password",
                        ),
                    ], width=4, align='center')

                ], align='center', justify='center'),
            dbc.Row(
                [
                    dbc.Col([
                        utl.Button('submit-val', 'Create User')
                    ], width=2, align='center')
                ], align='center', justify='center'),
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.Div(id='container-button-basic')

                        ]
                    )
                ]
            )
        ])
    ], justify='center', align='center', style={"height": "100vh"})
])

login = dbc.Container([
    dbc.Row([
        dbc.Form([
            dbc.Row([
                dbc.Col([
                    html.H2('''Please log in to continue:''', className='text-center', id='h1')], align='center',
                    width=4)
            ], justify='center', align='center'),
            dbc.Row([
                dbc.Col([
                    utl.Input(placeholder='Enter your username', name='Enter your username', type='text',
                              identity='uname-box')
                ], align='center', width=4)
            ], justify='center', align='center'),
            dbc.Row([
                dbc.Col([
                    utl.Input(placeholder='Enter your password', name='Enter your password', type='password',
                              identity='pwd-box'),
                    dbc.FormFeedback("Login successful!", type="valid"),
                    dbc.FormFeedback("Unable to login!", type="invalid"),
                ], align='center', width=4)
            ], justify='center', align='center'),
            dbc.Row([
                dbc.Col([
                    utl.Button('login-button', 'Login')
                ], align='center', width=1),
                dbc.Col([
                    utl.Button('create-button-ref', 'Create', href='/create')
                ], align='center', width=1)
            ], justify='center', align='center'),
        ])
    ], justify='center', align='center', style={"height": "100vh"})
], class_name="h-100", fluid=True)

failed = html.Div([
    html.Div([html.H2('Authorisation failed.', className='text-center')
                 , html.Br()
                 , html.Div([login])
                 , html.Br()
                 , utl.Button('back-button', 'Go back')
              ])  # end div
])  # end div

logout = html.Div([
    html.Br()
    , html.Div(html.H2('You have been logged out - Please login'))
    , html.Br()
    , html.Div([login])
    , utl.Button('back-button', 'Go back')
])  # end div
dashboard = dbc.Container([
    dbc.Row(
        [
            html.H1("Welcome to NettaDash app", className='text-center'),
        ], justify="center", align="center",
    ),

])


@app.callback(
    [Output('container-button-basic', "children")]
    , [Input('submit-val', 'n_clicks')]
    , [State('username', 'value'), State('email', 'value'), State('password', 'value')])
def insert_users(n_clicks, un, em, pw):  # Registration function
    if un is not None and pw is not None and em is not None:
        hashed_password = generate_password_hash(pw, method='sha256')
        new_user = Users(username=un, email=em, password=hashed_password)
        existing_user_username = Users.query.filter_by(username=un).first()
        existing_email_username = Users.query.filter_by(email=em).first()
        if existing_user_username:
            return [
                html.Div([html.H2('This username is taken, already have an  account?', className='text-center'),
                          dcc.Link('Click here to Log In', href='/', className='text-center')],
                         style={"display": "flex", "justifyContent": "center"})]
        if existing_email_username:
            return [
                html.Div([html.H2('This email is taken, already have an account?', className='text-center'),
                          dcc.Link('Click here to Log In', href='/', className='text-center')],
                         style={"display": "flex", "justifyContent": "center"})]
        db.session.add(new_user)
        db.session.commit()
        return [
            html.Div([
                html.H2('Successfully created account!', className='text-center'),
                dcc.Link('Click here to Log In', href='/', className='text-center')],
                style={"display": "flex", "justifyContent": "center"}, )]
    else:
        return [html.Div([html.H2('Register failed, try again.')])]


@app.callback(
    [Output('url', 'pathname')]
    , [Input('login-button', 'n_clicks')]
    , [State('uname-box', 'value'), State('pwd-box', 'value')])
def successful(n_clicks, input1, input2):
    if n_clicks:
        user = Users.query.filter_by(username=input1).first()
        if user:
            if check_password_hash(user.password, input2):
                login_user(user)
                return '/'
    return no_update


@app.callback(
    [Output('uname-box', "valid"), Output('uname-box', "invalid")]
    , [Input('login-button', 'n_clicks')]
    , [State('uname-box', 'value'), State('pwd-box', 'value')])
def update_output(n_clicks, input1, input2):
    if n_clicks:
        user = Users.query.filter_by(username=input1).first()
        if user:
            if user.password == input2:
                return False, False
            else:
                return False, True
        else:
            return False, True
    else:
        return False, False


# if __name__ == '__main__':
#     app.run_server(debug=True)
