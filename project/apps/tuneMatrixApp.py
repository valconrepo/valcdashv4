import dash_bootstrap_components as dbc
from dash import html, callback_context, no_update
from dash_extensions.enrich import Input, Output, State, ALL
from .utils.database.database_manager import UserData
from dash.exceptions import PreventUpdate
from .utils import components as utl
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user

clear_popup = dbc.Modal(
    [
        dbc.ModalHeader('Delete tune results'),
        dbc.ModalFooter([
            utl.Button('tune-matrix-clear-cancel', 'Cancel'),
            utl.Button('tune-matrix-clear-delete', 'Delete')
        ])
    ], id='tune-matrix-clear-pop-up'
)

layout = dbc.Container([
    # html.H1('Tune matrix'),
    html.Hr(style={'margin-top':0, 'width': '100%'}),
    dbc.Row([dbc.Table(bordered=True, className='m-3', id='tune-matrix-model-table', children=""#html.Thead(
        #html.Tr([html.Th('Tune Name'), html.Th("K"), html.Th("Ti"), html.Th("Td"), html.Th("Tf"), html.Th("T1"),
                 #html.Th("Scaling gain")]))
                       )]),
    dbc.Row([utl.Button('tune-matrix-clear-models-btn', 'Clear')]),
    html.Div(clear_popup),

])


@app.callback(
    Output("tune-matrix-clear-pop-up", "is_open"),
    [Input("tune-matrix-clear-models-btn", "n_clicks"),
     Input("tune-matrix-clear-cancel", "n_clicks")],
    [State("tune-matrix-clear-pop-up", "is_open")],
)
def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open
@app.callback(
    Output("tune-matrix-clear-pop-up", "is_open"),
    [Input("tune-matrix-clear-delete", "n_clicks")],
    [State("tune-matrix-clear-pop-up", "is_open")],
)
def delete(n1, is_open):
    '''Deletes all controllers'''
    if n1:
        # Read all controllers
        with database_context_manager(database_pool) as db:
            controllers = db.getAttr(current_user.username, 'controllers')

            #Delete all non Temp controllers
            for name in controllers.name:
                if name != 'Temp':
                    db.deleteAttr(current_user.username, 'controllers', name)
        
        return not is_open
    return is_open

@app.callback(
    Output('tune-matrix-model-table', 'children'),
    Input("tune-matrix-clear-pop-up", "is_open"),
    Input({'id': 'loop-data-select-button', 'data': ALL}, 'n_clicks'),
)
def update_table(is_open, dummy):
    '''Populates controller table'''
    
    #when no loop selection was done or when clear modal is open
    # if dummy.count(None) == len(dummy) or is_open:
    #     raise PreventUpdate
    # read all controllers of current user, current loop
    with database_context_manager(database_pool) as db:
        controllers = db.getAttr(current_user.username, 'controllers')
    
    # relevant columns
    cols = ['name', 'model', 'P', 'I', 'D', 'F', 'T1', 'scaling']
    # get relevant data from df
    df = controllers.loc[controllers.name != "Temp", controllers.columns.isin(cols)]
    #capitalized column names
    df.columns = df.columns.str.capitalize()
    # create dcc component
    table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)

    return table
