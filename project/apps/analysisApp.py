import dash_bootstrap_components as dbc
from dash import html, dcc, callback_context, no_update
from dash_extensions.enrich import Input, Output, State, ALL, MATCH
from dash.exceptions import PreventUpdate
from .app import app
from .utils import components as utl
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user
import plotly.graph_objects as go
import numpy as np
from scipy import stats
from inspect import currentframe
from .app import logger
from datetime import datetime
import json
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup
from plotly.graph_objects import Figure
import pandas as pd
from plotly.subplots import make_subplots
from apps.utils.functions import save_data, load_data
import os

logger.info(f'{__name__}')
# At the top of the file, add common style definitions
STYLES = {
    "label_style": {
        "fontSize": "0.8rem",
        "fontWeight": "normal"
    },
    "input_classes": "m-0 pt-0 pb-0",
    "label_classes": "m-0 p-0 small",
    "row_classes": "g-2 mb-2",
    "col_classes": {
        "label_col": "pe-1",
        "input_col": "px-1",
    },
    "card_body": {'padding': '0.5rem', 'height': '25vh'},
    "card_style": {'backgroundColor': 'rgba(255, 255, 255, 0.7)'},
    "main_height": {'height':'5vh'}
}
# Then in your chart configuration sections, use these consistent styles:
num_filters = 5
num_vars = 5
def safe_eval_config(config_str, default=None):
    """Safely evaluate configuration string with fallback to default value"""
    if not config_str:
        return default
    try:
        return eval(config_str)
    except:
        logger.warning(f"Failed to evaluate config string: {config_str}")
        return default
def filterDF(df, field, range):
    '''This function filter dataframe based on provided range for a specific field'''
    df2 = df.dropna(how='any')
    if field == "DateTime":
        df2 = df
        mask = pd.Series(True, index=df2.index)
        if range[1] not in [None, 'None', '']:
            try:
                min_date = pd.to_datetime(range[1])
                mask &= df2.index >= min_date
            except ValueError:
                pass
        if range[0] not in [None, 'None', '']:
            try:
                max_date = pd.to_datetime(range[0])
                mask &= df2.index <= max_date
            except ValueError:
                pass
        return df2[mask]
    else:
        mask = pd.Series(True, index=df2.index)
        if range[1] not in [None, 'None']:
            mask &= df2[field] >= range[1]
        if range[0] not in [None, 'None']:
            mask &= df2[field] <= range[0]
        return df2[mask]

def getField(source, field, indices = [], default = None):
    '''Read field by checking if it exists'''
    if not field in source:
        return default
        
    result = source[field]
    for i, index in enumerate(indices):
        if not isinstance(result, (list, tuple)) or len(result) <= index:
            return default
        result = result[index]
            
    return result
    
def getSelector(idx = 0, title = 'Y-Axis Variable', options={}, value="", 
                axis_type="primary", marker_type="lines", opacity=1, marker_shape="circle", min_val=None, max_val=None):
    '''This is a typical element that allows to customize selected point '''
    # Create summary string for dropdown button
    config_summary = f"{'y1' if axis_type == 'primary' else 'y2'}, {'dots' if marker_type == 'markers' else 'line' if marker_type == 'lines' else 'line+dots'}, {marker_shape}, {opacity:.1f}"
    return html.Div([
            dbc.Row([
                # Variable Name Selector
                dbc.Col([
                    utl.Selector({"prop": "var-selector", "id": idx}, title, 
                                options=options, value=value, 
                                className = STYLES['input_classes'])
                ], width=6),
                # Range Selection
                dbc.Col(
                    dbc.Row([
                        dbc.Col([
                            dbc.Input(
                                id={"prop": "var-range", "type": "min", "id": idx},
                                type="number",
                                placeholder="Min",
                                debounce=True,
                                className=STYLES['input_classes'],
                                style=utl.height,
                                value=min_val
                                    )
                                ], width=6),
                            dbc.Col([
                                dbc.Input(
                                    id={"prop": "var-range", "type": "max", "id": idx},
                                    type="number",
                                    placeholder="Max",
                                    debounce=True,
                                    className=STYLES['input_classes'],
                                    style=utl.height,
                                    value=max_val
                                )
                            ], width=6),
                    ], className="g-0"), width=3),
                # Configuration dropdown - Updated with dynamic label
                dbc.Col([
                    dbc.DropdownMenu(
                        label=config_summary,  # Use the summary string
                        id={"type": "var-config-menu", "id": idx},
                        children=[
                            # Y-Axis Selection
                            dbc.Label("Y-Axis:", className=STYLES['label_classes']),
                            dbc.Select(
                                id={"type": "var-config", "prop": "axis-selector", "id": idx},
                                options=[
                                    {"label": "Primary", "value": "primary"},
                                    {"label": "Secondary", "value": "secondary"}
                                ],
                                value=axis_type,
                                className=STYLES['input_classes'],
                                style={"width": "90%"}.update(utl.height)
                            ),
                            # Marker Style and Shape Row
                            dbc.Row([
                                dbc.Col([
                                    dbc.Label("Style:", className=STYLES['label_classes']),
                                    dbc.Select(
                                        id={"type": "var-config", "prop": "marker-type", "id": idx},
                                        options=[
                                            {"label": "Line", "value": "lines"},
                                            {"label": "Markers", "value": "markers"},
                                            {"label": "Line+Markers", "value": "lines+markers"}
                                        ],
                                        value=marker_type,
                                        className=STYLES['input_classes'],
                                        style={"width": "90%"}.update(utl.height)
                                    ),
                                ])]),
                            dbc.Row([
                                dbc.Col([
                                    dbc.Label("Shape:", className=STYLES['label_classes']),
                                    dbc.Select(
                                        id={"type": "var-config", "prop": "marker-shape", "id": idx},
                                        options=[
                                            {"label": "●", "value": "circle"},
                                            {"label": "■", "value": "square"},
                                            {"label": "▲", "value": "triangle-up"},
                                            {"label": "✖", "value": "x"},
                                            {"label": "◆", "value": "diamond"}
                                        ],
                                        value=marker_shape,
                                        className=STYLES['input_classes'],
                                        style={"width": "90%"}.update(utl.height)
                                    ),
                                ], width=6),
                            # Opacity Selection
                            dbc.Col([
                                dbc.Label("Opacity:", className=STYLES['label_classes']),
                                dbc.Input(
                                    id={"type": "var-config", "prop": "opacity", "id": idx},
                                    type="number",
                                    min=0,
                                    max=1,
                                    step=0.1,
                                    value=opacity,
                                    className=STYLES['input_classes'],
                                    style={"width": "90%"}.update(utl.height)
                                )], width=6)], className="g-0"),
                        ],
                        color="secondary",
                        className=STYLES['input_classes'],
                        # style=utl.height,
                        toggle_style={"width":"100%", **utl.height}
                    )
                ], width=3),
            ], className="g-1"),
        ], id={"prop": "var-container", "id": idx}, 
           className="mb-1")

# Add helper function for filter selector
# Update getFilterSelector to use DatePickerSingle for datetime
def getFilterSelector(idx=0, title='Filter Variable', options={}, value="", min_val=None, max_val=None):
    return html.Div([
        dbc.Row([
            # Variable selector column
            dbc.Col([
                utl.Selector({"prop": "filter-selector", "id": idx}, title, 
                            options=options, value=value,
                            className="m-0 pt-0 pb-0")
            ], width=6),
            dbc.Col(
                dbc.Row([
                        # Min range column
                        dbc.Col([
                            html.Div(id={"prop": "filter-range-container", "type": "min", "id": idx})
                        ], width=5),
                        # Max range column
                        dbc.Col([
                            html.Div(id={"prop": "filter-range-container", "type": "max", "id": idx})
                        ], width=5),
                        # Clear button column
                        dbc.Col([
                            dbc.Button(
                                "×",  # Using × symbol for clear
                                id={"prop": "filter-clear", "id": idx},
                                size="sm",
                                className="p-0",
                            )
                        ], width=1, className="d-flex align-items-center"),
                ]), width = 6)
        ], className="mb-2 g-0"),
    ])

# define modal objects
deletePop = deleteAttrPopup(
                    app=app,
                    name='analysis')
savePop = saveAttrPopup(
                    app=app,
                    name='analysis')
loadPop = loadAttrPopup(
                    app=app,
                    name='analysis')
# Modal layout for custom file name input
downloadPop = dbc.Modal(
        [
            dbc.ModalHeader("Download Chart"),
            dbc.ModalBody([
                html.Label("Enter file name:"),
                dbc.Input(id="analysis-file-name-input", type="text", placeholder="Enter file name"),
            ]),
            dbc.ModalFooter([
                dbc.Button("Download", id="analysis-download-btn", className="ms-auto"),
                dbc.Button("Cancel", id="analysis-download-close-btn", className="ms-2")
            ]),
        ],
        id="analysis-download-modal",
        is_open=False,  # Modal starts closed
    )
def serve_layout():
    # Read available data tables
    with database_context_manager(database_pool) as db:
        dataTables = db.getDataTables(current_user.username)
        currentAnalysis = db.getAttrValues(current_user.username, 'analysiss', 'Temp')
    
    data_options = [{'label': table, 'value': table} for table in dataTables]

    # Parse configuration
    chart_config = safe_eval_config(currentAnalysis['chart_config'], {})
    
    # get variables and their settings
    variables = getField(chart_config, 'variables')
    axis_types = getField(chart_config, 'axis_types')
    marker_types = getField(chart_config, 'marker_types')
    marker_shapes = getField(chart_config, 'marker_shapes')    
    opacities = getField(chart_config, 'opacities')
    chart_range = getField(chart_config, 'chart_range')

    # Parse filters configuration
    filters_config = safe_eval_config(currentAnalysis['filters_config'], {})
    filter_vars = getField(filters_config, 'variables') or ["",] * num_filters
    filter_ranges = getField(filters_config, 'ranges') or [None,] * num_filters * 2

    layout = dbc.Container([
        html.Hr(style={'margin-top':0, 'width': '100%'}),
        # Add loading spinners for all callbacks
        utl.LoadingSpinner(id='analysis-loading-update_chart_config_options'),
        utl.LoadingSpinner(id='analysis-loading-update_configuration'),
        utl.LoadingSpinner(id='analysis-loading-update_chart'),
        utl.LoadingSpinner(id='analysis-loading-download_image'),
        utl.LoadingSpinner(id='analysis-loading-update_config_menu_labels'),
        
        # Top Control Bar
        dbc.Row([
            # Left side: Dropdowns
            dbc.Col([
                dbc.Row([
                    dbc.Col([
                        utl.Selector('data-table-selector', 'Data Table', 
                                   options=data_options, 
                                   value=currentAnalysis['datatable'] or dataTables[-1] if dataTables else '',
                                   style=STYLES['main_height'])
                    ], width=6),
                    dbc.Col([
                        utl.Selector('chart-type', 'Chart Type',
                            options=[
                                {'label': 'Distribution', 'value': 'distribution'},
                                {'label': 'Time Series', 'value': 'timeseries'},
                                {'label': 'Scatter', 'value': 'scatter'},
                            ],
                            value=currentAnalysis['chart_type'] or 'distribution',
                            style=STYLES['main_height']
                        )
                    ], width=6),
                ])
            ], width=6),
            
            # Right side: Configuration Buttons
            dbc.Col([
                dbc.ButtonGroup([
                    dbc.Button("Configure", 
                             id="open-top-config", 
                             color="info",
                            #  outline=True,
                             title="Open chart configuration panel",
                             style=STYLES['main_height']),
                    dbc.Button("Filters", 
                             id="open-filter-config", 
                             color="warning",
                            #  outline=True,
                             title="Open chart filtering panel",
                             style=STYLES['main_height']),
                    dbc.Button("Download", 
                             id="analysis-download", 
                             color="primary",
                             outline=True,
                             title="Download chart",
                             style=STYLES['main_height']),
                    dbc.DropdownMenu(
                            label="Analysis menu",
                            children=[
                                dbc.DropdownMenuItem('Save', id = savePop.btn_id),
                                dbc.DropdownMenuItem('Load', id = loadPop.btn_id),
                                dbc.DropdownMenuItem('Delete', id = deletePop.btn_id),
                                # dbc.DropdownMenuItem(divider=True),
                                # dbc.DropdownMenuItem('Zoom Reset', id = 'zoom-reset-analyze-b')
                                ],
                                color='primary', group=True,
                                style=STYLES['main_height']),
                    # dbc.Button("Overview", 
                    #         id="open-charts", 
                    #         color='secondary')
                ], className="float-end"),
            ], width=4),
        ], className="mb-3"),
        
        # Main Chart Area
        dbc.Row([
            dbc.Col([
                dcc.Graph(id='main-chart', style={'height':'85vh'})
            ])
        ]),
        
        # Top Configuration Offcanvas
        dbc.Offcanvas(
            id="top-config-offcanvas",
            title="Chart Configuration",
            placement="top",
            children=[
                dbc.Row([
                    # Left Column: Variables
                    dbc.Col([
                        dbc.Card([
                            dbc.CardHeader("Variables", style={'padding':'0px 5px'}),
                            dbc.CardBody([
                                html.Div([
                                    getSelector(
                                        i, 
                                        f'{i+1}',
                                        axis_type=axis_types[i] if axis_types and len(axis_types) > 0 else "primary",
                                        marker_type=marker_types[i] if marker_types and len(marker_types) > 0 else "lines",
                                        opacity=opacities[i] if opacities and len(opacities) > 0 else 1,
                                        value=variables[i] if variables and len(variables) > 0 else "",
                                        marker_shape=marker_shapes[i] if marker_shapes and len(marker_shapes) > 0 else "circle",
                                        min_val=chart_range[i*2+1] if chart_range and len(chart_range) > 0 else None,
                                        max_val=chart_range[i*2] if chart_range and len(chart_range) > 0 else None,
                                    )
                                for i in range(num_vars)], id="var-selectors"),
                            ], style=STYLES['card_body'])
                        ], style=STYLES['card_style'])
                    ], width=5),
                    # Right Column: Display Options
                    dbc.Col([
                        html.Div(id="chart-options", children = [])
                        ,
                    ], width=7),                    
                ]
                , className="g-3"),
            ],
            style={"height": "42vh", "background-color": "rgba(255, 255, 255, 0.8)"}  # Fixed height and transparent background, no padding
        ),
        
        # Add Offcanvas for saved charts
        dbc.Offcanvas(
            id="charts-offcanvas",
            title="Saved Charts",
            placement="end",
            children=[
                # Save new chart section
                dbc.Card([
                    dbc.CardBody([
                        dbc.Input(
                            id="chart-name",
                            placeholder="Enter chart name",
                            type="text",
                            className="mb-2",
                            #to prevent refresh until Enter is pressed or field is deactivated
                            debounce=True 
                        ),
                        dbc.Button(
                            "Save Current Chart",
                            id="save-chart-btn",
                            color="success",
                            size="sm",
                            className="w-100"
                        )
                    ])
                ], className="mb-3", style=STYLES['card_style']),
                
                # List of saved charts with previews
                html.Div(id="saved-charts-list"),
                
                # Status messages
                html.Div(id="chart-action-message")
            ],
            style={"width": "600px"}  # Increased width for chart previews
        ),
        dbc.Offcanvas(
            id="filters-offcanvas",
            title="Chart Filters",
            placement="top",
            children=[
                dbc.Row([
                    dbc.Col([
                        html.Div([
                            getFilterSelector(
                                idx=i,
                                title=f"{i+1}",
                                value=filter_vars[i] if filter_vars and len(filter_vars) > 0 else "",
                                # min_val=filter_ranges[i*2] if filter_ranges and len(filter_ranges) > 0 else None,
                                # max_val=filter_ranges[i*2+1] if filter_ranges and len(filter_ranges) > 1 else None
                            ) for i in range(num_filters)
                        ]),
                    ], width=12),
                ], className="g-3"),
            ],
            style={"height": "37vh", "width": "50vw", "background-color": "rgba(255, 255, 255, 0.8)"}  # Fixed height and transparent background, no padding
        ),
        
        # Store for saved configurations and figures
        dcc.Store(id='saved-charts', storage_type='local'),
        
        # Store for current configuration
        dcc.Store(id='current-config', data={}, storage_type='local'),
        # Store for filename
        dcc.Store(id="analysis-file-name-store", data="", storage_type='local'),  # Store for the file name
        # analysis chart download
        dcc.Download(id="analysis-download-image"),
        # add Analysis menu modal windowses
        html.Div([savePop.layout(), 
                  loadPop.layout(),  
                  deletePop.layout(),
                  downloadPop]) 
        
    ], fluid=True)
    
    return layout
# trigger Analysis menu modal object callbacks
deletePop.register_callbacks()
savePop.register_callbacks()
loadPop.register_callbacks()


@app.callback(
    Output("chart-type", "value"),
    Output('data-table-selector', 'value'),
    Input(loadPop.store_id, 'data'),
    prevent_initial_call=True
)
def post_load_analysis(store):
    ''' Postprocess for analysis loading'''
    logger.info(currentframe().f_code.co_name)
    # do not execute this callback if it was not triggered or nothing was loaded
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    if id != loadPop.store_id or store == 0:
        raise PreventUpdate
    return store['chart_type'], store['datatable']


# Update variable selectors based on chart type
@app.callback(
    [
     Output({"prop": "var-selector", "id": ALL}, "options"),
     Output({"prop": "var-selector", "id": ALL}, "value"),
     Output("chart-options", "children"),     
     Output({"type": "var-config", "prop":"var-range", "type": ALL, "id": ALL}, 'value'),
     Output({"prop":"chart-titles", "type": ALL}, 'value'),
     Output({"prop": "filter-selector", "id": ALL}, "options"),
     Output({"prop": "filter-selector", "id": ALL}, "value"),
     Output('analysis-loading-update_chart_config_options', 'children')],
    [Input("chart-type", "value"),
     Input("data-table-selector", "value"),
     State({"prop": "var-selector", "id": ALL}, "value"),
     State({"prop":"chart-titles", "type": ALL}, 'value'),
     State({"type": "var-config", "prop":"var-range", "type": ALL, "id": ALL}, 'value'),
     State({"prop": "filter-selector", "id": ALL}, 'value')],
)
def update_chart_config_options(chart_type, table_name, pts, titles, tmp, filters):
    """Update configuration options based on chart type"""
    logger.info(currentframe().f_code.co_name)
    if not table_name:
        raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    # Check if the chart type has changed
    table_callback =  "data-table-selector" in callback_context.triggered[0]['prop_id']
    # Check if the chart type has changed
    chart_callback =  "chart-type" in callback_context.triggered[0]['prop_id']
    # Get data and current configuration
    with database_context_manager(database_pool) as db:
        currentAnalysis = db.getAttrValues(current_user.username, 'analysiss', 'Temp')
        # do not laod new data when chart type is changed
        if not chart_callback:
            df = db.getData(current_user.username, table_name, numRows=1e9).sort_index(ascending=True)            
            # store data in parquet
            save_data(df, "main", current_user.username)
            DATA["main"] = df
        else:
            df = DATA["main"]
    if df is None:
        raise PreventUpdate
        
    # Get numeric columns and create options with empty option at the top
    numeric_cols = df.select_dtypes(include=['int64', 'float64']).columns
    var_options = [{'label': '-- None --', 'value': ''}] + [{'label': col, 'value': col} for col in numeric_cols]
    
    # Parse configuration
    chart_config = safe_eval_config(currentAnalysis['chart_config'], {})
    
    # get variables and their settings
    variables = getField(chart_config, 'variables') or ['',] * len(pts)
    axis_types = getField(chart_config, 'axis_types') or ['primary',] * len(pts)
    marker_types = getField(chart_config, 'marker_types') or ['line',] * len(pts)
    marker_shapes = getField(chart_config, 'marker_shapes') or ['circle',] * len(pts)
    opacities = getField(chart_config, 'opacities') or [1,] * len(pts)
    
    # Set y variable and range
    if variables and variables[0] in numeric_cols:
        y_var = variables[0] 
        x_var = variables[1] if len(variables) > 1 else numeric_cols[0]
        y_range = [chart_config['chart_range'][i] for i in range(2)] 
    else:
        y_var = numeric_cols[0]
        x_var = numeric_cols[1]
        y_range = [None, None]

    # Set default titles based on chart type    
    chart_type_changed = chart_callback and currentAnalysis['chart_type'] != chart_type
    default_titles = getField(chart_config, 'chart_titles')
    if chart_type == 'distribution':        
        default_titles = default_titles if default_titles and len(default_titles) == 4 and not chart_type_changed else ['', '', 'Probability density', '']
    else:
        default_titles = default_titles if default_titles and len(default_titles) == 4 and not chart_type_changed else ['', '', '', '']

    # get customization and use it to set the default values
    custom_config = getField(chart_config, 'chart_custom')
    custom_config1 = custom_config if custom_config and len(custom_config) > 4 and not chart_type_changed else [100, 15, 2, 20, 20]
    custom_config2 = custom_config if custom_config and len(custom_config) > 5 and not chart_type_changed else [10, 15, 2, 20, 20, 3]
    custom_config3 = custom_config if custom_config and len(custom_config) > 4 and not chart_type_changed else [3, 15, 2, 20, 20]
    # Default values
    config = {
        'distribution': {
            'label': "Distribution Variable",
            'chart_options': [
                dbc.Row([
                    # Graph elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Graph elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([
                            dbc.Row([
                                # Checklist column
                                dbc.Col(dbc.Checklist(
                                    id={"prop":"chart-options", "type": "graph"},
                                    options=[
                                        {"label": x, "value": x} 
                                        for x in ["Title", "X title", "Y title", "Legend"]
                                    ],
                                    value=getField(chart_config, 'chart_config') or ["title", "X title", "Y title", "legend"],
                                    switch=True,
                                    className=STYLES['label_classes']
                                ), width=4),
                                # Input fields column
                                dbc.Col([
                                    dbc.Input(
                                        id={"prop":"chart-titles", "type": t},
                                        type="text",
                                        placeholder=p,
                                        value=v,
                                        debounce=True,
                                        style=utl.height,
                                        className=STYLES['input_classes'],
                                        disabled=t == "y2title"  # Disable Y2 title input for distribution
                                    ) for i, (t, p, v) in enumerate([
                                        ("title", "Title", default_titles[0]),
                                        ("xtitle", "X title", default_titles[1]),
                                        ("ytitle", "Y title", default_titles[2]),
                                        ("y2title", "Y2 title", default_titles[3])
                                    ])
                                ], width=8)
                            ])
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=5),
                    
                    # Additional elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Additional elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([dbc.Checklist(
	                            id={"prop":"chart-options", "type": "aditional"},
	                            options=[
	                                {"label": x, "value": x.lower()} 
	                                for x in ["Statistics", "KDE estimation", "Gaussian estimation"]
	                            ],
	                            value=getField(chart_config, 'chart_options') or ["statistics", "kde estimation", "gaussian estimation"],
	                            switch=True,
	                            className=STYLES['label_classes']
	                        ),
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=3),
                    
                    # Customization card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Customization", style={'padding':'0px 5px'}),
                        dbc.CardBody(dbc.Row([
                            dbc.Col([
                                dbc.Label(label, html_for=id_suffix, className=STYLES['label_classes']),
                                dbc.Input(
                                    id={"prop":"chart-conf", "type": id_suffix},
                                    type="number",
                                    placeholder=label,
                                    value=getField(chart_config, 'chart_custom', [i]) or default if table_callback else default,
                                    debounce=True,
                                    style=utl.height,
                                    className=STYLES['input_classes']
                                )
                            ], width=6) for i, (label, id_suffix, default) in enumerate([
                                ("# Bins", "nbins", custom_config1[0]),
                                ("Font Size", "font_size", custom_config1[1]),
                                ("Line Width", "line_width", custom_config1[2]),
                                ("# X Ticks", "xticks", custom_config1[3]),
                                ("# Y Ticks", "yticks", custom_config1[4])
                            ])
                        ], style={'overflowY': 'auto', 'maxHeight': '22vh'}),
                        style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=4)
                ])
            ]
        },
        'timeseries': {
            'label': "Y-Axis Variable",
            'chart_options': [
                dbc.Row([
                    # Graph elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Graph elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([
                            dbc.Row([
                                # Checklist column
                                dbc.Col(dbc.Checklist(
                                    id={"prop":"chart-options", "type": "graph"},
                                    options=[
                                        {"label": x, "value": x} 
                                        for x in ["Title", "X title", "Y title", "Y2 title", "Legend"]
                                    ],
                                    value=getField(chart_config, 'chart_config') or ["title", "X title", "Y title", "Y2 title", "legend"],
                                    switch=True,
                                    className=STYLES['label_classes']
                                ), width=4),
                                # Input fields column
                                dbc.Col([
                                    dbc.Input(
                                        id={"prop":"chart-titles", "type": t},
                                        type="text",
                                        placeholder=p,
                                        value=v,
                                        debounce=True,
                                        style=utl.height,
                                        className=STYLES['input_classes']
                                    ) for i, (t, p, v) in enumerate([
                                        ("title", "Title", default_titles[0]),
                                        ("xtitle", "X title", default_titles[1]),
                                        ("ytitle", "Y title", default_titles[2]),
                                        ("y2title", "Y2 title", default_titles[3])
                                    ])
                                ], width=8)
                            ])
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=5),
                    
                    # Additional elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Additional elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([
                            dbc.Checklist(
                                id={"prop":"chart-options", "type": "aditional"},
                                options=[
                                    {"label": x, "value": x.lower()} 
                                    for x in ["Use DateTime", "Moving Average", "Tendency"]
                                ],
                                value=getField(chart_config, 'chart_options') or ["moving average"],
                                switch=True,
                                className=STYLES['label_classes']
                            ),
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=3),
                    
                    # Customization card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Customization", style={'padding':'0px 5px'}),
                        dbc.CardBody(dbc.Row([
                            dbc.Col([
                                dbc.Label(label, html_for=id_suffix, className=STYLES['label_classes']),
                                dbc.Input(
                                    id={"prop":"chart-conf", "type": id_suffix},
                                    type="number",
                                    placeholder=label,
                                    value=getField(chart_config, 'chart_custom', [i]) or default if table_callback else default,
                                    debounce=True,
                                    style=utl.height,
                                    className=STYLES['input_classes']
                                )
                            ], width=6) for i, (label, id_suffix, default) in enumerate([
                                ("MA Window", "ma_window", custom_config2[0]),
                                ("Font Size", "font_size", custom_config2[1]),
                                ("Line Width", "line_width", custom_config2[2]),
                                ("# X Ticks", "xticks", custom_config2[3]),
                                ("# Y Ticks", "yticks", custom_config2[4]),
                                ("Marker Size", "marker_size", custom_config2[5])
                            ])
                        ], style={'overflowY': 'auto', 'maxHeight': '22vh'}),
                        style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=4),
                ])
            ]
        },
        'scatter': {
            'label': "Y-Axis Variable",
            'chart_options': [
                dbc.Row([
                    # Graph elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Graph elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([
                            dbc.Row([
                                # Checklist column
                                dbc.Col(dbc.Checklist(
                                    id={"prop":"chart-options", "type": "graph"},
                                    options=[
                                        {"label": x, "value": x} 
                                        for x in ["Title", "X title", "Y title", "Y2 title", "Legend"]
                                    ],
                                    value=getField(chart_config, 'chart_config') or ["title", "X title", "Y title", "Y2 title", "legend"],
                                    switch=True,
                                    className=STYLES['label_classes']
                                ), width=4),
                                # Input fields column
                                dbc.Col([
                                    dbc.Input(
                                        id={"prop":"chart-titles", "type": t},
                                        type="text",
                                        placeholder=p,
                                        value=v,
                                        debounce=True,
                                        style=utl.height,
                                        className=STYLES['input_classes']
                                    ) for i, (t, p, v) in enumerate([
                                        ("title", "Title", default_titles[0]),
                                        ("xtitle", "X title", default_titles[1]),
                                        ("ytitle", "Y title", default_titles[2]),
                                        ("y2title", "Y2 title", default_titles[3])
                                    ])
                                ], width=8)
                            ])
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=5),
                    
                    # Additional elements card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Additional elements", style={'padding':'0px 5px'}),
                        dbc.CardBody([dbc.Checklist(
	                            id={"prop":"chart-options", "type": "aditional"},
	                            options=[
	                                {"label": x, "value": x.lower()} 
	                                for x in ["Regression Line", "Error Distribution"]  # Removed 'Confidence Interval'
	                            ],
	                            value=getField(chart_config, 'chart_options') or ["regression line"],
	                            switch=True,
	                            className=STYLES['label_classes']
	                          ),
                        ], style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=3),
                    
                    # Customization card
                    dbc.Col(dbc.Card([
                        dbc.CardHeader("Customization", style={'padding':'0px 5px'}),
                        dbc.CardBody(dbc.Row([
                            dbc.Col([
                                dbc.Label(label, html_for=id_suffix, className=STYLES['label_classes']),
                                dbc.Input(
                                    id={"prop":"chart-conf", "type": id_suffix},
                                    type="number",
                                    placeholder=label,
                                    value=getField(chart_config, 'chart_custom', [i]) or default if table_callback else default,
                                    debounce=True,
                                    style=utl.height,
                                    className=STYLES['input_classes']
                                )
                            ], width=6) for i, (label, id_suffix, default) in enumerate([
                                ("Marker Size", "marker_size", custom_config3[0]),
                                ("Font Size", "font_size", custom_config3[1]),
                                ("Line Width", "line_width", custom_config3[2]),
                                ("# X Ticks", "xticks", custom_config3[3]),
                                ("# Y Ticks", "yticks", custom_config3[4])
                            ])
                        ], style={'overflowY': 'auto', 'maxHeight': '22vh'}),
                        style=STYLES['card_body'])
                    ], style=STYLES['card_style']), width=4)
                ])
            ]
        }
    }

    chart_settings = config.get(chart_type, {'label': "Variable", 'extra_selectors': [], 'chart_options': []})
    # this is to make sure that return contents are correct
    numPts = len(pts)  
    # get filter configuration if it is not None
    filter_config = safe_eval_config(currentAnalysis['filters_config'], {})
    filter_vars = filter_config['variables'] if 'variables' in filter_config else ["",] * len(filters)
    filter_ranges = filter_config['ranges'] if 'ranges' in filter_config else [None,] * len(filters)*2
    # adding in datetime to filter point list
    filter_options = var_options.copy()
    filter_options.insert(1, {'label':'DateTime', 'value':'DateTime'})
    return (
        [var_options,] * numPts,
        [variables[0]] if numPts < 2 else variables, # only load first variable because others might not be loaded yet
        chart_settings['chart_options'],
        chart_config['chart_range'] if 'chart_range' in chart_config and len(chart_config['chart_range']) == numPts*2 else ['None',] * numPts*2,
        default_titles if titles else [],  # Add default titles to return values
        [filter_options,] * len(filters),
        filter_vars,
        no_update)

# Callback to update configuration store when Apply is clicked
@app.callback(
    Output('current-config', 'data'),
    Output('analysis-loading-update_configuration', 'children'),
    # Input('apply-chart-config', 'n_clicks'),
    [Input({"prop": "var-selector", "id": ALL}, 'value'),
     Input({'prop':'chart-options', 'type': ALL}, 'value'),
     Input({"prop":"chart-conf", "type": ALL}, 'value'),
     Input({"prop":"chart-titles", "type": ALL}, 'value'),
     Input({"type": "var-config", "prop":"var-range", "type": ALL, "id": ALL}, 'value'),
     Input({"type": "var-config", "prop": "axis-selector", "id": ALL}, 'value'),  # New input
     Input({"type": "var-config", "prop": "marker-type", "id": ALL}, 'value'),    # New input
     Input({"type": "var-config", "prop": "opacity", "id": ALL}, 'value'),        # New input
     Input({"type": "var-config", "prop": "marker-shape", "id": ALL}, 'value'),    # New input
     ],
    # prevent_initial_call=True
)
def update_configuration(variables, chart_options, chart_custom, chart_titles, 
                        chart_range, axis_types, marker_types, opacities, marker_shapes):
    """Store current configuration when Apply is clicked"""
    logger.info(f'{currentframe().f_code.co_name}')
    if "var-selector" in callback_context.triggered[0]['prop_id']:
        chart_range = [None, ] * len(variables)*2
    new_config = {
        'variables': variables,
        'chart_config': chart_options[0] if chart_options and chart_options[0] else [],
        'chart_options': chart_options[1] if chart_options and chart_options[1] else [],
        'chart_custom': chart_custom if chart_custom else [],
        'chart_titles': chart_titles if chart_titles else [],
        'chart_range': chart_range,
        'axis_types': axis_types or [],
        'marker_types': marker_types or [],
        'marker_shapes': marker_shapes or [],
        'opacities': opacities or []
    }
    return new_config, no_update

# Updated callback to update chart based on configuration changes
@app.callback(
    Output("main-chart", "figure"),
    Output('analysis-loading-update_chart', 'children'),
    [Input("current-config", "data"),
     Input({"prop": "filter-selector", "id": ALL}, "value"),
     Input({"prop": "filter-range", "type": ALL, "id": ALL}, "value")],   
    State("data-table-selector", "value"),
    State("chart-type", "value"),
)
def update_chart(config, filter_vars, filter_ranges, table_name, chart_type):
    """Update the main chart based on all configuration settings"""
    logger.info(f'{currentframe().f_code.co_name}')
    if not config:  # If no configuration yet, use defaults
        config = {
            'chart_config': [],
            'chart_options': [],
            'chart_custom': [],
            'chart_titles': [],
            'chart_range' : [],
            'density_type': None,
            'ma_window': None,
            'trend_type': None
        }
    # collect configuration data
    ctitle = getField(config, 'chart_titles', [0], '')
    xtitle = getField(config, 'chart_titles', [1], '')
    ytitle = getField(config, 'chart_titles', [2], '')
    bins = getField(config, 'chart_custom', [0], 100)
    font_size = getField(config, 'chart_custom', [1], 15)
    line_width = getField(config, 'chart_custom', [2], 2)
    xticks = getField(config, 'chart_custom', [3], 20)
    yticks = getField(config, 'chart_custom', [4], 20)
    variables = getField(config, 'variables', [], [])
    x_min = getField(config, 'chart_range', [1], None)
    x_max = getField(config, 'chart_range', [0], None)
    # Get styling options for all traces
    marker_shapes = getField(config, 'marker_shapes', [], [])
    marker_types = getField(config, 'marker_types', [], [])
    axis_types = getField(config, 'axis_types', [], [])
    opacities = getField(config, 'opacities', [], [])
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    df = DATA['main'].copy()
    if df is None:
        return go.Figure()
    
    # Apply filters to the dataframe
    for i, filter_var in enumerate(filter_vars):
        if filter_var and (filter_var in df.columns or filter_var == 'DateTime'):
            if len(filter_ranges) > i*2:
                filter_range = [filter_ranges[i*2], filter_ranges[i*2+1]]
                df = filterDF(df, filter_var, filter_range)

    variables_ = [v for v in variables if v]
    axis_types_ = [a for v, a in zip(variables, axis_types) if v]
    # Return empty figure if no variables are selected
    if not variables_ or (chart_type == 'scatter' and len(variables_) < 2):        
        return go.Figure(), no_update
  
    # Common axis properties
    common_axis_props = {
        'title_font': dict(size=font_size),
        'tickfont': dict(size=font_size-2),
        'showgrid': True
    }
    if chart_type == 'distribution':    
        # Count active variables
        single_var_mode = len(variables_) == 1
        # for distribution chart, x axis is always concatenated list of all variables if not specified otherwise
        # y axis is always probability density if not specified otherwise
        # there is no secondary y axis        
        fig = go.Figure()
        # Set histogram overlay mode in layout
        fig.update_layout(barmode='overlay')
                # Set the title and axis labels
        fig.update_yaxes(title_text= ytitle or 'Probability density' if 'Y title' in config.get('chart_config', []) else '', 
                        nticks = yticks,
                        **common_axis_props)
        fig.update_xaxes(title_text = xtitle or ', '.join(variables_) if 'X title' in config.get('chart_config', []) else '', 
                        nticks = xticks,
                        **common_axis_props)
        # Create traces for each variable
        stats_data = []  # Store statistics for table
        colors = [
            'rgba(100,100,255,0.75)',  # Blue with 0.5 opacity
            'rgba(255,100,100,0.75)',  # Red with 0.5 opacity
            'rgba(100,255,100,0.75)',  # Green with 0.5 opacity
            'rgba(255,165,0,0.75)',    # Orange with 0.5 opacity
            'rgba(238,130,238,0.75)'   # Violet with 0.5 opacity
        ]
        
        for i, var in enumerate(variables):
            if not var:
                continue

            # Get data and apply filtering for each variable separately
            x_min = getField(config, 'chart_range', [i*2+1])
            x_max = getField(config, 'chart_range', [i*2])
            df2 = filterDF(df.copy(), var, [x_min, x_max])
            data = df2[var]

            # Calculate statistical measures for this variable
            mean = data.mean()
            std = data.std()
            skewness = data.skew()
            kurtosis = data.kurtosis()
            datamin = data.min()
            datamax = data.max()
            
            # Store statistics for table
            stats_data.append({
                'Variable': var,
                'Mean': f'{mean:.2f}',
                'Std Dev': f'{std:.2f}',
                'Skewness': f'{skewness:.2f}',
                'Kurtosis': f'{kurtosis:.2f}',
                'Min': f'{datamin:.2f}',
                'Max': f'{datamax:.2f}',
                'Color': colors[i % len(colors)].replace('0.75', '1')
            })

            # Add histogram with different colors for each distribution
            fig.add_trace(go.Histogram(
                x=data,
                name=f"{var if not single_var_mode else ''}",
                histnorm='probability density',
                nbinsx=config.get('bin_count', bins),
                marker_color=colors[i % len(colors)],
            ))

            # Only show KDE and Gaussian for single variable mode
            if single_var_mode:
                # x value samples for density estimations
                x_range = np.linspace(datamin, datamax, 1000)

                # Add density estimations if requested
                if 'kde estimation' in config.get('chart_options', []):
                    try:
                        kde = stats.gaussian_kde(data)
                        fig.add_trace(go.Scatter(
                            x=x_range,
                            y=kde(x_range),
                            mode='lines',
                            name='Gaussian KDE',
                            line=dict(color='green', width = line_width)
                        ))
                    except:
                        logger.debug("Gaussian KDE calculation error")

                if 'gaussian estimation' in config.get('chart_options', []):
                    mean = mean
                    std = std
                    gaussian = stats.norm.pdf(x_range, mean, std)
                    fig.add_trace(go.Scatter(
                        x=x_range,
                        y=gaussian,
                        mode='lines',
                        name='Gaussian',
                        line=dict(color='red', width = line_width),
                        
                    ))

        # Add statistics table if requested
        if 'statistics' in config.get('chart_options', []):
            headers = ['Variable', 'Mean', 'Std Dev', 'Skewness', 'Kurtosis', 'Min', 'Max']
            
            # Calculate the width needed for the table based on longest variable name
            max_var_len = max(len(str(stat['Variable'])) for stat in stats_data)
            table_width = min(0.4, max(0.3, max_var_len * 0.025))  # Adjust width based on longest name
            table_x_start = 1 - table_width
            
            fig.add_trace(go.Table(
                domain=dict(x=[table_x_start, 1], y=[0.7, 1]),
                header=dict(
                    values=headers,
                    font=dict(size=font_size-2),
                    align='left',
                    fill_color='rgba(211, 211, 211, 0.5)'
                ),
                cells=dict(
                    values=[[stat[h] for stat in stats_data] for h in headers],
                    font=dict(size=font_size-2),
                    align='left',
                    fill_color=[['rgba(255, 255, 255, 0.)'] * len(stats_data)],  # White background
                    line_color=[[stat['Color'] for stat in stats_data]],  # Colored borders
                    height=25
                ),
                columnwidth=[2] + [1] * (len(headers)-1)  # Variable column is three times as wide
            ))

    elif chart_type == 'timeseries':
        # Get marker size from chart customization
        marker_size = config.get('chart_custom')[5] if config.get('chart_custom') else 3
        
        # Check if any additional measurements are present
        has_add_meas = len(variables_) > 1

        # Check if datetime should be used
        use_datetime = 'use datetime' in config.get('chart_options', [])
        # for timeseries chart, x axis is always Sample number or Time if datetime is selected and if not specified otherwise
        # y axis is always concatenated list of primary variables if not specified otherwise
        # y2 axis is always concatenated list of secondary variables if not specified otherwise
        # create figure with secondary y axis support
        fig = make_subplots(specs=[[{"secondary_y": True}]])
        fig.update_xaxes(title_text = (xtitle or 'Time' if use_datetime else 'Sample number') if 'X title' in config.get('chart_config', []) else '', 
                nticks = xticks,
                **common_axis_props)                        
        # Update x-axis format if using datetime
        if use_datetime:
            fig.update_xaxes(
                type='date',
                tickformat='%Y-%m-%d\n%H:%M:%S'
            )        
        # # Update Y-axis titles based on point configuration
        update_axis_titles(fig, config, variables, axis_types, common_axis_props, yticks)

        if 'datetime' in str(df.index.dtype):
            data = df[variables_[0]] 
        else:
            use_datetime = False  # Fallback to sample numbers if datetime is not present
        
        if use_datetime:
            x_data = df.index
        else:
            x_data = np.arange(len(data))
        # Plot each variable with its specified settings
        for i, var in enumerate(variables):
            if not var:
                continue
            # apply filters for each variable separately
            x_min = getField(config, 'chart_range', [i*2+1])
            x_max = getField(config, 'chart_range', [i*2])
            df2 = filterDF(df.copy(), var, [x_min, x_max])
            data = df2[var]

            use_secondary = axis_types[i] if i < len(axis_types) else "primary"
            marker_type = marker_types[i] if i < len(marker_types) else "lines"
            opacity = opacities[i] if i < len(opacities) else 1.0
            marker_shape = marker_shapes[i] if i < len(marker_shapes) else "circle"
            
            # Create marker dictionary based on marker type
            marker_dict = dict(
                size=marker_size,  # Use the marker size from chart customization
                symbol=marker_shape
            )
            
            fig.add_trace(
                go.Scatter(
                    x=x_data,
                    y=data,
                    mode=marker_type,
                    name=var,
                    opacity=opacity,
                    line=dict(width=line_width),
                    marker=marker_dict
                ),
                secondary_y=(use_secondary == "secondary")
            )

        # Add moving average if requested
        if not has_add_meas and 'moving average' in config.get('chart_options', []):
            window = config.get('chart_custom')[0] or 10
            ma = data.rolling(window=window).mean()
            fig.add_trace(go.Scatter(
                x=x_data,
                y=ma,
                mode='lines',
                name=f'MA({window})',
                line=dict(dash='dash', width=line_width)
            ),
                secondary_y=(use_secondary == "secondary"))

        # Add linear trend if requested
        if not has_add_meas and 'tendency' in config.get('chart_options', []):
            z = np.polyfit(range(len(data)), data, 1)
            p = np.poly1d(z)
            trend = p(range(len(data)))
            fig.add_trace(go.Scatter(
                x=x_data,
                y=trend,
                mode='lines',
                name='Trend',
                line=dict(dash='dot', width=line_width)
            ),
                secondary_y=(use_secondary == "secondary"))


    elif chart_type == 'scatter':
        # return empty figure if no variables are selected
        if len(variables_) < 2:        
            return go.Figure(), no_update
        # for scatter chart, x axis is the first variable if not specified otherwise
        # y axis is the concatenated list of all primary variables if not specified otherwise
        # y2 axis is the concatenated list of all secondary variables if not specified otherwise
        # when there is only one y variable and regression and error distribution are requested, there should be two subplots
            # subplot 1: 
                # y axis is the primary variable
                # x axis is the first variable
            # subplot 2: 
                # y2 axis is the error distribution
                # x axis is Residuals
        # check if error distribution is configured
        err_distr = all(opt in config.get('chart_options', []) for opt in ['regression line', 'error distribution'])
        single_y_var = len(variables_) == 2
        show_regression = single_y_var and 'regression line' in config.get('chart_options', [])
        err_distr = show_regression and err_distr
        if err_distr:
            # Create subplot figure with two side-by-side plots
            fig = make_subplots(rows=1, cols=2,
                                column_widths=[0.65, 0.35],
                                horizontal_spacing=0.002,
                                shared_yaxes=True,
                                specs=[[{"secondary_y": False}, {"secondary_y": True}]])

            # Update left plot axes
            fig.update_yaxes(
                title_text=ytitle or variables_[1] if 'Y title' in config.get('chart_config', []) else '',
                nticks=yticks,
                row=1, col=1,
                **common_axis_props
            )
            fig.update_xaxes(
                title_text=xtitle or variables_[0] if 'X title' in config.get('chart_config', []) else '',
                nticks=xticks,
                row=1, col=1,
                **common_axis_props
            )

            # Update right plot axes
            fig.update_yaxes(
                title_text='',
                nticks=yticks,
                row=1, col=2,
                **common_axis_props
            )
            fig.update_yaxes(
                title_text='Error probability density',
                nticks=yticks,
                secondary_y=True,
                gridcolor='lightblue',
                row=1, col=2,
                **common_axis_props
            )
            fig.update_xaxes(
                title_text='Approximation error',
                nticks=xticks,
                gridcolor='lightblue',
                row=1, col=2,
                **common_axis_props
            )
        else:
            # create figure with secondary y axis support
            fig = make_subplots(specs=[[{"secondary_y": True}]])            
            fig.update_xaxes(title_text = xtitle or variables_[0] if 'X title' in config.get('chart_config', []) else '', 
                    nticks = xticks,
                    **common_axis_props)    
            # # Update Y-axis titles based on point configuration
            # update_axis_titles(fig, config, variables, axis_types, common_axis_props, yticks)
            
            # Get current titles from config, copy to avoid reference issues
            current_titles = config.get('chart_titles', ['', '', '', '']).copy()
            # Update Y-axis titles based on point configuration
            update_axis_titles(fig, config, variables_[1:], axis_types_[1:], common_axis_props, yticks)


        # apply filters in a cumulative way
        df2 = df.copy()
        for i, var in enumerate(variables):
            if not var:
                continue
            x_min = getField(config, 'chart_range', [i*2+1])
            x_max = getField(config, 'chart_range', [i*2])
            df2 = filterDF(df2, var, [x_min, x_max])
        # Get X variable data
        x_var = variables_[0]            
        x_data = df2[x_var]
        # Plot each Y variable
        first_x_var_found = False
        for i, var in enumerate(variables):
            if not var:  # Skip empty variables
                continue
            if var == x_var and not first_x_var_found:  # Skip first occurrence of x_var
                first_x_var_found = True
                continue
            # Create a copy of the dataframe for filtering
            df_filtered = df2.copy()
            
            # Get styling options for this trace
            use_secondary = axis_types[i] if i < len(axis_types) else "primary"
            marker_type = marker_types[i] if i < len(marker_types) else "markers"
            opacity = opacities[i] if i < len(opacities) else 1.0
            marker_shape = marker_shapes[i] if i < len(marker_shapes) else "circle"
            
            # Create marker dictionary
            marker_dict = dict(
                size=config.get('chart_custom')[0] or 4,
                symbol=marker_shape
            )
            # create line dictionary
            line_dict = dict(
                width=line_width
            )
            # Get Y limits for this variable
            y_min = getField(config, 'chart_range', [i*2+1])
            y_max = getField(config, 'chart_range', [i*2])
            # Filter Y variable
            df_y_filtered = filterDF(df_filtered, var, [y_min, y_max])
            
            # Get filtered data
            x_data = df_y_filtered[x_var]
            data = df_y_filtered[var]
            # Basic scatter plot
            fig.add_trace(go.Scatter(
                x=x_data,
                y=data,
                mode=marker_type,
                name='Actual data' if single_y_var else f'{var}({x_var})',
                marker=marker_dict,
                line=line_dict
            ),             
            row=1 if err_distr else None, 
            col=1 if err_distr else None,
            secondary_y=(use_secondary == "secondary" and not err_distr))

        if any(opt in config.get('chart_options', []) 
            for opt in ['regression line', 'error distribution']) and single_y_var:
                        
            # Fit polynomial regression
            coeffs = np.polyfit(x_data, data, 1)
            poly = np.poly1d(coeffs)
            
            # Sort x_data for smooth line plotting
            x_sorted = np.sort(x_data)
            y_pred = poly(x_sorted)
            
            # Calculate residuals for unsorted data
            y_pred_unsorted = poly(x_data)
            residuals = data - y_pred_unsorted
            
            # Format polynomial equation
            poly_eq = " + ".join([f"{coef:.2f}x^{i}" for i, coef in enumerate(reversed(coeffs))])

            # Add regression line if requested
            if 'regression line' in config.get('chart_options', []):
                fig.add_trace(go.Scatter(
                    x=x_sorted,
                    y=y_pred,
                    mode='lines',
                    name=f'y = {poly_eq}',
                    line=dict(color='red', width=line_width)
                ), secondary_y=(use_secondary == "secondary" and not err_distr))
            # Add error distribution if requested
            if err_distr:
                # Add histogram of residuals
                fig.add_trace(go.Histogram(
                    x=residuals,
                    name="Error Distribution",
                    histnorm='probability density',
                    opacity=0.5,  # Adjust opacity for better visualization
                    nbinsx=200,
                    # marker_color='rgba(0, 255, 200, 0.5)',
                ), row=1, col=2, secondary_y=True)                

                # Add scatter plot of residuals
                fig.add_trace(go.Scatter(
                    x=residuals,
                    y=data,
                    mode='markers',
                    name='Residuals',
                    marker=dict(
                        size=config.get('chart_custom')[0] or 4,
                        # color='rgba(200, 255, 100, 0.5)'
                    ),
                ), row=1, col=2)
    # Update layout
    fig.update_layout(
        legend=dict(
            # bordercolor="Black",
            # borderwidth=1,
            font=dict(size=font_size),
            x=0,          # Position legend on the left
            y=1,          # Position legend at the top
            xanchor='left',  # Anchor to the left
            yanchor='top',    # Anchor to the top
            bgcolor='rgba(255,255,255,0.5)'  # Optional: make legend background semi-transparent
            ),
        showlegend = 'Legend' in config.get('chart_config', [])
    )
    title = f"{chart_type.title()} Analysis: {', '.join(variables_[1:])+' over '+variables_[0] if chart_type == 'scatter' else ', '.join(variables_)}"
    # Set the title if provided and adjust the layout margins
    if 'Title' in config.get('chart_config', []):
        fig.update_layout(
            title=dict(
                text=ctitle or title,
                font=dict(size=font_size+2),
                x=0.5,
                xanchor='center'),
        margin=dict(l=0, r=5, t=50, b=0)
        )
    else:
        fig.update_layout(
            margin=dict(l=0, r=5, t=10, b=0)  # Adjust these values as needed
        )

    # update DB
    with database_context_manager(database_pool) as db:
        db.updateAttr(current_user.username, 'analysiss', {
            'datatable' : table_name,
            'chart_type' : chart_type,
            'chart_config' : str(config),
            'filters_config' : str({'variables': filter_vars, 'ranges': filter_ranges})
        })

    return fig, no_update

# Add callback for top configuration offcanvas
@app.callback(
    Output("top-config-offcanvas", "is_open"),
    Input("open-top-config", "n_clicks"),
    State("top-config-offcanvas", "is_open"),
)
def toggle_top_config(n_clicks, is_open):
    if n_clicks:
        return not is_open
    return is_open

# Callback to open the modal
@app.callback(
    Output("analysis-download-modal", "is_open"),
    Output("analysis-file-name-input", "value"),
    [Input("analysis-download", "n_clicks"), 
     Input("analysis-download-close-btn", "n_clicks"),     
    Input("analysis-download-btn", "n_clicks"),],
    [State("analysis-download-modal", "is_open"),
     State("current-config", "data"),
    State("chart-type", "value")]
)
def toggle_modal(open_click, close_click, download_click, is_open, config, chartType):
    """File name is automatically suggested based on chart title or chart type and 1st tagname"""
    logger.info(f'{currentframe().f_code.co_name}')
        # collect configuration data
    ctitle = getField(config, 'chart_titles', [0])
    y_var = getField(config, 'variables', [0])
    title = ctitle or f'{chartType.title()}-{y_var}'
    if open_click or close_click or download_click:
        return not is_open, title
    return is_open, title

# Store the file name when "Download" is clicked in the modal
@app.callback(
    Output("analysis-file-name-store", "data"),
    Input("analysis-download-btn", "n_clicks"),
    State("analysis-file-name-input", "value"),
    prevent_initial_call=True
)
def store_file_name(n_clicks, file_name):
    """"""
    logger.info(f'{currentframe().f_code.co_name}')
    return file_name or "chart"  # Default name if no input

# Callback to perform the download using the stored file name
@app.callback(
    Output("analysis-download-image", "data"),
    Output("analysis-loading-download_image", "children"),
    Input("analysis-file-name-store", "data"),
    State("main-chart", "figure"),
    prevent_initial_call=True
)
def download_image(file_name, fig):
    """Download the chart as a PNG image"""
    logger.info(f'{currentframe().f_code.co_name}')
    # Convert the figure to an image in PNG format with improved size. 
    # scale parameter can be increased if higher resolution is required
    img_bytes = Figure(fig).to_image(format="png", 
                                    width=1920, 
                                    height=1080, 
                                    scale=5.0, 
                                    engine="orca" if os.name == 'nt' else "kaleido")
    # Trigger the download with the custom file name and improved image size
    return dcc.send_bytes(img_bytes, filename=f"{file_name}.png"), no_update

# Add new callback to update dropdown menu labels
@app.callback(
    Output({"type": "var-config-menu", "id": ALL}, "label"),
    Output('analysis-loading-update_config_menu_labels', 'children'),
        [Input({"prop": "var-selector", "id": ALL}, "value"),
         Input({"type": "var-config", "prop": "axis-selector", "id": ALL}, "value"),
        Input({"type": "var-config", "prop": "marker-type", "id": ALL}, "value"),
     Input({"type": "var-config", "prop": "marker-shape", "id": ALL}, "value"),
     Input({"type": "var-config", "prop": "opacity", "id": ALL}, "value")]
)
def update_config_menu_labels(values, axis_types, marker_types, marker_shapes, opacities):
    """Update the configuration dropdown menu labels with current settings"""
    logger.info(f'{currentframe().f_code.co_name}')
    labels = []
    for i in range(len(values)):
        value = values[i] if i < len(values) else ""
        marker_type = marker_types[i] if i < len(marker_types) else "lines"
        marker_shape = marker_shapes[i] if i < len(marker_shapes) else "circle"
        opacity = opacities[i] if i < len(opacities) else 1.0
        axis_type = axis_types[i] if i < len(axis_types) else "primary"

        # Create summary string
        summary = f"{'y1' if axis_type == 'primary' else 'y2'}, {'dots' if marker_type == 'markers' else 'line' if marker_type == 'lines' else 'line+dots'}, {marker_shape}, {opacity:.1f}"
        labels.append(summary)
    
    return labels, no_update

def update_axis_titles(fig, config, variables, axis_types, common_axis_props, yticks):
    """Update axis titles based on variable configuration"""
    primary_vars = [var for i, var in enumerate(variables) if axis_types[i] != 'secondary' and var]
    secondary_vars = [var for i, var in enumerate(variables) if axis_types[i] == 'secondary' and var]
    
    current_titles = config.get('chart_titles', ['', 'Time', '', '']).copy()
    
    if not current_titles[2]:
        current_titles[2] = ', '.join(primary_vars) if primary_vars else ''
    if not current_titles[3]:
        current_titles[3] = ', '.join(secondary_vars) if secondary_vars else ''
    
    if 'Y title' in config.get('chart_config', []):
        if len(primary_vars) > 0:
            fig.update_yaxes(
                title_text=current_titles[2] or 'Value',
                secondary_y=False,
                nticks=yticks,
                **common_axis_props
            )
    if 'Y2 title' in config.get('chart_config', []):
        if len(secondary_vars) > 0:
            fig.update_yaxes(
                title_text=current_titles[3] or 'Value2',
                secondary_y=True,
                nticks=yticks,
                gridcolor='lightblue',
                **common_axis_props
        )
# Add callback for filters offcanvas
@app.callback(
    Output("filters-offcanvas", "is_open"),
    Input("open-filter-config", "n_clicks"),
    State("filters-offcanvas", "is_open"),
)
def toggle_filters(n_clicks, is_open):
    logger.info(f'{currentframe().f_code.co_name}')
    if n_clicks:
        return not is_open
    return is_open

# Update callback to create appropriate date/number inputs
@app.callback(
    [Output({"prop": "filter-range-container", "type": "min", "id": ALL}, "children"),
     Output({"prop": "filter-range-container", "type": "max", "id": ALL}, "children")],
    [Input({"prop": "filter-selector", "id": ALL}, "value"),
     Input("data-table-selector", "value")]
)
def update_filter_ranges(filter_vars, table_name):
    """Update filter range inputs based on selected variables"""
    logger.info(f'{currentframe().f_code.co_name}')
    
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    df = DATA['main']
    
    if df is None or not filter_vars:
        raise PreventUpdate

    # Get saved filter configuration from database
    with database_context_manager(database_pool) as db:
        currentAnalysis = db.getAttrValues(current_user.username, 'analysiss', 'Temp')
    
    # Parse filters configuration
    filters_config = safe_eval_config(currentAnalysis['filters_config'], {})
    saved_ranges = getField(filters_config, 'ranges') or [None] * len(filter_vars) * 2
    min_inputs = []
    max_inputs = []
    
    for i, var in enumerate(filter_vars):
        if var:
            # Get saved ranges for this variable
            saved_min = saved_ranges[i*2+1] if len(saved_ranges) > i*2+1 else None
            saved_max = saved_ranges[i*2] if len(saved_ranges) > i*2 else None
            
            # Special handling for datetime index
            if var == "DateTime":
                min_val = saved_min or df.index.min().strftime('%Y-%m-%d %H:%M:%S')
                max_val = saved_max or df.index.max().strftime('%Y-%m-%d %H:%M:%S')
                
                min_input = [dbc.Tooltip(
                    f"MIN: {df.index.min().strftime('%Y-%m-%d %H:%M:%S')}",
                    target={"prop": "filter-range", "type": "min", "id": i},
                    placement="bottom"
                ), dbc.Input(
                    id={"prop": "filter-range", "type": "min", "id": i},
                    type="text",
                    placeholder="YYYY-MM-DD HH:MM:SS",
                    value=min_val,
                    debounce=True,
                    className=STYLES['input_classes'],
                    style=utl.height
                )]
                max_input = [dbc.Tooltip(
                    f"MAX: {df.index.max().strftime('%Y-%m-%d %H:%M:%S')}",
                    target={"prop": "filter-range", "type": "max", "id": i},
                    placement="bottom"
                ), dbc.Input(
                    id={"prop": "filter-range", "type": "max", "id": i},
                    type="text",
                    placeholder="YYYY-MM-DD HH:MM:SS",
                    value=max_val,
                    debounce=True,
                    className=STYLES['input_classes'],
                    style=utl.height
                )]
            else:
                # For numeric columns
                if var in df.columns:
                    min_val = saved_min if saved_min is not None else df[var].min()
                    max_val = saved_max if saved_max is not None else df[var].max()
                else:
                    min_val = saved_min
                    max_val = saved_max
                
                min_input = [dbc.Tooltip(
                    f"MIN: {df[var].min():.3g}" if var in df.columns else "MIN: N/A",
                    target={"prop": "filter-range", "type": "min", "id": i},
                    placement="bottom"
                ), dbc.Input(
                    id={"prop": "filter-range", "type": "min", "id": i},
                    type="number",
                    placeholder="Min",
                    value=min_val,
                    debounce=True,
                    className=STYLES['input_classes'],
                    style=utl.height
                )]
                max_input = [dbc.Tooltip(
                    f"MAX available: {df[var].max():.3g}" if var in df.columns else "MAX: N/A",
                    target={"prop": "filter-range", "type": "max", "id": i},
                    placement="bottom"
                ), dbc.Input(
                    id={"prop": "filter-range", "type": "max", "id": i},
                    type="number",
                    placeholder="Max",
                    value=max_val,
                    debounce=True,
                    className=STYLES['input_classes'],
                    style=utl.height
                )]
        else:
            # Create dummy inputs when no variable is selected
            min_input = dbc.Input(
                id={"prop": "filter-range", "type": "min", "id": i},
                type="number",
                placeholder="Min",
                disabled=True,
                className=STYLES['input_classes'],
                style=utl.height
            )
            max_input = dbc.Input(
                id={"prop": "filter-range", "type": "max", "id": i},
                type="number",
                placeholder="Max",
                disabled=True,
                className=STYLES['input_classes'],
                style=utl.height
            )
        
        min_inputs.append(html.Div(min_input))
        max_inputs.append(html.Div(max_input))
    
    return min_inputs, max_inputs

@app.callback(
    [Output({"prop": "filter-range", "type": "min", "id": MATCH}, "value"),
     Output({"prop": "filter-range", "type": "max", "id": MATCH}, "value")],
    Input({"prop": "filter-clear", "id": MATCH}, "n_clicks"),
    State({"prop": "filter-selector", "id": MATCH}, "value"),
    prevent_initial_call=True
)
def clear_filter_range(n_clicks, selected_var):
    """Reset min and max range values to the actual min and max of the selected variable"""
    logger.info(f'{currentframe().f_code.co_name}')
    if n_clicks:
        # get data from parquet
        DATA = {}
        DATA['main'] = load_data(current_user.username, "main")
        df = DATA['main']
        if df is not None and selected_var in df.columns or selected_var == "DateTime":            
            if selected_var == "DateTime":
                min_val = df.index.min().strftime('%Y-%m-%d %H:%M:%S')
                max_val = df.index.max().strftime('%Y-%m-%d %H:%M:%S')
            else:
                min_val = df[selected_var].min()
                max_val = df[selected_var].max()
            return min_val, max_val
    raise PreventUpdate