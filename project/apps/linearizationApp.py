import plotly.graph_objects as go
from .utils import components as utl
import dash_bootstrap_components as dbc
from dash import dcc, html, no_update, callback_context
from dash_extensions.enrich import Input, Output, State, ALL
from dash.exceptions import PreventUpdate
from apps.app import app
from scipy.interpolate import interp1d
import numpy as np
from apps.utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from apps.utils.database.database_manager import linearization_points
from types import SimpleNamespace
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup
from inspect import currentframe #this is used to read callback name, for debugging
from .app import logger #logger object for debuging
from .trendApp import custom as customTF
from apps.utils.tuning_layouts import custom_get
import pandas as pd
from .utils.functions import save_data, load_data  # Add these imports

logger.info(f'{__name__}')
log_trigger = False

from flask_login import current_user

def scale(values, scaling_factor):
    return values if scaling_factor is False else (values - min(values)) / (max(values) - min(values)) * 100

# define modal objects
deletePop = deleteAttrPopup(
                    app=app,
                    name='linearization')
savePop = saveAttrPopup(
                    app=app,
                    name='linearization')
loadPop = loadAttrPopup(
                    app=app,
                    name='linearization')
def serve_layout():
    # read loop data tables
    with database_context_manager(database_pool) as db:
        dataTables = db.getDataTables(current_user.username)
        currLin = db.getAttrValues(current_user.username, 'linearizations', 'Temp')

    show_popup = dbc.Modal(
        [
            dbc.ModalHeader('Show linearizations'),
            dbc.ModalBody(id='show-modal-content')
        ], id='show-lin-pop-up', size="xl"
    )

    clear_popup = dbc.Modal(
    [
        dbc.ModalHeader('Are you sure you want to clear all saved linearizations for this loop?'),
        dbc.ModalFooter([
            utl.Button('clear-lin-cancel', 'Cancel'),
            utl.Button('clear-lin-delete', 'Clear')
        ])
    ], id='clear-lin-pop-up',
    is_open=False
    )
    # display available data tables
    data_options = [{'label': col_name, 'value': col_name} for col_name in dataTables
                    if col_name != 'trend_data']
    
    # Set default value for data selector
    if currLin and 'datatable' in currLin and currLin['datatable'] in dataTables    :
        default_data = currLin['datatable']
    else:
        default_data = data_options[0]['value'] if data_options else None
    # Loade the whole data into a global variable
    with database_context_manager(database_pool) as db:
        DATA = db.getData(current_user.username, default_data, numRows=1e9).sort_index(ascending=True)
    # save data to parquet
    save_data(DATA, "main", current_user.username)

    layout = dbc.Container([
        # html.H1('Netta Linearization app'),
        html.Hr(style={'margin-top':0, 'width': '100%'}),
        # Add loading spinners for all time-consuming callbacks
        utl.LoadingSpinner(id='linear-loading-update_options'),
        utl.LoadingSpinner(id='linear-loading-update_number_value_table'),
        utl.LoadingSpinner(id='linear-loading-interpolate'),
        utl.LoadingSpinner(id='linear-loading-post_load_linearization'),
        utl.LoadingSpinner(id='linear-loading-update_graph'),
        dbc.Row([
            dbc.Col(id='linear-body', children=[
                dbc.Row(dcc.Graph(id='linearization-graph',
                                    figure=go.Figure(
                                    layout=go.Layout(#height=600,
                                                        margin = dict(l=30, r=30, t=30, b=30))), 
                                                        style={'height':'74vh'})),#helps to avoid execive margins around the graph
                dbc.Row([
                    dbc.Col([utl.Selector('x-axis-variable', 'X Axis'),
                            utl.Selector('y-axis-variable', 'Y Axis')], width=3),
                    # added T1 and TD constants for non linear tranformation 
                    dbc.Col([
                        utl.Input({'type': 'tuning', 'var': 't1', 'in': 'x'}, 'T1, s', 'T1, s', step = 0.1),
                        utl.Input({'type': 'tuning', 'var': 't1', 'in': 'y'}, 'T1, s', 'T1, s', step = 0.1)], width = 2),
                    dbc.Col([
                        utl.Input({'type': 'tuning', 'var': 'td', 'in': 'x'}, 'TD, s', 'TD, s', step = 0.1),
                        utl.Input({'type': 'tuning', 'var': 'td', 'in': 'y'}, 'TD, s', 'TD, s', step = 0.1)], width = 2),
                    dbc.Col([
                            dbc.Row([
                                dbc.Checkbox(id="x-axis-percent", label="X Axis %", value=False),
                                dbc.Checkbox(id="y-axis-percent", label="Y Axis %", value=False)
                                ])], width=1),
                    dbc.Col([utl.Selector('data-selector', 'Data', 
                                         options=data_options, 
                                         value=default_data,  # Add default value here
                                         persistence=False),
                            utl.Input('value-number', 'Enter integer', '# breakpoints', step = 1, value=5)], width=2),
                    dbc.Col([dbc.DropdownMenu(
                                    label = "Linearization menu",
                                    children = [
                                        dbc.DropdownMenuItem('Save', savePop.btn_id),
                                        dbc.DropdownMenuItem('Load', loadPop.btn_id),
                                        dbc.DropdownMenuItem('Delete', deletePop.btn_id)], 
                                    color='primary', group=True, style={'width': '100%'}),
                            dbc.DropdownMenu(
                                    label = "Linearization matrix",
                                    children=[
                                        dbc.DropdownMenuItem('Show', 'show-linear-btn'),
                                        dbc.DropdownMenuItem('Clear', 'clear-linear-btn')],
                                    color='secondary', group=True, style={'width': '100%'})], width=2)], 
                    style={'height':'14vh'})], width=10),
            dbc.Col(id='axis-values', width=2)]),
        html.Div([clear_popup,
                  show_popup,
                  savePop.layout(),
                  loadPop.layout(),
                  deletePop.layout()])
    ], fluid=True)

    return layout

# trigger modal object callbacks
deletePop.register_callbacks()
savePop.register_callbacks()
loadPop.register_callbacks()
@app.callback(
    Output('x-axis-variable', 'options'),
    Output('y-axis-variable', 'options'),
    Output('x-axis-variable', 'value'),
    Output('y-axis-variable', 'value'),
    Output({'type': 'tuning', 'var': ALL, 'in': 'x'}, 'value'),
    Output({'type': 'tuning', 'var': ALL, 'in': 'y'}, 'value'),
    Output('linear-loading-update_options', 'children'),
    Input('data-selector', 'value')
)
def update_options(data_name):
    '''Update point lists based on data selection'''    
    logger.info(f'{currentframe().f_code.co_name}{" << " + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    if data_name in [None, ""]:
        raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    with database_context_manager(database_pool) as db:
        # get current linearization configuration
        currLin = db.getAttrValues(current_user.username,'linearizations', 'Temp')
    df = DATA["main"]
    # do not update when no data is available
    if df is None:
        raise PreventUpdate
    
    # Initialize parameters
    parY = [0]*2
    parX = [0]*2
    if currLin:
        # this is the case when the user changes the data name
        if 'datatable' in currLin and currLin['datatable'] != data_name:
            # load the new data
            with database_context_manager(database_pool) as db:
                df = db.getData(current_user.username, data_name, numRows=1e9).sort_index(ascending=True)
            # save data to parquet
            save_data(df, "main", current_user.username)
            DATA['main'] = df
            # reuse the same columns if possible
            if 'x_variable_name' in currLin and currLin['x_variable_name'] in df.columns:
                ins = currLin['x_variable_name']
            else:
                ins = df.columns[0]
            if 'y_variable_name' in currLin and currLin['y_variable_name'] in df.columns:
                outs = currLin['y_variable_name']
            else:
                outs = df.columns[1]
        # this is the case for loading
        else:
            ins = currLin['x_variable_name']
            outs = currLin['y_variable_name']
        # always reuse parameters if available
        parX = eval(currLin['x_params']) if 'x_params' in currLin and currLin['x_params'] else [0]*2
        parY = eval(currLin['y_params']) if 'y_params' in currLin and currLin['y_params'] else [0]*2

    # DateTime column is excluded by default because it is index
    options = [{'label': col_name, 'value': col_name} for col_name in df.columns]
    return options, options, ins, outs, parX, parY, no_update

@app.callback(
    Output('axis-values', 'children'),
    Output('linear-loading-update_number_value_table', 'children'),
    Input('value-number', 'value')
)
def update_number_value_table(number):
    '''Populate table based on breakpoint number selection'''    
    logger.info(f'{currentframe().f_code.co_name}{" << " + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    if number is None or number <= 0:
        raise PreventUpdate

    # Get stored linearization values
    with database_context_manager(database_pool) as db:
        linearization = db.getAttrValues(current_user.username, 'linearizations', 'Temp')
    
    # Extract stored X and Y values if they exist
    if 'number_of_values' in linearization and linearization['number_of_values'] == number:
        stored_x = eval(linearization.get('X', '[]')) if linearization else []
        stored_y = eval(linearization.get('Y', '[]')) if linearization else []
    else:
        stored_x = [0,]*number
        stored_y = [0,]*number   

    # define header
    table_header = [html.Thead(html.Tr([
        html.Th(""),
        html.Th("X"),
        html.Th("Y")]))]
    # define body elements with stored values if available
    table_body = [html.Tbody([html.Tr(
                                [html.Th(i+1),
                                html.Td(dbc.Input(
                                    id={'type': 'x-axis-input', 'id': i}, 
                                    type='number',
                                    value=stored_x[i],
                                    debounce=True
                                )),
                                html.Td(dbc.Input(
                                    id={'type': 'y-axis-input', 'id': i}, 
                                    type='number',
                                    value=stored_y[i],
                                    debounce=True
                                ))])
                                for i in range(number)
                                ])]
    # combine all into table, it will be scrollable if too many breakpoints are used
    table = html.Div(dbc.Table(table_header + table_body, bordered=True, hover=True), 
                     style={'height': '90vh', 'overflowY': 'auto'})

    return table, no_update


@app.callback(
    Output({'type': 'x-axis-input', 'id': ALL}, 'value'),
    Output({'type': 'y-axis-input', 'id': ALL}, 'value'),
    Output('linear-loading-interpolate', 'children'),
    Input('axis-values', 'children'),
    Input('x-axis-variable', 'value'),
    Input('y-axis-variable', 'value'),
    Input("x-axis-percent", 'value'),
    Input("y-axis-percent", 'value'),
    State('value-number', 'value'),
    State('data-selector', 'value'),
)
def interpolate(body, xAxisVariable, yAxisVariable, x_axis_percent, y_axis_percent, numberValues, data_name):
    '''Interpolate between breakpoints'''
    logger.info(f'{currentframe().f_code.co_name}{" << " + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    # do not execute this callback if it was not triggered
    id = callback_context.triggered[0]['prop_id'].split('.')[0]
    if id not in ['axis-values', 'x-axis-variable', 'y-axis-variable', 'x-axis-percent', 'y-axis-percent']:
        raise PreventUpdate
    if '' in [x_axis_percent, y_axis_percent, xAxisVariable, yAxisVariable, numberValues, data_name]:
        raise PreventUpdate
    with database_context_manager(database_pool) as db:
        # do not update if number of breakpoints or variables are the same as in the linearization    
        linearization = db.getAttrValues(current_user.username, 'linearizations', 'Temp')        
    if 'number_of_values' in linearization and numberValues == linearization['number_of_values'] and \
        id in ['axis-values', 'x-axis-variable', 'y-axis-variable', 'x-axis-percent', 'y-axis-percent'] and \
        xAxisVariable == linearization.get('x_variable_name') and yAxisVariable == linearization.get('y_variable_name') and \
        x_axis_percent == linearization.get('x_in_percent') and y_axis_percent == linearization.get('y_in_percent'):
        raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    if DATA['main'] is None:
        raise PreventUpdate
    df = DATA['main']
    scaledXAxis = scale(df [xAxisVariable], x_axis_percent)
    scaledYAxis = scale(df [yAxisVariable], y_axis_percent)
    f = interp1d(scaledXAxis, scaledYAxis, fill_value="extrapolate")
    
    # breakpoints rounded to 4 decimal values
    xValues = np.linspace(min(scaledXAxis), max(scaledXAxis), numberValues)
    # prevent division by zero
    xValues[0] = xValues[0] + 1e-9
    yValues = f(xValues)

    return list(np.around(np.array(xValues), 4)), list(np.around(np.array(yValues), 4)), no_update


@app.callback(
    Output('value-number', 'value'),
    Output("x-axis-percent", 'value'),
    Output("y-axis-percent", 'value'),
    Output('data-selector', 'value'),
    Output('linear-loading-post_load_linearization', 'children'),
    Input(loadPop.store_id, 'data')
)
def post_load_linearization(store):
    ''' Postprocess for linearization loading.
    It reads loaded linearization parameters and updates layout.
    !!!For some reason intialization loading is not triggere by store, therefore
    prevent_initial_call=True and trigger id check were removed '''
    logger.info(currentframe().f_code.co_name)

    
    with database_context_manager(database_pool) as db:
        linearization = db.getAttrValues(current_user.username, 'linearizations', 'Temp')
        dataTables = db.getDataTables(current_user.username)
        available_tables = [t for t in dataTables if 'trend_data' not in t]
        default_table = available_tables[0] if available_tables else None
    # during initialization store is number,
    # during loading it is a dictionary
    if type(store) == dict:
        linearization = store
    datatable = linearization.get('datatable', default_table)
    return custom_get(linearization, 'number_of_values', 5), \
           custom_get(linearization, 'x_in_percent', False), \
           custom_get(linearization, 'y_in_percent', False), \
           datatable if str(datatable) != 'None' else default_table, no_update

@app.callback(
    Output('show-lin-pop-up', "is_open"),
    Output('show-modal-content', 'children'),
    Input('show-linear-btn', "n_clicks"),
    prevent_initial_call=True
)
def show(n1):
    logger.info(currentframe().f_code.co_name)
    if n1 is None:
        raise PreventUpdate

    with database_context_manager(database_pool) as db:
        linearizations = db.getAttr(current_user.username, 'linearizations')
    
    # get the same columns as was done in the original data structure
    df = linearizations.loc[linearizations.name != "Temp", linearizations.columns.isin(list(linearization_points.__annotations__.keys()))]
    #converting boolean columns to int
    df.x_in_percent = df.x_in_percent.replace({True: 1, False: 0})
    df.y_in_percent = df.y_in_percent.replace({True: 1, False: 0})
    #column name capitalization
    df.columns = df.columns.str.capitalize()
    table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)

    return True, table


@app.callback(
    Output('clear-lin-pop-up', "is_open"),
    Input('clear-linear-btn', 'n_clicks'), 
    Input('clear-lin-cancel', "n_clicks"), 
    Input('clear-lin-delete', 'n_clicks'),
    State('clear-lin-pop-up', "is_open"),
    prevent_initial_call=True
)
def clear(n1, n2, n3, is_open):
    '''Deletes all linearizations'''
    logger.info(currentframe().f_code.co_name)
    ctx = callback_context
    callback_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    if callback_id == 'clear-lin-delete':
        # Delete all saved models 
        with database_context_manager(database_pool) as db:
            # Read all linearizations for current user and current loop
            linearizations = db.getAttr(current_user.username, 'linearizations')

            # Delete all non Temp linearizations
            for name in linearizations.name:
                if name != 'Temp':
                    db.deleteAttr(current_user.username, 'linearizations', name) 
    if n1 or n2 or n3:
        return not is_open
    return is_open


@app.callback(
    output=[Output('linearization-graph', 'figure'),
            Output('linear-loading-update_graph', 'children')],
    inputs=[
        Input({'type': 'x-axis-input', 'id': ALL}, 'value'),
        Input({'type': 'y-axis-input', 'id': ALL}, 'value'),        
        Input({'type': 'tuning', 'var': ALL, 'in': 'x'}, 'value'),
        Input({'type': 'tuning', 'var': ALL, 'in': 'y'}, 'value'),
        State('x-axis-variable', 'value'),
        State('y-axis-variable', 'value'),
        State("x-axis-percent", 'value'),
        State("y-axis-percent", 'value'),
        State('value-number', 'value'),
        State('data-selector', 'value')
    ]
)
def update_graph(X, Y, parX, parY, x_variable_name, y_variable_name, x_in_percent, y_in_percent, number_of_values, data_name):
    '''Populate the scatter plots'''
    logger.info(f'{currentframe().f_code.co_name}{" << " + callback_context.triggered[0].get("prop_id") if log_trigger else ""}')
    if not X or None in X or \
        not Y or None in Y or \
        None in [x_variable_name, y_variable_name, x_in_percent, y_in_percent, data_name, number_of_values]:
        raise PreventUpdate
    # get data from parquet
    DATA = {}
    DATA['main'] = load_data(current_user.username, "main")
    if DATA['main'] is None:
        raise PreventUpdate
    # update current linearization
    # put values to dictionary
    lin = {}
    lin['X'] = str(X)
    lin['Y'] = str(Y)
    lin['x_variable_name'] = x_variable_name
    lin['y_variable_name'] = y_variable_name
    lin['x_in_percent'] = x_in_percent
    lin['y_in_percent'] = y_in_percent
    lin['x_params'] = str(parX)
    lin['y_params'] = str(parY)
    lin['datatable'] = data_name
    lin['number_of_values'] = number_of_values


    #only update if breakpoint number count match
    if number_of_values!=len(X) or number_of_values!=len(Y):
        return no_update
    with database_context_manager(database_pool) as db:        
        # Update current linearization
        db.updateAttr(current_user.username, 'linearizations', lin)
    df = DATA["main"]

    # calculate transfer functions if any parameter is not zero 
    scaledXAxis = scale(df [x_variable_name], x_in_percent)
    if any(parX):
        # it was found that scaled array is not sorted
        input = pd.Series(scaledXAxis, index=df.index).sort_index()
        calc = customTF(input,
                        poles = str([parX[0], 1]), 
                        delay = parX[1], 
                        keep_init=1)
        scaledXAxis = np.squeeze(calc)

    scaledYAxis = scale(df [y_variable_name], y_in_percent)
    if any(parY):
        input = pd.Series(scaledYAxis, index=df.index).sort_index()
        calc = customTF(input, 
                        poles = str([parY[0], 1]), 
                        delay = parY[1], 
                        keep_init=1)
        scaledYAxis = np.squeeze(calc)

    # show actual values
    trace1 = go.Scatter(x=scaledXAxis,
                        y=scaledYAxis,
                        name='scatter', yaxis='y', marker_color="#1f77b4", mode='markers')
    # show linearization
    trace2 = go.Scatter(x=X, y=Y,
                        name='linearization', yaxis='y', marker_color="#ff7f0e", mode='lines')
    # format chart
    layout = go.Layout(xaxis=dict(domain=[0.1, 0.95]),
                       yaxis=dict(titlefont=dict(color="#1f77b4"), tickfont=dict(color="#1f77b4")),
                    #    height=600,
                       legend=dict(orientation='h'),
                       uirevision=True,
                       autosize = True,
                       margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                       )
    fig = go.Figure(data=[trace1, trace2], layout=layout)
    return fig, no_update