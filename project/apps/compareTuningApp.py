from dash import html, dcc, dash_table, no_update
from dash_extensions.enrich import Input, Output, callback_context, State
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate
from .utils import components as utl
from .utils import model
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from .utils.model import OvPID
from flask_login import current_user
import plotly.graph_objects as go
from inspect import currentframe #this is used to read callback name, for debugging
from .app import logger #logger object for debuging
logger.info(f'{__name__}')
def serve_layout():
    # initialize variable for empty load
    ctrlNames = []
    # initialize controller table
    table=''
    # read all controllers of current user, current loop
    with database_context_manager(database_pool) as db:
        controllers = db.getAttr(current_user.username, 'controllers')
    
    
    # relevant columns
    cols = ['name', 'layout', 'model', 'smith', 'P', 'I', 'D', 'F', 'scaling', 'T1', 'PID1', 'PID2']
    # get relevant data from df
    df = controllers.loc[controllers.name != "Temp"  , controllers.columns.isin(cols)].copy().reset_index(drop=True)

    # change column order
    df = df[cols]
    # clear columns when they are not relevant
    clear_columns = {
    'Simple': ['PID1', 'PID2'],
    'Cascade': ['model', 'P', 'I', 'D', 'F', 'scaling', 'T1']
    }
    # Iterate over the layout and corresponding columns to clear
    for layout, cols_to_clear in clear_columns.items():
        # Create a boolean mask for rows where the layout matches the current layout
        mask = df['layout'] == layout
        # Replace values in the specified columns with '-' where the mask is True
        df.loc[mask, cols_to_clear] = '-'
     
    #column name capitalization
    df.columns = df.columns.str.capitalize()

    
    # Set id based on controller name
    df['id'] = df['Name']
    
    # Set index based on controller name
    df.set_index('id', inplace=True, drop=False)

    # store row names for callback
    ctrlNames = list(df['id'])
    # create dcc component
    #when no controllers are saved do not return table
    table = dash_table.DataTable(
        id='compare-datatable-row-ids',
        columns=[
            {'name': i, 'id': i, 'deletable': False} for i in df.columns
            # omit the id column
            if i != 'id'
        ],
        data=df.to_dict('records'),
        # editable=True,
        filter_action="native",
        sort_action="native",
        sort_mode='multi',
        row_selectable='multi',
        row_deletable=False,
        selected_rows = list(range(min(3, len(df.index)))), #at least three first rows are selected
        page_action='native',
        page_current= 0,
        page_size= 100,
        style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(220, 220, 220)',
                }],
        style_header={
            'backgroundColor': 'rgb(210, 210, 210)',
            'color': 'black',
            'fontWeight': 'bold',
            'textAlign':'left',
            'minWidth':'10vw',
        },        
        style_table={'overflowX': 'scroll', 'maxWidth': '100vw'},
        fixed_columns = {'headers':True, 'data':3},
    )


    layout = dbc.Container([
        # html.H1('Netta Compare tuning app'),
        html.Hr(style={'margin-top':0, 'width': '100%'}),
        utl.LoadingSpinner(id='compare-loading-compare'),
        dcc.Store(id='compare-store', data=ctrlNames),
        dcc.Graph(id='compare-graph', figure=go.Figure(layout=go.Layout(#height=600,
                    margin = dict(l=30, r=30, t=30, b=30))), style={'height':'70vh'}), #helps to avoid execive margins around the graph
        dbc.Row([
            dbc.Col([
                    utl.Selector('compare-type', 'Compare',
                                    options=[{'label': 'Controller Output', 'value': 'co'},
                                            {'label': 'Controlled Variable', 'value': 'cv'},
                                            {'label': 'Master Output', 'value': 'co1'},
                                            {'label': 'Slave Variable', 'value': 'cv2'}],
                                    value='co'),
                    dbc.Card(style={'margin-bottom': '10px'},
                                    children=[
                                        dbc.CardHeader("Simulation:", style={'padding':'0px 5px'}),
                                        dbc.CardBody(
                                            children = [dbc.Row(
                                                [dbc.Col(utl.Input('cmp-sim-stop', '', 'Duration, s', step=1, value = 1000), width=6),
                                                dbc.Col(utl.Input('cmp-sim-number', '', 'Samples', step=1, value = 1000), width=6)])],
                                                style={'padding':'0px 5px'})])], width=3),
            dbc.Col(table, id='tuning-table', width=8, style={"margin-top": "0",
                                                    'padding':'0px',
                                                    'maxHeight': '25vh',
                                                    'maxWidth': '100vw',
                                                    'overflowY':'scroll'})], 
                align='start', justify='center', style={'height':'16vh'}),    
    ], fluid=True)
    return layout


@app.callback(
    Output('compare-graph', 'figure'),
    Output('compare-loading-compare', 'children'),
    Input('compare-datatable-row-ids', 'selected_rows'),
    Input('compare-type', 'value'),
    Input('cmp-sim-stop', 'value'),
    Input('cmp-sim-number', 'value'),
    State('compare-store', 'data')
)
def compare(rows, compareFunctionName, stopT, simNum, ctrlNames):
    '''Loads comparison of all loops for a selected model'''
    logger.info(currentframe().f_code.co_name)
           
    logger.debug(f'''ctrlNames: {ctrlNames},\n
                 compareFunctionName:{compareFunctionName},\n
                 rows: {rows}''')

    # get selected controllers based on row ids
    selectedControllers = [ctrlNames[row] for row in rows]

    # empty figure
    zeroFig = go.Figure(layout=go.Layout(#height=600,
                        margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                        ))

    if compareFunctionName is None or not selectedControllers:        
        return zeroFig, no_update

    traces = []

    with database_context_manager(database_pool) as db:
        # get all controllers
        controllers = db.getAttr(current_user.username, 'controllers')
        
        # filter out only the selected controllers
        controllers = controllers.loc[controllers.name.isin(selectedControllers)]

    # relevant columns
    cols = ['P', 'I', 'D', 'F', 'scaling', 'name', 'model', 'PID1', 'PID2', 'layout', 'smith']
    # get relevant data from df
    df = controllers[cols]
    
    
    # do not update when there are no controllers
    # Added empty figure return
    if len(df.index) < 1:
        return zeroFig, no_update
    
    if None in [stopT, simNum]:
        stopT = 100
        simNum = 100
    state = compareStates[compareFunctionName]
    for tunedController in df.to_dict(orient="records"):
        # if cascade layout is used, then controller and model list should be create
        controllers = []
        plants = []
        if tunedController['layout']=='Cascade':
            for i in range(2):
                # when only inner controller is compared
                if compareFunctionName == 'cv2':
                    #  only single PID loop will be simulated
                    ident = 1
                    # skip the master:
                    if i < 1:
                        continue
                else:
                    ident = i+1

                pidn = f'PID{i+1}'
                # get the correct PID and convert it to dictionary
                ctrl = eval(tunedController[pidn])
                controllers.append(OvPID(**ctrl, id=ident))
                # get name and create plant model
                with database_context_manager(database_pool) as db:
                    plants.append(
                        model.modelFactory(**db.getAttrValues(current_user.username,'models', 
                                                              ctrl['model']), id=ident ))
                                
            state_=state[0]            
            # here Res is a dictionary that holds measurements at different points
            Res, coTime = model.Loop.closedCascadedCO(plants, controllers, True)\
                .getStepResponse(stopT, simNum)
        elif 'smith' in tunedController and tunedController['smith']==1:
            controllers.append(OvPID(**tunedController, id=1))
            # get used model
            with database_context_manager(database_pool) as db:
                modelData = db.getAttrValues(current_user.username,'models', 
                                                        tunedController['model'])
            #Collect three models:
            # full model
            plants.append(model.modelFactory(**modelData, id=1))
            # model without deadtime
            mdlData = modelData.copy()
            mdlData.update({'td':0})
            plants.append(model.modelFactory(**mdlData, id=2))
            # model with only a deadtime
            mdlData = modelData.copy()
            mdlData.update({'K':1, 't1':0, 't2':0})
            plants.append(model.modelFactory(**mdlData, id=3))             
            
            # always take the first loop states
            state_=state[1]    
            # here Res is a dictionary that holds measurements at different points
            Res, coTime = model.Loop.closedSmithCO(plants, controllers, True)\
                .getStepResponse(stopT, simNum)
        else:
            controllers.append(OvPID(**tunedController, id=1))
            # get name and create plant model
            with database_context_manager(database_pool) as db:
                plants.append(
                    model.modelFactory(**db.getAttrValues(current_user.username,'models', 
                                                          tunedController['model']), id=1))
            # always take the first loop states
            state_=state[1]                        
            # here Res is a dictionary that holds measurements at different points
            Res, coTime = model.Loop.closedCascadedCO(plants, controllers, True)\
                .getStepResponse(stopT, simNum)
        traces.append(go.Scatter(x=coTime, y=Res[state_], name = tunedController['name']))

    layout = go.Layout(xaxis=dict(domain=[0.05, 0.95]),
                       yaxis=dict(titlefont=dict(color="#1f77b4"), tickfont=dict(color="#1f77b4")),
                       #height=600,
                       legend=dict(orientation='h'),
                       uirevision=True,
                       title='',
                       margin = dict(l=30, r=30, t=30, b=30) #helps to avoid execive margins around the graph
                       )

    fig = go.Figure(data=traces, layout=layout)
    return fig, no_update

compareStates = {
    'co': ['p2', 'p1'],
    'cv': ['y1', 'y1'],
    'co1': ['p1','p1'],
    'cv2': ['y1','y1'] 
    }


