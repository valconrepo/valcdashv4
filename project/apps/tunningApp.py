from .utils import components as utl
import dash_bootstrap_components as dbc
from dash import dcc, html, callback_context, no_update
from dash_extensions.enrich import Input, Output, State, ALL
from .app import app
from .utils.database.database_context_manager import database_context_manager
from .utils.database import database_pool
from flask_login import current_user
from apps.utils.popups import deleteAttrPopup, saveAttrPopup, loadAttrPopup
from inspect import currentframe #this is used to read callback name, for debugging
from apps.utils.tuning_layouts import tuneLayouts, Simple
from time import time
from .app import logger #logger object for debuging
logger.info(f'{__name__}')

clear_popup = dbc.Modal(
    [
        dbc.ModalHeader('Are you sure you want to clear all saved controllers for this loop?'),
        dbc.ModalFooter([
            utl.Button('clear-tune-cancel', 'Cancel'),
            utl.Button('clear-tune-delete', 'Clear')
        ])
    ], id='clear-tune-pop-up',
    is_open=False
)
   
show_popup = dbc.Modal(
    [
        dbc.ModalHeader('Show controllers'),
        dbc.ModalBody(id='show-tune-content')
    ], id='show-tune-pop-up', size="xl",
    scrollable=True,
    is_open=False
)

# define modal objects
deletePop = deleteAttrPopup(
                    app=app,
                    name='controller')
savePop = saveAttrPopup(
                    app=app,
                    name='controller')
loadPop = loadAttrPopup(
                    app=app,
                    name='controller')
allLayouts = {layout:tuneLayouts[layout](savePop, loadPop, deletePop) for layout in tuneLayouts}
lay = allLayouts['Simple']

modals = html.Div([clear_popup, 
                show_popup, 
                savePop.layout(), 
                loadPop.layout(), 
                deletePop.layout()])
def serve_layout():
    layout = dbc.Container(
        children=[
            html.Div(children=[*lay.layout], id = 'tune-dynamic-layout'),
            modals
    ], fluid=True)
    return layout
# trigger modal object callbacks
deletePop.register_callbacks()
savePop.register_callbacks()
loadPop.register_callbacks()
# register callbacks for all layouts
for layout in allLayouts:
    allLayouts[layout].register_callbacks()

@app.callback(
    Output('tune-dynamic-layout', 'children'),
    Output('tune-layout-selector', 'label'),
    Output(loadPop.store_id, 'data'),
    [Input({'type': 'layouts', 'value': ALL}, "n_clicks")],
    State(loadPop.store_id, 'data'),
    prevent_initial_call=True
)
def update_layout(sel, load_store):
    logger.info(currentframe().f_code.co_name)
    global lay
    # global callbacks
    id = callback_context.triggered [0] ['prop_id'].split('.') [0]
    with database_context_manager(database_pool) as db:
        controllerData = db.getAttrValues(current_user.username, 'controllers', 'Temp')

        # during initialization
        if not id:
            new_layout = controllerData['layout'] if 'layout' in controllerData and controllerData['layout'] else 'Simple'
        # when selection is made
        else:
            new_layout = eval(id)['value'] if '{' in id else 'Simple'
            # do not update if layout was not changed
            if type(lay).__name__ == new_layout:
                return no_update, new_layout, no_update

        # update DB
        db.updateAttr(current_user.username, 'controllers', {'layout': new_layout})
    
    # new layout
    # lay = allLayouts[new_layout]#(savePop, loadPop, deletePop)
    # remove original layout callbacks
    # callback_map = dict(app.callback_map)
    # print(callback_map)
    # print(f'Before: {callb[0] if callb else "Empty"}')
    # store(callbacks, 'before')
    # for id, fx in callback_map.items():
    #     # print(fx.keys())
    #     if 'callback' in fx.keys():
            
    #         # print(f'removing: {fx["callback"]}')
    #         if fx['callback'] in callbacks:
    #             app.callback_map.pop(id, None) 

    # # Get new layout's callbacks
    # callbacks = new_lay.register_callbacks(app)  
    
    # print(f'After: {callbacks}')
    # store(callbacks, 'after') 
    # print(dict(app.callback_map))
    # overwrite layout
    # lay = new_lay  
    # print(new_lay.layout)  
    lay = allLayouts[new_layout]
    return [*lay.layout], new_layout, load_store+1


def store(data, filename):
    '''Helper function to stora callbacks for comparison'''
    if data:
        with open(f'{filename}.txt', 'w') as f:
            # Write a single line to the file
            # Write multiple lines to the file
            for item in data:
                f.writelines(f'{str(item)}\n')

        f.close

